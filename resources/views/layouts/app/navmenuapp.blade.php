<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container-fluid">
        <a href="{{route('module.index')}}" class="navbar-brand">
            <img src="{{asset('images/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light"></span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Left navbar links -->
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <ul class="navbar-nav">

                <!--Modules-->
                <li class="nav-item dropdown">
                    <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">{{__('lang.modules')}}</a>
                    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                        <li><a href="{{route('dashboard.index')}}" class="dropdown-item">{{__('lang.dashboard')}}</a>
                        </li>

                        <li><a href="https://mail.hostinger.com/" target="_blank" class="dropdown-item">{{__('lang.module_email')}}</a></li>
                        @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==5 || auth()->user()->role_id ==2)
                        <li><a href="{{route('admin.customer')}}" class="dropdown-item">{{__('lang.module_customer')}}</a></li>
                        @endif
                        @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==3 || auth()->user()->role_id ==2)
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_order')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('admin.order')}}" class="dropdown-item">{{__('lang.order_component')}}</a></li>
                                <li><a href="{{route('admin.sendingorder')}}" class="dropdown-item">{{__('lang.sending_component')}}</a></li>
                                <li><a href="{{route('admin.donesend')}}" class="dropdown-item">{{__('lang.arrived_component')}}</a></li>
                                <li><a href="{{route('admin.showcancelorder')}}" class="dropdown-item">{{__('lang.cancelorder_component')}}</a></li>
                                <li><a href="{{route('admin.coupons')}}" class="dropdown-item">{{__('lang.coupon_component')}}</a></li>
                                <li><a href="{{route('admin.divid')}}" class="dropdown-item">{{__('lang.divid_component')}}</a></li>
                                <li><a href="{{route('admin.exchange')}}" class="dropdown-item">{{__('lang.exchange')}}</a></li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_divid')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('admin.withrawingcustomer')}}" class="dropdown-item">{{__('lang.customer_request_withraw_money')}}</a></li>
                                <li><a href="{{route('admin.dividcustomer')}}" class="dropdown-item">{{__('lang.total_divid_customer')}}</a></li>
                                <li><a href="{{route('admin.withrawedcustomer')}}" class="dropdown-item">{{__('lang.history_withraw_customer')}}</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==4 || auth()->user()->role_id ==5 || auth()->user()->role_id ==2)
                        <!-- Module Website-->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_website')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('news.index')}}" class="dropdown-item">{{__('blog.news')}}</a></li>
                                <li><a href="{{route('page.index')}}" class="dropdown-item">{{__('lang.page')}}</a></li>
                                <li><a href="{{route('product.index')}}" class="dropdown-item">{{__('lang.product')}}</a></li>
                                <li><a href="{{route('catalog.index')}}" class="dropdown-item">{{__('lang.catalog')}}</a></li>
                                <li><a href="{{route('slide.index')}}" class="dropdown-item">{{__('lang.slide')}}</a>
                                </li>
                                <li><a href="{{route('service.index')}}" class="dropdown-item">{{__('lang.service')}}</a></li>
                                <li><a href="{{route('solutions.index')}}" class="dropdown-item">{{__('blog.solutions')}}</a></li>
                                <li><a href="{{route('solution_type.index')}}" class="dropdown-item">{{__('blog.solution_type')}}</a></li>
                                <li><a href="{{route('customer_logo.index')}}" class="dropdown-item">{{__('lang.customer')}}</a></li>
                            </ul>
                        </li>
                        @endif
                        <!-- Module Documents-->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_document')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('import_doc.index')}}" class="dropdown-item">{{__('lang.import_doc')}}</a></li>
                                <li><a href="{{route('export_doc.index')}}" class="dropdown-item">{{__('lang.export_doc')}}</a></li>
                                <li><a href="{{route('local_doc.index')}}" class="dropdown-item">{{__('lang.local_doc')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('doc_type.index')}}" class="dropdown-item">{{__('lang.doc_type')}}</a></li>
                                <li><a href="{{route('storage_file.index')}}" class="dropdown-item">{{__('lang.storage_file')}}</a></li>
                                <li><a href="{{route('external_parts.index')}}" class="dropdown-item">{{__('lang.external_parts')}}</a></li>
                            </ul>
                        </li>
                        @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==2)
                        <!-- Module Account-->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_account')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('transection.index')}}" class="dropdown-item">{{__('lang.transection')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('settingacc.index')}}" class="dropdown-item">{{__('lang.settingacc')}}</a></li>
                                <li><a href="{{route('branch.index')}}" class="dropdown-item">{{__('lang.branch')}}</a>
                                </li>
                                <li><a href="{{route('statusacc.index')}}" class="dropdown-item">{{__('lang.statusacc')}}</a></li>
                                <li><a href="{{route('currency.index')}}" class="dropdown-item">{{__('lang.currency')}}</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==3 || auth()->user()->role_id ==2)
                        <!-- Module Employee-->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_employee')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('admin.employees')}}" class="dropdown-item">{{__('lang.employee')}}</a></li>
                                <li><a href="{{route('admin.payrollBefore')}}" class="dropdown-item">{{ __('lang.payroll_before') }}</a></li>
                                <li><a href="{{route('admin.payroll')}}" class="dropdown-item">{{__('lang.payroll')}}</a></li>
                                <li><a href="{{route('salary.index')}}" class="dropdown-item">{{__('lang.salary')}}</a>
                                </li>
                                <li><a href="{{route('admin.commission')}}" class="dropdown-item">{{__('lang.divid_component')}}</a></li>
                                <li><a href="{{route('admin.trackemployee')}}" class="dropdown-item">{{__('lang.track_employee')}}</a></li>
                                <li><a href="{{route('admin.fund')}}" class="dropdown-item">{{__('lang.fund_money')}}</a></li>
                                <li><a href="{{route('admin.calculateinterest')}}" class="dropdown-item">{{__('lang.calculate_interest')}}</a></li>
                                <li><a href="{{route('admin.loan')}}" class="dropdown-item">{{__('lang.loan')}}</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 || auth()->user()->role_id == 3)
                        <!-- Module Product -->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_product')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('admin.p1-exchange')}}" class="dropdown-item">{{__('lang.setting_online_price')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('admin.p1-unit')}}" class="dropdown-item">{{__('lang.unit')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('admin.p1-product-type')}}" class="dropdown-item">{{__('lang.product_type')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('admin.p1-product')}}" class="dropdown-item">{{__('lang.product')}}</a></li>
                            </ul>
                        </li>
                        <!-- Module order online -->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_order_online')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li><a href="{{route('admin.p1-order')}}" class="dropdown-item">{{__('lang.order')}}</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="{{route('admin.cancenl-p1-order')}}" class="dropdown-item">{{__('lang.cancelorder_component')}}</a></li>
                            </ul>
                        </li>
                        <!-- Module purchase product -->
                        <li class="dropdown-submenu dropdown-hover">
                            <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_purchase')}}</a>
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                <li class="dropdown-submenu dropdown-hover">
                                <li><a href="{{route('admin.p1-contact')}}" class="dropdown-item">{{__('lang.distributor')}}</a></li>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-submenu dropdown-hover">
                        <li><a href="{{route('admin.p1-purchase')}}" class="dropdown-item">{{__('lang.order_product_warehouse')}}</a></li>
                </li>
                <li class="dropdown-divider"></li>
                <li class="dropdown-submenu dropdown-hover">
                <li><a href="{{route('admin.p1-purchase_order')}}" class="dropdown-item">{{__('lang.order_list_of_warehouse')}}</a></li>
                </li>
            </ul>
            </li>
            <!-- Module order online -->
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_report')}}</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="{{ route('admin.report_order')}}" class="dropdown-item">{{__('lang.reports')}}</a></li>
                    <li><a href="{{ route('admin.report_product')}}" class="dropdown-item">{{__('lang.report_sold_well')}}</a></li>
                    <li><a href="{{ route('admin.report_product_isoutstock')}}" class="dropdown-item">{{__('lang.report_isoutofstock_product')}}</a></li>
                </ul>
            </li>
            @endif
            <!-- Module Setting-->
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_setting')}}</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="{{route('depart.index')}}" class="dropdown-item">{{__('lang.depart')}}</a>
                    </li>
                    <li><a href="{{route('province.index')}}" class="dropdown-item">{{__('lang.province')}}</a></li>
                    <li><a href="{{route('district.index')}}" class="dropdown-item">{{__('lang.district')}}</a></li>
                    <li><a href="{{route('village.index')}}" class="dropdown-item">{{__('lang.village')}}</a></li>
                    <li><a href="{{route('marries.index')}}" class="dropdown-item">{{__('lang.marriesstatus')}}</a></li>
                    <li><a href="{{route('position.index')}}" class="dropdown-item">{{__('lang.position')}}</a></li>
                    <li class="dropdown-divider"></li>
                </ul>
            </li>
            <!-- Module System-->
            @if (Auth::user()->rolename->name == 'admin' || Auth::user()->rolename->name == 'manager')
            <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">{{__('lang.module_system')}}</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="{{route('branch.index')}}" class="dropdown-item">{{__('lang.branch')}}</a>
                    </li>
                    @if (Auth::user()->rolename->name == 'admin')
                    <li><a href="{{route('role.index')}}" class="dropdown-item">{{__('lang.role')}}</a></li>
                    <li><a href="{{route('user.index')}}" class="dropdown-item">{{__('lang.user')}}</a></li>
                    @endif
                    <li><a href="{{route('misstype.index')}}" class="dropdown-item">{{__('lang.misstype')}}</a></li>
                    <li class="dropdown-divider"></li>
                </ul>
            </li>
            @endif

            </ul>
            </li>
            <!--
                @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==3)
                <li class="nav-item dropdown">
                    <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        class="nav-link dropdown-toggle">{{__('lang.payroll')}}</a>
                    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                        <li><a href="{{route('admin.payroll')}}" class="dropdown-item">{{__('lang.payroll')}}</a></li>
                    </ul>
                </li>
                @endif
                -->
            <!--
          <li class="nav-item">
            <a href="{{route('dashboard.index')}}" class="nav-link">{{__('lang.dashboard')}}</a>
          </li>-->
            </ul>

        </div>

        <!-- Right navbar links -->
        @forelse($customer_transition as $ct)
        @php

        $d1= time();
        $d2= strtotime($ct->end_date);
        $wai= $d2 - $d1;
        $nas = floor($wai/86400);
        if($nas < $branchs->qtydate_range){ $notif++; }else{ $no; } @endphp @endforeach <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">

                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">{{$orders->count() + $no + $notif + $count_requestwithraw}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                        <span class="dropdown-item dropdown-header">{{$orders->count() + $no + $notif + $count_requestwithraw}}
                            {{__('lang.notifications')}}</span>

                        <div class="dropdown-divider"></div>

                        @if($orders->count() > 0)
                        <a href="{{ route('admin.order')}}" class="dropdown-item">
                            <i class="fas fa-cart-arrow-down"></i>
                            {{$orders->count()}} {{__('lang.neworder')}}
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        @endif
                        @if($count_requestwithraw > 0)
                        <a href="{{ route('admin.withrawingcustomer')}}" class="dropdown-item">
                            <i class="fas fa-sign-in-alt"></i>
                            {{$count_requestwithraw}} {{__('lang.customer_request_withraw_money')}}
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        @endif
                        <a href="/admincustomer" class="dropdown-item">
                            @if($notif == 0)
                            <i class="fas fa-envelope mr-2"></i>{{$no}} {{__('lang.nearly_expired')}}
                            @else
                            <i class="fas fa-envelope mr-2"></i>{{$notif}} {{__('lang.nearly_expired')}}
                            @endif
                            <!-- <span class="float-right text-muted text-sm">3 mins</span> -->
                        </a>

                        <!-- <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> -->
                    </div>
                </li>
                @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 || auth()->user()->role_id == 3)
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-cart-arrow-down"></i>
                        <span class="badge badge-warning navbar-badge">{{$count_p1_order}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                        <span class="dropdown-item dropdown-header">
                            {{__('lang.notifications')}}</span>

                        <div class="dropdown-divider"></div>
                        <a href="{{ route('admin.p1-order')}}" class="dropdown-item">
                            <i class="fas fa-cart-arrow-down"></i>
                            {{$count_p1_order}} {{__('lang.neworder')}}
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                    </div>
                </li>
                @endif
                <!-- Language Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="nav-item" href="{{url('localization/lo')}}">
                        <i class="flag-icon flag-icon-la"></i>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" data-toggle="nav-item" href="{{url('localization/en')}}">
                        <i class="flag-icon flag-icon-us"></i>
                    </a>
                </li>

                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="user-image img-circle elevation-2" alt="User Image">
                        <span class="d-none d-md-inline"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- User image -->
                        <li class="user-header bg-primary">
                            <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">

                            <p>{{__('lang.phone')}}: {{Auth::user()->phone}}</p>
                            @if(Auth::user()->emp_id)
                            <p>{{Auth::user()->empname->firstname}} {{Auth::user()->empname->lastname}}</p>
                            @endif
                        </li>
                        <!-- Menu Body
          <li class="user-body">
            <div class="row">
              <div class="col-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Friends</a>
              </div>
            </div>
          </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a href="{{route('user.changepass', Auth::user()->id)}}" class="btn btn-default btn-flat">{{__('lang.profile')}}</a>
                            <a href="{{route('logout')}}" class="btn btn-default btn-flat float-right">{{__('lang.signout')}}</a>
                        </li>
                    </ul>
                </li>

            </ul>
    </div>
    <!-- <h5>0</h5> -->
</nav>
<!-- <script>
    let counter =1;
    var date = new Date();
    setInterval(() => {
      document.querySelector('h5').innerText = counter;
      counter++;
    }, date.getMinutes());
  </script> -->