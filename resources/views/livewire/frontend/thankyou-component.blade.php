
   <main id="main" class="main-site">
		<div class="container pb-60">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2>{{__('lang.success')}}</h2>
                    <p>{{__('lang.message_thankyou')}}<p>
						@if(Auth::user()->role_id != 10)
                    		<a href="{{ route('shop')}}"  class="btn btn-block btn-primary font-weight-bold py-3">{{__('lang.continue_shopping')}}</a>
						@else
							<a href="{{ route('customer.dailycustomershop')}}"  class="btn btn-block btn-primary font-weight-bold py-3">{{__('lang.continue_shopping')}}</a>
						@endif
				</div>
			</div>
		</div><!--end container-->
	</main>