<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('home')}}">{{__('blog.home')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.checkout')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Checkout Start -->
    <style>
    .bank-profile {
        position: relative;
        width: 250px;
        height: 250px;
        overflow: hidden;
        cursor: pointer;
    }

    .bank-profile img {
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    </style>
    <div class="container-fluid">
        @if(Auth::user()->role_id ==10 && !empty(session()->get('check_detail')['log_detail']))
        <div class="row px-xl-5">
            <div class="col-lg-12">
                <form wire:submit.prevent="Daily_customer_placeorder">
                    <h5 class="section-title position-relative text-uppercase mb-3"><span
                            class="bg-secondary pr-3">{{__('lang.payment')}}</span></h5>
                    <div class="bg-light p-30">
                        <div class="border-bottom pt-3 pb-2">
                            <div class="pt-2">
                                <div class="d-flex justify-content-between mt-2">
                                    <h5>{{__('lang.total')}}:</h5>
                                    <p class="btn btn-success">{{number_format(session()->get('checkout')['total'])}}
                                    </p>
                                </div>
                                @foreach($exchange as $item)
                                <div class="d-flex justify-content-between mt-2">
                                    <h5>{{$item->symbol}}:</h5>
                                    <h5><b>{{number_format((session()->get('checkout')['total'])/$item->rate,2)}}</b>
                                    </h5>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" value="cod" id="directcheck" wire:model="paymentmode">
                                <label class="custom-control-label" for="directcheck">{{__('lang.pay_on_cash')}}</label><br>
                            </div>
                        </div>
                        -->
                        <div class="form-group mb-4">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" value="banktransfer" name="payment"
                                    id="banktransfer" wire:model="paymentmode">
                                <label class="custom-control-label"
                                    for="banktransfer">{{__('lang.pay_on_bcelone')}}</label>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="debt"
                                    wire:model="paymentmode">
                                <label class="custom-control-label" for="debt">{{__('blog.debt')}}</label>
                            </div>
                        </div>
                        @error('paymentmode')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <div class="form-group mb-4">
                            <!--
                        <div class="bank-image" style="display: flex; justify-content: center;  flex-direction: column; align-items: center;">
                        @foreach($branches as $branch)
                         <div class="bank-profile">    
                               <img  src="{{asset($branch->payment_logo)}}" alt="">
                        </div><br>
                        @endforeach
                        -->
                            <img src="{{asset($branchs->payment_logo)}}" alt="" width="100%">
                            @if($paymentmode == 'banktransfer')
                            @if($image)
                            @php
                            try {
                            $url = $image->temporaryUrl();
                            $status = true;
                            }catch (RuntimeException $exception){
                            $this->status = false;
                            }
                            @endphp
                            @if($status)
                            <div class="bank-profile">
                                <img src="{{ $url }}" alt="" style="border: 5px solid green;"><br>
                            </div><br>
                            @endif
                            <i class="fas fa-check-circle" style="color:green;font-size:30px;"></i>
                            @else
                            <h4 class="card-title">{{__('lang.choose_image')}}</h4>
                            @endif
                            <br>
                            <div class="form-group">
                                <input type="file" class="form-control" wire:model="image" />
                            </div>
                            @error('image')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                            @endif
                        </div>
                    </div>
                    <div>
                        <h4>
                            <p class="text-danger">
                                @if (Config::get('app.locale') == 'lo')
                                {{$branchs->note_checkout_lo}}
                                @elseif (Config::get('app.locale') == 'en')
                                {{$branchs->note_checkout_en}}
                                @endif
                            </p>
                        </h4>
                    </div>
                    <button type="submit"
                        class="btn btn-block btn-primary font-weight-bold py-3">{{__('lang.place_order')}}</button>
            </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="row px-xl-5">
    <div class="col-lg-8">
        <h5 class="section-title position-relative text-uppercase mb-3">
            <span class="bg-secondary pr-3">{{__('lang.billing_address')}}</span>
        </h5>
        <div class="bg-light p-30 mb-5">
            <form wire:submit.prevent="PlaceOrder">
                @if(empty(auth()->user()->vill_id) &&
                empty(auth()->user()->dis_id) && empty(auth()->user()->pro_id))
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.name')}} </label>
                        <input class="form-control" type="text" wire:model="firstname"
                            placeholder="{{__('lang.name')}}">
                        @error('firstname')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.lastname')}} </label>
                        <input class="form-control" type="text" placeholder="{{__('lang.lastname')}}"
                            wire:model="lastname">
                        @error('lastname')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.phone')}} </label>
                        <input class="form-control" type="number" placeholder="55944671" wire:model="phone">
                        @error('phone')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.email')}} </label>
                        <input class="form-control" type="text" placeholder="example@email.com" wire:model="email">
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.address')}} </label>
                        <input class="form-control" type="text" placeholder="{{__('lang.street_address')}}"
                            wire:model="address">
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('lang.home_no')}} </label>
                        <input class="form-control" type="number" placeholder="228" wire:model="home_no">
                    </div>

                    <div class="col-md-6 form-group">
                        <label>{{__('blog.province')}}</label>
                        <select class="custom-select" wire:model="pro_id">
                            <option selected>{{__('lang.select')}}</option>
                            @foreach($provinces as $province)
                            <option value={{$province->id}}>{{$province->name}}</option>
                            @endforeach
                        </select>
                        @error('pro_id')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('blog.district')}}</label>
                        <select class="custom-select" wire:model="dis_id">
                            <option selected>{{__('lang.select')}}</option>
                            @foreach($districts as $district)
                            <option value={{$district->id}}>{{$district->name}}</option>
                            @endforeach
                        </select>
                        @error('dis_id')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group">
                        <label>{{__('blog.village')}}</label>
                        <select class="custom-select" wire:model="vill_id">
                            <option selected>{{__('lang.select')}}</option>
                            @foreach($villages as $village)
                            <option value={{$village->id}}>{{$village->name}}</option>
                            @endforeach
                        </select>
                        @error('vill_id')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                @endif
                @if($shipping->count() > 0)
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            {{__('blog.delivery_location_information')}}
                        </h4>
                    </div>
                </div>
                <div style=" overflow-x: scroll; height: 450px;">
                    <div class="row">
                        @php
                        $i = 1;
                        @endphp
                        @foreach($shipping as $item)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-center">{{__('blog.delivery_location_information')}} {{$i++}}</h5>
                                    <p>{{__('lang.name')}}: {{$item->firstname}}</td>
                                    <p>{{__('lang.lastname')}}: {{$item->lastname}}</td>
                                    <p>{{__('lang.phone')}}: {{$item->phone}}</p>
                                    <p>{{__('lang.email')}}: {{$item->email}}</p>
                                    <p>{{__('lang.village')}}: @if(!empty($item->villname->name))
                                        {{$item->villname->name}} @endif</p>
                                    <p>{{__('lang.districtname')}}: @if(!empty($item->disname->name))
                                        {{$item->disname->name}} @endif</p>
                                    <p>{{__('lang.province')}}: @if(!empty($item->proname->name))
                                        {{$item->proname->name}} @endif</p>
                                    <input type="radio" value='{{$item->id}}' id="shipping" wire:model="shipping_id">
                                    <label for="shipping">{{__('lang.select')}}</label>
                                    @if($this->shipping_id == $item->id)
                                    <p class="text-success">{{__('blog.choose_delivery_success')}}</p>
                                    <i class="fas fa-check-circle" style="color:green;font-size:16px;"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        @error('shipping_id')
                        <p class="text-danger">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="transferbank" value="1"
                                wire:model="ship_to_different" />
                            <label class="custom-control-label" for="transferbank"
                                data-toggle="collapse">{{__('lang.Add_shipping_location')}}</label>
                        </div>
                    </div>
                </div>
        </div>
        @if($this->ship_to_different)
        <h5 class="section-title position-relative text-uppercase mb-3"><span
                class="bg-secondary pr-3">{{__('lang.Add_shipping_location')}}</span></h5>
        <div class="bg-light p-30">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>{{__('lang.name')}}</label>
                    <input class="form-control" type="text" placeholder="{{__('lang.name')}}" wire:model="s_firstname">
                    @error('s_firstname')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.lastname')}}</label>
                    <input class="form-control" type="text" placeholder="{{__('lang.lastname')}}"
                        wire:model="s_lastname">
                    @error('s_lastname')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.phone')}}</label>
                    <input class="form-control" type="text" placeholder="02055944671" wire:model="s_phone">
                    @error('s_phone')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.email')}}</label>
                    <input class="form-control" type="text" placeholder="example@email.com" wire:model="s_email">
                    @error('s_email')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.address')}}</label>
                    <input class="form-control" type="text" placeholder="{{__('lang.street_address')}}"
                        wire:model="s_address">
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.home_no')}}</label>
                    <input class="form-control" type="number" placeholder="228" wire:model="s_home_no">
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.province')}}</label>
                    <select class="custom-select" wire:model="s_pro_id">
                        <option selected>{{__('lang.select')}}</option>
                        @foreach($provinces as $province)
                        <option value={{$province->id}}>{{$province->name}}</option>
                        @endforeach
                    </select>
                    @error('s_pro_id')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.districtname')}}</label>
                    <select class="custom-select" wire:model="s_dis_id">
                        <option selected>{{__('lang.select')}}</option>
                        @foreach($s_districts as $district)
                        <option value={{$district->id}}>{{$district->name}}</option>
                        @endforeach
                    </select>
                    @error('s_dis_id')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <label>{{__('lang.village')}}</label>
                    <select class="custom-select" wire:model="s_vill_id">
                        <option selected>{{__('lang.select')}}</option>
                        @foreach($s_villages as $village)
                        <option value={{$village->id}}>{{$village->name}}</option>
                        @endforeach
                    </select>
                    @error('s_vill_id')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="col-lg-4">
        <h5 class="section-title position-relative text-uppercase mb-3"><span
                class="bg-secondary pr-3">{{__('lang.order_detail')}}</span></h5>
        <div class="bg-light p-30 mb-5">
            <div class="border-bottom">
                <h6 class="mb-3">{{__('lang.product')}}</h6>
                @foreach (Cart::instance('cart')->content() as $item)
                <div class="d-flex justify-content-between">
                    <img src="{{asset($item->model->image)}}" alt="{{$item->model->name}}"
                        style="width: 30px; height:30px;">
                    <p> {{$item->model->name}}:</p>
                    <p>{{$item->qty}}</p>
                </div>
                @endforeach
            </div>
            <div class="border-bottom pt-3 pb-2">
                <div class="d-flex justify-content-between mb-3">
                    <h6>{{__('lang.subtotal')}}:</h6>
                    <h6>{{number_format(session()->get('checkout')['subtotal'])}}</h6>
                </div>
                <div class="d-flex justify-content-between mb-3">
                    <h6>{{__('lang.discount')}}:</h6>
                    @if(!empty(session()->get('checkout')['discount']))
                    <h6>- {{number_format(session()->get('checkout')['discount'])}}</h6>
                    @else
                    <h6>0</h6>
                    @endif
                </div>
                <div class="d-flex justify-content-between">
                    <h6 class="font-weight-medium">{{__('blog.tax')}}:</h6>
                    <h6 class="font-weight-medium">{{session()->get('checkout')['tax']}}</h6>
                </div>
            </div>
            <div class="pt-2">
                <div class="d-flex justify-content-between mt-2">
                    <h5>{{__('lang.grandtotal')}}:</h5>
                    <h5>{{number_format(session()->get('checkout')['total'])}}</h5>
                </div>
            </div>
            @foreach($exchange as $item)
            <div class="pt-2">
                <div class="d-flex justify-content-between mt-2">
                    <h5>{{$item->symbol}}:</h5>
                    <h5><b>{{number_format((session()->get('checkout')['total'])/$item->rate,2)}}</b></h5>
                </div>
            </div>
            @endforeach
        </div>
        <div class="mb-5">
            <h5 class="section-title position-relative text-uppercase mb-3"><span
                    class="bg-secondary pr-3">{{__('lang.payment')}}</span></h5>
            <div class="bg-light p-30">
                <!--
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" value="cod" id="directcheck" wire:model="paymentmode">
                                <label class="custom-control-label" for="directcheck">{{__('lang.pay_on_cash')}}</label><br>
                            </div>
                        </div>
                        -->
                <div class="form-group mb-4">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" value="banktransfer" name="payment"
                            id="banktransfer" wire:model="paymentmode">
                        <label class="custom-control-label" for="banktransfer">{{__('lang.pay_on_bcelone')}}</label>
                    </div>
                </div>
                @error('paymentmode')
                <span class="text-danger">{{$message}}</span>
                @enderror
                <div class="form-group mb-4">
                    <!--
                        <div class="bank-image" style="display: full; justify-content: center;  flex-direction: column; align-items: center;">
                        @foreach($branches as $branch)
                         <div class="bank-profile">   
                             <img   src="{{asset($branch->payment_logo)}}" alt="">
                        </div><br><br>
                        @endforeach
                        -->
                    <img src="{{asset($branchs->payment_logo)}}" alt="" width="100%">
                    @if($paymentmode == 'banktransfer')
                    @if($image)
                    @php
                    try {
                    $url = $image->temporaryUrl();
                    $status = true;
                    }catch (RuntimeException $exception){
                    $this->status = false;
                    }
                    @endphp
                    @if($status)
                    <div class="bank-profile">
                        <img src="{{ $url }}" alt="" style="border: 5px solid green;">
                    </div><br>
                    @endif
                    <i class="fas fa-check-circle" style="color:green;font-size:30px;"></i>
                    @else
                    <h4 class="card-title">{{__('lang.choose_image')}}</h4>
                    @endif
                    <br>
                    <div class="form-group">
                        <input type="file" class="form-control" wire:model="image" />
                    </div>
                    @error('image')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                    @endif
                </div>
            </div>
            <div>
                <h4>
                    <p class="text-danger">
                        @if (Config::get('app.locale') == 'lo')
                        {{$branchs->note_checkout_lo}}
                        @elseif (Config::get('app.locale') == 'en')
                        {{$branchs->note_checkout_en}}
                        @endif
                    </p>
                </h4>
            </div>
            <button type="submit"
                class="btn btn-block btn-primary font-weight-bold py-3">{{__('lang.place_order')}}</button>
        </div>
    </div>
    </form>
</div>
</div>

@endif

</div>
<!-- Checkout End -->
</div>