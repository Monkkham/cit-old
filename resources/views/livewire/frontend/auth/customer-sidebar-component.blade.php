
<div class="col-lg-3 col-md-4">
 <div class="bg-light p-4 mb-30">
    <a  id="control-font" href="{{route('customer.dashboard')}}" class="form-control text-dark"><i class="fas fa-layer-group"></i> {{__('blog.customer_dashboard')}}</a>
          @if(Auth::user()->role_id ==10)
            <a id="control-font" href="{{ route('customer.dailycustomershop') }}" class="form-control text-dark"><i class="fas fa-store"></i> {{__('blog.shop')}}<span class="special-product">
             @if($this->percent)
              {{__('blog.special_price')}}({{__('blog.buy_get')}} 
              {{$this->percent}}
              %)
            @endif
            </span></a>
            <a id="control-font" href="{{ route('customer.dailycart') }}" class="form-control text-dark">
              <i class="fas fa-cart-arrow-down"></i> {{__('blog.cart')}}
              @if(Cart::instance('cart')->count() > 0)
                ({{Cart::instance('cart')->count()}})
              @endif
            </a>
            {{-- <a href="{{ route('customer.orderdivid') }}" class="form-control text-dark"><i class="fas fa-wallet"></i> {{__('blog.order_divid')}}</a> --}}
            <a id="control-font" href="{{ route('customer.withrawdivid') }}" class="form-control text-dark"><i class="fas fa-sign-in-alt"></i> {{__('blog.withdraw_money')}}</a>
            <a id="control-font" href="{{ route('customer.historywithraw') }}" class="form-control text-dark"><i class="fas fa-sign-out-alt"></i> {{__('blog.amount_withdrawn')}}</a>
          @endif
          {{-- <a href="{{ route('customer.likedproduct') }}" class="form-control text-dark"><i class="fas fa-heart"></i>{{__('lang.wishlist_to_cart')}}</a> --}}
        <a id="control-font" href="{{ route('customer.history') }}" class="form-control text-dark"><i class="fa fa-folder-open"></i> {{__('blog.order_history')}}</a>
        <a id="control-font" href="{{route('customer.profile', auth()->user()->id)}}" class="form-control text-dark"><i class="fa fa-id-badge"></i> {{__('blog.profile')}}</a>
        <a id="control-font" href="{{route('customer.shipping', auth()->user()->id)}}" class="form-control text-dark"><i class="fa fa-truck"></i> {{__('lang.Add_shipping_location')}}</a>
        <a id="control-font" href="{{route('customer_sign_out')}}" class="form-control text-dark"><i class="fa fa-unlock"></i> {{__('blog.sign_out')}}</a>             
    </div>
</div>