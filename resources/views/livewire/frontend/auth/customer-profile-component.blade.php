<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('home')}}">{{__('blog.home')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.customer_dashboard')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <style>
    .bank-profile {
        position: relative;
        width: 250px;
        height: 250px;
        overflow: hidden;
        cursor: pointer;
    }

    .bank-profile img {
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    </style>
    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">

            <!-- Shop Sidebar Start-->
            <livewire:frontend.auth.customer-sidebar-component />
            <!-- Shop Sidebar End -->

            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-8">
                <div class="bg-light p-4 mb-30">
                    <div class="contact-form bg-light p-30">
                        <div class="form-group text-center">
                            <label>{{__('lang.edit')}}</label>
                        </div>
                        <div>
                            @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" wire:model="name" wire:keydown.enter="register"
                                class="form-control @error('name') is-invalid @enderror"
                                placeholder="{{__('lang.name')}}">
                            @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"> 020</i></span>
                            </div>
                            <input type="text" wire:model="phone" wire:keydown.enter="register"
                                class="form-control @error('phone') is-invalid @enderror"
                                placeholder="{{__('lang.phone')}}">
                        </div>
                        <div>
                            @error('phone') <span style="color: red" class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="email" wire:model="email" wire:keydown.enter="register"
                                class="form-control @error('email') is-invalid @enderror"
                                placeholder="{{__('lang.email')}}">
                            @error('email') <span style="color: red" class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="address" wire:keydown.enter="register"
                                    class="form-control @error('address') is-invalid @enderror"
                                    placeholder="{{__('lang.address')}}">
                                @error('address') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="home_no" wire:keydown.enter="register"
                                    class="form-control @error('home_no') is-invalid @enderror"
                                    placeholder="{{__('lang.home_no')}}">
                                @error('home_no') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>{{__('lang.province')}}</label>
                                <select class="custom-select" wire:model="pro_id">
                                    <option selected>{{__('lang.select')}}</option>
                                    @foreach($provinces as $province)
                                    <option value={{$province->id}}>{{$province->name}}</option>
                                    @endforeach
                                </select>
                                @error('pro_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label>{{__('lang.districtname')}}</label>
                                <select class="custom-select" wire:model="dis_id">
                                    <option selected>{{__('lang.select')}}</option>
                                    @foreach($districts as $district)
                                    <option value={{$district->id}}>{{$district->name}}</option>
                                    @endforeach
                                </select>
                                @error('dis_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label>{{__('lang.village')}}</label>
                                <select class="custom-select" wire:model="vill_id">
                                    <option selected>{{__('lang.select')}}</option>
                                    @foreach($villages as $village)
                                    <option value={{$village->id}}>{{$village->name}}</option>
                                    @endforeach
                                </select>
                                @error('vill_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="password" wire:model="password" wire:keydown.enter="register"
                                class="form-control @error('password') is-invalid @enderror"
                                placeholder="{{__('lang.password')}}">
                            @error('password') <span style="color: red" class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" wire:model="confirmpassword" wire:keydown.enter="register"
                                class="form-control @error('confirmpassword') is-invalid @enderror"
                                placeholder="{{__('lang.confirmpassword')}}">
                            @error('confirmpassword') <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        @if(Auth::user()->role_id ==10)
                        <div class="form-group">
                            <input type="text" wire:model="balance_id"
                                class="form-control @error('balance_id') is-invalid @enderror"
                                placeholder="{{__('blog.balance_id')}}">
                            @error('balance_id') <span style="color: red" class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="bank-image"
                            style="display: flex; justify-content: center;  flex-direction: column; align-items: center;">
                            @if($image)
                            @php
                            try {
                            $url = $image->temporaryUrl();
                            $status = true;
                            }catch (RuntimeException $exception){
                            $this->status = false;
                            }
                            @endphp
                            @if($status)
                            <div class="bank-profile">
                                <img src="{{ $url }}" alt="" style="border: 5px solid green;">
                            </div><br>
                            @endif
                            <i class="fas fa-check-circle" style="color:green;font-size:30px;"></i>
                            @elseif($newimage)
                            <div class="bank-profile">
                                <img src="{{asset('images/qrcode')}}/{{$newimage}}" alt=""
                                    style="border: 5px solid green;">
                            </div>
                            @endif
                        </div>
                        <label class="card-title">{{__('blog.bcelone_image')}}</label>
                        <div class="form-group">
                            <input type="file" class="form-control" wire:model="image" />
                        </div>
                        @endif

                        <div class="text-center">
                            <button class="btn btn-primary py-2 px-4"
                                wire:click="updateProfile">{{__('lang.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>