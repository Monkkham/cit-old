<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('home')}}">{{__('blog.home')}}</a>
                    <span class="breadcrumb-item active">{{__('lang.Add_shipping_location')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">

            <!-- Shop Sidebar Start-->
            <livewire:frontend.auth.customer-sidebar-component />
            <!-- Shop Sidebar End -->

            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-8">
                <div class="bg-light p-4 mb-30">
                    <div class="contact-form bg-light p-30">
                        <div class="form-group text-center">
                            <label>{{__('lang.Add_shipping_location')}}</label>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="name"
                                    class="form-control @error('name') is-invalid @enderror"
                                    placeholder="{{__('lang.name')}}">
                                @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="lastname"
                                    class="form-control @error('lastname') is-invalid @enderror"
                                    placeholder="{{__('lang.lastname')}}">
                                @error('lastname') <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"> 020</i></span>
                                    </div>
                                    <input type="text" wire:model="phone"
                                        class="form-control @error('phone') is-invalid @enderror"
                                        placeholder="{{__('lang.phone')}}">
                                </div>
                                @error('phone') <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="email" wire:model="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    placeholder="{{__('lang.email')}}">
                                @error('email') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="address"
                                    class="form-control @error('address') is-invalid @enderror"
                                    placeholder="{{__('lang.address')}}">
                                @error('address') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="text" wire:model="home_no"
                                    class="form-control @error('home_no') is-invalid @enderror"
                                    placeholder="{{__('lang.home_no')}}">
                                @error('home_no') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>{{__('blog.province')}}</label>
                                <select class="custom-select" wire:model="pro_id">
                                    @foreach($provinces as $province)
                                    <option value={{$province->id}}>{{$province->name}}</option>
                                    @endforeach
                                </select>
                                @error('pro_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label>{{__('blog.district')}}</label>
                                <select class="custom-select" wire:model="dis_id">
                                    @foreach($districts as $district)
                                    <option value={{$district->id}}>{{$district->name}}</option>
                                    @endforeach
                                </select>
                                @error('dis_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-md-4 form-group">
                                <label>{{__('blog.village')}}</label>
                                <select class="custom-select" wire:model="vill_id">
                                    @foreach($villages as $village)
                                    <option value={{$village->id}}>{{$village->name}}</option>
                                    @endforeach
                                </select>
                                @error('vill_id')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="text-center">
                            @if(!empty($this->hiddenId))
                            <button class="btn btn-warning py-2 px-4"
                                wire:click="Saveshipping">{{__('lang.edit')}}</button>
                            @else
                            <button class="btn btn-success py-2 px-4"
                                wire:click="Saveshipping">{{__('lang.save')}}</button>
                            @endif
                        </div>
                    </div>
                    @if($data->count() > 0)
                    <div class="row">
                        <i class="fa fa-truck"></i>
                        <h5>{{__('blog.delivery_information')}}</h5>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th>{{__('lang.no')}}</th>
                                        <th>{{__('lang.name')}}</th>
                                        <th>{{__('lang.lastname')}}</th>
                                        <th>{{__('lang.phone')}}</th>
                                        <th>{{__('lang.email')}}</th>
                                        <th>{{__('lang.village')}}</th>
                                        <th>{{__('lang.district')}}</th>
                                        <th>{{__('lang.province')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach($data as $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td class="text-left"> <a href="#" wire:click="showEdit({{$item->id}})"><i
                                                    class="fa fa-pen text-dark"></i></a>&nbsp;{{$item->firstname}}</td>
                                        <td class="text-left">{{$item->lastname}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>@if(!empty($item->villname->name)) {{$item->villname->name}} @endif</td>
                                        <td>@if(!empty($item->disname->name)) {{$item->disname->name}} @endif</td>
                                        <td>@if(!empty($item->proname->name)) {{$item->proname->name}} @endif</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>