<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.order_divid')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">

        

            <!-- Shop Product Start -->
            <div class="col-lg-12 col-md-8">
                <div class="bg-light p-4 mb-30 text-center">
                        <div class="row">
                            <div class="col-md-12 p-2 bg-warning text-light text-center">
                                    <div class="text-value">
                                        @if($all_balance)
                                           <h4><b>{{number_format($all_balance)}}</b> {{__('blog.lak')}}</h4>
                                        @else
                                           <h4><b>0</b></h4>
                                        @endif
                                    </div>
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-wallet" style="font-size: 80px;"></i>
                                    </div>
                                    <div class="text-value">
                                        @if($order_count)
                                            <h5><b>{{$order_count}}</b> {{__('blog.item')}}</h5>
                                        @else
                                              <h5><b>0</b></h5>
                                        @endif
                                    </div>
                                    <div class="card-info">
                                        <div>{{__('blog.order_divid')}}</div>
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-12 p-2 text-center">
                                 <a href="{{ route('customer.withrawdivid') }}" class="btn btn-success"> <i class="fas fa-sign-in-alt" style="font-size: 20px;"></i>&nbsp;{{__('blog.withdraw_money')}}</a>
                            </div>
                        </div>
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    @if($divids->count() > 0)
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-12 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                    <tr>
                    <th>{{__('lang.no')}}</th>
                    <th>{{__('lang.code')}}</th>
                    <th>{{__('lang.divid_name')}}</th>
                    <th>{{__('lang.date')}}</th>
                    </tr>
                    </thead>
                    @php
                    $i=1;
                    @endphp
                    <tbody>
                       @foreach($divids as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->id}}</td>
                            <td>
                                @if($item->amount)
                                {{number_format($item->amount)}} {{__('blog.lak')}}
                                @endif
                            </td>
                            <td>
                                {{$item->created_at}}
                            </tr>
                       @endforeach
                    </tbody>
                  </table>
            </div>
       </div>
   </div>
   @endif
    <!-- Shop End -->
</div>

