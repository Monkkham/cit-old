<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark"
                        href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <a class="breadcrumb-item text-dark"
                        href="{{route('customer.history')}}">{{__('blog.order_history')}}</a>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Product Start -->
            <div class="col-lg-12">
                <div class="bg-light mb-20">
                    <div class="contact-form bg-light p-30">
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <input type="text" wire:model="search" class="form-control"
                                    placeholder="{{__('blog.search')}}">
                            </div>
                        </div>
                        <style>
                        .controllsubtotal {
                            display: flex;
                            justify-content: space-between;
                            align-items: center;
                        }
                        </style>

                        <div class="row">
                            <div class="col-md-1 mb-2">
                                <a href="#" onclick="printDiv('printOrder')"
                                    class="btn btn-info btn-sm w-100 mt-2 p-2"><i
                                        class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <input type="text" wire:model="search" class="form-control"
                                    placeholder="{{__('blog.search')}}">
                            </div>
                        </div>
                        @if($showcustomerhistory->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-light table-borderless table-hover text-center mb-0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>{{__('lang.order_no')}}</th>
                                        <th>{{__('lang.orderitem_image')}}</th>
                                        <th>{{__('lang.orderitem_proname')}}</th>
                                        <th>{{__('lang.order_qty')}}</th>
                                        <th>{{__('lang.order_price')}}</th>
                                        <th>{{__('lang.total')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach($showcustomerhistory as $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td><img src="{{asset($item->product->image)}}" width="50px"></td>
                                        <td class="text-left" style="white-space:nowrap;">{{$item->product->name}}</td>
                                        <td>{{$item->quantity}}</td>
                                        <td>{{number_format($item->price)}}</td>
                                        <td>{{number_format($item->quantity * $item->price)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <style>
                        .controllsubtotal {
                            display: flex;
                            justify-content: space-between;
                            align-items: center;
                        }
                        </style>
                        <div class="controllsubtotal">
                            <div class="discount">
                                @if(Auth::user()->role_id ==10)
                                @if($discount > 0)
                                <h5>{{__('lang.divid_name')}}: {{number_format($discount)}} </h5>
                                @endif
                                @else
                                @if($discount > 0)
                                <h5>{{__('lang.discount')}}: {{number_format($discount)}} </h5>
                                @endif
                                @endif
                            </div>
                            <div class="subtotal">
                                @if($total > 0)
                                <h5>{{__('lang.total')}}: {{number_format($total)}}</h5>
                                @endif
                                @foreach($exchange as $item)
                                <div style="display:flex;justify-content:space-between;">
                                <h5>{{$item->symbol}}:</h5>
                                <h5><b>{{number_format($total/$item->rate,2)}}</b></h5>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @else
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tab-pane-1">
                                <h4 class="mb-3 text-center">{{__('lang.no_order')}}</h4>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <!-- Shop Product End -->
            </div>
        </div>
        <!-- Shop End -->
        <!-- print -->
        <style type="text/css" media="print">
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
        </style>
        <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
        </script>
        <div class="container" id='printOrder' style="display:none;">
            @if(!empty($branchs->bill_header))
            <div class="row">
                <div class="col-md-12">
                    <p class="text-left">{!! $branchs->bill_header !!}</p>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{__('lang.bill_data')}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    @php
                    $date=date('d/m/Y H:i:s');
                    @endphp
                    <p>{{__('lang.order_id')}}: {{$this->order_id}}</p>
                    <p>{{__('lang.date')}}: {{date('d/m/Y H:i:s', strtotime($item->created_at ))}}</p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6 text-left">
                    <h5><b>{{__('blog.order_information')}}</b></h5>
                    <p>{{__('lang.name')}}: {{$orders->firstname}} {{$orders->lastname}} - {{__('lang.phone')}}:
                        {{$orders->phone}}</p>
                    <p>{{__('lang.address')}}: {{$orders->address}}, {{$orders->home_no}},
                        @if(!empty($orders->village->name)){{$orders->village->name}}@endif,
                        @if(!empty($orders->district->name)){{$orders->district->name}}@endif,
                        @if(!empty($orders->province->name)){{$orders->province->name}}@endif </p>
                </div>

                <div class="col-md-6 text-right">
                    <h5><b>{{__('blog.delivery_information')}}</b></h5>
                    <p>{{__('lang.name')}}:
                        @if(!empty($orders->shippingname->firstname)){{$orders->shippingname->firstname}} @endif
                        @if(!empty($orders->shippingname->lastname)){{$orders->shippingname->lastname}} @endif -
                        {{__('lang.phone')}}:
                        @if(!empty($orders->shippingname->lastname)){{$orders->shippingname->phone}} @endif</p>
                    <p>{{__('lang.address')}}:
                        @if(!empty($orders->shippingname->address)){{$orders->shippingname->address}} @endif,
                        @if(!empty($orders->shippingname->home_no)){{$orders->shippingname->home_no}} @endif,
                        @if(!empty($orders->shippingname->villname->name)){{$orders->shippingname->villname->name}}@endif,
                        @if(!empty($orders->shippingname->disname->name)){{$orders->shippingname->disname->name}}@endif,
                        @if(!empty($orders->shippingname->proname->name)){{$orders->shippingname->proname->name}}@endif
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="text-left">
                        <h4><b>@if(!empty($this->total_salarylog))
                                {{__('lang.grandtotal')}}:{{number_format($this->total_salarylog)}}
                                @endif</b></h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="text-align: center">
                            <thead>
                                <tr>
                                    <th>{{__('lang.order_no')}}</th>
                                    <th>{{__('lang.orderitem_image')}}</th>
                                    <th>{{__('lang.orderitem_proname')}}</th>
                                    <th>{{__('lang.order_qty')}}</th>
                                    <th>{{__('lang.order_price')}}</th>
                                    <th>{{__('lang.total')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $i = 1;
                                @endphp
                                @foreach($showcustomerhistory as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><img src="{{asset($item->product->image)}}" style="width:20%"></td>
                                    <td class="text-left" style="white-space:nowrap;">{{$item->product->name}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td class="text-right">{{number_format($item->price)}}</td>
                                    <td class="text-right">{{number_format($item->quantity * $item->price)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5" class="text-right">
                                        <b>{{__('lang.total')}}</b><br>
                                        @foreach($exchange as $item)
                                        <b>{{$item->symbol}}</b><br>
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        @if($total > 0)
                                        <b>{{number_format($total)}}</b><br>
                                        @endif
                                        @foreach($exchange as $item)
                                        <b>{{number_format($this->total/$item->rate,2)}}</b><br>
                                        @endforeach
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            @if(!empty($branchs->bill_footer))
            <div class="row">
                <div class="col-md-12">
                    <p class="text-left">{!! $branchs->bill_footer !!}</p>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6 text-center">
                    <p><b>{{$branchs->staff_sign}}</b></p>
                </div>
                <div class="col-md-6 text-center">
                    <p><b>{{$branchs->customer_sign}}</b></p>
                </div>
            </div>
        </div>
        <!-- end -->
    </div>