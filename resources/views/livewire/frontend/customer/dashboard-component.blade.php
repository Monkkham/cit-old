<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('home')}}">{{__('blog.home')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.customer_dashboard')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">

            <!-- Shop Sidebar Start-->
            <livewire:frontend.auth.customer-sidebar-component />
            <!-- Shop Sidebar End -->
            <style>
                .a-hover-show{
                    color:white;
                    text-decoration:none;
                }
                .a-hover-show:hover{
                    color:black;
                    text-decoration:none;
                }
            </style>
            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-8">
                <div class="bg-light p-4 mb-30">
                    @if(Auth::user()->role_id ==10)
                      <div class="row">
                          <div class="col-md-4 p-2 bg-warning text-light text-center">
                                <div class="text-value">
                                    @if($this->sum_balance)
                                        <h5><b>{{number_format($this->sum_balance)}}</b>{{__('blog.lak')}}</h5>
                                    @else
                                        <h5><b>0</b></h5>
                                    @endif
                                </div>
                              
                                <div class="card-icon mr-2">
                                    <i class="fas fa-wallet" style="font-size: 80px;"></i>
                                </div>
                                
                                <div class="card-info">
                                    {{__('blog.order_divid')}}
                                </div>
                                
                        </div>
                        <div class="col-md-4 p-2 text-light text-center" style="background-color:#E33D26">
                                <div class="text-value">
                                  @if($this->withrawal_balance)
                                     <h5><b>{{number_format($this->withrawal_balance)}}</b>{{__('blog.lak')}}</h5>
                                  @else
                                     <h5><b>0</b></h5>
                                  @endif
                                </div>
                              
                                <div class="card-icon mr-2">
                                    <i class="fas fa-sign-out-alt" style="font-size: 80px;"></i>
                                </div>
                                <a href="{{ route('customer.historywithraw') }}"  class="a-hover-show" >
                                        <div class="card-info">
                                           <i class="fas fa-arrow-circle-right"></i> {{__('blog.amount_withdrawn')}}
                                        </div>
                                </a>
                        </div>
                        <div class="col-md-4 p-2 bg-info text-light text-center">
                                   <div class="text-value">
                                       @if($this->balance)
                                          <h5><b>{{number_format($this->balance)}}</b>{{__('blog.lak')}}</h5>
                                       @else
                                       <h5><b>0</b></h5>
                                       @endif
                                    </div>
                                  
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-money-check" style="font-size: 80px;"></i>
                                    </div>
                                    <a href="{{ route('customer.withrawdivid') }}"  class="a-hover-show" >
                                        <div class="card-info">
                                           <i class="fas fa-arrow-circle-right"></i>  {{__('blog.amount_remaining')}}
                                    </div>
                                </a>
                        </div>
                        
                    </div>
                    @endif
                    <br>
                    @if($this->sum_requestwithraw)
                    <div class="row">
                        <div class="col-md-4 p-2 text-light text-center" style="background-color:#c27b51">
                                    <div class="text-value">
                                        @if($this->sum_requestwithraw)
                                            <h5><b>{{number_format($this->sum_requestwithraw)}}</b>{{__('blog.lak')}}</h5>
                                        @else
                                        <h5><b>0</b></h5>
                                        @endif
                                        </div>
                                        <div class="card-icon mr-2">
                                        <i class="fas fa-sign-in-alt" style="font-size: 80px;"></i>
                                        </div>
                                        <div class="card-info">
                                            {{__('blog.request_balance')}}
                                        </div>
                            </div>
                        <div class="col-md-4 p-2 text-light text-center" style="background-color:#21A90A">
                                   <div class="text-value">
                                       @if($sum_ordertotal)
                                           <h5><b>{{number_format($sum_ordertotal)}}</b>{{__('blog.lak')}}</h5>
                                       @else
                                            <h5><b>0</b></h5>
                                       @endif
                                    </div>
                                  
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-cart-arrow-down" style="font-size: 80px;"></i>
                                    </div>
                                    @if($count_totalorder)
                                       <div>{{$count_totalorder}} {{__('blog.item')}}</div>
                                    @endif
                                    <a href="{{ route('customer.history') }}"  class="a-hover-show" >
                                        <div class="card-info">
                                        <i class="fas fa-arrow-circle-right"></i> {{__('blog.order_history')}}
                                        </div>
                                   </a>
                        </div>
                        <div class="col-md-4 p-2 text-light text-center"  style="background-color:rgb(248, 112, 134)">
                            <div class="text-value">
                                <h5><b>{{$count_likeproduct->count()}}</b>{{__('blog.item')}}</h5>
                            </div>
                          
                            <div class="card-icon mr-2">
                                <i class="fas fa-heart" style="font-size: 80px;"></i>
                            </div>
                            <div class="card-info">
                                {{__('lang.wishlist_to_cart')}}
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-6 p-2 text-light text-center" style="background-color:#21A90A">
                                   <div class="text-value">
                                       @if($sum_ordertotal)
                                           <h5><b>{{number_format($sum_ordertotal)}}</b>{{__('blog.lak')}}</h5>
                                       @else
                                            <h5><b>0</b></h5>
                                       @endif
                                    </div>
                                  
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-cart-arrow-down" style="font-size: 80px;"></i>
                                    </div>
                                    @if($count_totalorder)
                                       <div>{{$count_totalorder}} {{__('blog.item')}}</div>
                                    @endif
                                    <a href="{{ route('customer.history') }}"  class="a-hover-show" >
                                        <div class="card-info">
                                        <i class="fas fa-arrow-circle-right"></i> {{__('blog.order_history')}}
                                        </div>
                                   </a>
                        </div>
                        <div class="col-md-6 p-2 text-light text-center"  style="background-color:rgb(248, 112, 134)">
                            <div class="text-value">
                                <h5><b>{{$count_likeproduct->count()}}</b>{{__('blog.item')}}</h5>
                            </div>
                          
                            <div class="card-icon mr-2">
                                <i class="fas fa-heart" style="font-size: 80px;"></i>
                            </div>
                                {{__('lang.wishlist_to_cart')}}
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>

