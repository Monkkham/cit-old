<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.withdraw_money')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">

            <!-- Shop Product Start -->
            <div class="col-lg-12 col-md-8">
                 @if($this->check_status ==1)
                        <div class="alert alert-success text-center">
                             {{__('blog.yourbalance_no_enaough')}}
                        </div>
                  @endif
                <div class="bg-light p-4 mb-30 text-center">
                    <form wire:submit.prevent="request_withraw">
                        <div class="row">
                            <style>
                                .format_number{
                                    background:none;
                                    border:none;
                                    font-size:20pt;
                                    font-weight:bold;
                                    text-align:center;
                                    color:green;
                                }
                            </style>
                             <div class="col-md-12 p-2 text-center">
                                 <div class="form-group">
                                   <h2><b>{{__('blog.money_your_account')}}:<a style="color:red">{{number_format($divid_total)}}{{__('lang.lak')}}</a></b></h2>
                                 </div>
                                <div class="form-group">
                                    <input class="format_number" wire:model="slug">
                                </div>
                                <div class="form-group">
                                    <input wire:model="request_money" type="text"  placeholder="{{__('lang.qty_money')}}" class="form-control @error('request_money') is-invalid @enderror" wire:keyup="Autoformat">
                                     @error('request_money') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="form-group">
                                     <button type="submit" class="btn btn-success w-100"><i class="fas fa-sign-in-alt"></i> {{__('blog.withdraw_money')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>

