<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">ຖອນເງິນສໍາເລັດ</span>
                </nav>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="bg-light p-4 mb-30">
               
                 <div style="float:right">
                      <i class="far fa-times-circle text-danger" wire:click.prevent="close_withraw" style="font-size:30px;"></i>
                 </div>
                        <div class="row">
                            <div class="col-md-12 p-2 text-center">
                               <i class="fas fa-check-circle" style="font-size:100px;color:green"></i>
                           </div> 
                             <div class="col-md-12 p-2 text-center">
                                <div class="form-group">
                                    <label>{{session()->get('request_money')['date']}}</label>
                                    <input type="text" placeholder="ລາຍລະອຽດ"   class="form-control text-center" disabled>
                                </div>
                                <div class="form-group">
                                    <label>{{__('lang.code')}}: {{Auth::user()->id}}</label>
                                </div>
                                <div class="form-group">
                                    <label>{{__('lang.name')}}: {{Auth::user()->name}}</label>
                                </div>
                                <div class="form-group">
                                    <h4><b style="color:red">{{number_format(session()->get('request_money')['money'])}} {{__('lang.lak')}}</b></h4>
                                </div>
                                <div class="form-group">
                                     <p style="background-color:green; padding:10px;width:100%;color:white;font-size:20px;">{{__('lang.success')}}</p>
                                </div>
                            </div>
                        </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
</div>

