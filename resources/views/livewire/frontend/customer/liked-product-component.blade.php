<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">ສິນຄ້າທີ່ມັກ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Product Start -->
            <div class="col-lg-12 col-md-8">
                <div class="bg-light p-4 mb-30">
                       <div class="row">
                            <div class="col-md-12 p-2 text-light text-center" style="background-color:rgb(248, 112, 134)">
                                    <div class="text-value">
                                        <h5><b>{{$count_likeproduct->count()}}</b>{{__('blog.item')}}</h5>
                                    </div>
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-heart" style="font-size: 80px;"></i>
                                    </div>
                                    <div class="card-info">
                                        <div>{{__('lang.wishlist_to_cart')}}</div>
                                    </div>
                                </div>
                        </div>
                </div>
                <div class="row">
                  <div class="col-md-3 pb-2">
                            <div class="form-group">
                                <select wire:model="searchByCatalog" id="selectProduct" class="form-control">
                                    <option value="" selected>{{__('lang.select')}}</option>
                                      @foreach($all_catalogs as $item)
                                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                                      @endforeach
                                </select>
                            </div>
                    </div>
                   <div class="col-md-9 mb-2">
                      <input type="text" wire:model="search_products" class="form-control" placeholder="{{__('blog.search')}}">
                   </div>
                </div>
                @if (Session::has('success_message'))
                    <div class="alert alert-success">
                        {{Session::get('success_message')}}
                    </div>
                @endif
                <div class="row pb-3">
                          @foreach ($likeproducts as $item)
                            <div class="col-lg-3 col-md-3 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4">
                                    <div class="product-img position-relative overflow-hidden">
                                        <style>
                                            .fa-heart{
                                                color:red;
                                                padding:10px;
                                            }
                                        </style>
                                           <i class="fas fa-heart"></i>
                                        <a href="{{route('product_detail', $item->id)}}"><img class="img-fluid w-100" src="{{asset($item->image)}}" alt=""></a>
                                        
                                        <div class="product-action">
                                             <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price}})"><i class="fa fa-shopping-cart"></i></a>
                                        </div>
                                    </div>
                                    <div class="text-center py-4">
                                        <p><a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a></p>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            
                                            <h5>
                                                @if ($item->price == 0)
                                                    {{__('blog.call')}}
                                                @else
                                                    {{number_format($item->price)}} {{__('lang.lak')}}
                                                @endif
                                            </h5>
                                            <!--<h5>$123.00</h5><h6 class="text-muted ml-2"><del>$123.00</del></h6>-->
                                        </div>
                                        <div class="d-flex align-items-center justify-content-center mb-1">
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                    <div class="col-12">
                        <nav>
                        <ul class="pagination justify-content-center">
                            {{ $likeproducts->links() }}
                        </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $('#selectProduct').select2();
        $('#selectProduct').on('change', function (e) {
            var data = $('#selectProduct').select2("val");
            @this.set('product_id', data);
        });
    });
</script>
@endpush

