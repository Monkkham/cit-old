<div>
    
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.amount_withdrawn')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Product Start -->
            <div class="col-lg-12">
                <div class="bg-light p-4 mb-30 text-center">
                        <div class="col-md-12 p-2 text-light text-center" style="background-color:#E33D26">
                                <div class="text-value">
                                  @if($sum_historywithraw)
                                     <h5><b>{{number_format($sum_historywithraw)}}</b>{{__('blog.lak')}}</h5>
                                  @else
                                     <h5><b>0</b></h5>
                                  @endif
                                </div>
                              
                                <div class="card-icon mr-2">
                                    <i class="fas fa-sign-in-alt" style="font-size: 80px;"></i>
                                </div>
                                <div class="card-info">
                                            {{__('blog.amount_withdrawn')}}
                                </div>
                        </div>
                    
                 </div>
           </div>
        </div>
    </div>

    <!-- Cart Start -->
    @if($historywithrawal->count() > 0)
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-12 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th>{{__('lang.no')}}</th>
                            <th>{{__('lang.attached_photo')}}</th>
                            <th>{{__('lang.code')}}{{__('lang.customer')}}</th>
                            <th>{{__('lang.name')}}</th>
                            <th>{{__('lang.phone')}}</th>
                            <th>{{__('lang.email')}}</th>
                            <th>{{__('lang.withrawed')}}</th>
                            <th>{{__('lang.date')}}</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                    @php
                            $i = 1;
                            @endphp
                            @foreach($historywithrawal as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> 
                                        <a href="{{asset('upload/transfermoneytocustomer')}}/{{$item->image}}">
                                            <img src="{{asset('upload/transfermoneytocustomer')}}/{{$item->image}}" alt="" style="width: 60px; height:60px; object-fit: contain;">
                                        </a></td>
                                    <td>{{$item->user->id}}</td>
                                    <td>{{$item->user->name}}</td>
                                    <td>{{$item->user->phone}}</td>
                                    <td>{{$item->user->email}}</td>
                                        <td style="text-align:center">
                                        @if($item->amount)
                                            <p class="btn btn-danger btn-sm"><i class="fas fa-minus"></i> {{number_format($item->amount)}}</p>
                                        @endif
                                    </td>
                                    <td>{{date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                        <nav>
                        <ul class="pagination justify-content-center">
                            {{ $historywithrawal->links() }}
                        </ul>
                        </nav>
            </div>
        </div>
    </div>
    @endif
    <!-- Cart End -->
</div>