<div>
<style>
        @media only screen and (max-width: 600px){
            .control-respone-table{
                overflow-x: scroll;
            }
        }
    </style>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
               <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{route('customer.dashboard')}}">{{__('blog.customer_dashboard')}}</a>
                    <span class="breadcrumb-item active">{{__('blog.order_history')}}</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Product Start -->
            <div class="col-lg-12">
                <div class="bg-light p-4 mb-4">
                 <div class="contact-form bg-light p-4">
                      <div class="col-md-12 p-2 text-light text-center" style="background-color:#21A90A">
                                   <div class="text-value">
                                   <h5><b>{{number_format($order_total)}}</b>{{__('blog.lak')}}</h5>
                                    </div>
                                    <div class="card-icon mr-2">
                                        <i class="fas fa-cart-arrow-down" style="font-size: 80px;"></i>
                                    </div>
                                    <div>{{$customerhistory->count()}} {{__('blog.item')}}</div>
                                        <div class="card-info">
                                         {{__('blog.order_history')}}
                                        </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <input type="text" wire:model="search" class="form-control" placeholder="{{__('blog.search')}}{{__('lang.order_id')}}">
                            </div>
                        </div>
                   </div>
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
    <div class="container-fluid">
        <div class="row px-xl-5">
                      <div class="col-lg-12 table-responsive mb-5">
                      @if($customerhistory->count() > 0)
                          <table class="table table-light table-borderless table-hover text-center mb-0">
                            <thead class="thead-dark">
                            <tr>
                            <th>{{__('lang.order_no')}}</th>
                            <th>{{__('lang.order_id')}}</th>
                            <th>{{__('lang.date')}}</th>
                            <th>{{__('lang.status')}}</th>
                            <th>{{__('lang.order_total')}}</th>
                            <th>{{__('lang.status_pay')}}</th>
                            <th>{{__('lang.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                            $i = 1;
                            @endphp
                            @foreach($customerhistory as $item)
                            <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->id}}</td>
                            <td>{{date('d/m/Y H:i:s', strtotime($item->created_at ))}}</td>
                            <td>
                            @if($item->status == 'arrived')
                                <p span="" class="badge badge-success p-2">{{__('lang.status_arrived')}}</p>
                            @endif
                            </td>
                            <td>
                                @if($item->total)
                                   {{number_format($item->total)}}
                                @endif
                            </td>
                            <td>
                                @if($item->mode =='onepay' && $item->check_payment =='0')
                                    <a href="{{ route('admin.detailpayment',['detailpayment_slug'=>$item->order_id])}}">{{__('lang.pay_on_bcelone')}}</a> </td>
                                @elseif($item->mode =='debt')
                                <p  class="badge badge-warning p-2">{{__('blog.debt')}}</p>
                                @elseif($item->check_payment =='1' && $item->mode !='debt')
                                    <p  class="badge badge-success p-2">{{__('lang.yes_pay')}}</p>
                                @else
                                    <p  class="badge badge-danger p-2">{{__('lang.no_pay')}}</p>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('customer.showhistory',['order_slug'=>$item->id]) }}" class="btn btn-default btn-sm"><i class="fas fa-eye"></i></a>
                             </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                        @endif
                         </div>
                    </div>
                    <div class="col-12">
                        <nav>
                            <ul class="pagination justify-content-center">
                                {{ $customerhistory->links() }}
                            </ul>
                         </nav>
                     </div>
            </div>
</div>