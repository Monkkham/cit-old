
    @if(Cart::instance('wishlist')->count() > 0)
    <a href="{{route('wishlist')}}" class="btn px-0">
        <i class="fas fa-heart text-primary"></i>
        <span class="badge text-secondary border border-secondary rounded-circle" style="padding-bottom: 2px; margin-left:-10px;">{{Cart::instance('wishlist')->count()}}</span>
    </a>
    @endif