@if(Cart::instance('cart')->count() > 0)
@if(Auth::check())
@if(Auth::user()->role_id ==10)
<a href="{{route('customer.dailycart')}}" class="btn px-0 ml-3">
    <i class="fas fa-shopping-cart text-primary"></i>
    <span class="badge text-secondary border border-secondary rounded-circle"
        style="padding-bottom: 2px;">{{Cart::instance('cart')->count()}}</span>
</a>
@else
<a href="{{route('cart')}}" class="btn px-0 ml-3">
    <i class="fas fa-shopping-cart text-primary"></i>
    <span class="badge text-secondary border border-secondary rounded-circle"
        style="padding-bottom: 2px;">{{Cart::instance('cart')->count()}}</span>
</a>
@endif
@else
<a href="{{route('cart')}}" class="btn px-0 ml-3">
    <i class="fas fa-shopping-cart text-primary"></i>
    <span class="badge text-secondary border border-secondary rounded-circle"
        style="padding-bottom: 2px;">{{Cart::instance('cart')->count()}}</span>
</a>
@endif
@endif