 @if(Cart::instance('cart')->count() > 0)
 @if(Auth::check())
 @if(Auth::user()->role_id ==10)
 <a href="{{route('customer.dailycart')}}" class="btn px-0 ml-2">
     <i class="fas fa-shopping-cart text-dark"></i>
     <span class="badge text-dark border border-dark rounded-circle"
         style="padding-bottom: 4px;">{{Cart::instance('cart')->count()}}</span>
 </a>
 @else
 <a href="{{route('cart')}}" class="btn px-0 ml-2">
     <i class="fas fa-shopping-cart text-dark"></i>
     <span class="badge text-dark border border-dark rounded-circle"
         style="padding-bottom: 4px;">{{Cart::instance('cart')->count()}}</span>
 </a>
 @endif
 @else
 <a href="{{route('cart')}}" class="btn px-0 ml-2">
     <i class="fas fa-shopping-cart text-dark"></i>
     <span class="badge text-dark border border-dark rounded-circle"
         style="padding-bottom: 4px;">{{Cart::instance('cart')->count()}}</span>
 </a>
 @endif
 @endif