<div>
    <!-- Carousel Start -->
    <div class="container-fluid mb-3">
        <div class="row px-xl-5">
            <div class="col-lg-8">
                
                <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">

                        @foreach ($sliders as $key => $item)
                        <div class="carousel-item position-relative {{$key == 0 ? 'active' : '' }}" style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="{{asset($item->image)}}" style="object-fit:cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">{{__('blog.service')}}</h1>
                                    <p class="mx-md-5 px-5 animate__animated animate__bounceIn">
                                        @if (Config::get('app.locale')== 'lo')
                                            {{$item->name_la}}
                                        @elseif (Config::get('app.locale') == 'en')
                                            {{$item->name_en}}
                                        @endif
                                    </p>
                                    <a class="btn btn-outline-light py-2 px-4 mt-3 animate__animated animate__fadeInUp" href="{{route('services')}}">{{__('lang.detail')}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="product-offer mb-30" style="height: 200px;">
                    <img class="img-fluid" src="{{asset('images/it-solutions.jpg')}}" alt="">
                    <div class="offer-text">
                        <h3 class="text-white mb-3">{{__('blog.solutions')}}</h3>
                        <a href="{{route('solutions')}}" class="btn btn-outline-light">{{__('lang.detail')}}</a>
                    </div>
                </div>
                <div class="product-offer mb-30" style="height: 200px;">
                    <img class="img-fluid" src="{{asset('images/customers.jpg')}}" alt="">
                    <div class="offer-text">
                        <h3 class="text-white mb-3">{{__('blog.service')}}</h3>
                        <a href="{{route('services')}}" class="btn btn-outline-light">{{__('lang.detail')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel End -->

    <!-- Download App -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h5 class="font-weight-semi-bold m-0">{{__('blog.download_app')}}</h5> <br>

                <a href="{{$branchs->app_store}}" target="_blank"><img src="{{asset('images/appgle-app-store.png')}}" height="60"></a>
                <a href="{{$branchs->play_store}}" target="_blank"><img src="{{asset('images/google-play-store.png')}}" height="60"></a>
                <a href="{{$branchs->app_gallery}}" target="_blank"><img src="{{asset('images/huawei-app-gallery.png')}}" height="60"></a>
            </div>
        </div>
    </div>
    <!-- Featured End -->

    <!-- Categories Start 
    <div class="container-fluid pt-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Categories</span></h2>
        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                <a class="text-decoration-none" href="">
                    <div class="cat-item d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="{{asset('frontend/img/cat-1.jpg')}}" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>Category Name</h6>
                            <small class="text-body">100 Products</small>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                <a class="text-decoration-none" href="">
                    <div class="cat-item img-zoom d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="{{asset('frontend/img/cat-2.jpg')}}" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>Category Name</h6>
                            <small class="text-body">100 Products</small>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                <a class="text-decoration-none" href="">
                    <div class="cat-item img-zoom d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="{{asset('frontend/img/cat-3.jpg')}}" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>Category Name</h6>
                            <small class="text-body">100 Products</small>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                <a class="text-decoration-none" href="">
                    <div class="cat-item img-zoom d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="{{asset('frontend/img/cat-4.jpg')}}" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>Category Name</h6>
                            <small class="text-body">100 Products</small>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>-->
    <!-- Categories End -->

    <!-- All Products Start 
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
            <a href="{{route('shop')}}"><span class="bg-secondary pr-3">{{__('blog.new_products')}}</span></a>
        </h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($all_products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">
                            <div class="product-action">
                                @if($item->min_reserve != -1)
                                @if($item->price_online !=0)
                                    <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                @endif
                                @else
                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                @endif
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                            
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>
                                    @if ($item->price_online == 0)
                                        {{__('blog.call')}}
                                    @else
                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                    @endif
                                </h5>
                            </div>

                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>-->
    <!-- All Products End -->

    <!-- New HDCVI Products Start 
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
            <a href="{{route('shop')}}"><span class="bg-secondary pr-3">{{__('blog.new_cctv_products')}}</span></a>
        </h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($new_cctv_hdcvi_products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">
                            <div class="product-action">
                                @if($item->min_reserve != -1)
                                @if($item->price_online !=0)
                                    <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                @endif
                                @else
                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                @endif
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                            
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>
                                    @if ($item->price_online == 0)
                                        {{__('blog.call')}}
                                    @else
                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                    @endif
                                </h5>
                            </div>

                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>-->
    <!-- New HDCVI Products End -->

    <!-- New IPC Products Start 
    <div class="container-fluid py-5">
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($new_cctv_ipc_products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">
                            <div class="product-action">
                                @if($item->min_reserve != -1)
                                @if($item->price_online !=0)
                                    <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                @endif
                                @else
                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                @endif
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                            
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>
                                    @if ($item->price_online == 0)
                                        {{__('blog.call')}}
                                    @else
                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                    @endif
                                </h5>
                            </div>

                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>-->
    <!-- New IPC Products End -->

    <!-- New Kits Products Start 
    <div class="container-fluid py-5">
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($new_cctv_kits_products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">
                            <div class="product-action">
                                @if($item->min_reserve != -1)
                                @if($item->price_online !=0)
                                    <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                @endif
                                @else
                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                @endif
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                            
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>
                                    @if ($item->price_online == 0)
                                        {{__('blog.call')}}
                                    @else
                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                    @endif
                                </h5>
                            </div>

                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>-->
    <!-- New IPC Products End -->

    <!-- New POS Products Start 
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
            <a href="{{route('shop')}}"><span class="bg-secondary pr-3">{{__('blog.new_pos_products')}}</span></a>
        </h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($new_pos_products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">
                            <div class="product-action">
                                @if($item->min_reserve != -1)
                                @if($item->price_online !=0)
                                    <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                @endif
                                @else
                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                @endif
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                            
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>
                                    @if ($item->price_online == 0)
                                        {{__('blog.call')}}
                                    @else
                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                    @endif
                                </h5>
                            </div>

                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>-->
    <!-- New POS Products End -->

    <!-- Products Start 
    <div class="container-fluid">

        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
            <a href="{{route('shop')}}"><span class="bg-secondary pr-3">{{__('lang.products')}}</span></a>
        </h2>

        <div class="row px-xl-5">

            <!-- Shop Sidebar Start 
            <div class="col-lg-3 col-md-4">
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">{{__('lang.catalog')}}</span></h5>
                <div class="bg-light p-4 mb-30">
                    @foreach ($all_catalogs as $item)
                        <div class="custom-control d-flex align-items-center justify-content-between mb-3">
                            <label class="custom-control-label"><a href="javascript:void(0)" wire:click="searchByCatalog({{$item->id}})" class="text-dark">{{$item->name}}</a></label>
                        </div> 
                    @endforeach
                </div>
            </div>-->
            <!-- Shop Sidebar End -->

            <!-- Shop Product Start 
            <div class="col-lg-12 col-md-8">
                <div class="row">
                    <div class="col-md-3 pb-2">
                        <div class="form-group">
                            <select wire:model="searchByCatalog" id="selectProduct" class="form-control">
                                <option value="" selected>{{__('lang.select')}}</option>
                                  @foreach($all_catalogs as $item)
                                      <option value="{{ $item->id }}">{{ $item->name }}</option>
                                  @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-9 pb-2">
                        <input type="text" wire:model="search_products" class="form-control" placeholder="{{__('blog.search_for_products')}}">
                    </div>
                </div>
                <div class="row pb-3">
                        @php
                          $witems= Cart::instance('wishlist')->content()->pluck('id');
                        @endphp
                      @foreach ($products as $item)
                        <div class="col-lg-3 col-md-6 col-sm-6 pb-1">
                            <div class="product-item bg-light mb-4">
                                <div class="product-img position-relative overflow-hidden">
                                    <style>
                                        .fa-heart{
                                            color:red;
                                            padding:10px;
                                        }
                                    </style>
                                     @if($witems->contains($item->id))
                                       <i class="fas fa-heart"></i>
                                    @endif
                                    <a href="{{route('product_detail', $item->id)}}"><img class="img-fluid w-100" src="{{asset($item->image)}}" alt=""></a>
                                    
                                    @if(!empty(session()->get('check_detail')['log_detail']))
                                    
                                        @if(Auth::check() && Auth::user()->role_id == 10 && session()->get('check_detail')['log_detail'] == 1)
                                            @if($item->min_reserve != -1)
                                            @if($item->price !=0)
                                            <div class="product-action">
                                                @if($witems->contains($item->id))
                                                <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="removeWishList({{$item->id}})"><i class="fas fa-heart"></i></a>
                                                @else
                                                <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->price}})"><i class="far fa-heart"></i></a>
                                                @endif
                                                <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price}})"><i class="fa fa-shopping-cart"></i></a>
                                            </div>
                                            @endif
                                            @else
                                            <div class="product-action">
                                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                            </div>
                                            @endif
                                        @else
                                            @if($item->min_reserve != -1)
                                            @if($item->price_online !=0)
                                            <div class="product-action">
                                                @if($witems->contains($item->id))
                                                <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="removeWishList({{$item->id}})"><i class="fas fa-heart"></i></a>
                                                @else
                                                <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="far fa-heart"></i></a>
                                                @endif
                                                <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                            </div>
                                            @endif
                                            @else
                                            <div class="product-action">
                                                <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                            </div>
                                            @endif
                                        @endif
                                    @else
                                        @if($item->min_reserve != -1)
                                        @if($item->price_online !=0)
                                        <div class="product-action">
                                            @if($witems->contains($item->id))
                                            <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="removeWishList({{$item->id}})"><i class="fas fa-heart"></i></a>
                                            @else
                                            <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="far fa-heart"></i></a>
                                            @endif
                                            <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                                        </div>
                                        @endif
                                        @else
                                        <div class="product-action">
                                            <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                                        </div>
                                        @endif     
                                    @endif

                                </div>
                                <div class="text-center py-4">
                                    <p><a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a></p>
                                    
                                    <div class="d-flex align-items-center justify-content-center mt-2">
                                        <h5>
                                            @if(!empty(session()->get('check_detail')['log_detail']))
                                                @if(Auth::check() && Auth::user()->role_id == 10 && session()->get('check_detail')['log_detail'] == 1)
                                                    @if ($item->price == 0)
                                                        {{__('blog.call')}}
                                                    @else
                                                        {{number_format($item->price)}} {{__('lang.lak')}}
                                                    @endif
                                                @else
                                                    @if ($item->price_online == 0)
                                                        {{__('blog.call')}}
                                                    @else
                                                        {{number_format($item->price_online)}} {{__('lang.lak')}}
                                                    @endif
                                                @endif
                                            @else
                                                @if ($item->price_online == 0)
                                                    {{__('blog.call')}}
                                                @else
                                                    {{number_format($item->price_online)}} {{__('lang.lak')}}
                                                @endif
                                            @endif
                                        </h5>
                                    </div>
                                    
                                    <div class="d-flex align-items-center justify-content-center mb-1">
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                        <small class="fa fa-star text-primary mr-1"></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                </div>
                <div class="col-12">
                    <nav>
                    <ul class="pagination justify-content-center">
                        {{ $products->links() }}
                    </ul>
                    </nav>
                </div>
            </div>-->
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->

    <!-- Products Start 
    <div class="container-fluid pt-2 pb-2">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4">
            <a href="{{route('shop')}}"><span class="bg-secondary pr-3">{{__('lang.products')}}</span></a>
        </h2>
        <div class="row px-xl-5">
            @php
                $witems= Cart::instance('wishlist')->content()->pluck('id');
            @endphp
            @foreach ($products as $item)
            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                <div class="product-item bg-light mb-4">
                    <div class="product-img position-relative overflow-hidden">
                    <style>
                        .fa-heart{
                            color:red;
                            padding:10px;
                        }
                    </style>
                        @if($witems->contains($item->id))
                        <i class="fas fa-heart"></i>
                        @endif
                        <img class="img-fluid w-100" src="{{asset($item->image)}}" alt="">

                        @if($item->min_reserve != -1)
                        <div class="product-action">
                        @if($witems->contains($item->id))
                            <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="removeWishList({{$item->id}})"><i class="fas fa-heart"></i></a>
                        @else
                            <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="addToWishlist({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="far fa-heart"></i></a>
                        @endif
                            <a class="btn btn-outline-dark btn-square" href="javascript:void(0)" wire:click="addtoCart({{$item->id}},'{{$item->name}}',{{$item->price_online}})"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                        @else
                        <div class="product-action">
                            <h5 class="text-danger">{{__('blog.out_of_stock')}}</h5>
                        </div>
                        @endif

                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a>
                        
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>
                                
                                @if ($item->price_online == 0)
                                    {{__('blog.call')}}
                                @else
                                    {{number_format($item->price_online)}} {{__('lang.lak')}}
                                @endif
                            </h5>
                        </div>
                        
                        <div class="d-flex align-items-center justify-content-center mb-1">
                            <small class="fa fa-star text-primary mr-1"></small>
                            <small class="fa fa-star text-primary mr-1"></small>
                            <small class="fa fa-star text-primary mr-1"></small>
                            <small class="fa fa-star text-primary mr-1"></small>
                            <small class="fa fa-star text-primary mr-1"></small>
                        </div>
                    </div>
                </div>
            </div> 
            @endforeach

        </div>
    </div>-->
    <!-- Products End -->

    <!-- Service Start -->
    <div class="container-fluid pt-5 pb-3">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">{{__('blog.service')}}</span></h2>
        <div class="row px-xl-5">

            @foreach ($services as $item)
                <div class="col-md-3">
                    <div class="product-offer mb-30" style="height: 300px;">
                        <img class="img-fluid" src="{{asset($item->image)}}" alt="">
                        <div class="offer-text text-center">
                            <!--<h6 class="text-white text-uppercase">Save 20%</h6>-->
                            <h3 class="text-white mb-3">
                                @if (Config::get('app.locale') == 'lo')
                                    {{$item->name_la}}
                                @elseif (Config::get('app.locale') == 'en')
                                    {{$item->name_en}}
                                @endif
                            </h3>
                            <a href="{{route('service_detail', $item->id)}}" class="btn btn-outline-light">{{__('lang.detail')}}</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
    <!-- Offer End -->

    <!-- Customer 
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">{{__('blog.gov_customers')}}</span></h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel vendor-carousel">

                    @foreach ($gov_customers as $item)
                    <div class="bg-light p-4">
                        <img src="{{asset($item->image)}}" alt="">
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">{{__('blog.organization_customers')}}</span></h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel vendor-carousel">

                    @foreach ($original_customers as $item)
                    <div class="bg-light p-4">
                        <img src="{{asset($item->image)}}" alt="">
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>-->
    
    <!-- End Customer -->
</div>

<!--
@push('scripts')
<script>
    $(document).ready(function() {
        $('#selectProduct').select2();
        $('#selectProduct').on('change', function (e) {
            var data = $('#selectProduct').select2("val");
            @this.set('product_id', data);
        });
    });
</script>
-->
@endpush
