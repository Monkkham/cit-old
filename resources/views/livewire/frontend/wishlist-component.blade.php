@if(Cart::instance('wishlist')->content()->count() > 0)
<div>
        <!-- Breadcrumb Start -->
        <div class="container-fluid">
            <div class="row px-xl-5">
                <div class="col-12">
                    <nav class="breadcrumb bg-light mb-30">
                        <a class="breadcrumb-item text-dark" href="{{route('home')}}">{{__('blog.home')}}</a>
                        <span class="breadcrumb-item active">{{__('lang.wishlist_to_cart')}}</span>
                    </nav>
                </div>         
                @php
                    $witems= Cart::instance('wishlist')->content()->pluck('id');
                @endphp
                @foreach(Cart::instance('wishlist')->content() as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4">
                                    <div class="product-img position-relative overflow-hidden">
                                    <style>
                                            .fa-heart{
                                                color:red;
                                                padding:10px;
                                            }
                                        </style>
                                         @if($witems->contains($item->id))
                                           <i class="fas fa-heart"></i>
										@endif
                                        <a href="{{route('product_detail', $item->id)}}"><img class="img-fluid w-100" src="{{asset($item->model->image)}}" alt=""></a>
                                        <div class="product-action">
                                            <a class="btn btn-outline-dark btn-square" href="#"  wire:click.prevent="removeWishList({{$item->model->id}})"><i class="fas fa-trash"></i></a>
                                            <a class="btn btn-outline-dark btn-square" href="#" wire:click.prevent="moveProductFormwishListToCart('{{$item->rowId}}')"><i class="fa fa-shopping-cart"></i></a>
                                        </div>
                                    </div>
                                    <div class="text-center py-4">
                                        <p><a class="h6 text-decoration-none" href="{{route('product_detail', $item->id)}}">{{$item->name}}</a></p>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            
                                            <h5>
                                                @if ($item->price_online == 0)
                                                    {{__('blog.call')}}
                                                @else
                                                    {{number_format($item->price_online)}} {{__('lang.lak')}}
                                                @endif
                                            </h5>
                                            <!--<h5>$123.00</h5><h6 class="text-muted ml-2"><del>$123.00</del></h6>-->
                                        </div>
                                        <div class="d-flex align-items-center justify-content-center mb-1">
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <a href="#" wire:click.prevent="deleteallwishlist" class="btn btn-block btn-primary font-weight-bold my-3 py-3">{{__('lang.delete_all_cart')}}</a>
                    </div>
                    
            </div>
            
        </div>
        <!-- Breadcrumb End -->
 @endif