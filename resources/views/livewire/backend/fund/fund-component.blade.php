<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.fund_money')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.fund_money')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- show all data -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                @if(Session::has('no_message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if($this->show_detail_status == 'true')
                                <h3 class="text-left text-primary"><i class="fas fa-user"></i> {{$this->fullname}}</h3>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">{{__('lang.select')}}</th>
                                            <th style="text-align: center">{{__('lang.no')}}</th>
                                            <th style="text-align: center">{{__('lang.fund_money')}}</th>
                                            <th style="text-align: center">{{__('lang.status')}}</th>
                                            <th>{{__('lang.content')}}</th>
                                            <th style="text-align: center">{{__('lang.created_at')}}</th>
                                            <th>{{__('lang.created_at')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $stt = 1;
                                        $show_total = 0;
                                        @endphp

                                        @foreach($this->DataDetail as $item)
                                        @php
                                        $show_total += $item->amount;
                                        @endphp
                                        <tr>
                                            <td style="text-align: center;width:100px;"><input type="radio" value="{{$item->id}}" wire:model="selectOne" wire:click="showEdit({{$item->id}})"></td>
                                            <td width=5% style="text-align: center">{{$stt++}}</td>
                                            <td class="text-center">
                                                @if($this->selectOne == $item->id)
                                                @if(!empty($this->slug)) <span class="text-success"><b>{{$this->slug}}</b></span> <br> @endif
                                                <div class="d-flex justify-content-space-between">
                                                    <input type="text" class="form-control" wire:model="fund_amount" wire:keyup="Autoformat">
                                                    <a href="#" class="btn btn-primary btn-sm" wire:click="edit"><i class="fas fa-edit"></i></a>
                                                </div>
                                                @error('fund_amount')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                                @else
                                                {{number_format($item->amount)}}{{__('lang.lak')}}
                                                @endif
                                            </td>
                                            <td style="text-align: center">
                                                @if($item->status ==1)
                                                <span class="badge badge-success">{{__('lang.fund_money')}}</span>
                                                @else
                                                <span class="badge badge-danger">{{__('lang.interest')}}</span>
                                                @endif
                                            </td>
                                            <td>{{$item->note}}</td>
                                            <td class="text-center">
                                                {{date('d/m/Y', strtotime($item->date))}}
                                            </td>
                                            <td>
                                                @if(!empty($item->user->name))
                                                {{$item->user->name}}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="2" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                            <td class="text-center">
                                                <b><u>{{number_format($show_total)}}{{__('lang.lak')}}</u></b>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a wire:click='back()' class="btn btn-warning mt-2">{{__('lang.back')}}</a>
                                @elseif($this->createData == 'true')
                                <form wire:submit.prevent='create'>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group" wire:ignore>
                                                    <label>{{__('lang.employee')}}</label>
                                                    <select class="form-control @error('emp_id') is-invalid @enderror" wire:model='emp_id' id="employee" style="width: 100%;">
                                                        <option value="">{{__('lang.select')}}</option>
                                                        @forelse($employees as $item)
                                                        <option value="{{$item->id}}">{{$item->firstname}} -
                                                            {{$item->lastname}}
                                                        </option>
                                                        @empty
                                                        <h1>Record not found</h1>
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>{{__('lang.qty_money')}}
                                                        @if(!empty($this->slug)) <span class="text-success">{{$this->slug}}</span> @endif</label>
                                                    <input type="text" class="form-control" wire:keyup="Autoformat" wire:model='fund_amount' placeholder="{{__('lang.fund_money')}}">
                                                    @error('fund_amount')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="note">{{__('lang.date')}}</label>
                                                    <input type="date" class="form-control @error('date') is-invalid @enderror" wire:model='date' placeholder="{{__('lang.date')}}">
                                                    <span class="text-danger"> @error('date'){{$message}} @enderror
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label for="note">{{__('lang.content')}}</label>
                                                    <input type="text" class="form-control" wire:model='note' placeholder="{{__('lang.content')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                        <a class="btn btn-warning" wire:click="back()">{{__('lang.back')}}</a>
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#employee').select2();
                                        $('#employee').on('change', function(e) {
                                            var data = $('#employee').select2("val");
                                            @this.set('emp_id', data);
                                        });
                                    });
                                </script>
                                @elseif($this->withraw == 'true')
                                <form>
                                    <div class="card-body">
                                        <div class="row text-center">
                                            <div class="col-md-12">
                                                <h1 class="text-success"><b>{{__('blog.amount_remaining')}}
                                                        {{number_format($this->total)}} {{__('lang.lak')}}</b></h1>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            <h3>{{__('lang.please_select_package')}}</h3>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-md-2">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="d-flex justify-space-between text-center align-items-center">
                                                            <input type="radio" wire:model="pay_status" class="form-control" value="1">
                                                            <h3 style="white-space:nowrap;" class="text-success">
                                                                <b>{{__('lang.cash')}}</b>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="d-flex justify-space-between text-center align-items-center">
                                                            <input type="radio" wire:model="pay_status" class="form-control" value="2">
                                                            <h3 style="white-space:nowrap;" class="text-danger">
                                                                <b>{{__('lang.transfer_money')}}</b>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center mt-2">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="" class="text-success">{{number_format($this->another_amount)}}</label>
                                                            <input type="number" class="form-control" wire:model='another_amount' placeholder="ຈໍານວນເງິນ" min="10000" wire:keyup="AutoformatAmount" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center mt-1">
                                            <div class="col-md-4 text-center">
                                                @if($this->bcel_image)
                                                @php
                                                try {
                                                $url = $bcel_image->temporaryUrl();
                                                $status = true;
                                                }catch (RuntimeException $exception){
                                                $this->status = false;
                                                }
                                                @endphp
                                                @if($status)
                                                <img src="{{ $url }}" alt="" class="img-thumbnail" style="border: 5px solid green;">
                                                @endif
                                                <i class="fas fa-check-circle" style="color:green;font-size:20px;padding:10px;"></i>
                                                @endif
                                            </div>
                                        </div>
                                        @if($this->pay_status == 2)
                                        <div class="row justify-content-center mt-2">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">{{__('lang.choose_image')}}</label>
                                                            <input type="file" class="form-control @error('bcel_image') is-invalid @enderror" wire:model='bcel_image'>
                                                            <span class="text-danger"> @error('bcel_image'){{$message}}
                                                                @enderror
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if(!empty($this->amount))
                                        <div class="row justify-content-center mt-2">
                                            <h1 class="text-success"><b>{{number_format($this->amount)}}</b>
                                            </h1>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="row justify-content-center mt-2">
                                        <a class="btn btn-warning" wire:click="back()">{{__('lang.back')}}</a>
                                        <button type="button" class="btn btn-primary" wire:click="withrawFund">{{__('lang.save')}}</button>
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#employee').select2();
                                        $('#employee').on('change', function(e) {
                                            var data = $('#employee').select2("val");
                                            @this.set('emp_id', data);
                                        });
                                    });
                                </script>
                                @else
                                <div class="row">
                                    <div class="col-md-10 text-left">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <button wire:click='showFrom' class="btn btn-success mb-2 mt-2"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                    {{__('lang.add')}}</button>
                                                <a href="{{ route('admin.calculateinterest') }}" class="btn btn-warning mb-2 mt-2"><i class="fa fa-calculator" aria-hidden="true"></i>
                                                    {{__('lang.calculate_interest')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <a href="#" onclick="printDiv('printOrder')" class="btn btn-primary btn-sm w-100 mt-2 p-2"><i class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">{{__('lang.no')}}</th>
                                                <th>{{__('lang.payment_image')}}</th>
                                                <th>{{__('lang.first_and_lastname')}}</th>
                                                <th style="text-align: center">{{__('lang.sex')}}</th>
                                                <th style="text-align: center">
                                                    {{__('lang.totals')}}{{__('lang.fund_money')}}
                                                </th>
                                                <th style="text-align: center">
                                                    {{__('lang.totals')}}{{__('lang.withrawed_total')}}
                                                </th>
                                                <th style="text-align: center">
                                                    {{__('lang.remaining_fund')}}
                                                </th>
                                                <th style="text-align: center">
                                                    {{__('lang.status')}}
                                                </th>
                                                <th style="text-align: center">
                                                    {{__('lang.creator')}}
                                                </th>
                                                <th style="text-align: center">{{__('lang.action')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $stt = 1;
                                            $fund_total = 0;
                                            $remain_total = 0;
                                            $withraw_total = 0;
                                            @endphp

                                            @foreach($data as $item)
                                            @php
                                            $status = DB::table('fund_details')->where('fund_id', $item->id)->first();
                                            $fund_total += $item->fund_total;
                                            $withraw_total += $item->withrawed_total;
                                            $remain_total += $item->remain_total;
                                            @endphp
                                            <tr>
                                                <td width=5% style="text-align: center">{{$stt++}}</td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->bcel_image))
                                                    <a href="{{asset($item->bcel_image)}}">
                                                        <img src="{{asset($item->bcel_image)}}" alt="" style="width:50px;height:50px;">
                                                    </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($item->employee->firstname))
                                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->employee->sex))
                                                    @if($item->employee->sex == 1)
                                                    <button class="btn btn-primary btn-sm">{{__('lang.man')}}</button>
                                                    @else
                                                    <button class="btn btn-success btn-sm">{{__('lang.woman')}}</button>
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" wire:click="showdetail({{$item->id}})"><u>{{number_format($item->fund_total)}}</u></a>
                                                </td>
                                                <td class="text-center text-danger">
                                                    {{number_format($item->withrawed_total)}}
                                                </td>
                                                <td class="text-center">
                                                    @if($item->remain_total > 0)
                                                    <button class="btn btn-success btn-sm w-100">{{number_format($item->remain_total)}}
                                                    </button>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($item->withrawed_total > 0)
                                                    <span class="badge badge-danger">{{__('lang.withrawed')}}</span>
                                                    @elseif($status && $status->status ==1)
                                                    <span class="badge badge-success">{{__('lang.fund_money')}}</span>
                                                    @elseif($status && $status->status ==2)
                                                    <span class="badge badge-warning">{{__('lang.interest')}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if(!empty($item->user->name))
                                                    {{$item->user->name}}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($item->withrawed_total <= 0) <a href="#" class="btn btn-primary btn-sm" wire:click="showWithraw({{$item->id}})"><i class="fa fa-sign-in-alt"></i></a>
                                                        @endif
                                                        <!-- <a href="#" class="btn btn-default btn-sm"><i
                                                        class="fas fa-eye"></i></a> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                                <td class="text-center">
                                                    <b><u>{{number_format($fund_total)}}{{__('lang.lak')}}</u></b>
                                                </td>
                                                <td class="text-center">
                                                    <b><u>{{number_format($withraw_total)}}{{__('lang.lak')}}</u></b>
                                                </td>
                                                <td class="text-center">
                                                    <b><u>{{number_format($remain_total)}}{{__('lang.lak')}}</u></b>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- </div> -->
                                <style type="text/css" media="print">
                                    @page {
                                        size: auto;
                                        /* auto is the initial value */
                                        margin: 0;
                                        /* this affects the margin in the printer settings */
                                    }
                                </style>
                                <script>
                                    function printDiv(divName) {
                                        var printContents = document.getElementById(divName).innerHTML;
                                        var originalContents = document.body.innerHTML;
                                        document.body.innerHTML = printContents;
                                        window.print();
                                        document.body.innerHTML = originalContents;

                                    }
                                </script>
                                <div class="container" id='printOrder' style="display:none;">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <p>{!! $branchs->bill_header !!}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 text-left">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;" src="{{asset($branchs->logo)}}">
                                            <p>
                                                <b>
                                                    @if (Config::get('app.locale') == 'lo')
                                                    {{$branchs->name_la}}
                                                    @elseif(Config::get('app.locale') == 'en')
                                                    {{$branchs->name_en}}
                                                    @endif
                                                </b>
                                            </p>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <br>
                                            <h5><b>{{__('lang.calculate_salary')}}</b></h5>
                                            <h6><b>
                                                    @if(!empty($this->month))
                                                    {{__('lang.monthly')}} {{$this->month}}/ {{$this->year}}
                                                    @endif
                                                </b>
                                            </h6>
                                            <h6>@if(!empty($this->fromdate) && !empty($this->todate))
                                                {{__('lang.fromdate')}} {{date('d/m/Y', strtotime($this->fromdate)) }} -
                                                {{date('d/m/Y', strtotime($this->todate)) }}
                                                @endif
                                            </h6>
                                            @if((!empty($this->fromdate) && !empty($this->todate) &&
                                            !empty($this->emp_id)) ||
                                            (!empty($this->month) && !empty($this->emp_id)))
                                            <h6>
                                                <div class="text-center">
                                                    <h6><b>{{__('lang.first_and_lastname')}}: {{$this->fullname}}</b>
                                                    </h6>
                                                </div>
                                            </h6>
                                            @endif
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <br><br>
                                            @php
                                            $date=date('d/m/Y H:i:s');
                                            @endphp
                                            <p>{{__('lang.tran_no')}}: ........................</p>
                                            <p>{{__('lang.date')}}: {{$date}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('lang.no')}}</th>
                                                            <th>{{__('lang.first_and_lastname')}}</th>
                                                            <th>{{__('lang.sex')}}</th>
                                                            <th>
                                                                {{__('lang.totals')}}{{__('lang.fund_money')}}
                                                            </th>
                                                            <th>
                                                                {{__('lang.totals')}}{{__('lang.withrawed_total')}}
                                                            </th>
                                                            <th>
                                                                {{__('lang.remaining_fund')}}
                                                            </th>
                                                            <th>
                                                                {{__('lang.creator')}}
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $stt = 1;
                                                        $fund_total = 0;
                                                        $remain_total = 0;
                                                        $withraw_total = 0;
                                                        @endphp

                                                        @foreach($data as $item)
                                                        @php
                                                        $fund_total += $item->fund_total;
                                                        $withraw_total += $item->withrawed_total;
                                                        $remain_total += $item->remain_total;
                                                        @endphp
                                                        <tr>
                                                            <td>{{$stt++}}</td>
                                                            <td>
                                                                @if(!empty($item->employee->firstname))
                                                                {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(!empty($item->employee->sex))
                                                                @if($item->employee->sex == 1)
                                                                {{__('lang.man')}}
                                                                @else
                                                                {{__('lang.woman')}}
                                                                @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{number_format($item->fund_total)}}{{__('lang.lak')}}
                                                            </td>
                                                            <td>
                                                                {{number_format($item->withrawed_total)}}
                                                            </td>
                                                            <td>
                                                                {{number_format($item->remain_total)}}
                                                            </td>
                                                        </tr>
                                                        <td>
                                                            @if(!empty($item->user->name))
                                                            {{$item->user->name}}
                                                            @endif
                                                        </td>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="3" class="text-center">
                                                                <b>{{__('lang.totals')}}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b><u>{{number_format($fund_total)}}{{__('lang.lak')}}</u></b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b><u>{{number_format($withraw_total)}}{{__('lang.lak')}}</u></b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b><u>{{number_format($remain_total)}}{{__('lang.lak')}}</u></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!empty($branchs->bill_footer))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            <p><b>{{$branchs->director_sign}}</b></p>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <p><b>{{$branchs->chechker_sign}}</b></p>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <p><b>{{$branchs->staff_sign}}</b></p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>