<div class="content">
    <div class="container-fluid">
        <div class="row justify-content-center text-center">
            <div class="col-md-3 mt-2">
                <a href="/loan-employee" class="btn btn-warning btn-sm">{{__('lang.back')}}</a>
            </div>
            <div class="col-md-3 mt-2">
                <a href="#" onclick="printDiv('printOrder')" class="btn btn-success btn-sm"><i
                        class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
            </div>
        </div>
        <!-- </div> -->
        <style type="text/css" media="print">
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
        </style>
        <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
        </script>
        <div class="container" id='printOrder'>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>{!! $branchs->bill_header !!}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;"
                        src="{{asset($branchs->logo)}}">
                    <p>
                        <b>
                            @if (Config::get('app.locale') == 'lo')
                            {{$branchs->name_la}}
                            @elseif(Config::get('app.locale') == 'en')
                            {{$branchs->name_en}}
                            @endif
                        </b>
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <br>
                    <h5><b>{{__('lang.loan')}}</b></h5>
                </div>
                <div class="col-md-4 text-right">
                    <br><br>
                    @php
                    $date=date('d/m/Y H:i:s');
                    @endphp
                    <p>{{__('lang.tran_no')}}: ........................</p>
                    <p>{{__('lang.date')}}: {{$date}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <p>{{$this->fullname}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="text-align: center">
                            <thead>
                                <tr style="text-align: center">
                                    <th>{{__('lang.no')}}</th>
                                    <th>{{__('lang.amount')}}</th>
                                    <th>{{__('lang.interest')}}</th>
                                    <th>{{__('lang.total')}}</th>
                                    <th>{{__('lang.status')}}</th>
                                    <th>{{__('lang.date_payonbcelone')}}</th>
                                    <th>{{__('lang.creator')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $i = 1;
                                @endphp
                                @foreach($data as $item)
                                <tr style="text-align:center;">
                                    <td>{{$i++}}</td>
                                    <td>
                                        {{number_format($item->amount)}}
                                    </td>
                                    <td>
                                        {{number_format($item->profit)}}
                                    </td>
                                    <td>
                                        {{number_format($item->total)}}
                                    </td>
                                    <td>
                                        @if($item->status ==1)
                                        <b>{{__('lang.yes_pay')}}</b>
                                        @else
                                        {{__('lang.no_pay')}}
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($item->date))
                                        {{date('d/m/Y', strtotime($item->date)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($item->user->name))
                                        {{$item->user->name}}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(!empty($branchs->bill_footer))
            <div class="row">
                <div class="col-md-12">
                    <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-4 text-center">
                    <p><b>{{$branchs->director_sign}}</b></p>
                </div>
                <div class="col-md-4 text-center">
                    <p><b>{{$branchs->chechker_sign}}</b></p>
                </div>
                <div class="col-md-4 text-center">
                    <p><b>{{$branchs->staff_sign}}</b></p>
                </div>
            </div>
        </div>
    </div>
</div>