<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.loan')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.loan')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('no_message'))
    <div class="col-md-12 text-center mb-2">
        <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_message')}}</a>
    </div>
    @endif
    @if ($selectData == true)
    <!-- show all data -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-primary mb-2 mt-2"><i
                                        class="fa fa-calculator" aria-hidden="true"></i>
                                    {{__('lang.loan')}}</button>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.fromdate')}}</label>
                                            <input wire:model="start_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.todate')}}</label>
                                            <input wire:model="end_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                </div>
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th>{{__('lang.no')}}</th>
                                            <th>{{__('lang.first_and_lastname')}}</th>
                                            <th>{{__('lang.sex')}}</th>
                                            <th>{{__('lang.loan')}}</th>
                                            <th>{{__('lang.number_installment')}}</th>
                                            <th>{{__('lang.percent')}}</th>
                                            <th>{{__('lang.type')}}</th>
                                            <th>{{__('lang.created_at')}}</th>
                                            <th>{{__('lang.creator')}}</th>
                                            @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                            Auth::user()->role_id ==3)
                                            <th>{{__('lang.action')}}</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach($data as $item)
                                        @php 
                                        $count_paid = DB::table('loan_details')->where('loan_id', $item->id)->where('status', 1)->count();
                                        @endphp
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>
                                                @if(!empty($item->employee->firstname))
                                                {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($item->employee->sex))
                                                @if($item->employee->sex == 1)
                                                {{__('lang.man')}}
                                                @else
                                                {{__('lang.woman')}}
                                                @endif
                                                @endif
                                            </td>
                                            <td style="text-align:center;">
                                                <a href="#"
                                                    wire:click="showDetail({{$item->id}})"><u>{{number_format($item->amount)}}</u></a>
                                            </td>
                                            <td style="text-align:center;">
                                               @if($count_paid > 0)<span class="text-success">{{$count_paid}}</span>/@endif{{number_format($item->qty_pay)}}
                                            </td>
                                            <td style="text-align:center;">
                                              @if(!empty($item->percent))
                                                {{$item->percent}}%
                                              @endif
                                            </td>
                                            <td style="text-align:center;">
                                                {{$item->type}}
                                            </td>
                                            <td style="text-align: center">
                                                {{date('d/m/Y H:i:s', strtotime($item->created_at)) }}
                                            </td>
                                            <td style="text-align: center">
                                                @if(!empty($item->user->name))
                                                {{$item->user->name}}
                                                @endif
                                            </td>
                                            @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                            Auth::user()->role_id ==3)
                                            <td style="text-align: center">
                                                <button wire:click='delete({{$item->id}})'
                                                    onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()"
                                                    class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @endif

    @if ($createData == true)
    <!-- form create by ajck -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="alert alert-success text-center">
                    {{__('lang.add')}}{{__('lang.success')}}
                </div>
                @endif
                <div class="col-lg-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.loan')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form wire:submit.prevent='create'>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-success"><b>{{__('lang.capital_balance')}}
                                                {{number_format($this->total)}} {{__('lang.lak')}}</b></h3>
                                    </div>
                                </div>
                                @if(!empty($this->show_percent_loan))
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-primary"><b>{{__('lang.loan')}}:
                                                {{$this->show_percent_loan}} %</b></h5>
                                    </div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.employee')}}</label>
                                            <select class="form-control @error('emp_id') is-invalid @enderror"
                                                wire:model='emp_id' id="employee" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($employees as $item)
                                                <option value="{{$item->id}}">{{$item->firstname}} -
                                                    {{$item->lastname}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{__('lang.qty_money')}}
                                                @if(!empty($this->slug)) <span
                                                    class="text-success">{{$this->slug}}</span> @endif</label>
                                            <input type="text" class="form-control" wire:keyup="AutoformatAmount"
                                                wire:model='amount' placeholder="{{__('lang.qty_money')}}">
                                            @error('amount')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.number_installment')}}</label>
                                            <input type="number"
                                                class="form-control @error('qty_pay') is-invalid @enderror"
                                                wire:model='qty_pay' placeholder="{{__('lang.number_installment')}}">
                                            <span class="text-danger"> @error('qty_pay'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.period')}}</label>
                                            <select wire:model="period" id="" class="form-control">
                                                <option value="month">{{__('lang.month')}}</option>
                                                <option value="year">{{__('lang.year')}}</option>
                                            </select>
                                            <span class="text-danger"> @error('qty_pay'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select wire:model="type" id="" class="form-control">
                                                <option value="ຕາຍຕົວ">ຕາຍຕົວ</option>
                                                <option value="ຫຼຸດຕາມທຶນ">ຫຼຸດຕາມທຶນ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                <a class="btn btn-warning" wire:click="back()">{{__('lang.back')}}</a>
                            </div>
                        </form>
                        <script>
                        $(document).ready(function() {
                            $('#employee').select2();
                            $('#employee').on('change', function(e) {
                                var data = $('#employee').select2("val");
                                @this.set('emp_id', data);
                            });
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- </div> -->
    @if($this->showDetail == true)
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-success"><u>{{$this->fullname}}</u></h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead class="bg-info">
                    <tr style="text-align: center">
                        <th>{{__('lang.select')}}</th>
                        <th>{{__('lang.no')}}</th>
                        <th>{{__('lang.qty_money')}}</th>
                        <th>{{__('lang.interest')}}</th>
                        <th>{{__('lang.total')}}</th>
                        <th>{{__('lang.date_payonbcelone')}}</th>
                        <th>{{__('lang.creator')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach($this->showdata as $item)
                    @if($item->status ==1)
                    <tr style="text-align:center;" class="text-success">
                        <td>
                           <i class="fa fa-check-circle text-success"></i>
                        </td>
                        <td>{{$i++}}</td>
                        <td>
                            {{number_format($item->amount)}}
                        </td>
                        <td>
                            {{number_format($item->profit)}}
                        </td>
                        <td>
                            {{number_format($item->total)}}
                        </td>
                        <td>
                            @if(!empty($item->date))
                            {{date('d/m/Y', strtotime($item->date)) }}
                            @endif
                        </td>
                        <td>
                            @if(!empty($item->user->name))
                            {{$item->user->name}}
                            @endif
                        </td>
                    </tr>
                    @else
                    <tr style="text-align:center;" class="text-danger">
                        <td>
                            <input type="radio" wire:model="selectOne" value="{{$item->id}}">
                            @if($this->selectOne == $item->id)
                               <a  href="#"  wire:click="Payloan" class="btn btn-success">{{__('lang.payment')}}</a>
                            @endif
                        </td>
                        <td>{{$i++}}</td>
                        <td>
                            {{number_format($item->amount)}}
                        </td>
                        <td>
                            {{number_format($item->profit)}}
                        </td>
                        <td>
                            {{number_format($item->total)}}
                        </td>
                        <td>
                            @if(!empty($item->date))
                            {{date('d/m/Y', strtotime($item->date)) }}
                            @endif
                        </td>
                        <td>
                            @if(!empty($item->user->name))
                            {{$item->user->name}}
                            @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            <div class="row mt-2">
                <div class="col-md-3">
                    <a class="btn btn-warning" wire:click="back()">{{__('lang.back')}}</a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>