<div>
<style type="text/css" media="print">
                  @page {
                      size: auto;   /* auto is the initial value */
                      margin: 0;  /* this affects the margin in the printer settings */
                  }
                  </style>
                  <script>
                      function printDiv(divName){
                        var printContents = document.getElementById(divName).innerHTML;
                        var originalContents = document.body.innerHTML;
                        document.body.innerHTML = printContents;
                        window.print();
                        document.body.innerHTML = originalContents;
                          }
                    </script>
                  <style>
                      .header_print{
                          display:flex;
                          justify-content: space-between;
                          align-items: center;
                          padding:20px;
                      }
                                .header_bill{
                                      display:flex;
                                      justify-content: space-between;
                                      align-items: center;
                                      padding:10px;
                                  }
                                  table,tr,th,td{
                                    border-collapse: collapse; border: 1px solid;
                                  }
                  </style>
                  <div class="container-fluid" id='printOrder' style="">
                      <div class="card">
                          <div class="card-body">
                          <ul class="nav">
                              <li class="nav-item">
                                  <img height="50" src="{{asset('images/logo.png')}}" class="rounded-circle" alt="..."> ບໍລິສັດ CIT ຈຳກັດຜູ້ດຽວ
                              </li>
                          </ul>
                            <center><h3 style="font-weight:bold">ໃບບິນ</h3></center>
                              <style>
                                  .header_bill{
                                      display:flex;
                                      justify-content: space-between;
                                      align-items: center;
                                      padding:10px;
                                  }
                              </style>
                              @php
                              $date=date('d/m/Y H:i:s');
                              @endphp
                              <div class="header_bill">
                                  <p style="font-weight:bold"></p>
                                  <p style="font-weight:bold">  {{__('lang.date')}} : {{$date}}</p>
                              </div>
                          </div>
                          <div class="text-center">
                              <h3>{{__('lang.name')}}-{{__('lang.lastname')}}:{{$this->fullname}}</h3></br>
                              <h4>{{__('lang.amount_finded')}}:{{number_format($this->commission)}}</h4>
                          </div>
                            <style>
                                  .print_footer{
                                          display:flex;
                                          justify-content: space-between;
                                          align-items: center;
                                          height:150px;
                                          padding: 50px;
                                          
                                      }
                             </style>
                             <div class="text-center">
                                    <table style="padding: 30px;">
                                      <thead>
                                        <tr >
                                                <th>{{__('lang.no')}}</th>
                                                <th>{{__('lang.des')}}</th>
                                                <th>{{__('lang.amount_finded')}}</th>
                                                <th>{{__('lang.percent')}}</th>
                                                <th>{{__('lang.total')}}</th>
                                                <th>{{__('lang.note')}}</th>
                                                <th>{{__('lang.created_at')}}</th>
                                                <th>{{__('lang.creator')}}</th>
                                                <th>{{__('lang.action')}}</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                              @php
                                                $stt = 1; 
                                              @endphp
                    
                                              @foreach($printcommissions as $item)
                                              <tr>
                                                <td width=5% style="text-align: center">{{$stt++}}</td>
                                                <td>
                                                  @if(!empty($item->employee->firstname))
                                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                  @endif
                                                </td>
                                                <td style="text-align: center">
                                                @if(!empty($item->employee->sex))
                                                      @if($item->employee->sex == 1)
                                                        <button class="btn btn-primary btn-sm">{{__('lang.man')}}</button>
                                                      @else
                                                        <button class="btn btn-info btn-sm">{{__('lang.woman')}}</button>
                                                      @endif
                                                  @endif
                                                </td>
                                                <td>{{$item->des}}</td>
                                                <td>
                                                  @if(!empty($item->amount))
                                                  {{number_format($item->amount)}}{{__('lang.lak')}}
                                                  @endif
                                                </td>
                                                <td style="text-align: center">{{$item->percent}}%</td>
                                                <td>
                                                  @if(!empty($item->total))
                                                  {{number_format($item->total)}}{{__('lang.lak')}}
                                                  @endif
                                                </td>
                                                <td>{{$item->note}}</td>
                                                <td style="text-align: center">{{date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                          </tr>
                                          @endforeach
                                          <tr>
                                              <td colspan="9">
                                                <div class="d-flex align-items-center justify-content-between p-1">
                                                    <h5>
                                                        <b>{{__('lang.total_amount')}}:</b>
                                                    </h5>
                                                    <h5>
                                                        <b>@if(!empty($sum_counts))
                                                        {{number_format($sum_counts)}} {{__('lang.lak')}}
                                                        @endif</b>
                                                    </h5>
                                                </div>
                                                </td>
                                          </tr>
                                      </tbody>
                                    </table>
                             </div>
                            <div class="print_footer">
                                <p style="font-weight:bold">ຜູ້ຮັບເງິນ</p>
                                <p  style="font-weight:bold">ຜູ້ຈ່າຍເງິນ</p>
                            </div>
                      </div>
                  </div>
</div>
