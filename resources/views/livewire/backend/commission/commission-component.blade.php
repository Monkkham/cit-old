<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.divid_component')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.divid_component')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    @if ($selectData == true)
    <!-- show all datas -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="row mt-1 text-center">
                                @if(!empty($this->total_all_commission))
                                <div class="col-md-6">
                                    <h6 class="bg-warning p-2">
                                        <b>
                                            {{__('lang.divid_name')}}{{__('lang.grandtotal')}}
                                            @if(!empty($this->fullname))({{$this->fullname}})@endif:
                                            {{number_format($this->total_all_commission)}}
                                        </b>
                                    </h6>
                                </div>
                                @endif
                                @if(!empty($this->amount_all_commission))
                                <div class="col-md-6">
                                    <h6 class="bg-success p-2">
                                        <b>
                                            {{__('lang.amount_finded')}}{{__('lang.grandtotal')}}:
                                            {{number_format($this->amount_all_commission)}}
                                        </b>
                                    </h6>
                                </div>
                                @endif
                            </div>
                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{__('lang.add')}}</button>
                                <button onclick="ExportExcel('xlsx')" class="btn btn-success"><i class="fas fa-print"></i>&nbsp;Excel</button>
                                <a href="#" onclick="printDiv('printOrder')" class="btn btn-info"><i class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                            </div>
                            <div class="card-body">
                                <div class="row mt-2">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.fromdate')}}</label>
                                            <input wire:model="fromdate" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.todate')}}</label>
                                            <input wire:model="todate" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>{{__('lang.month')}}</label>
                                            <select class="form-control" id="selectMonth" wire:model="month" style="width: 100%;">
                                                <option value="">{{__('lang.month')}}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>{{__('lang.year')}}</label>
                                            <select class="form-control" wire:model="year" style="width: 100%;">
                                                <option value="">{{__('lang.year')}}</option>
                                                {{$year = date('yyyy')}}
                                                @for ($year = 2019; $year <=2050; $year++) <option value="{{$year}}">
                                                    {{$year}}</option>
                                                    @endfor
                                            </select>
                                            @error('year')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.employee')}}-{{__('lang.lastname')}}</label>
                                            <select class="form-control" id="selectEmployee" wire:model="select_emp_id" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @foreach ($employees as $item)
                                                <option value="{{$item->id}}">
                                                    {{$item->firstname}}-{{$item->lastname}}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('user_id') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">{{__('lang.show')}}</label>
                                            <select wire:model="select_page" class="form-control">
                                                <option value="10000000">ທັງໝົດ</option>
                                                <option value="10">10</option>
                                                <option value="50">30</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.search')}}</label>
                                            <input wire:model="search" class='form-control' placeholder="ຄົ້ນຫາຂໍ້ມູນ..." type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table  class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th>{{__('lang.no')}}</th>
                                                <th>{{__('lang.first_and_lastname')}}</th>
                                                <!-- <th>{{__('lang.sex')}}</th> -->
                                                <th>{{__('lang.customer')}}</th>
                                                <th>{{__('lang.des')}}</th>
                                                <th>{{__('lang.amount_finded')}}</th>
                                                <th>{{__('lang.percent')}}</th>
                                                <th>{{__('lang.divid_name')}}</th>
                                                <th>{{__('lang.note')}}</th>
                                                <th>{{__('lang.date')}}</th>
                                                <th>{{__('lang.creator')}}</th>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <th>{{__('lang.action')}}</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $stt = 1;
                                            @endphp

                                            @foreach($commissions as $item)
                                            <tr>
                                                <td width=5% style="text-align: center">{{$page++}}</td>
                                                <td>
                                                    @if(!empty($item->employee->firstname))
                                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                    @endif
                                                </td>
                                                <!-- <td style="text-align: center">
                                      @if(!empty($item->employee->sex))
                                            @if($item->employee->sex == 1)
                                              <button class="btn btn-primary btn-sm">{{__('lang.man')}}</button>
                                            @else
                                              <button class="btn btn-info btn-sm">{{__('lang.woman')}}</button>
                                            @endif
                                        @endif
                                      </td> -->
                                      <td>
                                        @if($item->Customers)
                                        @if(!empty($item->Customers))
                                        {{$item->Customers->name_la}}
                                        @endif
                                            @else
                                            {{$item->customer}}
                                        @endif
                                    </td>
                                                <td>{{$item->des}}</td>
                                                <td>
                                                    @if(!empty($item->amount))
                                                    {{number_format($item->amount)}}
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->percent))
                                                    {{$item->percent}}%
                                                </td>
                                                @endif
                                                <td>
                                                    @if(!empty($item->total))
                                                    {{number_format($item->total)}}
                                                    @endif
                                                </td>
                                                <td>{{$item->note}}</td>
                                                <td style="text-align: center">{{date('d/m/Y', strtotime($item->date)) }}
                                                </td>
                                                <td>
                                                    @if(!empty($item->user->name))
                                                    {{$item->user->name}}
                                                    @endif
                                                </td>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <td style="text-align: center">
                                                    <button wire:click='delete({{$item->id}})' onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                    <button wire:click='edit({{$item->id}})' class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></button>
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                                <td>
                                                    <b><u>{{number_format($this->amount_all_commission)}}</u></b>
                                                </td>
                                                <td></td>
                                                <td><b><u>{{number_format($this->total_all_commission)}}</u></b></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="float-right">
                                    {{$commissions->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#selectEmployee').select2();
                    $('#selectEmployee').on('change', function(e) {
                        var data = $('#selectEmployee').select2("val");
                        @this.set('select_emp_id', data);
                    });
                });
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                    location.reload();
                }

                function ExportExcel(type, fn, dl) {
                    var elt = document.getElementById('table-excel');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "Sheet JS"
                    });
                    return dl ?
                        XLSX.write(wb, {
                            bookType: type,
                            bookSST: true,
                            type: 'base64'
                        }) :
                        XLSX.writeFile(wb, fn || ('ຂໍ້ມູນເງິນຄອມມິດຊັ່ນ.' + (type || 'xlsx')));
                }
            </script>
            @endpush
            <div class="container" id='printOrder' style="display:none;">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>{!! $branchs->bill_header !!}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;" src="{{asset($branchs->logo)}}">
                        <p>
                            <b>
                                @if (Config::get('app.locale') == 'lo')
                                {{$branchs->name_la}}
                                @elseif(Config::get('app.locale') == 'en')
                                {{$branchs->name_en}}
                                @endif
                            </b>
                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <br>
                        <h5><b>{{__('lang.follow_commission')}}</b></h5>
                        <h6><b>
                                @if(!empty($this->month))
                                {{__('lang.monthly')}} {{$this->month}}/ {{$this->year}}
                                @endif</b></h6>
                        <h6><b> @if(!empty($this->fromdate) && !empty($this->todate))
                                {{__('lang.fromdate')}} {{date('d/m/Y', strtotime($this->fromdate)) }} -
                                {{date('d/m/Y', strtotime($this->todate)) }}
                                @endif</b></h6>
                        @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                        (!empty($this->month)
                        && !empty($this->emp_id)))
                        <h6>
                            <div class="text-center">
                                <h6><b>{{__('lang.first_and_lastname')}}: {{$this->fullname}}</b></h6>
                            </div>
                        </h6>
                        @endif
                    </div>
                    <div class="col-md-4 text-right">
                        <br><br>
                        @php
                        $date=date('d/m/Y H:i:s');
                        @endphp
                        <p>{{__('lang.tran_no')}}: ........................</p>
                        <p>{{__('lang.date')}}: {{$date}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                        (!empty($this->month)
                        &&
                        !empty($this->emp_id)))
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table-excel" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th>{{__('lang.no')}}</th>
                                        <th>{{__('lang.des')}}</th>
                                        <th>{{__('lang.amount_finded')}}</th>
                                        <th>{{__('lang.percent')}}</th>
                                        <th>{{__('lang.divid_name')}}</th>
                                        <th>{{__('lang.created_at')}}</th>
                                        <th>{{__('lang.creator')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $stt = 1;
                                    @endphp

                                    @foreach($commissions as $item)
                                    <tr>
                                        <td width=5% style="text-align: center">{{$stt++}}</td>
                                        <td>
                                            {{$item->des}}
                                        <td style="text-align: center">
                                            @if(!empty($item->amount))
                                            {{number_format($item->amount)}}
                                            @endif
                                        </td>
                                        <td style="text-align: center" style="text-align: center">@if($item->percent
                                            >
                                            0) {{$item->percent}}%@endif</td>
                                        <td style="text-align: center">
                                            @if(!empty($item->total))
                                            {{number_format($item->total)}}
                                            @endif
                                        </td>
                                        <td style="text-align: center">{{date('d/m/Y H:i:s', strtotime($item->created_at)) }}
                                        </td>
                                        <td>
                                            @if(!empty($item->user->name))
                                            {{$item->user->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                        <td>
                                            <b><u>{{number_format($this->amount_all_commission)}}</u></b>
                                        </td>
                                        <td></td>
                                        <td><b><u>{{number_format($this->total_all_commission)}}</u></b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table-excel" style="text-align: center">
                                <thead>
                                    <tr style="text-align: center">
                                        <th>{{__('lang.no')}}</th>
                                        <th>{{__('lang.first_and_lastname')}}</th>
                                        <th>{{__('lang.sex')}}</th>
                                        <th>{{__('lang.customername')}}</th>
                                        <th>{{__('lang.des')}}</th>
                                        <th>{{__('lang.amount_finded')}}</th>
                                        <th>{{__('lang.percent')}}</th>
                                        <th>{{__('lang.divid_name')}}</th>
                                        <th>{{__('lang.note')}}</th>
                                        <th>{{__('lang.created_at')}}</th>
                                        <th>{{__('lang.creator')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $stt = 1;
                                    @endphp

                                    @foreach($commissions as $item)
                                    <tr>
                                        <td width=5% style="text-align: center">{{$stt++}}</td>
                                        <td>
                                            @if(!empty($item->employee->firstname))
                                            {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                            @endif
                                        </td>
                                        <td style="text-align: center">
                                            @if(!empty($item->employee->sex))
                                            @if($item->employee->sex == 1)
                                            <b>{{__('lang.man')}}</b>
                                            @else
                                            <b>{{__('lang.woman')}}</b>
                                            @endif
                                            @endif
                                        </td>
                                        <td>
                                            {{-- @if($item->Customers)
                                            @if(!empty($item->Customers))
                                            {{$item->Customers->name_la}}
                                            @endif
                                                @else --}}
                                                {{$item->customer}}
                                            {{-- @endif --}}
                                        </td>
                                        <td>{{$item->des}}</td>
                                        <td>
                                            @if(!empty($item->amount))
                                            {{number_format($item->amount)}}
                                            @endif
                                        </td>
                                        <td style="text-align: center">@if($item->percent > 0)
                                            {{$item->percent}}%@endif
                                        </td>
                                        <td style="text-align: center">
                                            @if(!empty($item->total))
                                            {{number_format($item->total)}}
                                            @endif
                                        </td>
                                        <td>{{$item->note}}</td>
                                        <td style="text-align: center">
                                            {{date('d/m/Y H:i:s', strtotime($item->created_at)) }}
                                        </td>
                                        <td>
                                            @if(!empty($item->user->name))
                                            {{$item->user->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                        <td>
                                            <b><u>{{number_format($this->amount_all_commission)}}</u></b>
                                        </td>
                                        <td></td>
                                        <td><b><u>{{number_format($this->total_all_commission)}}</u></b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
                </div>
                @if(!empty($branchs->bill_footer))
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->director_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->chechker_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->staff_sign}}</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($createData == true)
    <!-- form create by ajck -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                <div class="col-lg-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.add')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form action="" wire:submit.prevent='create'>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.employee')}}</label>
                                            <select class="form-control @error('emp_id') is-invalid @enderror" wire:model='emp_id' id="employee" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($employees as $item)
                                                <option value="{{$item->id}}">{{$item->firstname}} - {{$item->lastname}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                            <span class="text-danger"> @error('emp_id'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.customer')}}</label>
                                            <select class="form-control @error('customer_id') is-invalid @enderror" wire:model='customer_id' id="customer" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($customers as $item)
                                                <option value="{{$item->id}}">{{$item->name_la}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                            <span class="text-danger"> @error('customer_id'){{$message}} @enderror </span>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.customer')}}</label>
                                            <input type="text" class="form-control" wire:model='customer' placeholder="{{__('lang.customer')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.des')}}</label>
                                            <input type="text" class="form-control" wire:model='des' placeholder="{{__('lang.des')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.amount_finded')}} <span class="text-success">{{$this->slug}}</span></label>
                                            <input type="number" wire:keyup="Autoformat" class="form-control money @error('amount') is-invalid @enderror" wire:model='amount' placeholder="{{__('lang.amount_finded')}}">
                                            <span class="text-danger"> @error('amount'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{__('lang.select')}}</label>
                                            <select class="form-control" wire:model="select_divid" id="selectDivid" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @if(!empty($this->amount))
                                                <option value="percent">{{__('lang.percent')}}</option>
                                                @endif
                                                <option value="fixed">{{__('lang.cash')}}</option>
                                            </select>
                                            <span class="text-danger"> @error('select_divid'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    @if(!empty($this->select_divid) && $this->select_divid == 'fixed')
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.divid_name')}} <span class="text-success">{{$this->cash_format}}</span></label>
                                            <input type="number" class="form-control" wire:keyup="AutoformatCash" wire:model='fixed' placeholder="{{__('lang.divid_name')}}">
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($this->select_divid) && $this->select_divid == 'percent')
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="percent">{{__('lang.percent')}}</label>
                                            <input type="number" class="form-control @error('percent') is-invalid @enderror" wire:model='percent' placeholder="{{__('lang.percent')}}">
                                            <span class="text-danger"> @error('percent'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.date')}}</label>
                                            <input type="date" class="form-control @error('date') is-invalid @enderror" wire:model='date' placeholder="{{__('lang.date')}}">
                                            <span class="text-danger"> @error('date'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.note')}}</label>
                                            <input type="text" class="form-control" wire:model='note' placeholder="{{__('lang.note')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                <a wire:click='back()' class="btn btn-warning">{{__('lang.back')}}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#employee').select2();
            $('#employee').on('change', function(e) {
                var data = $('#employee').select2("val");
                @this.set('emp_id', data);
            });
        });

        $(document).ready(function() {
            $('#customer').select2();
            $('#customer').on('change', function(e) {
                var data = $('#customer').select2("val");
                @this.set('customer_id', data);
            });
        });
    </script>
    @endif

    @if ($updateData == true)
    <div>
        <div class="content">
            <div class="container-fluid">
                <div class="col-lg-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.add')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form action="" wire:submit.prevent='update'>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.employee')}}</label>
                                            <select class="form-control select2" wire:model='ed_emp_id' id="ed_employee">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($employees as $item)
                                                <option value="{{$item->id}}">{{$item->firstname}} - {{$item->lastname}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                            <span class="text-danger"> @error('ed_emp_id'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.customer')}}</label>
                                            <select class="form-control @error('customer_id') is-invalid @enderror" wire:model='customer_id' id="customer" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($customers as $item)
                                                <option value="{{$item->id}}">{{$item->name_la}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                            <span class="text-danger"> @error('customer_id'){{$message}} @enderror </span>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.customer')}}</label>
                                            <input type="text" class="form-control" wire:model='customer' placeholder="{{__('lang.customer')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.des')}}</label>
                                            <input type="text" class="form-control" wire:model='ed_des' placeholder="{{__('lang.des')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.amount_finded')}} <span class="text-success">{{$this->ed_slug}}</span></label>
                                            <input type="number" wire:keyup="Edit_Autoformat" class="form-control @error('ed_amount') is-invalid @enderror" wire:model='ed_amount' placeholder="{{__('lang.amount_finded')}}">
                                            <span class="text-danger"> @error('ed_amount'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.select')}}</label>
                                            <select class="form-control" wire:model="ed_select_divid" id="ed_selectDivid" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @if(!empty($this->ed_amount))
                                                <option value="percent">{{__('lang.percent')}}</option>
                                                @endif
                                                <option value="fixed">{{__('lang.cash')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    @if(!empty($this->ed_select_divid) && $this->ed_select_divid == 'fixed')
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.divid_name')}} <span class="text-success">{{$this->ed_cash_format}}</label>
                                            <input type="number" class="form-control" wire:keyup="AutoformatCashEdit" wire:model='ed_fixed' placeholder="{{__('lang.divid_name')}}">
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($this->ed_select_divid) && $this->ed_select_divid == 'percent')
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.percent')}}</label>
                                            <input type="number" class="form-control @error('percent') is-invalid @enderror" wire:model='ed_percent' placeholder="{{__('lang.percent')}}">
                                            <span class="text-danger"> @error('percent'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.note')}}</label>
                                            <input type="text" class="form-control" wire:model='ed_note' placeholder="{{__('lang.note')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                <a wire:click='back()' class="btn btn-warning">{{__('lang.back')}}</a>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#ed_employee').select2();
            $('#ed_employee').on('change', function(e) {
                var data = $('#ed_employee').select2("val");
                @this.set('ed_emp_id', data);
            });
        });
    </script>
    @endif
</div>