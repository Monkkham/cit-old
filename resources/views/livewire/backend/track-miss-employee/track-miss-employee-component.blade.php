<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ຕິດຕາມວັນຂາດພະນັກງານ</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">ຕິດຕາມວັນຂາດພະນັກງານ</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    @if ($selectData == true)
    <!-- show all data -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                @if(Session::has('no_message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{__('lang.add')}}</button>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.fromdate')}}</label>
                                            <input wire:model="start_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.todate')}}</label>
                                            <input wire:model="end_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.employee')}}</label>
                                            <select class="form-control" wire:model='emp_id' style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($employees as $item)
                                                <option value="{{$item->id}}">{{$item->firstname}} - {{$item->lastname}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.show')}}</label>
                                            <select class="form-control" wire:model='select_page'>
                                                <option value="10000000000">{{__('lang.all')}}</option>
                                                <option value="10">10</option>
                                                <option value="30">30</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <button onclick="ExportExcel('xlsx')" class="btn btn-success btn-sm w-100 mt-2 p-2"><i class="fas fa-print"></i>&nbsp;Excel</button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <a href="#" onclick="printDiv('printOrder')" class="btn btn-info btn-sm w-100 mt-2 p-2"><i class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-center">
                                        <h4 class="bg-danger p-3">{{__('lang.amount_out')}}:
                                            {{number_format($this->miss_total)}} {{__('lang.lak')}}
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th>{{__('lang.no')}}</th>
                                                <th>{{__('lang.first_and_lastname')}}</th>
                                                <th>{{__('lang.sex')}}</th>
                                                <th>{{__('lang.absent_day')}}</th>
                                                <th>{{__('lang.amount_out')}}</th>
                                                <th>{{__('lang.date')}}</th>
                                                <th>{{__('lang.creator')}}</th>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <th>{{__('lang.action')}}</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($trackmissemployees as $item)
                                            <tr>
                                                <td style="text-align: center">{{$page++}}</td>
                                                <td>
                                                    @if(!empty($item->employee->firstname))
                                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->employee->sex))
                                                    @if($item->employee->sex == 1)
                                                    <button class="btn btn-primary btn-sm">{{__('lang.man')}}</button>
                                                    @else
                                                    <button class="btn btn-success btn-sm">{{__('lang.woman')}}</button>
                                                    @endif
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">
                                                    @if(!empty($item->misstype->type))
                                                    {{$item->misstype->type}}
                                                    @endif
                                                </td>
                                                <td style="text-align:center;">
                                                    <p class="badge badge-danger p-2">
                                                        @if(!empty($item->total_salary))
                                                        {{number_format($item->total_salary)}}{{__('lang.lak')}}
                                                        @endif
                                                    </p>
                                                </td>
                                                <td style="text-align: center">{{date('d/m/Y', strtotime($item->date)) }}
                                                </td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->user->name))
                                                    {{$item->user->name}}
                                                    @endif
                                                </td>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <td style="text-align: center">
                                                    <button wire:click='delete({{$item->id}})' onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                    <button wire:click="edit({{$item->id}})" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></button>
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                                <td style="text-align: center"><b><u>{{number_format($this->miss_total)}}
                                                            {{__('lang.lak')}}</u></b></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="float-right">
                                    {{$trackmissemployees->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- </div> -->
            @push('scripts')
            <script>
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                    location.reload();
                }

                function ExportExcel(type, fn, dl) {
                    var elt = document.getElementById('table-excel');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "Sheet JS"
                    });
                    return dl ?
                        XLSX.write(wb, {
                            bookType: type,
                            bookSST: true,
                            type: 'base64'
                        }) :
                        XLSX.writeFile(wb, fn || ('ຕິດຕາມວັນຂາດພະນັກງານ.' + (type || 'xlsx')));
                }
            </script>
            @endpush
            <div class="container" id='printOrder' style="display:none;">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>{!! $branchs->bill_header !!}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;" src="{{asset($branchs->logo)}}">
                        <p>
                            <b>
                                @if (Config::get('app.locale') == 'lo')
                                {{$branchs->name_la}}
                                @elseif(Config::get('app.locale') == 'en')
                                {{$branchs->name_en}}
                                @endif
                            </b>
                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <br>
                        <h5><b>{{__('lang.calculate_salary')}}</b></h5>
                        <h6><b>
                                @if(!empty($this->month))
                                {{__('lang.monthly')}} {{$this->month}}/ {{$this->year}}
                                @endif
                            </b>
                        </h6>
                        <h6>@if(!empty($this->fromdate) && !empty($this->todate))
                            {{__('lang.fromdate')}} {{date('d/m/Y', strtotime($this->fromdate)) }} -
                            {{date('d/m/Y', strtotime($this->todate)) }}
                            @endif
                        </h6>
                        @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                        (!empty($this->month) && !empty($this->emp_id)))
                        <h6>
                            <div class="text-center">
                                <h6><b>{{__('lang.first_and_lastname')}}: {{$this->fullname}}</b></h6>
                            </div>
                        </h6>
                        @endif
                    </div>
                    <div class="col-md-4 text-right">
                        <br><br>
                        @php
                        $date=date('d/m/Y H:i:s');
                        @endphp
                        <p>{{__('lang.tran_no')}}: ........................</p>
                        <p>{{__('lang.date')}}: {{$date}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="table-excel" class="table table-bordered" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th>{{__('lang.no')}}</th>
                                        <th>{{__('lang.first_and_lastname')}}</th>
                                        <th>{{__('lang.sex')}}</th>
                                        <th>{{__('lang.absent_day')}}</th>
                                        <th>{{__('lang.amount_out')}}</th>
                                        <th>{{__('lang.date')}}</th>
                                        <th>{{__('lang.creator')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp

                                    @foreach($trackmissemployees as $item)
                                    <tr>
                                        <td style="text-align: center">{{$i++}}</td>
                                        <td>
                                            @if(!empty($item->employee->firstname))
                                            {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                            @endif
                                        </td>
                                        <td style="text-align: center">
                                            @if(!empty($item->employee->sex))
                                            @if($item->employee->sex == 1)
                                            {{__('lang.man')}}
                                            @else
                                            {{__('lang.woman')}}
                                            @endif
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if(!empty($item->misstype->type))
                                            {{$item->misstype->type}}
                                            @endif
                                        </td>
                                        <td style="text-align:center;">
                                            @if(!empty($item->total_salary))
                                            {{number_format($item->total_salary)}}{{__('lang.lak')}}
                                            @endif
                                        </td>
                                        <td style="text-align: center">{{date('d/m/Y', strtotime($item->date)) }}
                                        </td>
                                        <td style="text-align: center">
                                            @if(!empty($item->user->name))
                                            {{$item->user->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                        <td style="text-align: center"><b><u>{{number_format($this->miss_total)}} {{__('lang.lak')}}</u></b></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(!empty($branchs->bill_footer))
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->director_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->chechker_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->staff_sign}}</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($createData == true)
    <!-- form create by ajck -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="alert alert-success text-center">
                    {{__('lang.add')}}{{__('lang.success')}}
                </div>
                @endif
                <div class="col-lg-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.add')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form wire:submit.prevent='create'>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-2">
                                        <div class="form-group" wire:ignore>
                                            <label>{{__('lang.employee')}}</label>
                                            <select class="form-control @error('emp_id') is-invalid @enderror" wire:model='emp_id' id="employee" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @forelse($employees_add as $item)
                                                <option value="{{$item->id}}">{{$item->firstname}} - {{$item->lastname}}
                                                </option>
                                                @empty
                                                <h1>Record not found</h1>
                                                @endforelse
                                            </select>
                                            <span class="text-danger"> @error('emp_id'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ຈໍານວນມື້ຂາດ</label>
                                            <select class="form-control @error('qty_date') is-invalid @enderror select2" wire:model="qty_date" style="width: 100%;">
                                                <option value="">{{__('lang.select')}}</option>
                                                @foreach($misstypes as $item)
                                                <option value="{{$item->id}}">{{$item->type}}
                                                    {{number_format($item->amount)}}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('qty_date')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.date')}}</label>
                                            <input type="date" class="form-control @error('date') is-invalid @enderror" wire:model='date' placeholder="{{__('lang.date')}}">
                                            <span class="text-danger"> @error('date'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.note')}}</label>
                                            <input type="text" class="form-control" wire:model='note' placeholder="{{__('lang.note')}}">
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                @if(!empty($this->hiddenId))
                                <button type="button" wire:click="update" class="btn btn-success">{{__('lang.edit')}}</button>
                                @else
                                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                @endif
                                <a class="btn btn-warning" wire:click.prevent="back()">{{__('lang.back')}}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#employee').select2();
            $('#employee').on('change', function(e) {
                var data = $('#employee').select2("val");
                @this.set('emp_id', data);
            });
        });
    </script>
    @endif
    <!-- </div> -->
</div>