@if($transactions->count() > 0)

  <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.titleorder')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.titleorder')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
          @if(Session::has('message_confirmpaymentbefore'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-danger font-weight-bold py-3">{{Session::get('message_confirmpaymentbefore')}}</a>
              </div>
          @endif
          @if(Session::has('pay_debt'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{Session::get('pay_debt')}}</a>
              </div>
          @endif
          @if(Session::has('message_sended'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{Session::get('message_sended')}}</a>
              </div>
          @endif
          @if(Session::has('message_nosupporttoeditorderitem'))
                        <div class="alert alert-success text-center">
                            {{Session::get('message_nosupporttoeditorderitem')}}
            </div>
            @endif
            @if(Session::has('editorderitem_success'))
                        <div class="alert alert-success text-center">
                            {{Session::get('editorderitem_success')}}
                        </div>
            @endif
            @if(Session::has('message_removeorderitem'))
                        <div class="alert alert-success text-center">
                            {{Session::get('message_removeorderitem')}}
                        </div>
            @endif
            @if(Session::has('editcancelorder_successfully'))
                        <div class="alert alert-success text-center">
                        {{Session::get('lang.success')}}
                        </div>
            @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <input wire:model="search_by_date" class='col-3 form-control' type="date"/>
                    <thead>
                    <tr>
                    <th>{{__('lang.order_no')}}</th>
                    <th>{{__('lang.order_id')}}</th>
                    <th>{{__('lang.date')}}</th>
                    <th>{{__('lang.order_firstname')}}</th>
                    <th>{{__('lang.order_tel')}}</th>
                    <th>{{__('lang.order_email')}}</th>
                    <th>{{__('lang.payment')}}</th>
                    <th>{{__('lang.status')}}</th>
                    <th>{{__('lang.total')}}</th>
                    <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $i = 1;
                    @endphp
                    @foreach($transactions as $transaction)
                    @if($transaction->order->status !='canceled')
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$transaction->order_id}}</td>
                      <td>{{date('d/m/Y H:i:s', strtotime($transaction->created_at)) }}</td>
                      <td>{{$transaction->order->firstname}} {{$transaction->order->lastname}}</td>
                      <td>{{$transaction->order->phone}}</td>
                      <td>{{$transaction->order->email}}</td>
                      <td>
                        @if($transaction->mode =='onepay' && $transaction->check_payment =='0')
                              <a href="{{ route('admin.detailpayment',['detailpayment_slug'=>$transaction->order_id])}}">{{__('lang.pay_on_bcelone')}}</a> </td>
                         @elseif($transaction->mode =='debt')
                         <p  class="badge badge-warning p-2">{{__('blog.debt')}}</p>
                         @elseif($transaction->check_payment =='1' && $transaction->mode !='debt')
                              <p  class="badge badge-success p-2">{{__('lang.yes_pay')}}</p>
                         @else
                            <p  class="badge badge-danger p-2">{{__('lang.no_pay')}}</p>
                         @endif
                      </td>
                       <td>
                        @if($transaction->status =='declined')
                          <p  class="badge badge-danger p-2">{{__('lang.cancel')}}</p>
                        @elseif($transaction->status =='approved' && $transaction->order->status=='delivered')
                          <p  class="badge badge-warning p-2">{{__('lang.status_sending')}}</p>
                        @elseif($transaction->status =='refunded' && $transaction->order->status=='arrived')
                          <p  class="badge badge-success p-2">{{__('lang.status_arrived')}}</p>
                        @else
                          <p  class="badge badge-info p-2">{{__('lang.status_order')}}</p>
                        @endif
                      </td>
                      <td>{{number_format($transaction->order->total)}}</td>
                      <td>
                        @if($transaction->status !='refunded' && $transaction->order->status !='arrived')
                           <a href="{{ route('admin.confirmorder',['order_slug'=>$transaction->order_id])}}" class="btn btn-primary btn-sm"><i class="fas fa-check-circle"></i></a>
                        @endif
                          <!--
                          @if($transaction->status =='approved' && $transaction->order->status=='delivered')
                              <a href="{{ route('admin.confirmorder',['order_slug'=>$transaction->order_id])}}" class="btn btn-primary btn-sm"><i class="fas fa-check-circle"></i></a>
                          @endif
                          -->
                          <a href="{{ route('admin.printbillorder',['order_slug'=>$transaction->order_id])}}" class="btn btn-info btn-sm"><i class="fas fa-print"></i></a>
                          @if($transaction->status !='refunded' && $transaction->order->status!='arrived')
                              <a href="{{ route('admin.editorder',['order_slug'=>$transaction->order_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                          @endif
                          <a href="{{ route('admin.orderitem',['order_slug'=>$transaction->order_id])}}" class="btn btn-default btn-sm"><i class="fas fa-eye"></i></a>
                          @if($transaction->status !='refunded' && $transaction->order->status !='arrived')
                          <a  href="{{ route('admin.cancelorder',['cancelorder_slug'=>$transaction->order_id])}}"  class="btn btn-danger btn-sm" onclick="return confirm('ທ່ານຕ້ອງການຍົກເລີກລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()" ><i class="fas fa-trash"></i></a>
                          <a href="{{ route('admin.dosend',['order_slug'=>$transaction->order_id])}}" class="btn btn-warning btn-sm"><i class="fas fa-truck"></i></a>
                          @endif
                          @if(Auth::user()->role_id ==1)
                              @if($transaction->status =='refunded' && $transaction->order->status =='arrived' && $transaction->mode=='debt')
                                  <a href="{{ route('admin.paydebt',['slug'=>$transaction->order_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                              @endif
                          @endif
                        </td>
                    </tr>
                    @endif
                   @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
            </section>
        </div>
      </section>

@else
  <div style="padding:100px;">
    <main id="main" class="main-site">
      <div class="container pb-60">
        <div class="row">
          <div class="col-md-12 text-center">
              <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{__('lang.no_order')}}</a>
          </div>
        </div>
      </div><!--end container-->
    </main>
  </div>
@endif