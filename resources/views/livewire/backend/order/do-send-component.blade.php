
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{__('lang.do_send')}}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
            <li class="breadcrumb-item active">{{__('lang.do_send')}}</li>
          </ol>
        </div>
      </div>
      <section class="content">
    <div class="container-fluid">
    @if(Session::has('message_sendtorider'))
    <div class="col-md-12 text-center mt-6">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{Session::get('message_sendtorider')}}</a>
    </div>
    @endif
        <div class="col-lg-12">
          <div class="card card-default">
            <div class="card-header">
            <h3 class="card-title"><h4 class="card-title">{{__('lang.bill_no')}}: {{$this->order_id}}</h4></h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
            <form wire:submit.prevent="SendToRider">
                @csrf
                <div class="card-body">
                      <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>{{__('lang.rider_fullname')}}</label>
                              <select class="form-control" wire:model="rider_id" id="users" style="width: 100%;">
                                  <option value="">{{__('lang.select')}}</option>
                                  @foreach($riders as $rider)
                                     <option value={{$rider->id}}>{{$rider->name}}</option>
                                  @endforeach
                              </select>
                              @error('rider_id')
                                  <span class="text-danger">{{$message}}</span>
                              @enderror
                            </div>
                          </div>
                          <script>
                            $(document).ready(function(){
                              $('#user').select2();
                              $('#users').on('change', function(e){
                                var  data= $('#users').select2("val");
                                @this.set('id', data);
                              })
                            })
                          </script>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="bod">{{__('lang.note')}}</label>
                              <input type="text" class="form-control" wire:model="note" placeholder="{{__('lang.note')}}">
                            </div>
                          </div>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                    <a class="btn btn-warning" href="{{route('admin.order')}}" >{{__('lang.back')}}</a>
                </div>
            </form>
          </div>
      </div>
    </div>
  </section>
    </div>
</section>
  
