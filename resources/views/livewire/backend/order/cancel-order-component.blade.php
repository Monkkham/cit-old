<div style="padding:50px;">
   <main id="main" class="main-site">
		<div class="container pb-60">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2>{{__('lang.remove_success')}} !</h2>
                    <a href="/orders"  class="btn btn-block btn-success font-weight-bold py-3">{{__('lang.close')}}</a>
				</div>
			</div>
		</div><!--end container-->
	</main>
</div>