



    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.edit_orderitem')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.edit_orderitem')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
                      @if (Session::has('errore_qty'))
                        <div class="alert alert-danger text-center">
                            {{__('lang.something_wrong')}}
                        </div>
                      @endif
              <div class="card">
                <div class="card-body">
                 <form  wire:submit.prevent="UpdateOrderItem">
                     @csrf
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>{{__('lang.order_no')}}</th>
                      <th>{{__('lang.orderid')}}</th>
                      <th>{{__('lang.orderitem_image')}}</th>
                      <th>{{__('lang.orderitem_proname')}}</th>
                      <th>{{__('lang.orderitem_category')}}</th>
                      <th>{{__('lang.order_qty')}}</th>
                      <th>{{__('lang.order_price')}}</th>
                        @if($this->total !=0)
                          <th>{{__('lang.total')}}</th>
                          @endif
                    </tr>
                    </thead>
                    <tbody>
                      @php
                      $i=1;
                      @endphp
                      @foreach($orderitems as $orderitem)
                        <tr>
                          <td>
                            {{$i++}}
                          </td>
                          <td>{{$orderitem->id}}</td>
                          <td><img src="{{asset($orderitem->product->image)}}" width="50px"></td>
                          <td>{{$orderitem->product->name}}</td>
                          <td>{{$orderitem->product->product_catalog->name}}</td>
                           <td>
                           <div style="display: flex;
                      justify-content:space-between;
                      align-items: center;">
                                <select class="form-control" wire:model="type">
                                        <option value="">{{__('lang.select')}}</option>
                                        <option value="1">{{__('lang.increase')}}</option>
                                        <option value="0">{{__('lang.minus')}}</option>
                                </select>
                                <input type="number" wire:model="qty" class="form-control"  placeholder="{{$orderitem->quantity}}">
                             </div>
                               @error('qty')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                                @error('type')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </td>
                           <td>{{number_format($orderitem->price)}}</td>
                            @if($this->total !=0)
                             <td>{{number_format($this->total)}}{{__('lang.lak')}}</td>
                             @endif
                         </tr>
                   @endforeach
                    </tbody>
                  </table><br>
                  <div class="col-md-12 text-center">
                      <button type="submit"   class="btn btn-block btn-success font-weight-bold py-3"><i class="fas fa-pencil-alt"></i> {{__('lang.edit')}}</button>
                  </div>
                  </form>
                  </div>
              </div>
            </div>
          </div>
          </section>
        </div>
      </section>
