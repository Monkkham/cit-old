
    <section class="content-header">
      <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.confirmorder_component')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.confirmorder_component')}}</li>
              </ol>
            </div>
          </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                   {{__('lang.bill_no')}}:{{$this->order_slug}}
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>{{__('lang.order_no')}}</th>
                      <th>{{__('lang.orderitem_image')}}</th>
                      <th>{{__('lang.order_id')}}</th>
                      <th>{{__('lang.orderitem_proname')}}</th>
                      <th>{{__('lang.orderitem_category')}}</th>
                      <th>{{__('lang.order_qty')}}</th>
                      <th>{{__('lang.order_price')}}</th>
                      <th>{{__('lang.total')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                      $i=1;
                      @endphp
                      @foreach($confirmorderitem as $item)
                        <tr>
                          <td>{{$i++}}</td>
                          <td><img src="{{asset($item->product->image)}}" width="50px"></td>
                          <td>{{$item->id}}</td>
                          <td>
                            @if($item->product_id)
                                {{$item->product->name}}
                            @endif
                          </td>
                          <td>
                          @if(!empty($item->product->product_catalog->name))
                            {{$item->product->product_catalog->name}}
                          @endif
                          </td>
                          <td>{{$item->quantity}}</td>
                          <td>{{number_format($item->price)}}</td>
                          <td>{{number_format($item->quantity * $item->price)}}{{__('lang.lak')}}</td>
                        </tr>
                   @endforeach
                    </tbody>
                  </table>
                  <style>
                    .controllsubtotal{
                      display: flex;
                      justify-content:space-between;
                      align-items: center;
                    }
                  </style>
                  <div class="controllsubtotal">
                    <div class="discount">
                       @if($item->order->discount > 0)
                         <h3>{{__('lang.discount')}}: {{number_format($item->order->discount)}} {{__('lang.lak')}}</h3>
                        @endif
                    </div>
                   <div class="subtotal">
                       <h3>{{__('lang.total')}}: {{number_format($item->order->total)}} {{__('lang.lak')}}</h3>
                   </div>
                </div>
                </div>
              </div>
              <div class="col-md-12 text-center">
                  <form  wire:submit.prevent="Confirmorder">
                    @csrf
                       <button type="submit"  class="btn btn-block btn-success font-weight-bold py-3"><i style="font-size:25px" class="fas fa-check"></i> {{__('lang.confirm')}}</button>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      