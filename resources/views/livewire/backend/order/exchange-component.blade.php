<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.exchange')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.exchange')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            @if(Session::has('success'))
            <div class="row mb-2">
                <div class="col-md-12">
                    <a class="btn btn-success w-100 btn-sm">{{Session::get('success')}}</a>
                </div>
            </div>
            @endif
            <div class="row">
                <!--Foram add new-->
                <div class="col-md-4">
                    <div class="card card-default">
                        <div class="card-header bg-info">
                            <label>{{__('lang.add')}}</label>
                        </div>
                        <form>

                            <div class="card-body">
                                <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.name')}} </label>
                                            <input wire:model="name" type="text"
                                                class="form-control @error('name') is-invalid @enderror"
                                                placeholder="{{__('lang.name')}}">
                                            @error('name') <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.symbol')}}</label>
                                            <input wire:model="symbol" type="text"
                                                class="form-control @error('symbol') is-invalid @enderror"
                                                placeholder="{{__('lang.symbol')}}">
                                            @error('symbol') <span style="color: red"
                                                class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.rate')}}</label>
                                            <input wire:model="rate" type="text"
                                                class="form-control @error('rate') is-invalid @enderror"
                                                placeholder="{{__('lang.rate')}}">
                                            @error('rate') <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.status')}}</label>
                                           <select name="" id="" wire:model="active" class="form-control">
                                                <option value="1">{{__('lang.open')}}</option>
                                                <option value="0">{{__('lang.close')}}</option>
                                           </select>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between md-2">
                                    <button type="button" wire:click="store"
                                        class="btn btn-success">{{__('lang.save')}}</button>
                                    <button type="button" wire:click="resetField"
                                        class="btn btn-primary">{{__('lang.reset')}}</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <!--List users- table table-bordered table-striped -->

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>{{__('lang.exchange')}}</label>
                                </div>
                                <div class="col-md-6">
                                    <input wire:model="search" type="text" class="form-control"
                                        placeholder="{{__('lang.search')}}">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="bg-info">
                                        <tr>
                                            <th>{{__('lang.no')}}</th>
                                            <th>{{__('lang.name')}}</th>
                                            <th>{{__('lang.symbol')}}</th>
                                            <th>{{__('lang.rate')}}</th>
                                            <th>{{__('lang.status')}}</th>
                                            <th>{{__('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $stt = 1;
                                        @endphp

                                        @foreach ($data as $item)
                                        <tr>
                                            <td>{{$stt++}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->symbol}}</td>
                                            <td>
                                                {{$item->rate}}
                                            </td>
                                            <td>
                                                @if($item->active ==1)
                                                <span class="text-success">{{__('lang.open')}}</span>
                                                @else
                                                <span class="text-danger">{{__('lang.close')}}</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if(Auth()->user()->rolename->name == 'admin')
                                                <button wire:click="edit({{$item->id}})" type="button"
                                                    class="btn btn-warning btn-sm"><i
                                                        class="fas fa-pencil-alt"></i></button>
                                                <button wire:click='delete({{$item->id}})'
                                                    onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()" type="button"
                                                    class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                </form>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                                <!-- <div class="float-right" wire:ignore>
                      
                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
