
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.order_item')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.order_item')}}</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-12">
              <div class="card">
                <div class="card-body">
                {{__('lang.order_no')}}:{{$this->order_slug}}
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>{{__('lang.order_no')}}</th>
                      <th>{{__('lang.orderitem_image')}}</th>
                      <th>{{__('lang.order_id')}}</th>
                      <th>{{__('lang.orderitem_proname')}}</th>
                      <th>{{__('lang.orderitem_category')}}</th>
                      <th>{{__('lang.order_qty')}}</th>
                      <th>{{__('lang.order_price')}}</th>
                      <th>{{__('lang.total')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                      $i=1;
                      @endphp
                      @foreach($orderitems as $orderitem)
                        <tr>
                          <td>{{$i++}}</td>
                          <td><img src="{{asset($orderitem->product->image)}}" width="50px"></td>
                          <td>{{$orderitem->id}}</td>
                          <td>
                            @if(!empty($orderitem->product->name))
                                {{$orderitem->product->name}}
                             @endif
                        </td>
                          <td>
                            @if(!empty($orderitem->product->product_catalog->name))
                                 {{$orderitem->product->product_catalog->name}}
                             @endif
                        </td>
                          <td>{{$orderitem->quantity}}</td>
                          <td>{{number_format($orderitem->price)}}</td>
                          <td>{{number_format($orderitem->quantity * $orderitem->price)}}{{__('lang.lak')}}</td>
                        </tr>
                   @endforeach
                    </tbody>
                  </table>
                  <style>
                    .controllsubtotal{
                      display: flex;
                      justify-content:space-between;
                      align-items: center;
                    }
                  </style>
                  <div class="controllsubtotal">
                  <div class="discount">
                       @if($orderitem->order->discount > 0)
                         <h3>{{__('lang.discount')}}: {{number_format($orderitem->order->discount)}} {{__('lang.lak')}}</h3>
                        @endif
                    </div>
                   <div class="subtotal">
                       <h3>{{__('lang.total')}}: {{number_format($orderitem->order->total)}} {{__('lang.lak')}}</h3>
                   </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      