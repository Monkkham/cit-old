
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.arrived_component')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.arrived_component')}}</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
  
      <section class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>{{__('lang.order_no')}}</th>
                    <th>{{__('lang.order_id')}}</th>
                    <th>{{__('lang.rider_id')}}</th>
                    <th>{{__('lang.rider_fullname')}}</th>
                    <th>{{__('lang.order_tel')}}</th>
                    <th>{{__('lang.order_email')}}</th>
                    <th>{{__('lang.address')}}</th>
                    <th>{{__('lang.rider_get_divid')}}</th>
                    <th>{{__('lang.order_total')}}</th>
                    <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $i = 1;
                    @endphp
                    @foreach($orderdonesends as $item)
                  
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->id}}</td>
                            @if($item->rider_id > 0)
                              <td>{{$item->user->id}}</td>
                              <td>{{$item->user->name}}</td>
                              <td>{{$item->user->phone}}</td>
                              <td>{{$item->user->email}}</td>
                              <td>{{$item->user->address}}</td>
                              <td>({{$item->user->divid->percent}}%){{number_format($item->amount)}}</td>
                            @else
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            @endif
                            <td>
                              @if(!empty($item->order->subtotal))
                                  {{number_format($item->order->subtotal)}}{{__('lang.lak')}}
                              @endif
                            </td>
                          <td>
                            <a href="{{ route('admin.orderitem',['order_slug'=>$item->order_id])}}" class="btn btn-default btn-sm"><i class="fas fa-eye"></i></a>
                          </td>
                          </tr>
                     @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>