<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<script>
		function printDiv(divName){
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;

        }
	
	</script>
<style>
        .header_print{
            display:flex;
            justify-content: space-between;
            align-items: center;
            padding:20px;
        }
 </style>
 <div class="header_print">
        <a href="{{route('admin.order')}}" class="btn btn-default"><i class="fas fa-arrow-circle-left"></i>Back</a>
       <a href="#" onclick="printDiv('printOrder')" class="btn btn-default"><i class="fa fa-print"></i>Print</a>
 </div>
<div class="container-fluid" id='printOrder' style="">
    <div class="card">
        <div class="card-body">
            <ul class="nav">
                <li class="nav-item">
                    <img height="50" src="{{asset('images/logo.png')}}" class="rounded-circle" alt="..."> ບໍລິສັດ CIT ຈຳກັດຜູ້ດຽວ
                </li>
            </ul>
           <center><h3 style="font-weight:bold">{{__('lang.bill_data')}}</h3></center>
            <style>
                .header_bill{
                    display:flex;
                    justify-content: space-between;
                    align-items: center;
                }
            </style>
            @php
             $date=date('d-m-Y');
            @endphp
            <div class="header_bill">
                <p style="font-weight:bold">  {{__('lang.bill_no')}} : {{$orders->id}}</p>
                <p style="font-weight:bold">  {{__('lang.date')}} : {{$date}}</p>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align:center" class="table-light" colspan="2" scope="col">{{__('lang.customer')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">{{__('lang.name')}}</th>
                                        <td>{{$orders->firstname}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.lastname')}}</th>
                                        <td>{{$orders->lastname}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.phone')}}</th>
                                        <td colspan="2">{{$orders->phone}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.email')}}</th>
                                        <td colspan="2">{{$orders->email}}</td>

                                    </tr>

                                    <tr>
                                        <th scope="row">{{__('lang.address')}}</th>
                                        <td colspan="2">{{$orders->address}} ,
                                            @if($orders->vill_id)
                                             {{$orders->village->name}},
                                            @endif
                                            @if($orders->dis_id)
                                              {{$orders->district->name}},
                                            @endif
                                            @if($orders->pro_id)
                                               {{$orders->province->name}}
                                            @endif
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th  style="text-align:center"  class="table-light" colspan="2" scope="col">{{__('lang.rider')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">{{__('lang.name')}}</th>
                                        <td>{{$orders->user->name}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.phone')}}</th>
                                        <td>{{$orders->user->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.email')}}</th>
                                        <td>{{$orders->user->email}}</td>

                                    </tr>
                                    <tr>
                                         <th scope="row">{{__('lang.address')}}</th>
                                        <td>{{$orders->user->address}}</td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="text-align:center" class="table-light" colspan="6" scope="col">{{__('lang.product')}}</th>
                            </tr>
                            <tr>
                                <th scope="col">{{__('lang.no')}}</th>
                                <th scope="col">{{__('lang.orderitem_image')}}</th>
                                <th scope="col">{{__('lang.orderitem_proname')}}</th>
                                <th scope="col">{{__('lang.order_qty')}}</th>
                                <th scope="col">{{__('lang.order_price')}}</th>
                                <th scope="col">{{__('lang.total')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                      $i=1;
                      @endphp
                      @foreach($orderitems as $orderitem)
                            <tr>
                                <td scope="row">{{$i++}}</td>
                                <td>
                                    <div>
                                        <img height="50" src="{{asset($orderitem->product->image)}}" class="rounded mx-auto d-block" alt="Max-width 100%">
                                    </div>
                                </td>
                                <td>{{$orderitem->product->name}}</td>
                                <td>{{$orderitem->quantity}}</td>
                                <td>{{number_format($orderitem->price)}}</td>
                                <td>{{number_format($orderitem->quantity * $orderitem->price)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-6">
                    <!--  -->
                </div>  
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="table-light" colspan="2" scope="col">{{__('lang.calculate')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">{{__('lang.subtotal')}}:</th>
                                        <td>{{number_format($orders->subtotal)}} {{__('lang.lak')}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.discount')}}:</th>
                                        <td>{{number_format($orders->discount)}} {{__('lang.lak')}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.vat')}}:</th>
                                        <td colspan="2">{{number_format($orders->tax)}} {{__('lang.lak')}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">{{__('lang.total')}}:</th>
                                        <td colspan="2"><p  style="font-weight:bold">{{number_format($orders->total)}} {{__('lang.lak')}}</p></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
                        <style>
                            .print_footer{
                                display:flex;
                                justify-content: space-between;
                                align-items: center;
                                margin-left:auto;
                                margin-right:auto;
                                width:80%;
                                
                            }
                        </style>
                        <div class="print_footer">
                            <p style="font-weight:bold">{{__('lang.signature_of_rider')}}:</p>
                            <p  style="font-weight:bold">{{__('lang.signature_of_customer')}}:</p>
                        </div>
            </div>
        </div>
    </div>
</div>