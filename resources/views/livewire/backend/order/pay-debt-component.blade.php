



    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.pay_debt')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.pay_debt')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
                      @if (Session::has('message_removeorderitem'))
                        <div class="alert alert-success text-center">
                            {{__('lang.delete')}} {{__('lang.success')}}
                        </div>
                    @endif
                    @if (Session::has('no_delete'))
                        <div class="alert alert-danger text-center">
                            {{__('lang.something_wrong')}}
                        </div>
                      @endif
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>{{__('lang.no')}}</th>
                      <th>{{__('lang.select')}}</th>
                      <th>{{__('lang.order_id')}}</th>
                      <th>{{__('lang.orderitem_image')}}</th>
                      <th>{{__('lang.orderitem_proname')}}</th>
                      <th>{{__('lang.orderitem_category')}}</th>
                      <th>{{__('lang.order_qty')}}</th>
                      <th>{{__('lang.order_price')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                      $i=1;
                      @endphp
                      @foreach($orderitems as $orderitem)
                        <tr>
                          <td>
                            {{$i++}}
                          </td>
                          <td>
                            {{$orderitem->id}}
                          </td>
                          <td><img src="{{asset($orderitem->product->image)}}" width="50px"></td>
                          <td>{{$orderitem->product->name}}</td>
                          <td>{{$orderitem->product->product_catalog->name}}</td>
                          <td>
                               {{$orderitem->quantity}}
                          </td>
                          <td>{{number_format($orderitem->price)}}</td>
                          <td>
                              {{number_format($orderitem->price * $orderitem->quantity )}} {{__('lang.lak')}}
                          </td>
                        </tr>
                   @endforeach
                    </tbody>
                  </table><br>
                  <style>
                    .controllsubtotal{
                      display: flex;
                      justify-content:space-between;
                      align-items: center;
                    }
                  </style>
                  <div class="controllsubtotal">
                    @if($orderitem->order->subtotal >0)
                      <div class="subtotal">
                           <h3>{{__('lang.subtotal')}}: {{number_format($orderitem->order->subtotal)}} {{__('lang.lak')}}</h3>
                      </div>
                      @endif
                      @if($orderitem->order->discount > 0)
                      <div class="discount">
                           <h3>{{__('lang.discount')}}: {{number_format($orderitem->order->discount)}} {{__('lang.lak')}}</h3>
                      </div>
                      @endif
                      @if($orderitem->order->total >0)
                      <div class="subtotal">
                          <h3>{{__('lang.total')}}: {{number_format($orderitem->order->total)}} {{__('lang.lak')}}</h3>
                      </div>
                      @endif
                </div>

                 <div class="col-md-12 text-center">
                          <a  href="#" wire:click.prevent ="paydebt"  class="btn btn-block btn-warning font-weight-bold py-3"><i class="fas fa-pencil-alt"></i>ຊໍາລະໜີ້</a>
                 </div>
                </div>
              </div>
            </div>
          </div>
          </section>
        </div>
      </section>
