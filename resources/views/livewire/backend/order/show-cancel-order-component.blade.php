@if($ordercanceled->count() > 0)

<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.cancelorder_component')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.cancelorder_component')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
          @if(Session::has('message_confirmpaymentbefore'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-danger font-weight-bold py-3">{{Session::get('message_confirmpaymentbefore')}}</a>
              </div>
          @endif
          @if(Session::has('message_sended'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{Session::get('message_sended')}}</a>
              </div>
          @endif
          @if(Session::has('message_nosupporttoeditorderitem'))
                        <div class="alert alert-success text-center">
                            {{Session::get('message_nosupporttoeditorderitem')}}
            </div>
            @endif
            @if(Session::has('editorderitem_success'))
                        <div class="alert alert-success text-center">
                            {{Session::get('editorderitem_success')}}
                        </div>
            @endif
            @if(Session::has('message_removeorderitem'))
                        <div class="alert alert-success text-center">
                            {{Session::get('message_removeorderitem')}}
                        </div>
            @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>{{__('lang.order_no')}}</th>
                    <th>{{__('lang.order_id')}}</th>
                    <th>{{__('lang.order_firstname')}}</th>
                    <th>{{__('lang.order_tel')}}</th>
                    <th>{{__('lang.order_email')}}</th>
                    <th>ຊໍາລະເງິນ</th>
                    <th>ສະຖານະ</th>
                    <th>{{__('lang.order_total')}}</th>
                    <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $i = 1;
                    @endphp
                    @foreach($ordercanceled as $item)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$item->order_id}}</td>
                      <td>{{$item->order->firstname}} {{$item->order->lastname}}</td>
                      <td>{{$item->order->phone}}</td>
                      <td>{{$item->order->email}}</td>
                      <td>
                        @if($item->mode =='onepay' && $item->check_payment =='0')
                              <a href="{{ route('admin.detailpayment',['detailpayment_slug'=>$item->order_id])}}">{{__('lang.pay_on_bcelone')}}</a> </td>
                         @elseif($item->check_payment =='1')
                         <p class="badge badge-success p-2">{{__('lang.yes_pay')}}</p>
                         @else
                           <span class="badge badge-danger p-2">{{__('lang.no_pay')}}</span>
                         @endif
                      </td>
                       <td>
                      @if($item->status =='declined')
                         <p class="badge badge-danger p-2">{{__('lang.cancel')}}</p>
                      @elseif($item->status =='approved' && $item->order->status=='delivered')
                      <p class="badge badge-warning p-2">{{__('lang.status_sending')}}</p>
                      @elseif($item->status =='refunded' && $item->order->status=='arrived')
                      <p class="badge badge-success p-2">{{__('lang.status_arrived')}}</p>
                      @else
                      <p>{{__('lang.status_order')}}</p>
                      @endif
                      </td>
                      <td>{{number_format($item->order->total)}}</td>
                      <td>
                          <a href="{{ route('admin.editcancelorder',['order_slug'=>$item->order_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                        </td>
                    </tr>
                   @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
            </section>
        </div>
      </section>

@else
<div style="padding:100px;">
   <main id="main" class="main-site">
		<div class="container pb-60">
			<div class="row">
				<div class="col-md-12 text-center">
            <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{__('lang.no_order')}}</a>
				</div>
			</div>
		</div><!--end container-->
	</main>
</div>
@endif