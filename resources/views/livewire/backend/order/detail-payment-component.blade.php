<div class="detailpayment" style="padding:10px;">
<div class="container-fluid">
              <div class="row">
                @if($detailpayments->order->image =='0')
                @else
                <div class="col-md-4">
                  <!-- Profile Image -->
                  <div class="card card-primary card-outline">
                  <img   src="{{asset('admin/dist/img')}}/{{$detailpayments->order->image}}" >
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                @endif
                <div class="col-md-8">
                  <!-- About Me Box -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">{{__('lang.detail_payonbcelone')}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <strong><i class="fas fa-address-card"></i>{{__('lang.orderid')}}</strong>
                      <p class="text-muted">
                      {{$detailpayments->order_id}}
                      </p>
                      <hr>
                      <strong><i class="fas fa-clock mr-1"></i>{{__('lang.total')}}</strong>
                      <p class="text-muted">{{number_format($detailpayments->order->total)}} ກີບ</p>
                      <hr>
                      <strong><i class="fas fa-clock mr-1"></i>{{__('lang.date_payonbcelone')}}</strong>
                      <p class="text-muted">{{$detailpayments->updated_at}} </p>
                      <hr>
      
                      <strong><i class="far fa-file-alt mr-1"></i>{{__('lang.status')}}</strong>
                      <p class="text-muted">
                          ຊໍາລະເງິນແລ້ວ
                      </p>
                      <hr>
                      <strong><i class="fas fa-book mr-1"></i>{{__('lang.note')}}</strong>
                      <p class="text-muted"> 
                        @if($detailpayments->order->image =='0')
                            ຊໍາລະເງິນສົດ
                         @else
                           ຊໍາລະຜ່ານ Bcel One
                         @endif
                        </p>
                    </div>
                    <div class="card-body box-profile">
                     
                        <a href="#" wire:click.prevent="confirmpayonebcelone" target="_blank" class="btn btn-primary"><b>{{__('lang.confirm')}}</b></a>
                        <a href="{{route('admin.order')}}" class="btn btn-warning"><b>{{__('lang.back')}}</b></a>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>

              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->

</div>