<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.report')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.report_sold_well_and_isoutofstock_product')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('lang.report_sold_well_and_isoutofstock_product')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{__('lang.start_date')}}</label>
                                        <input type="date" class="form-control" wire:model="start_date">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{__('lang.end_date')}}</label>
                                        <input type="date" class="form-control" wire:model="end_date">
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <label for=""></label>
                                    <a href="#" class="btn btn-primary" id="print"><i class="fa fa-print"></i> {{__('lang.print')}} </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-1 ml-2">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <select name="" id="" class="form-control" wire:model="select_page">
                                          <option value="">{{__('lang.all')}}</option>
                                            <option value="10">10</option>
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="card-body print-content">
                            @if(!empty($branchs->bill_header))
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>{!! $branchs->bill_header !!}</h4>
                                </div>
                            </div> <!-- end row -->
                            @endif
                            <div class="row">
                                <div class="col-6">
                                    <img src="{{asset('images/logo.png')}}" alt="" width="50">
                                    <p> @if (Config::get('app.locale') == 'lo')
                                        {{$branchs->name_la}}
                                        @else
                                        {{$branchs->name_en}}
                                        @endif
                                    </p>
                                </div>
                                <div class="col-6 text-right"><br>
                                    <p>{{__('lang.date')}}: {{date('d/m/Y h:i:s', time())}}</p>
                                </div>
                            </div> <!-- end row -->
                            @if($this->start_date && $this->end_date)
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h4><b>{{__('lang.start_date')}}: {{date('d/m/Y', strtotime($this->start_date))}} -
                                            {{date('d/m/Y', strtotime($this->end_date))}}</b>
                                    </h4>
                                </div>
                            </div> <!-- end row -->
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">{{__('lang.no')}}</th>
                                                    <th class="text-center">{{__('lang.image')}}</th>
                                                    <th class="text-center">{{__('lang.code')}}</th>
                                                    <th>{{__('lang.product')}}</th>
                                                    <th class="text-center">{{__('lang.qty')}}{{__('lang.order')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 1;
                                                @endphp
                                                @foreach($data as $item)
                                                <tr>
                                                    <td class="text-center">{{$i++}}</td>
                                                    <td class="text-center">
                                                        @if(!empty($item->image))
                                                        <a href="{{asset($item->image)}}">
                                                            <img src="{{asset($item->image)}}" alt="" style="width:100px;">
                                                        </a>
                                                        @else
                                                        <img src="{{asset('images/logo.png')}}" alt="" style="width:100px;">
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        {{$item->code}}
                                                    </td>
                                                    <td class="text-info">
                                                        {{$item->name}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{$item->sum_total}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($branchs->bill_footer))
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>{!! $branchs->bill_footer !!}</h4>
                                </div>
                            </div> <!-- end row -->
                            <br>
                            <br>
                            <br>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
    <script>
        $('#print').click(function() {
            printDiv();

            function printDiv() {
                var printContents = $(".print-content").html();
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
            location.reload();
        });
    </script>
    @endpush
</div>