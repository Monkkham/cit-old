<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.report')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.report_sale_profit_loss')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.report_sale_profit_loss')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{__('lang.start_date')}}</label>
                                        <input type="date" class="form-control" wire:model="start_date">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{__('lang.end_date')}}</label>
                                        <input type="date" class="form-control" wire:model="end_date">
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="#" class="btn btn-primary" id="print"><i class="fa fa-print"></i> {{__('lang.print')}} </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body print-content">
                            @if(!empty($branchs->bill_header))
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>{!! $branchs->bill_header !!}</h4>
                                </div>
                            </div> <!-- end row -->
                            @endif
                            <div class="row">
                                <div class="col-6">
                                    <img src="{{asset('images/logo.png')}}" alt="" width="50">
                                    <p> @if (Config::get('app.locale') == 'lo')
                                        {{$branchs->name_la}}
                                        @else
                                        {{$branchs->name_en}}
                                        @endif
                                    </p>
                                </div>
                                <div class="col-6 text-right"><br>
                                    <p>{{__('lang.date')}}: {{date('d/m/Y h:i:s', time())}}</p>
                                </div>
                            </div> <!-- end row -->
                            @if($this->start_date && $this->end_date)
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h4><b>{{__('lang.start_date')}}: {{date('d/m/Y', strtotime($this->start_date))}} -
                                            {{date('d/m/Y', strtotime($this->end_date))}}</b>
                                    </h4>
                                </div>
                            </div> <!-- end row -->
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <h4><b>{{__('lang.total_cost')}}</b></h4>
                                                    </th>
                                                    <th>
                                                        <h4><b>{{__('lang.total_sale')}}</b></h4>
                                                    </th>
                                                    <th>
                                                        <h4><b>{{__('lang.profit')}}</b></h4>
                                                    </th>
                                                    <th>
                                                        <h4><b>{{__('lang.lack_of_fund')}}</b></h4>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h4 class="text-primary"><b>{{number_format($this->sum_total_cost)}}</b></h4>
                                                    </td>
                                                    <td>
                                                        <h4 class="text-primary"><b>{{number_format($this->sum_total_sale)}}</b></h4>
                                                    </td>
                                                    <td>
                                                        <h4 class="text-success"><b>{{number_format($this->sum_profit)}}</b></h4>
                                                    </td>
                                                    <td>
                                                        <h4 class="text-danger"><b>
                                                                @if($this->sum_profit <= 0) {{number_format($this->sum_profit)}} @else 0 @endif </b>
                                                        </h4>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">{{__('lang.no')}}</th>
                                                    <th class="text-center">{{__('lang.code')}}</th>
                                                    <th class="text-center">{{__('lang.created_at')}}</th>
                                                    <th class="text-center">{{__('lang.customer')}}</th>
                                                    <th class="text-center">{{__('lang.qty')}}</th>
                                                    <th class="text-center">{{__('lang.shipping_cost')}}</th>
                                                    <th class="text-center">{{__('lang.vat')}}</th>
                                                    <th class="text-center">{{__('lang.total')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data as $item)
                                                <tr>
                                                    <td class="text-center">{{$page++}}</td>
                                                    <td class="text-center">
                                                        {{$item->code}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{date('d/m/Y H:i:s', strtotime($item->created_at))}}
                                                    </td>
                                                    <td class="text-center text-info">
                                                        @if(!empty($item->customer->name))
                                                        <i class="fa fa-user text-info"></i> {{$item->customer->name}}
                                                        @endif
                                                        @if(!empty($item->customer->phone))
                                                        <i class="fa fa-phone text-info"></i> {{$item->customer->phone}}
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        {{$item->total_qty}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{number_format($item->shipping_cost)}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{number_format($item->vat)}}
                                                    </td>
                                                    <td class="text-center">
                                                        @if($item->status ==1)
                                                        <p><b>{{number_format($item->total_price)}}
                                                                @if(!empty($item->paid_type)){{$item->paid_type}}
                                                                @endif</b></p>
                                                        @elseif($item->status ==2)
                                                        <p class="text-primary"><b>{{number_format($item->total_price)}}
                                                                @if(!empty($item->paid_type)){{$item->paid_type}}
                                                                @endif</b></p>
                                                        @else
                                                        <p class="text-success"><b>{{number_format($item->total_price)}}
                                                                @if(!empty($item->paid_type)){{$item->paid_type}}
                                                                @endif</b></p>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($branchs->bill_footer))
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>{!! $branchs->bill_footer !!}</h4>
                                </div>
                            </div> <!-- end row -->
                            <br>
                            <br>
                            <br>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
    <script>
        $('#print').click(function() {
            printDiv();

            function printDiv() {
                var printContents = $(".print-content").html();
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
            location.reload();
        });
    </script>
    @endpush
</div>