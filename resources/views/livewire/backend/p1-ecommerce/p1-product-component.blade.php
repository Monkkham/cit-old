<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.module_order')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.product')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.product')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="{{route('admin.create.p1-product')}}" class="btn btn-primary">{{__('lang.add')}}</a>
                                    <a href="#" wire:click="show_import_excel" class="btn btn-success">Excel</a>
                                </div>
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                    <div wire:ignore>
                                        <select name="" id="product_type" class="form-control" wire:model="catalog_id">
                                            <option value="">{{__('lang.product_type')}}</option>
                                            @foreach($categories as $item)
                                            <option value="{{$item->id }}">
                                                @if(Config::get('app.locale') == 'lo')
                                                {{$item->name_la}}
                                                @elseif(Config::get('app.locale') == 'en')
                                                {{$item->name_en}}
                                                @endif
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input wire:model="search" type="text" class="form-control" placeholder="{{__('lang.search')}}">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{__('lang.no')}}</th>
                                            <th class="text-center">{{__('lang.image')}}</th>
                                            <th class="text-center">{{__('lang.code')}}</th>
                                            <th>{{__('lang.product')}}</th>
                                            <th>{{__('lang.product_type')}}</th>
                                            <th class="text-center">{{__('lang.qty')}}</th>
                                            <th class="text-center">{{__('lang.buy_price')}}</th>
                                            <th class="text-center">{{__('lang.sale_price')}}</th>
                                            <th class="text-center">{{__('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        @endphp
                                        @foreach ($data as $item)
                                        <tr>
                                            <td class="text-center">{{$page++}}</td>
                                            <td class="text-center">
                                                @if(!empty($item->image))
                                                <a href="{{asset($item->image)}}">
                                                    <img src="{{asset($item->image)}}" alt="" style="width:100px;">
                                                </a>
                                                @else
                                                <img src="{{asset('images/logo.png')}}" alt="" style="width:100px;">
                                                @endif
                                            </td>
                                            <td class="text-center">{{$item->code}}</td>
                                            <td>
                                                {{$item->name}}<br>
                                                <!-- {!! $item->des !!} -->
                                            </td>
                                            <td>
                                                @if(Config::get('app.locale') == 'lo')
                                                @if(!empty($item->catalog->name_la))
                                                {{$item->catalog->name_la}}
                                                @endif
                                                @else
                                                @if(!empty($item->catalog->name_en))
                                                {{$item->catalog->name_en}}
                                                @endif
                                                @endif
                                            </td>
                                            <td class="text-center">{{$item->qty}}</td>
                                            <td class="text-center">{{number_format($item->import_price)}}</td>
                                            <td class="text-center">{{number_format($item->wholesale_price)}}</td>
                                            <td class="text-center">
                                                @if(auth()->user()->rolename->name == 'admin')
                                                <a href="{{route('admin.edit.p1-product', ['id' => $item->id])}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                                <button wire:click="showDestroy({{$item->id}})" type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{$data->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-delete -->
            <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('lang.delete')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                            <h4>{{__('lang.do_you_want_to_delete')}}</h4>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button wire:click="destroy" type="button" class="btn btn-danger">{{__('lang.delete')}}</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-delete -->
            <div wire:ignore.self class="modal fade" id="modal-excel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">File excel</label>
                                <input type="file" class="form-control" wire:model="file_excel">
                                @error('file_excel') <span class="error" style="color:red">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button wire:click="ImportExel" type="button" class="btn btn-success">Import Excel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>
@push('scripts')
<script>
    $('.money').simpleMoneyFormat();
    $(document).ready(function() {
        $('#product_type').select2();
        $('#product_type').on('change', function(e) {
            var data = $('#product_type').select2("val");
            @this.set('catalog_id', data);
        });
    });

    function validate(evt) {
        var theEvent = evt || window.event;
        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    window.addEventListener('show-modal-delete', event => {
        $('#modal-delete').modal('show');
    })
    window.addEventListener('hide-modal-delete', event => {
        $('#modal-delete').modal('hide');
    })
    window.addEventListener('show-modal-excel', event => {
        $('#modal-excel').modal('show');
    })
    window.addEventListener('hide-modal-excel', event => {
        $('#modal-excel').modal('hide');
    })
</script>
@endpush