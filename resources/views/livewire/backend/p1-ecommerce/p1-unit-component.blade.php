<div>
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h5><i class="fa fa-layer-group"></i>
                {{__('lang.module_order')}}
                  <i class="fa fa-angle-double-right"></i>
                  {{__('lang.unit')}}</h5>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.unit')}}</li>
              </ol>
            </div>
          </div>
        </div>
      </section>

      <section class="content">
        <div class="container-fluid">
          <div class="row">

            <!--Foram add new-->
            <div class="col-md-4">
              <div class="card card-default">

                <div class="card-header">
                  <h4>{{__('lang.add')}}</h4>
                </div>
                <form>
                    <div class="card-body">
                        <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                 <label>{{__('lang.name_la')}}</label>
                                  <input wire:model="name_la" type="text" class="form-control @error('name_la') is-invalid @enderror" placeholder="{{__('lang.name_la')}}">
                                   @error('name_la') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                 <label>{{__('lang.name_en')}}</label>
                                  <input wire:model="name_en" type="text" class="form-control @error('name_en') is-invalid @enderror" placeholder="{{__('lang.name_en')}}">
                                   @error('name_en') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-between md-2">
                            <button type="button" wire:click="resetField" class="btn btn-primary">{{__('lang.reset')}}</button>
                            <button type="button" wire:click="store" class="btn btn-success">{{__('lang.save')}}</button>
                        </div>
                    </div>

                </form>

              </div>
            </div>
            <div class="col-md-8">
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-6">
                      <h4>{{__('lang.unit')}}</h4>
                    </div>
                    <div class="col-md-6">
                      <input wire:model="search" type="text" class="form-control" placeholder="{{__('lang.search')}}">
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th  class="text-center">{{__('lang.no')}}</th>
                          <th>{{__('lang.name')}}</th>
                          <th class="text-center">{{__('lang.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        @endphp
                        @foreach ($data as $item)
                        <tr>
                          <td class="text-center">{{$page++}}</td>
                          <td>
                                @if (Config::get('app.locale') == 'lo')
                                        {{$item->name_la}}
                                @elseif (Config::get('app.locale') == 'en')
                                    {{$item->name_en}}
                                @endif
                          </td>
                          <td class="text-center">
                          @if(Auth()->user()->rolename->name == 'admin')
                              <button wire:click="edit({{$item->id}})" type="button" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></button>
                              <button wire:click="showDestroy({{$item->id}})" type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            </form>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                      {{$data->links()}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <!-- /.modal-delete -->
                <div class="modal fade" id="modal-delete">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title">{{__('lang.delete')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                        <h4>{{__('lang.do_you_want_to_delete')}}</h4>
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button wire:click="destroy" type="button" class="btn btn-danger">{{__('lang.delete')}}</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
      </section>

</div>
@push('scripts')
    <script>
      window.addEventListener('show-modal-delete', event => {
          $('#modal-delete').modal('show');
      })
      window.addEventListener('hide-modal-delete', event => {
          $('#modal-delete').modal('hide');
      })
    </script>
@endpush
