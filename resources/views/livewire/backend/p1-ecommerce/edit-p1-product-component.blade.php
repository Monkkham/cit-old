<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{ __('lang.module_order') }}
                        <i class="fa fa-angle-double-right"></i>
                        {{ __('lang.product') }}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">{{ __('lang.home') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ __('lang.product') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <h3>{{ __('lang.edit') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>{{ __('lang.currency') }}: {{ $this->currency }}</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('lang.code') }}<span style="color:red">*</span></label>
                                            <input wire:model="code" type="text" placeholder="{{ __('lang.code') }}"
                                                class="form-control @error('code') is-invalid @enderror">
                                            @error('code')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>{{ __('lang.product') }}</label>
                                            <input wire:model="name" type="text"
                                                placeholder="{{ __('lang.product') }}"
                                                class="form-control @error('name') is-invalid @enderror">
                                            @error('name')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" wire:ignore>
                                            <label>{{ __('lang.product_type') }}<span
                                                    style="color:red">*</span></label>
                                            <select wire:model="catalog_id"
                                                class="form-control @error('catalog_id') is-invalid @enderror"
                                                id="product_type">
                                                <option value="" selected>{{ __('lang.select') }}</option>
                                                @foreach ($categories as $item)
                                                    <option value="{{ $item->id }}">
                                                        @if (Config::get('app.locale') == 'lo')
                                                            {{ $item->name_la }}
                                                        @elseif(Config::get('app.locale') == 'en')
                                                            {{ $item->name_en }}
                                                        @endif
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('catalog_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('lang.buy_price') }}</label>
                                            <input wire:model="import_price" type="text" onkeypress='validate(event)'
                                                placeholder="{{ __('lang.buy_price') }}" class="form-control money">
                                            @error('import_price')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('lang.sale_price') }}</label>
                                            <input wire:model="wholesale_price" type="text" min="1000"
                                                onkeypress='validate(event)'
                                                placeholder="{{ __('lang.dearler_price') }}"
                                                class="form-control money">
                                            @error('wholesale_price')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>{{ __('lang.profit_rate') }}</label>
                                            <input wire:model="profit_rate" readonly type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('lang.online_price') }} (Kip)</label>
                                            <input wire:model="price_to_kip" readonly type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('lang.online_price') }} (Dollar)</label>
                                            <input wire:model="price_to_dollar" readonly type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" wire:ignore>
                                            <label>{{ __('lang.unit') }}<span style="color:red">*</span></label>
                                            <select wire:model="unit_name"
                                                class="form-control @error('unit_name') is-invalid @enderror"
                                                id="unit_name">
                                                <option value="" selected>{{ __('lang.select') }}</option>
                                                @foreach ($units as $item)
                                                    <option value="{{ $item->id }}">
                                                        @if (Config::get('app.locale') == 'lo')
                                                            {{ $item->name_la }}
                                                        @elseif(Config::get('app.locale') == 'en')
                                                            {{ $item->name_en }}
                                                        @endif
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('unit_name')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('lang.vat') }}</label>
                                            <input wire:model="vat" type="text" placeholder="{{ __('lang.vat') }}"
                                                class="form-control @error('cost_price') is-invalid @enderror">
                                            @error('cost_price')
                                                <span style=" color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>{{ __('lang.note') }}</label>
                                            <input wire:model="note" type="text"
                                                placeholder="{{ __('lang.note') }}" class="form-control">
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('lang.qty') }}</label>
                                            <input wire:model="qty" type="text" placeholder="{{ __('lang.qty') }}" class="form-control" onkeypress='validate(event)'>
                                            @error('qty')
    <span style="color: red" class="error">{{ $message }}</span>
@enderror
                                        </div>
                                    </div> -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('lang.min_reserve') }}</label>
                                            <input wire:model="min_reserve" type="text"
                                                placeholder="{{ __('lang.min_reserve') }}" class="form-control"
                                                onkeypress='validate(event)'>
                                            @error('min_reserve')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" wire:ignore>
                                            <label>{{ __('lang.short_des') }}</label>
                                            <textarea wire:model="des" type="text" placeholder="{{ __('lang.short_des') }}" class="form-control"
                                                id="summernote_des">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" wire:ignore>
                                            <label>{{ __('lang.long_des') }}</label>
                                            <textarea wire:model="long_des" type="text" placeholder="{{ __('lang.long_des') }}" class="form-control"
                                                id="summernote_long_des">
</textarea>
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Thumb</label>
                                            <input wire:model="thumb" type="text" placeholder="Thumb"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input wire:model="link" type="text" placeholder="Link"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                                <div class="row">
                                    <div class="control-product">
                                        @if ($image)
                                            <div class="product-image" style="border: 3px solid green;">
                                                @php
                                                    try {
                                                        $url = $image->temporaryUrl();
                                                        $status = true;
                                                    } catch (RuntimeException $exception) {
                                                        $this->status = false;
                                                    }
                                                @endphp
                                                @if ($status)
                                                    <img src="{{ $url }}" alt=""
                                                        style="width: 250px;">
                                                @endif
                                            </div>
                                            <i class="fas fa-check-circle"
                                                style="color:green;font-size:20px;padding:10px;"></i>
                                        @elseif(!empty($this->new_image))
                                            <div class="product-image" style="border: 3px solid green;">
                                                <img src="{{ asset($this->new_image) }}" alt=""
                                                    style="width: 250px;">
                                            </div>
                                            <i class="fas fa-check-circle"
                                                style="color:green;font-size:20px;padding:10px;"></i>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('lang.image') }}</label>
                                            <input wire:model="image" type="file"
                                                placeholder="{{ __('lang.image') }}"
                                                class="form-control @error('image') is-invalid @enderror">
                                            @error('image')
                                                <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div> <!-- end row -->
                                @if ($this->product_id)
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>{{ __('lang.image') }}</label>({{ __('lang.maximum_two_photo') }})
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-product">
                                                    @if ($mutiple_image)
                                                        <div class="product-image" style="border: 3px solid green;">
                                                            @php
                                                                try {
                                                                    $url = $mutiple_image->temporaryUrl();
                                                                    $status = true;
                                                                } catch (RuntimeException $exception) {
                                                                    $this->status = false;
                                                                }
                                                            @endphp
                                                            @if ($status)
                                                                <img src="{{ $url }}" alt=""
                                                                    style="width: 250px;">
                                                            @endif
                                                        </div>
                                                        <i class="fas fa-check-circle"
                                                            style="color:green;font-size:20px;padding:10px;"></i>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <button type="button" wire:click="add_product_images"
                                                                type="button" class="btn btn-primary"><i
                                                                    class="fa fa-plus"></i></button>
                                                        </div>
                                                        <input wire:model="mutiple_image" type="file"
                                                            placeholder="{{ __('lang.mutiple_image') }}"
                                                            class="form-control @error('mutiple_image') is-invalid @enderror">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @error('mutiple_image')
                                                        <span style="color: red"
                                                            class="error">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row">
                                                @foreach ($productmages as $item)
                                                    <div class="col-md-3">
                                                        <a href="{{ asset($item->image) }}">
                                                            <img src="{{ asset($item->image) }}" alt=""
                                                                style="width: 250px;height:250px;">
                                                        </a>
                                                        <button
                                                            wire:click="show_delete_productImage({{ $item->id }})"
                                                            type="button" class="btn btn-danger"><i
                                                                class="fa fa-times"></i></button>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button wire:click="updateStore" type="button"
                                class="btn btn-success">{{ __('lang.edit') }}</button>
                            <button type="button" class="btn btn-warning"
                                wire:click="close">{{ __('lang.close') }}</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h4 class="modal-title">{{ __('lang.delete') }}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" wire:model="hiddenId" value="{{ $hiddenId }}">
                            <h4>{{ __('lang.do_you_want_to_delete') }}</h4>
                        </div>
                        <input type="hidden" wire:model="$hiddenId">
                        <div class="modal-footer justify-content-between">
                            <button wire:click="deleteProductImage({{ $hiddenId }})" type="button"
                                class="btn btn-danger">{{ __('lang.delete') }}</button>
                            <button type="button" class="btn btn-primary"
                                data-dismiss="modal">{{ __('lang.close') }}</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>
@push('scripts')
    <script>
        $('.money').simpleMoneyFormat();
        $(function() {
            $('#summernote_des').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'picture', 'video']],
                    ['fm-button', ['fm']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks: {
                    onChange: function(contents, $editable) {
                        @this.set('des', contents);
                    },
                }
            });
            $('#summernote_long_des').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'picture', 'video']],
                    ['fm-button', ['fm']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks: {
                    onChange: function(contents, $editable) {
                        @this.set('long_des', contents);
                    },
                }
            });
        });
        $(document).ready(function() {
            $('#product_type').select2();
            $('#product_type').on('change', function(e) {
                var data = $('#product_type').select2("val");
                @this.set('catalog_id', data);
            });
            $('#unit_name').select2();
            $('#unit_name').on('change', function(e) {
                var data = $('#unit_name').select2("val");
                @this.set('unit_name', data);
            });
        });
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })

        function validate(evt) {
            var theEvent = evt || window.event;
            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endpush
