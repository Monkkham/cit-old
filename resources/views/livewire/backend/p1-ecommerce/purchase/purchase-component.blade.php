<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.order_product_warehouse')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.order_product_warehouse')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-default">
                        <form>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>{{__('lang.distributor')}}</label>
                                        <div class="input-group" wire:ignore>
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
                                            </div>
                                            <select wire:model="contact_id" class="form-control" id="selectContact">
                                                <option value="">{{__('lang.select')}}</option>
                                                @foreach($contacts as $item)
                                                <option value="{{$item->id}}">{{ $item->name }} {{ $item->phone }}
                                                </option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-prepend">
                                                <button type="button" wire:click="create" class="btn btn-info"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        @error('contact_id') <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{__('lang.date_of_order')}}</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar-alt"></i>
                                                    </span>
                                                </div>
                                                <input type="date" class="form-control float-right" id="reservation" wire:model="purchase_date">
                                                @error('purchase_date') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="col-md-4" wire:ignore>
                                        <label>{{__('lang.branch')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
                                            </div>
                                            <select wire:model="branch_id" class="form-control @error('branch_id') is-invalid @enderror" id="branch_id">
                                                <option value="" selected>{{__('lang.select')}}</option>
                                                @foreach($branches as $item)
                                                <option value="{{ $item->id }}">
                                                    @if (Config::get('app.locale') == 'lo')
                                                    {{$item->name_la}}
                                                    @elseif (Config::get('app.locale') == 'en')
                                                    {{$item->name_en}}
                                                    @endif
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('branch_id') <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">{{__('lang.attached_file')}}</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" wire:model="attacth_file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                            </div>
                                        </div>
                                        @if($this->attacth_file)
                                        <i class="fa fa-check-circle text-success"></i>{{$this->attacth_file}}
                                        @endif
                                        @error('attacth_file') <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-2">
                                        <label>{{__('lang.status')}}</label>
                                        <div class="form-group">
                                            <select wire:model="status" class="form-control">
                                                <option value="order" class="text-primary">{{__('lang.order_status')}}</option>
                                                <option value="waiting" class="text-warning">{{__('lang.waiting')}}</option>
                                                <option value="received" class="text-success">{{__('lang.received')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" wire:ignore>
                                        <label>{{__('lang.product')}}</label>
                                        <select wire:model="pro_id" class="form-control" id="pro_id">
                                            <option value="">{{__('lang.select')}}</option>
                                            @foreach($products as $item)
                                            <option value="{{ $item->id }}">{{__('lang.code')}}:{{ $item->code }}, {{ $item->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('pro_id') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{__('lang.no')}}</th>
                                            <th>{{__('lang.image')}}</th>
                                            <th>{{__('lang.product')}}</th>
                                            <th class="text-center">{{__('lang.qty')}}</th>
                                            <th>{{__('lang.price')}}</th>
                                            <th>{{__('lang.amount')}}</th>
                                            <th>{{__('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    @php
                                    $i = 1;
                                    @endphp
                                    <tbody>
                                        @foreach (Cart::instance('import_order_cart')->content() as $item)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td class="text-center">
                                                @if(!empty($item->model->image))
                                                <a href="{{asset($item->model->image)}}">
                                                    <img src="{{asset($item->model->image)}}" alt="" style="width:50px;">
                                                </a>
                                                @else
                                                <img src="{{asset('images/logo.png')}}" alt="" style="width:50px;">
                                                @endif
                                            </td>
                                            <td>{{$item->model->name}}</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-danger" wire:click="decreaseQty('{{$item->rowId}}')"><i class="fa fa-minus"></i></a>
                                                <input type="text" value="{{$item->qty}}" style="border-radius: 5px;width:100px;height:38px;text-align:center;" disabled>
                                                <a href="javascript:void(0)" class="btn btn-primary" wire:click="increaseQty('{{$item->rowId}}')"><i class="fa fa-plus"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-warning" wire:click="showeditCart('{{$item->rowId}}')"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td>{{number_format($item->model->import_price)}}</td>
                                            <td>{{number_format($item->qty * $item->model->import_price)}}</td>
                                            <td><a href="#" class="btn btn-danger" wire:click="showdestroy('{{$item->rowId}}')"><i class="fa fa-times"></i></a></td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-right"><b>{{__('lang.total')}}</b></td>
                                            <td><b>{{number_format($this->subtotal)}}</b></td>
                                            <td></td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3><i class="fa fa-credit-card text-info"></i> {{__('lang.payment')}}</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div>
                                        <h3><b>{{__('lang.total')}}: {{number_format($this->subtotal)}}</b></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">{{__('lang.amount_money')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <button type="button" class="btn btn-default"> <i class="fas fa-money-bill-alt text-success"></i></button>
                                        </div>
                                        <input type="text" class="form-control money" placeholder="{{__('lang.amount_money')}}" onkeypress="validate(event)" wire:model="paid">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="#" class="btn btn-primary" wire:click="placeOrder"><i class="fa fa-shopping-cart"></i> {{__('lang.order_now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- /.modal-delete -->
            <div wire:ignore.self class="modal fade" id="modal-add">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h4 class="modal-title">{{__('lang.add')}}{{__('lang.distributor')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="card-body">
                                    <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{__('lang.name')}}</label>
                                                <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="{{__('lang.name')}}">
                                                @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{__('lang.phone')}}</label>
                                                <input wire:model="phone" type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="{{__('lang.phone')}}">
                                                @error('phone') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{__('lang.email')}}</label>
                                                <input wire:model="email" type="text" class="form-control @error('email') is-invalid @enderror" placeholder="{{__('lang.email')}}">
                                                @error('email') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{__('lang.detail')}}</label>
                                                <textarea wire:model="more_detail" type="text" class="form-control @error('more_detail') is-invalid @enderror" placeholder="{{__('lang.detail')}}"></textarea>
                                                @error('more_detail') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('lang.close')}}</button>
                            <button type="button" class="btn btn-primary" wire:click="storeContact">{{__('lang.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-delete -->
            <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('lang.delete')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                            <h4>{{__('lang.do_you_want_to_delete')}}</h4>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button wire:click="destroy('{{$this->hiddenId}}')" type="button" class="btn btn-danger">{{__('lang.delete')}}</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-edit -->
            <div wire:ignore.self class="modal fade" id="modal-edit-cart">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h4 class="modal-title">{{__('lang.edit')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                            <div class="form-group">
                                <label for="">{{__('lang.qty')}}</label>
                                <input type="text" class="form-control" wire:model="qty" onkeypress="validate(event)">
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button wire:click="updateCart('{{$this->hiddenId}}')" type="button" class="btn btn-warning"><i class="fa fa-edit"></i> {{__('lang.edit')}}</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @push('scripts')
    <script>
        $('.money').simpleMoneyFormat();
        $(document).ready(function() {
            $('#selectContact').select2();
            $('#selectContact').on('change', function(e) {
                var data = $('#selectContact').select2("val");
                @this.set('contact_id', data);
            })
            $('#pro_id').select2();
            $('#pro_id').on('change', function(e) {
                var data = $('#pro_id').select2("val");
                @this.set('pro_id', data);
            });
        })

        function validate(evt) {
            var theEvent = evt || window.event;
            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        window.addEventListener('show-modal-add', event => {
            $('#modal-add').modal('show');
        })
        window.addEventListener('hide-modal-add', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-edit-cart', event => {
            $('#modal-edit-cart').modal('show');
        })
        window.addEventListener('hide-modal-edit-cart', event => {
            $('#modal-edit-cart').modal('hide');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-add').modal('hide');
        })
        window.addEventListener('show-modal-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-modal-delete', event => {
            $('#modal-delete').modal('hide');
        })
    </script>
    @endpush
</div>