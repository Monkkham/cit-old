<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.order_product_warehouse')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.order_list_of_warehouse')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.order_list_of_warehouse')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-default">
                        <form>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>{{__('lang.distributor')}}</label>
                                        <div class="input-group" wire:ignore>
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
                                            </div>
                                            <select wire:model="contact_id" class="form-control" id="selectContact">
                                                <option value="">{{__('lang.select')}}</option>
                                                @foreach($contacts as $item)
                                                <option value="{{$item->id}}">{{ $item->name }} {{ $item->phone }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('contact_id') <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-4" wire:ignore>
                                        <label>{{__('lang.branch')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
                                            </div>
                                            <select wire:model="branch_id" class="form-control @error('branch_id') is-invalid @enderror" id="branch_id">
                                                <option value="" selected>{{__('lang.select')}}</option>
                                                @foreach($branches as $item)
                                                <option value="{{ $item->id }}">
                                                    @if (Config::get('app.locale') == 'lo')
                                                    {{$item->name_la}}
                                                    @elseif (Config::get('app.locale') == 'en')
                                                    {{$item->name_en}}
                                                    @endif
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('branch_id') <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-2">
                                        <label>{{__('lang.status')}}</label>
                                        <div class="form-group">
                                            <select wire:model="status" class="form-control">
                                                <option value="" selected>{{__('lang.all')}}</option>
                                                <option value="order" class="text-primary">{{__('lang.order_status')}}</option>
                                                <option value="waiting" class="text-warning">{{__('lang.waiting')}}</option>
                                                <option value="received" class="text-success">{{__('lang.received')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>{{__('lang.payment_status')}}</label>
                                        <div class="form-group">
                                            <select wire:model="payment_status" class="form-control">
                                                <option value="" selected>{{__('lang.all')}}</option>
                                                <option value="due" class="text-danger">{{__('lang.due')}}</option>
                                                <option value="partial" class="text-primary">{{__('lang.partial')}}</option>
                                                <option value="paid" class="text-success">{{__('lang.payment_success')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>{{__('lang.start_date')}}</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" wire:model="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>{{__('lang.end_date')}}</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" wire:model="end_date">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-9">
                                        <h4><i class="fa fa-shopping-cart text-default"></i> {{__('lang.order_list_of_warehouse')}}</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" wire:model="search" class="form-control" placeholder="{{__('lang.code')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>{{__('lang.no')}}</th>
                                            <th>{{__('lang.created_at')}}</th>
                                            <th>{{__('lang.code')}}</th>
                                            <th>{{__('lang.distributor')}}</th>
                                            <th>{{__('lang.total')}}</th>
                                            <th>{{__('lang.paid')}}</th>
                                            <th>{{__('lang.branch')}}</th>
                                            <th>{{__('lang.status')}}</th>
                                            <th>{{__('lang.payment_status')}}</th>
                                            <th>{{__('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    @php
                                    $i = 1;
                                    @endphp
                                    <tbody>
                                        @foreach ($data as $item)
                                        <tr class="text-center">
                                            <td>{{$page++}}</td>
                                            <td>{{date('d/m/Y H:i:s', strtotime($item->created_at))}}</td>
                                            <td><b>{{$item->code}}</b></td>
                                            <td>{{$item->contact->name}}</td>
                                            <td><b>{{number_format($item->amount)}}</b></td>
                                            <td>
                                                @if($item->paid_amount > 0 && $item->amount == $item->paid_amount)
                                                <p class="text-success"><b>{{number_format($item->paid_amount)}}</b></p>
                                                @else
                                                <p class="text-danger"><b>{{number_format($item->paid_amount)}}</b></p>
                                                @endif
                                            </td>
                                            <td>
                                                @if(Config::get('app.locale') == 'lo')
                                                {{$item->branch->name_la}}
                                                @else
                                                {{$item->branch->name_en}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->status =='order')
                                                <p class="text-primary">{{__('lang.order_status')}}</p>
                                                @elseif($item->status =='waiting')
                                                <p class="text-warning">{{__('lang.waiting')}}</p>
                                                @else
                                                <p class="text-success">{{__('lang.received')}}</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->t_status =='due')
                                                <p class="text-danger">{{__('lang.due')}}</p>
                                                @elseif($item->t_status =='partial')
                                                <p class="text-warning">{{__('lang.partial')}}</p>
                                                @else
                                                <p class="text-success">{{__('lang.paid')}}</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2)
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                                        {{__('lang.action')}}
                                                    </button>
                                                    <div class="dropdown-menu " role="menu">
                                                        <a class="dropdown-item" href="javascript:void(0)" wire:click="showDetail({{$item->id}})"><i class="fas fa-address-card text-primary"></i>
                                                            {{__('lang.detail')}}</a>
                                                        <!-- <a class="dropdown-item" href="javascript:void(0)" wire:click="showEdit({{$item->id}})"><i class="fas fa-edit text-warning"></i>
                                                            {{__('lang.edit')}}</a> -->

                                                        <a class="dropdown-item" href="javascript:void(0)" wire:click="showConfirm({{$item->id}})"><i class="fa fa-check-circle text-success"></i>
                                                            {{__('lang.edit')}}</a>

                                                    </div>
                                                    @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{$data->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-add product -->
            <div wire:ignore.self class="modal fade" id="modal-detail">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h4 class="modal-title"><i class="fas fa-address-card"> </i> {{__('lang.detail')}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-user"></i> {{__('lang.distributor')}}</h3>
                                        </div>
                                        <div class="card-body">
                                            <table>

                                                <tr>
                                                    <td><b>{{__('lang.name')}}:</b></td>
                                                    <td>
                                                        @if(!empty($this->transactionData->order->contact->name))
                                                        {{$this->transactionData->order->contact->name}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.phone')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->phone))
                                                        {{$this->transactionData->order->contact->phone}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.email')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->email))
                                                        {{$this->transactionData->order->contact->email}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.des')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->more_detail))
                                                        {{$this->transactionData->order->contact->more_detail}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-shopping-cart"></i> {{__('lang.order')}}
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td><b>{{__('lang.orderid')}}:</b></td>
                                                    <td>
                                                        <b> @if(!empty($this->transactionData->order->code))
                                                            {{$this->transactionData->order->code}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.total')}}:</b></td>
                                                    <td>
                                                        <b>@if(!empty($this->transactionData->amount))
                                                            {{number_format($this->transactionData->amount)}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.paid')}}:</b></td>
                                                    <td class="text-success">
                                                        <b> @if(!empty($this->transactionData->paid_amount))
                                                            {{number_format($this->transactionData->paid_amount)}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>
                                                            <p><i class="fas fa-money-bill-alt text-primary"></i>{{__('lang.payment_status')}}:</p>
                                                        </b></td>
                                                    <td class="text-success">
                                                        @if(!empty($this->transactionData->status))
                                                        <b> @if($this->transactionData->status =='due')
                                                            <p class="text-danger">{{__('lang.due')}}</p>
                                                            @elseif($this->transactionData->status =='partial')
                                                            <p class="text-warning">{{__('lang.partial')}}</p>
                                                            @else
                                                            <p class="text-success">{{__('lang.paid')}}</p>
                                                            @endif
                                                        </b>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($this->transactionData->order->status))
                            <div class="row">
                                <div class="col-md-3">
                                    @if($this->transactionData->order->status =='order')
                                    <h4 class="text-primary"> <i class="fa fa-shopping-cart"></i> {{__('lang.status')}}: {{__('lang.order_status')}}</h4>
                                    @elseif($this->transactionData->order->status =='waiting')
                                    <h4 class="text-warning"><i class="fa fa-truck"></i> {{__('lang.status')}}: {{__('lang.waiting')}}</h4>
                                    @else
                                    <h4 class="text-success"><i class="fa fa-check-circle"></i> {{__('lang.status')}}: {{__('lang.received')}}</h4>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">{{__('lang.no')}}</th>
                                                <th class="text-center">{{__('lang.image')}}</th>
                                                <th>{{__('lang.product')}}</th>
                                                <th>{{__('lang.product_type')}}</th>
                                                <th class="text-center">{{__('lang.qty')}}</th>
                                                <th>{{__('lang.price')}}</th>
                                                <th>{{__('lang.total')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($this->orderdetailData as $item)
                                            <tr>
                                                <td class="text-center">{{$i++}}</td>
                                                <td class="text-center">
                                                    @if(!empty($item->product->image))
                                                    <img src="{{asset($item->product->image)}}" alt="" style="width:100px;">
                                                    @else
                                                    <img src="{{asset('images/logo.png')}}" alt="" style="width:100px;">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($item->product->name))
                                                    {{$item->product->name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (Config::get('app.locale') == 'lo')
                                                    @if(!empty($item->product->catalog->name_la))
                                                    {{$item->product->catalog->name_la}}
                                                    @endif
                                                    @elseif (Config::get('app.locale') == 'en')
                                                    @if(!empty($item->product->catalog->name_en))
                                                    {{$item->product->catalog->name_en}}
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="text-center">{{$item->qty}}</td>
                                                <td>{{number_format($item->product_price)}}</td>
                                                <td>{{number_format($item->total_price)}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- /.modal-add product -->
            <div wire:ignore.self class="modal fade" id="modal-confirm">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h4 class="modal-title"><i class="fas fa-shopping-cart"> </i> {{__('lang.order')}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-user"></i> {{__('lang.distributor')}}</h3>
                                        </div>
                                        <div class="card-body">
                                            <table>

                                                <tr>
                                                    <td><b>{{__('lang.name')}}:</b></td>
                                                    <td>
                                                        @if(!empty($this->transactionData->order->contact->name))
                                                        {{$this->transactionData->order->contact->name}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.phone')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->phone))
                                                        {{$this->transactionData->order->contact->phone}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.email')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->email))
                                                        {{$this->transactionData->order->contact->email}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.des')}}:</b></td>
                                                    <td>@if(!empty($this->transactionData->order->contact->more_detail))
                                                        {{$this->transactionData->order->contact->more_detail}}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-shopping-cart"></i> {{__('lang.order')}}
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td><b>{{__('lang.orderid')}}:</b></td>
                                                    <td>
                                                        <b> @if(!empty($this->transactionData->order->code))
                                                            {{$this->transactionData->order->code}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.total')}}:</b></td>
                                                    <td>
                                                        <b>@if(!empty($this->transactionData->amount))
                                                            {{number_format($this->transactionData->amount)}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.paid')}}:</b></td>
                                                    <td class="text-success">
                                                        <b> @if(!empty($this->transactionData->paid_amount))
                                                            {{number_format($this->transactionData->paid_amount)}}
                                                            @endif</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>
                                                            <p><i class="fas fa-money-bill-alt text-primary"></i>{{__('lang.payment_status')}}:</p>
                                                        </b></td>
                                                    <td class="text-success">
                                                        @if(!empty($this->transactionData->status))
                                                        <b> @if($this->transactionData->status =='due')
                                                            <p class="text-danger">{{__('lang.due')}}</p>
                                                            @elseif($this->transactionData->status =='partial')
                                                            <p class="text-warning">{{__('lang.partial')}}</p>
                                                            @else
                                                            <p class="text-success">{{__('lang.paid')}}</p>
                                                            @endif
                                                        </b>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($this->transactionData->order->status))
                            <div class="row">
                                <div class="col-md-3">
                                    @if($this->transactionData->order->status =='order')
                                    <h4 class="text-primary"> <i class="fa fa-shopping-cart"></i> {{__('lang.status')}}: {{__('lang.order_status')}}</h4>
                                    @elseif($this->transactionData->order->status =='waiting')
                                    <h4 class="text-warning"><i class="fa fa-truck"></i> {{__('lang.status')}}: {{__('lang.waiting')}}</h4>
                                    @else
                                    <h4 class="text-success"><i class="fa fa-check-circle"></i> {{__('lang.status')}}: {{__('lang.received')}}</h4>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">{{__('lang.no')}}</th>
                                                    <th class="text-center">{{__('lang.image')}}</th>
                                                    <th>{{__('lang.product')}}</th>
                                                    <th>{{__('lang.product_type')}}</th>
                                                    <th class="text-center">{{__('lang.qty')}}</th>
                                                    <th>{{__('lang.price')}}</th>
                                                    <th>{{__('lang.total')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 1;
                                                @endphp
                                                @foreach($this->orderdetailData as $item)
                                                <tr>
                                                    <td class="text-center">{{$i++}}</td>
                                                    <td class="text-center">
                                                        @if(!empty($item->product->image))
                                                        <img src="{{asset($item->product->image)}}" alt="" style="width:100px;">
                                                        @else
                                                        <img src="{{asset('images/logo.png')}}" alt="" style="width:100px;">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($item->product->name))
                                                        {{$item->product->name}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (Config::get('app.locale') == 'lo')
                                                        @if(!empty($item->product->catalog->name_la))
                                                        {{$item->product->catalog->name_la}}
                                                        @endif
                                                        @elseif (Config::get('app.locale') == 'en')
                                                        @if(!empty($item->product->catalog->name_en))
                                                        {{$item->product->catalog->name_en}}
                                                        @endif
                                                        @endif
                                                    </td>
                                                    <td class="text-center">{{$item->qty}}</td>
                                                    <td>{{number_format($item->product_price)}}</td>
                                                    <td>{{number_format($item->total_price)}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if($this->subtotal > 0)
                            <div class="row">
                                <script>
                                    $('.money').simpleMoneyFormat();
                                </script>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3><i class="fa fa-credit-card text-info"></i> {{__('lang.payment')}}</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div>
                                                        <h3><b>{{__('lang.total')}}: {{number_format($this->subtotal)}}</b></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(!empty($this->subtotal - $this->transactionData->paid_amount))
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div>
                                                        <h3 class="text-danger"><b>{{__('lang.due')}}: {{number_format($this->subtotal - $this->transactionData->paid_amount)}}</b></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">{{__('lang.amount_money')}}{{__('lang.due')}}</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <button type="button" class="btn btn-default"> <i class="fas fa-money-bill-alt text-success"></i></button>
                                                        </div>
                                                        <input type="text" class="form-control money" placeholder="{{__('lang.amount_money')}}" onkeypress="validate(event)" wire:model="paid">
                                                    </div>
                                                    @error('paid') <span style="color: red" class="error">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                @if(!empty($this->transactionData->order->status) && $this->transactionData->order->status !='received')
                                                <div class="col-md-2">
                                                    <label>{{__('lang.status')}}</label>
                                                    <div class="form-group">
                                                        <select wire:model="confirm_status" class="form-control">
                                                            <option value="" class="text-primary">{{__('lang.status')}}</option>
                                                            <option value="order" class="text-primary">{{__('lang.order_status')}}</option>
                                                            <option value="waiting" class="text-warning">{{__('lang.waiting')}}</option>
                                                            <option value="received" class="text-success">{{__('lang.received')}}</option>
                                                        </select>
                                                        @error('confirm_status') <span style="color: red" class="error">{{ $message }}</span>
                                                    @enderror
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            @if($this->confirm_status == 'received')
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-primary"><b>{{__('lang.note')}}: {{__('lang.note_received_purchase')}}</b></p>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">{{__('lang.close')}}</button>
                            <button type="button" class="btn btn-primary" wire:click="confirmOrder">{{__('lang.edit')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
        </div>
    </section>
    @push('scripts')
    <script>
        $(document).ready(function() {
            $('#selectContact').select2();
            $('#selectContact').on('change', function(e) {
                var data = $('#selectContact').select2("val");
                @this.set('contact_id', data);
            })
        })

        function validate(evt) {
            var theEvent = evt || window.event;
            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        window.addEventListener('show-modal-detail', event => {
            $('#modal-detail').modal('show');
        })
        window.addEventListener('hide-modal-detail', event => {
            $('#modal-detail').modal('hide');
        })
        window.addEventListener('show-modal-confirm', event => {
            $('#modal-confirm').modal('show');
        })
        window.addEventListener('hide-modal-confirm', event => {
            $('#modal-confirm').modal('hide');
        })
    </script>
    @endpush
</div>