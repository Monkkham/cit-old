<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.module_order')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.setting_online_price')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.exchange')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!--Foram add new-->
                <div class="col-md-12">
                    <div class="card card-default">

                        <div class="card-header">
                            <h4>{{__('lang.setting_online_price')}}</h4>
                        </div>
                        <form>
                            <div class="card-body">
                                <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.currency')}}</label>
                                            <select wire:model="currencies_id" type="text"
                                                class="form-control @error('currencies_id') is-invalid @enderror">
                                                @foreach($currencies as $item)
                                                <option value="{{$item->id}}">
                                                    {{$item->symbol}}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('currencies_id') <span style="color: red"
                                                class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ເຄື່ອງໝາຍສໍາລັບ Dollar</label>
                                            <select wire:model="rate_to_dollar_operator" class="form-control">
                                                <option value="/">/</option>
                                                <option value="*">*</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>{{__('lang.rate_to_dollar')}}</label>
                                            <input wire:model="rate_to_dollar" type="text"
                                                class="form-control @error('rate_to_dollar') is-invalid @enderror"
                                                placeholder="{{__('lang.rate_to_dollar')}}">
                                            @error('rate_to_dollar') <span style="color: red"
                                                class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ເຄື່ອງໝາຍສໍາລັບ Lak</label>
                                            <select wire:model="rate_to_kip_operator" class="form-control">
                                                <option value="/">/</option>
                                                <option value="*">*</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>{{__('lang.rate_to_kip')}}</label>
                                            <input wire:model="rate_to_kip" type="text"
                                                class="form-control @error('rate_to_kip') is-invalid @enderror"
                                                placeholder="{{__('lang.rate_to_kip')}}">
                                            @error('rate_to_kip') <span style="color: red"
                                                class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{__('lang.profit_rate')}}</label>
                                            <input wire:model="profit_rate" type="text"
                                                class="form-control @error('profit_rate') is-invalid @enderror"
                                                placeholder="{{__('lang.profit_rate')}}">
                                            @error('profit_rate') <span style="color: red"
                                                class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                   <div class="col-md-12 ml-4">
                                   <h6>
                                        ກໍລະນີແປງຈາກ 100 {{$this->currency_name}} ເປັນ Dollar = (100 *
                                        {{$this->profit_rate}})
                                        {{$this->rate_to_dollar_operator}} {{$this->rate_to_dollar}} =
                                        @if($this->rate_to_dollar_operator == '*')
                                        @if(is_numeric($this->rate_to_dollar)&&is_numeric($this->profit_rate))
                                        {{ceil((100*$this->profit_rate) * $this->rate_to_dollar)}} @else 0 @endif Dollar
                                        @else @if(is_numeric($this->rate_to_dollar)&&is_numeric($this->profit_rate))
                                        {{ceil((100*$this->profit_rate)  / is_numeric($this->rate_to_dollar) ? $this->rate_to_dollar : 1)}} @else 0 @endif
                                        Dollar @endif
                                    </h6>
                                    <br>
                                    <h6>
                                        ກໍລະນີແປງຈາກ 100 {{$this->currency_name}} ເປັນ LAK = (100 *
                                        {{$this->profit_rate}})
                                        {{$this->rate_to_kip_operator}} {{$this->rate_to_kip}} =
                                        @if($this->rate_to_kip_operator == '*')
                                        @if(is_numeric($this->rate_to_kip)&&is_numeric($this->profit_rate))
                                        {{ceil(((100*$this->profit_rate) * $this->rate_to_kip)/1000)*1000}} @else 0 @endif LAK @else
                                        @if(is_numeric($this->rate_to_kip)&&is_numeric($this->profit_rate))
                                        {{ceil(((100*$this->profit_rate)  / $this->rate_to_kip)/1000)*1000}} @else 0 @endif LAK
                                        @endif
                                    </h6>
                                   </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between md-2">
                                    <button type="button" wire:click="resetField"
                                        class="btn btn-primary">{{__('lang.reset')}}</button>
                                    <button type="button" wire:click="store"
                                        class="btn btn-success">{{__('lang.save')}}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
    </section>
</div>