<div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="fa fa-layer-group"></i>
                        {{__('lang.module_order')}}
                        <i class="fa fa-angle-double-right"></i>
                        {{__('lang.cancelorder_component')}}
                    </h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.cancelorder_component')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">

                                </div>
                                <div class="col-md-4">
                                    <input wire:model="search" type="text" class="form-control"
                                        placeholder="{{__('lang.search')}}">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{__('lang.no')}}</th>
                                            <th class="text-center">{{__('lang.code')}}</th>
                                            <th class="text-center">{{__('lang.customer')}}</th>
                                            <th class="text-center">{{__('lang.qty')}}</th>
                                            <th class="text-center">{{__('lang.shipping_cost')}}</th>
                                            <th class="text-center">{{__('lang.vat')}}</th>
                                            <th class="text-center">{{__('lang.total')}}</th>
                                            <th class="text-center">{{__('lang.status')}}</th>
                                            <th class="text-center">{{__('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        @endphp
                                        @foreach ($data as $item)
                                        <tr>
                                            <td class="text-center">{{$page++}}</td>
                                            <td class="text-center">
                                                @if(!empty($item->code))
                                                <b> {{$item->code}}</b>
                                                @endif
                                            </td>
                                            <td class="text-center text-info">
                                                @if(!empty($item->customer->name))
                                                <i class="fa fa-user text-info"></i> {{$item->customer->name}}
                                                @endif
                                                @if(!empty($item->customer->phone))
                                                <i class="fa fa-phone text-info"></i> {{$item->customer->phone}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                {{$item->total_qty}}
                                            </td>
                                            <td class="text-center">
                                                {{number_format($item->shipping_cost)}}
                                            </td>
                                            <td class="text-center">
                                                {{number_format($item->vat)}}
                                            </td>
                                            <td class="text-center">
                                                <p class="text-primary"><b>{{number_format($item->total_price)}}
                                                        @if(!empty($item->paid_type)){{$item->paid_type}}
                                                        @endif</b></p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-danger"><b>{{__('lang.cancel')}}</b></p>
                                            </td>
                                            <td class="text-center">
                                                @if(Auth()->user()->rolename->name == 'admin')
                                                <div class="btn-group dropleft">
                                                    <button type="button"
                                                        class="btn btn-info btn-sm dropdown-toggle dropdown-icon"
                                                        data-toggle="dropdown">
                                                        {{__('lang.action')}}
                                                    </button>
                                                    <div class="dropdown-menu " role="menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"
                                                            wire:click="showDetail({{$item->id}})"><i
                                                                class="fas fa-address-card text-primary"></i>
                                                            {{__('lang.detail')}}</a>
                                                    </div>
                                                 @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{$data->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-add product -->
            <div wire:ignore.self class="modal fade" id="modal-detail">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h4 class="modal-title"><i class="fas fa-address-card"> </i> {{__('lang.detail')}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-user"></i> {{__('lang.customer')}}</h3>
                                        </div>
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td><b>{{__('lang.code')}}:</b></td>
                                                    <td>
                                                        @if(!empty($this->orderData->code))
                                                        {{$this->orderData->code}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.name')}}:</b></td>
                                                    <td>
                                                        @if(!empty($this->orderData->customer->name))
                                                        {{$this->orderData->customer->name}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.phone')}}:</b></td>
                                                    <td>@if(!empty($this->orderData->customer->phone))
                                                        {{$this->orderData->customer->phone}}
                                                        @endif</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="car-header">
                                            <h3 class="p-2"><i class="fa fa-shopping-cart"></i> {{__('lang.order')}}
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td><b>{{__('lang.product_cost')}}:</b></td>
                                                    <td>
                                                        @php
                                                        $c = 0;
                                                        foreach($this->transactionData as $item){
                                                        $c += $item->sub_total_price;
                                                        }
                                                        @endphp
                                                        {{number_format($c)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.shipping_cost')}}:</b></td>
                                                    <td>
                                                        @php
                                                        $a = 0;
                                                        foreach($this->transactionData as $item){
                                                        $a += $item->shipping_cost;
                                                        }
                                                        @endphp
                                                        {{number_format($a)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.vat')}}:</b></td>
                                                    <td>
                                                        @php
                                                        $b = 0;
                                                        foreach($this->transactionData as $item){
                                                        $b += $item->vat;
                                                        }
                                                        @endphp
                                                        {{number_format($b)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>{{__('lang.total')}}:</b></td>
                                                    <td>@if(!empty($this->orderData->total_price))
                                                        <b>{{number_format($this->orderData->total_price)}}</b>
                                                        @endif
                                                        @if(!empty($this->orderData->paid_type))
                                                        <b>{{$this->orderData->paid_type}}</b>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>{{__('lang.no')}}</th>
                                                <th>{{__('lang.image')}}</th>
                                                <th>{{__('lang.product')}}</th>
                                                <th>{{__('lang.product_type')}}</th>
                                                <th>{{__('lang.qty')}}</th>
                                                <th>{{__('lang.totals')}}{{__('lang.price')}}(Kip)</th>
                                                <th>{{__('lang.totals')}}{{__('lang.price')}}(USA)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($this->orderdetailData as $item)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>
                                                    @if(!empty($item->product->image))
                                                    <img src="{{asset($item->product->image)}}" alt=""
                                                        style="width:100px;">
                                                    @else
                                                    <img src="{{asset('images/logo.png')}}" alt="" style="width:100px;">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($item->product->name))
                                                    {{$item->product->name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (Config::get('app.locale') == 'lo')
                                                    @if(!empty($item->product->catalog->name_la))
                                                    {{$item->product->catalog->name_la}}
                                                    @endif
                                                    @elseif (Config::get('app.locale') == 'en')
                                                    @if(!empty($item->product->catalog->name_en))
                                                    {{$item->product->catalog->name_en}}
                                                    @endif
                                                    @endif
                                                </td>
                                                <td>{{$item->qty}}</td>
                                                <td>{{number_format($item->total_online_price_kip)}}</td>
                                                <td>{{number_format($item->total_online_price_dollar)}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="car-header bg-danger">
                                            <h3 class="p-2"> {{__('lang.address')}}{{__('lang.customer')}}
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            @if(!empty($this->orderData->lat) ||
                                            !empty($this->orderData->lng))
                                            <div class="text-center">
                                                <iframe
                                                    src="https://maps.google.com/maps?q={{ $this->orderData->lat }},{{ $this->orderData->lng }}&hl=es;z=13&output=embed"
                                                    style="width:100%;height:400px;"></iframe>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-right">
                            <button type="button" class="btn btn-primary"
                                data-dismiss="modal">{{__('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
        </div>
    </section>

</div>
@push('scripts')
<script>
window.addEventListener('show-modal-detail', event => {
    $('#modal-detail').modal('show');
})
window.addEventListener('hide-modal-detail', event => {
    $('#modal-detail').modal('hide');
})
</script>
@endpush