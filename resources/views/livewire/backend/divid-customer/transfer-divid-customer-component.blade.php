<section class="content-header">
<style>
        .bank-profile{
            position: relative;
            width: 250px;
            height: 250px;
            overflow: hidden;
            cursor: pointer;
        }
        .bank-profile img {
            position: relative;
            top:0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style> 
<div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.transfer_money_customer')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.transfer_money_customer')}}</li>
              </ol>
            </div>
          </div>
        </div>
        <section class="content">
        <div class="container-fluid">
          <div class="row">
           <div class="col-md-12">
              <div class="card card-default">
                <div class="card-header">
                  <button class="btn btn-success" >{{__('lang.transfer_money')}}: {{number_format($this->request_money)}} {{__('lang.lak')}}</button>
                </div><br>
                <div class="select-bank-image" style=" display: flex;align-items: center;justify-content: center;flex-direction: column;" >
                        @if($this->bcelone_image)
                          <div class="bank-profile">    
                              <img src="{{asset('images/qrcode')}}/{{$this->bcelone_image}}" style="border: 4px solid #007bff;" alt="" ><br>
                          </div>
                        @endif
                </div>
                <div class="text-center mt-4">
                      <h1 style="color: red;"><b>{{__('lang.acccode')}}:&nbsp;{{$this->balance_id}}</b></h1>
                </div>
                <form wire:submit.prevent="Transfermoney">
                    <div class="card-body">
                        @if(Session::has('balance_no_enough'))
                            <div class="alert alert-success text-center">
                                {{__('blog.yourbalance_no_enaough')}}
                            </div>
                       @endif
                         <div class="select-bank-image" style=" display: flex;align-items: center;justify-content: center;flex-direction: column;" >
                          @if($image)
                                    @php
                                                try {
                                                $url = $image->temporaryUrl();
                                                $status = true;
                                                }catch (RuntimeException $exception){
                                                    $this->status =  false;
                                                }
                                            @endphp
                                            @if($status)
                                            <div class="bank-profile">    
                                                <img src="{{ $url }}" alt="" style="border: 5px solid green;"><br>
                                            </div><br>
                                            @endif
                                            <i class="fas fa-check-circle" style="color:green;font-size:30px;"></i>
                              @endif
                         </div>
                        <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label><i class="fas fa-camera"></i> {{__('lang.attached_photo')}}</label>
                                <input wire:model="image" type="file"  class="form-control @error('image') is-invalid @enderror">
                                @error('image') <span style="color: red" class="error">{{ $message }}</span> @enderror
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                             <button type="submit" class="btn btn-primary">{{__('lang.confirm')}}</button>
                             <a class="btn btn-warning" href="{{route('admin.withrawingcustomer')}}" >{{__('lang.back')}}</a>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
     </div>
     </section>
</section>
