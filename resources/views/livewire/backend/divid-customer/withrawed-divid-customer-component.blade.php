
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.history_withraw_customer')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.history_withraw_customer')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
          @if(Session::has('message_confirmpaymentbefore'))
              <div class="col-md-12 text-center">
                <a href="{{ route('admin.transfercustomer')}}"  class="btn btn-block btn-danger font-weight-bold py-3">{{Session::get('message_confirmpaymentbefore')}}</a>
              </div>
          @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>{{__('lang.no')}}</th>
                    <th>{{__('lang.attached_photo')}}</th>
                    <th>{{__('lang.code')}}{{__('lang.customer')}}</th>
                    <th>{{__('lang.name')}}</th>
                    <th>{{__('lang.phone')}}</th>
                    <th>{{__('lang.email')}}</th>
                    <th>{{__('lang.withrawed')}}</th>
                    <th>{{__('lang.date')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $i = 1;
                    @endphp
                    @foreach($historywithrawal as $item)
                        <tr>
                           <td>{{$i++}}</td>
                           <td> 
                             <a href="{{asset('upload/transfermoneytocustomer')}}/{{$item->image}}">
                                <img src="{{asset('upload/transfermoneytocustomer')}}/{{$item->image}}" alt="" style="width: 60px; height:60px; object-fit: contain;">
                            </a></td>
                           <td>{{$item->user->id}}</td>
                           <td>{{$item->user->name}}</td>
                           <td>{{$item->user->phone}}</td>
                           <td>{{$item->user->email}}</td>
                            <td style="text-align:center">
                            @if($item->amount)
                                 <p class="btn btn-danger btn-sm"><i class="fas fa-minus"></i> {{number_format($item->amount)}}</p>
                            @endif
                           </td>
                           <td>{{date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
            </section>
        </div>
      </section>
