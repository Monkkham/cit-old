
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.total_divid_customer')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.total_divid_customer')}}</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
          @if(Session::has('message_confirmpaymentbefore'))
              <div class="col-md-12 text-center">
                <a href="{{ route('admin.transfercustomer')}}"  class="btn btn-block btn-danger font-weight-bold py-3">{{Session::get('message_confirmpaymentbefore')}}</a>
              </div>
          @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>{{__('lang.no')}}</th>
                    <th>ເລກບັນຊີ</th>
                    <th>{{__('lang.code')}}{{__('lang.customer')}}</th>
                    <th>{{__('lang.name')}}</th>
                    <th>{{__('lang.phone')}}</th>
                    <th>{{__('lang.email')}}</th>
                    <th>{{__('lang.address')}}</th>
                    <th>{{__('blog.order_divid')}}</th>
                    <th>{{__('lang.withrawed')}}</th>
                    <th>{{__('blog.amount_remaining')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                       @php $i=1;
                       @endphp
                       @foreach($Banlances as $item)
                        <tr>
                           <td>{{$i++}}</td>
                           <td>{{$item->id}}</td>
                           <td>{{$item->user->id}}</td>
                           <td>{{$item->user->name}}</td>
                           <td>{{$item->user->phone}}</td>
                           <td>{{$item->user->email}}</td>
                           <td>{{$item->user->address}}</td>
                           <td style="text-align:center">
                                 @if($item->all_balance)
                                 <p class="btn btn-warning btn-sm"> {{number_format($item->all_balance)}} {{__('lang.lak')}}</p>
                                 @endif
                           </td>
                           <td style="text-align:center">
                               @if($item->withraw_balance)
                                 <p class="btn btn-danger btn-sm"><i class="fas fa-minus"></i> {{number_format($item->withraw_balance)}} {{__('lang.lak')}}</p>
                                 @endif
                           </td>
                           <td style="text-align:center">
                               @if($item->balance)
                                 <p class="btn btn-success btn-sm">{{number_format($item->balance)}} {{__('lang.lak')}}</p>
                                 @endif
                           </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
            </section>
        </div>
      </section>
