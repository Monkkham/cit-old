
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>ລູກຄ້າຂໍຖອນເງິນລາຍໄດ້</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">ລູກຄ້າຂໍຖອນເງິນລາຍໄດ້</li>
              </ol>
            </div>
          </div>
        </div>
      <section class="content">
           @if(Session::has('transfer_success'))
              <div class="col-md-12 text-center">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">ໂອນເງິນສໍາເລັດແລ້ວ</a>
              </div>
          @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>{{__('lang.no')}}</th>
                    <th>{{__('lang.code')}}{{__('lang.customer')}}</th>
                    <th>{{__('lang.name')}}</th>
                    <th>{{__('lang.phone')}}</th>
                    <th>{{__('lang.email')}}</th>
                    <th>{{__('lang.date')}}</th>
                    <th>{{__('lang.total')}}</th>
                    <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                   @php
                      $i = 1;
                    @endphp
                    @foreach($RequestWithraws as $item)
                    <tr>
                      <td>{{$i++}}</td>
                           <td>{{$item->user->id}}</td>
                           <td>{{$item->user->name}}</td>
                           <td>{{$item->user->phone}}</td>
                           <td>{{$item->user->email}}</td>
                           <td>{{$item->created_at}}</td>
                           <td>
                              @if($item->request_money)
                                 {{number_format($item->request_money)}} {{__('lang.lak')}}
                              @endif
                           </td>
                           <td style="text-align:center">
                              <a href="{{ route('admin.transfercustomer',['transfer_id'=>$item->id])}}"  class="btn btn-success btn-sm"><i class="fas fa-sign-in-alt"></i> {{__('lang.transfer')}}</a>
                           </td>
                    </tr>
                   @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
            </section>
        </div>
      </section>
