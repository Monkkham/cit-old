<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.coupon_component')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.coupon_component')}}</li>
              </ol>
            </div>
          </div>


      <section class="content">
        <div class="container-fluid">
          @if(Session::has('update_couponsuccessfully'))
                        <div class="alert alert-success text-center">
                               {{__('lang.update_successfully')}}
                        </div>
            @endif
            @if(Session::has('add_couponsuccessfullly'))
                        <div class="alert alert-success text-center">
                        {{__('lang.add_successfully')}}
                        </div>
            @endif
            @if(Session::has('delete_couponsuccessfully'))
                        <div class="alert alert-success text-center">
                        {{__('lang.delete_successfully')}}
                        </div>
            @endif
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <a href="{{ route('admin.add-coupons') }}" class="btn btn-primary btn-sm"><i class="fas fa-add"></i>{{__('lang.add')}}</a>
                </div>
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped" style="text-align: center">
                    <thead>
                    <tr style="text-align: center">
                      <th>{{__('lang.no')}}</th>
                      <th>{{__('lang.coupon_code')}}</th>
                      <th>{{__('lang.coupon_type')}}</th>
                      <th>{{__('lang.coupon_value')}}</th>
                      <th>{{__('lang.coupon_cart_value')}}</th>
                      <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $stt = 1;
                    @endphp

                    @foreach ($coupons as $coupon)
                    <tr>
                      <td width=5% style="text-align: center">{{$stt++}}</td>

                        <td>{{$coupon->code}}</td>

                      <td>{{$coupon->type}}</td>
                      @if($coupon->type=='fixed')
                      <td>$ {{number_format($coupon->value)}}</td>
                      @else
                       <td>{{number_format($coupon->value)}} %</td>

                         <td>{{number_format($coupon->cart_value)}}</td>
                         @endif
                         <td>{{$coupon->expiry_date}}</td>
                      <td>

                          <a href="{{route('admin.edit-coupons',['coupon_slug'=>$coupon->id])}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></a>
                          <button type="submit" class="btn btn-danger btn-sm" wire:click.prevent="deletecoupon({{$coupon->id}})"  onclick="return confirm('ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ ຫຼື ບໍ?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>

                      </td>
                    </tr>
                    @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


    </div>
</section>
