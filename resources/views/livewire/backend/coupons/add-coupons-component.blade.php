<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{__('lang.coupon_component')}}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
            <li class="breadcrumb-item active">{{__('lang.coupon_component')}}</li>
          </ol>
        </div>
    </div>





<section class="content">
    <div class="container-fluid">
        <div class="col-lg-12">
          <div class="card card-default">
        <div class="card-header">
        <h3 class="card-title"><h4 class="card-title">{{__('lang.edit')}}</h4></h3>
        <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
              </button>
        </div>
    </div>
     <form class="form-horizontal" wire:submit.prevent="Addcoupon">
       @csrf
         <div class="card-body">
           <div class="row">
             <div class="col-md-12">
                <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                                <label for="firstname">{{__('lang.code')}}</label>
                                <input type="text" class="form-control"  wire:model="code" placeholder="{{__('lang.code')}}">
                                        @error('code')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                          </div>
                          </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label>{{__('lang.type')}}</label>
                                      <select class="form-control" wire:model="type">
                                          <option  value="">{{__('lang.select')}}</option>
                                        <option value="flixed">{{__('lang.fixed')}}</option>
                                        <option value="percent">{{__('lang.percent')}}</option>
                                      </select>
                                            @error('type')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                </div>
                            </div>
                 </div>
              </div>
        </div>
              <!-- /.col -->
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="value">{{__('lang.percent')}}</label>
                      <input type="text" class="form-control" wire:model="value" placeholder="{{__('lang.percent')}}">
                      @error('value')
                              <span class="text-danger">{{$message}}</span>
                          @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cart_value">{{__('lang.coupon_cart_value')}}</label>
                      <input type="text" class="form-control" wire:model="cart_value" placeholder="{{__('lang.coupon_cart_value')}}">
                      @error('cart_value')
                            <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                  </div>
                 <div class="col-md-6" wire:ignore>
                          <div class="form-group">
                            <label for="expiry-date">{{__('lang.expiry_date')}}</label>
                            <input type="date" id="expiry-date" class="form-control" wire:model="expiry_date" placeholder="{{__('lang.expiry_date')}}">
                                          @error('expiry_date')
                                              <span class="text-danger">{{$message}}</span>
                                          @enderror
                          </div>
                   </div>
                </div>
                </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                <a class="btn btn-warning" href="{{route('admin.coupons')}}" >{{__('lang.back')}}</a>
            </div>
    </form>
          </div>
      </div>
    </div>
  </section>
</div>
</section>
