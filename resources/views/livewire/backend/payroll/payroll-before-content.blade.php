<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('lang.payroll_before') }}</h1>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">{{ __('lang.home') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ __('lang.payroll_before') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    @if ($selectData == true)
        <!-- show all data -->
        <div>
            <div class="conten">
                <div class="container-fluid">
                    @if (Session::has('message'))
                        <div class="col-md-12 text-center mb-2">
                            <a href="#"
                                class="btn btn-block btn-success w-100 font-weight-bold">{{ Session::get('message') }}</a>
                        </div>
                    @endif
                    @if (Session::has('no_message'))
                        <div class="col-md-12 text-center mb-2">
                            <a href="#"
                                class="btn btn-block btn-danger w-100 font-weight-bold">{{ Session::get('no_message') }}</a>
                        </div>
                    @endif
                    @if (Session::has('no_withraw_message'))
                        <div class="col-md-12 text-center mb-2">
                            <a href="#"
                                class="btn btn-block btn-danger w-100 font-weight-bold">{{ Session::get('no_withraw_message') }}</a>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                {{-- @if (auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>{{__('lang.add')}}</button>
                                <a href="{{ route('admin.calculateinterest') }}" class="btn btn-warning mb-2 mt-2"><i class="fa fa-calculator" aria-hidden="true"></i>
                                    {{__('lang.calculate_interest')}}</a>
                            </div>
                            @endif --}}
                                <div class="card-body">
                                    {{-- @if ($this->total_salarylog > 0)
                                <div class="row text-left">
                                    <div class="col-md-5 bg-success p-2 text-center">
                                        <h4><b>@if (!empty($this->total_salarylog))
                                                {{__('lang.grandtotal')}}:{{number_format($this->total_salarylog)}}
                                                {{__('lang.lak')}}
                                                @endif</b></h4>
                                    </div>
                                </div>
                                @endif --}}
                                    <div class="row mt-2">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">{{ __('lang.fromdate') }}</label>
                                                <input wire:model="fromdate" class='form-control' type="date" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">{{ __('lang.todate') }}</label>
                                                <input wire:model="todate" class='form-control' type="date" />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>{{ __('lang.month') }}</label>
                                                <select class="form-control" id="selectMonth" wire:model="month"
                                                    style="width: 100%;">
                                                    <option value="">{{ __('lang.month') }}</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ __('lang.year') }}</label>
                                                <select class="form-control" wire:model="year" style="width: 100%;">
                                                    <option value="">{{ __('lang.year') }}</option>
                                                    {{ $year = date('yyyy') }}
                                                    @for ($year = 2019; $year <= 2050; $year++)
                                                        <option value="{{ $year }}">
                                                            {{ $year }}</option>
                                                    @endfor
                                                </select>
                                                @error('year')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ __('lang.employee') }}-{{ __('lang.lastname') }}</label>
                                                <select class="form-control" id="selectEmployee" wire:model="emp_id"
                                                    style="width: 100%;">
                                                    <option value="" selected>{{ __('lang.select') }}</option>
                                                    @foreach ($employees as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->firstname }}-{{ $item->lastname }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-1">
                                            <div class="form-group">
                                                <label for=""></label>
                                                <a href="#" onclick="printDiv('printOrder')"
                                                    class="btn btn-info btn-sm w-100 mt-2 p-2"><i
                                                        class="fas fa-print"></i>&nbsp;{{ __('lang.print') }}</a>
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr style="text-align: center">
                                                    <th>{{ __('lang.no') }}</th>
                                                    <th>{{ __('lang.first_and_lastname') }}</th>
                                                    <th>{{ __('lang.sex') }}</th>
                                                    <th>{{ __('lang.month') }}/{{ __('lang.year') }}</th>
                                                    <th>{{ __('lang.salary') }}</th>
                                                    <th>{{ __('lang.amount_finded') }}</th>
                                                    <th>{{ __('lang.amount_out') }}</th>
                                                    <th>{{ __('lang.fund_money') }}</th>
                                                    <th>{{ __('lang.total') }}</th>
                                                    <th>{{ __('lang.payroll_before') }}</th>
                                                    {{-- <th>{{ __('lang.status') }}</th> --}}
                                                    <th>{{ __('lang.creator') }}</th>
                                                    @if ((!empty(Auth::user()->role_id) && Auth::user()->role_id == 1) || Auth::user()->role_id == 3)
                                                        <th>{{ __('lang.action') }}</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp

                                                @foreach ($salary_logs as $item)
                                                    <tr>
                                                        <td style="text-align: center">{{ $i++ }}</td>
                                                        <td>
                                                            @if (!empty($item->employee->firstname))
                                                                {{ $item->employee->firstname }}-{{ $item->employee->lastname }}
                                                            @endif
                                                        </td>
                                                        <td style="text-align: center">
                                                            @if (!empty($item->employee->sex))
                                                                @if ($item->employee->sex == 1)
                                                                    <span
                                                                        class="text-danger">{{ __('lang.man') }}</span>
                                                                @else
                                                                    <span
                                                                        class="text-success">{{ __('lang.woman') }}</span>
                                                                @endif
                                                            @endif
                                                        </td>
                                                        <td style="text-align: center">
                                                            {{ $item->month }}/{{ $item->year }}</td>
                                                        <td class="text-success">
                                                            @if (!empty($item->salary))
                                                                {{ number_format($item->salary) }}
                                                            @endif
                                                        </td>
                                                        <td class="text-primary">
                                                            @if (!empty($item->commission_total))
                                                                + {{ number_format($item->commission_total) }}
                                                            @endif
                                                        </td>
                                                        <td class="text-danger">
                                                            @if (!empty($item->miss_total))
                                                                - {{ number_format($item->miss_total) }}
                                                            @endif
                                                        </td>
                                                        <td class="text-center text-danger">
                                                            @if ($item->amount_fund > 0)
                                                                - {{ number_format($item->amount_fund) }}
                                                            @endif
                                                        </td>
                                                      
                                                        <td>
                                                            @if (!empty($item->total_salary))
                                                                <button
                                                                    class="btn btn-success btn-sm"><b>{{ number_format($item->total_salary) }}</b></button>
                                                            @endif
                                                        </td>
                                                        <td class="text-danger">
                                                            @if (!empty($item->payroll_before))
                                                                - {{ number_format($item->payroll_before) }}
                                                            @endif
                                                        </td>
                                                        {{-- <td class="text-center">
                                                    @if ($item->status == '1')
                                                    <span class="badge badge-success">{{__('lang.withrawed')}}</span>
                                                    @else
                                                    <span class="badge badge-danger">{{__('lang.not_withrawed')}}</span>
                                                    @endif
                                                </td> --}}
                                                        {{-- <td class="text-center">
                                                            @if ($item->note == 'ເງິນສົດ')
                                                                <span
                                                                    class="badge badge-primary">{{ $item->note }}</span>
                                                            @elseif($item->note == 'ເງິນໂອນ')
                                                                <span
                                                                    class="badge badge-danger">{{ $item->note }}</span>
                                                            @else
                                                                <span
                                                                    class="badge badge-success">{{ $item->note }}</span>
                                                            @endif
                                                        </td> --}}
                                                        <td>
                                                            @if (!empty($item->user->name))
                                                                {{ $item->user->name }}
                                                            @endif
                                                        </td>
                                                        @if ((!empty(Auth::user()->role_id) && Auth::user()->role_id == 1) || Auth::user()->role_id == 3)
                                                            <td style="text-align: center">
                                                                @if (auth()->user()->role_id == 1)
                                                                    <button class="btn btn-info btn-sm"
                                                                        wire:click="ShowPayrollBefore({{ $item->id }})"><i
                                                                            class="fas fa-hand-holding-usd"></i> {{ __('lang.payroll_before') }}</button>
                                                                @endif
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                {{-- <tr>
                                                    <td colspan="5" class="text-center">
                                                        <b>{{ __('lang.totals') }}</b></td>
                                                    <td>
                                                        <b><u>{{ number_format($this->commission_total) }}</u></b>
                                                    </td>
                                                    <td><b><u>{{ number_format($this->sum_miss) }}</u></b></td>
                                                    <td><b><u>{{ number_format($this->sum_fund) }}</u></b></td>
                                                    <td><b><u>{{ number_format($this->total_salarylog) }}</u></b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr> --}}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="float-right">
                                        {{ $salary_logs->links() }}
                                    </div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between p-4">
                                    <h5>
                                        <b>
                                            @if (!empty($this->total_all_commission))
                                                {{ __('lang.amount_finded') }}{{ __('lang.grandtotal') }}:
                                                {{ number_format($this->total_all_commission) }}
                                            @endif
                                        </b>
                                    </h5>
                                    <h5>
                                        <b>
                                            @if (!empty($this->amount_all_commission))
                                                {{ __('lang.divid_name') }}{{ __('lang.grandtotal') }}:
                                                {{ number_format($this->amount_all_commission) }}
                                            @endif
                                        </b>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('livewire.backend.payroll.create-payroll-before')
            </div>
        </div>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#selectEmployee').select2();
                    $('#selectEmployee').on('change', function(e) {
                        var data = $('#selectEmployee').select2("val");
                        @this.set('emp_id', data);
                    });
                });
            </script>
        @endpush
    @endif

    <!-- </div> -->
    <style type="text/css" media="print">
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
    </style>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
    </script>


    @push('scripts')
        <script>
            window.addEventListener('show-modal-add', event => {
                $('#modal-add').modal('show');
            })
            window.addEventListener('hide-modal-add', event => {
                $('#modal-add').modal('hide');
            })

            $('.money').simpleMoneyFormat();
            $(document).ready(function() {
                window.addEventListener('show-modal-edit-salary', event => {
                    $('#modal-edit-salary').modal('show');
                })
                window.addEventListener('hide-modal-edit-salary', event => {
                    $('#modal-edit-salary').modal('hide');
                })
            });

            function validate(evt) {
                var theEvent = evt || window.event;
                // Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }
        </script>
    @endpush
</div>
