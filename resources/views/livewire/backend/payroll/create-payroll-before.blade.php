    <!-- /.modal-add -->
    <div wire:ignore.self class="modal fade" id="modal-add">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fas fa-hand-holding-usd text-success"></i> ຂໍເບີກເງິນກ່ອນ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <label>ເລືອກເພດ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1" wire:model="gender"
                                            checked>
                                        <label for="radioPrimary1">ຍີງ
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2" wire:model="gender">
                                        <label for="radioPrimary2">ຊາຍ
                                        </label>
                                    </div>
                                </div>
                                @error('gender')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div> --}}
                            {{-- </div>
                        <div class="row"> --}}

                        </div>
                        <div class="row">
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>--- ພະນັກງານ ---</label>
                                    <select wire:model="employee_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($select_employee as $item)
                                            <option value="{{ $item->id }}">{{ $item->firstname }}</option>
                                        @endforeach
                                    </select>
                                    @error('employee_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ຈຳນວນເງິນຂໍເບີກ</label>
                                    <input wire:model="amount" type="text" placeholder="0.00"
                                        class="form-control @error('amount') is-invalid @enderror">
                                    @error('amount')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ຄຳອະທິບາຍ(ຖ້າມີ)</label>
                                    <input wire:model="des" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('des') is-invalid @enderror">
                                    @error('des')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer justify-content-between bg-light">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> ຍົກເລີກ</button>
                        <button wire:click="PayrollBefore" type="button" class="btn btn-success"><i class="fas fa-check-circle"></i> ບັນທຶກ</button>
                    </div>
                </div>
            </div>
        </div>
