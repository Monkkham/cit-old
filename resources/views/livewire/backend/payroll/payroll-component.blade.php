<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.payroll')}}</h1>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.payroll')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    @if ($selectData == true)
    <!-- show all data -->
    <div>
        <div class="conten">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                @if(Session::has('no_message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_message')}}</a>
                </div>
                @endif
                @if(Session::has('no_withraw_message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_withraw_message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @if(auth()->user()->role_id ==1 || auth()->user()->role_id ==3)
                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>{{__('lang.add')}}</button>
                                <a href="{{ route('admin.calculateinterest') }}" class="btn btn-warning mb-2 mt-2"><i class="fa fa-calculator" aria-hidden="true"></i>
                                    {{__('lang.calculate_interest')}}</a>
                            </div>
                            @endif
                            <div class="card-body">
                                @if($this->total_salarylog > 0)
                                <div class="row text-left">
                                    <div class="col-md-5 bg-success p-2 text-center">
                                        <h4><b>@if(!empty($this->total_salarylog))
                                                {{__('lang.grandtotal')}}:{{number_format($this->total_salarylog)}}
                                                {{__('lang.lak')}}
                                                @endif</b></h4>
                                    </div>
                                </div>
                                @endif
                                <div class="row mt-2">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.fromdate')}}</label>
                                            <input wire:model="fromdate" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.todate')}}</label>
                                            <input wire:model="todate" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>{{__('lang.month')}}</label>
                                            <select class="form-control" id="selectMonth" wire:model="month" style="width: 100%;">
                                                <option value="">{{__('lang.month')}}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.year')}}</label>
                                            <select class="form-control" wire:model="year" style="width: 100%;">
                                                <option value="">{{__('lang.year')}}</option>
                                                {{$year = date('yyyy')}}
                                                @for ($year = 2019; $year <=2050; $year++) <option value="{{$year}}">
                                                    {{$year}}</option>
                                                    @endfor
                                            </select>
                                            @error('year')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">{{__('lang.show')}}</label>
                                            <select wire:model="select_page" class="form-control">
                                                <option value="10000000">ທັງໝົດ</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.employee')}}-{{__('lang.lastname')}}</label>
                                            <select class="form-control" id="selectEmployee" wire:model="emp_id" style="width: 100%;">
                                                <option value="" selected>{{__('lang.select')}}</option>
                                                @foreach ($employees as $item)
                                                <option value="{{$item->id}}">
                                                    {{$item->firstname}}-{{$item->lastname}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <a href="#" onclick="printDiv('printOrder')" class="btn btn-info btn-sm w-100 mt-2 p-2"><i class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th>{{__('lang.no')}}</th>
                                                <th>{{__('lang.first_and_lastname')}}</th>
                                                <th>{{__('lang.sex')}}</th>
                                                <th>{{__('lang.month')}}/{{__('lang.year')}}</th>
                                                <th>{{__('lang.salary')}}</th>
                                                <th>{{__('lang.amount_finded')}}</th>
                                                <th>{{__('lang.amount_out')}}</th>
                                                <th>{{__('lang.fund_money')}}</th>
                                                <th>{{__('lang.total')}}</th>
                                                <th>{{__('lang.payroll_before')}}</th>
                                                <th>{{__('lang.status')}}</th>
                                                <th>{{__('lang.type')}}</th>
                                                <th>{{__('lang.creator')}}</th>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <th>{{__('lang.action')}}</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp

                                            @foreach($salary_logs as $item)
                                            <tr>
                                                <td style="text-align: center">{{$i++}}</td>
                                                <td>
                                                    @if(!empty($item->employee->firstname))
                                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    @if(!empty($item->employee->sex))
                                                    @if($item->employee->sex == 1)
                                                    <span class="text-danger">{{__('lang.man')}}</span>
                                                    @else
                                                    <span class="text-success">{{__('lang.woman')}}</span>
                                                    @endif
                                                    @endif
                                                </td>
                                                <td style="text-align: center">{{$item->month}}/{{$item->year}}</td>
                                                <td class="text-success">
                                                    @if(!empty($item->salary))
                                                    {{number_format($item->salary)}}
                                                    @endif
                                                </td>
                                                <td class="text-primary">
                                                    @if(!empty($item->commission_total))
                                                    + {{number_format($item->commission_total)}}
                                                    @endif
                                                </td>
                                                <td class="text-danger">
                                                    @if(!empty($item->miss_total))
                                                    - {{number_format($item->miss_total)}}
                                                    @endif
                                                </td>
                                                <td class="text-center text-danger">@if($item->amount_fund > 0)- {{number_format($item->amount_fund)}} @endif</td>
                                                <td>
                                                    @if(!empty($item->total_salary))
                                                    <button class="btn btn-success btn-sm"><b>{{number_format($item->total_salary)}}</b></button>
                                                    @endif
                                                </td>
                                                <td class="text-danger">
                                                    @if(!empty($item->payroll_before))
                                                    - {{ number_format($item->payroll_before) }}
                                                    @endif
                                                    </td>
                                                <td class="text-center">
                                                    @if($item->status == '1')
                                                    <span class="badge badge-success">{{__('lang.withrawed')}}</span>
                                                    @else
                                                    <span class="badge badge-danger">{{__('lang.not_withrawed')}}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($item->note =='ເງິນສົດ')
                                                    <span class="badge badge-primary">{{$item->note}}</span>
                                                    @elseif($item->note =='ເງິນໂອນ')
                                                    <span class="badge badge-danger">{{$item->note}}</span>
                                                    @else
                                                    <span class="badge badge-success">{{$item->note}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($item->user->name))
                                                    {{$item->user->name}}
                                                    @endif
                                                </td>
                                                @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                                Auth::user()->role_id ==3)
                                                <td style="text-align: center">
                                                    <a href="#" wire:click='delete({{$item->id}})' onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()"><i class="fas fa-trash text-danger"></i></a>
                                                    @if($item->status != '1')
                                                    <a href="#" wire:click='createFund({{$item->id}})'><i class="fa fa-sign-in-alt text-primary"></i></a>
                                                    @endif
                                                    @if(auth()->user()->role_id ==1)
                                                    <button class="btn btn-warning btn-sm" wire:click="show_edit({{$item->id}})"><i class="fas fa-pen"></i></button>
                                                    @endif
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="5" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                                <td>
                                                    <b><u>{{number_format($this->commission_total )}}</u></b>
                                                </td>
                                                <td><b><u>{{number_format($this->sum_miss)}}</u></b></td>
                                                <td><b><u>{{number_format($this->sum_fund)}}</u></b></td>
                                                <td><b><u>{{number_format($this->total_salarylog)}}</u></b></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="float-right">
                                    {{$salary_logs->links()}}
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between p-4">
                                <h5>
                                    <b>
                                        @if(!empty($this->total_all_commission))
                                        {{__('lang.amount_finded')}}{{__('lang.grandtotal')}}:
                                        {{number_format($this->total_all_commission)}}
                                        @endif
                                    </b>
                                </h5>
                                <h5>
                                    <b>
                                        @if(!empty($this->amount_all_commission))
                                        {{__('lang.divid_name')}}{{__('lang.grandtotal')}}:
                                        {{number_format($this->amount_all_commission)}}
                                        @endif
                                    </b>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        $(document).ready(function() {
            $('#selectEmployee').select2();
            $('#selectEmployee').on('change', function(e) {
                var data = $('#selectEmployee').select2("val");
                @this.set('emp_id', data);
            });
        });
    </script>
    @endpush
    @endif

    @if ($createData == true)
    <!-- form create by ajck -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="alert alert-success text-center">
                    {{__('lang.add')}}{{__('lang.success')}}
                </div>
                @endif
                @if(Session::has('no_message'))
                <div class="alert alert-danger text-center">
                    {{Session::get('no_message')}}
                </div>
                @endif
                <div class="col-lg-12">
                    @if(!empty($this->hiddenId))
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                @if(!empty($this->fullname))
                                <h4 class="card-title">{{$this->fullname}}</h4>
                                @endif
                            </h3>
                        </div>
                        <form>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.payroll')}}</label>
                                            <input type="text" class="form-control" value="{{number_format($this->total_all_salary)}}" placeholder="{{__('lang.payroll')}}" disabled>
                                        </div>
                                        @error('note')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.deduct_fund')}} <span class="text-success">{{$this->fund_money_format}}</span></label>
                                            <input type="text" class="form-control" wire:keyup="AutoformatFund" wire:model='fund_money' placeholder="{{__('lang.deduct_fund')}}">
                                        </div>
                                        @error('note')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.add')}}{{__('lang.fund_money')}} <span
                                                    class="text-success">{{$this->add_fund_money_format}}</span></label>
                                            <input type="text" class="form-control" wire:keyup="AutoformatAddfund"
                                                wire:model='add_fund_money'
                                                placeholder="{{__('lang.add')}}{{__('lang.fund_money')}}">
                                        </div>
                                        @error('note')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div> -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.type')}}</label>
                                            <select class="form-control" wire:model="note" style="width: 100%;">
                                                <option value="ເງິນສົດ" selected>ເງິນສົດ</option>
                                                <option value="ເງິນໂອນ">ເງິນໂອນ</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="button" wire:click="SaveFund" class="btn btn-primary">{{__('lang.payment')}}</button>
                                <a class="btn btn-warning" wire:click.prevent="back()">{{__('lang.back')}}</a>
                            </div>
                        </form>
                    </div>
                    @else
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.add')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form wire:submit.prevent='payroll'>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.month')}}</label>
                                            <select class="form-control select2" wire:model="month" style="width: 100%;">
                                                <option value="">{{__('lang.month')}}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            @error('month')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{__('lang.year')}}</label>
                                            <select class="form-control select2" wire:model="year" style="width: 100%;">
                                                <option value="">{{__('lang.year')}}</option>
                                                {{$year = date('yyyy')}}
                                                @for ($year = 2019; $year <=2050; $year++) <option value="{{$year}}">
                                                    {{$year}}</option>
                                                    @endfor
                                            </select>
                                            @error('year')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="note">{{__('lang.note')}}</label>
                                            <input type="text" class="form-control" wire:model='note'
                                                placeholder="{{__('lang.note')}}">
                                        </div>
                                        @error('note')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div> -->

                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                                <a class="btn btn-warning" wire:click.prevent="back()">{{__('lang.back')}}</a>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- </div> -->
    <style type="text/css" media="print">
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
    </style>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
    </script>
    <div class="container" id='printOrder' style="display:none;">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>{!! $branchs->bill_header !!}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;" src="{{asset($branchs->logo)}}">
                <p>
                    <b>
                        @if (Config::get('app.locale') == 'lo')
                        {{$branchs->name_la}}
                        @elseif(Config::get('app.locale') == 'en')
                        {{$branchs->name_en}}
                        @endif
                    </b>
                </p>
            </div>
            <div class="col-md-4 text-center">
                <br>
                <h5><b>{{__('lang.calculate_salary')}}</b></h5>
                <h6><b>
                        @if(!empty($this->month))
                        {{__('lang.monthly')}} {{$this->month}}/ {{$this->year}}
                        @endif
                    </b>
                </h6>
                <h6>@if(!empty($this->fromdate) && !empty($this->todate))
                    {{__('lang.fromdate')}} {{date('d/m/Y', strtotime($this->fromdate)) }} -
                    {{date('d/m/Y', strtotime($this->todate)) }}
                    @endif
                </h6>
                @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                (!empty($this->month) && !empty($this->emp_id)))
                <h6>
                    <div class="text-center">
                        <h6><b>{{__('lang.first_and_lastname')}}: {{$this->fullname}}</b></h6>
                    </div>
                </h6>
                @endif
            </div>
            <div class="col-md-4 text-right">
                <br><br>
                @php
                $date=date('d/m/Y H:i:s');
                @endphp
                <p>{{__('lang.tran_no')}}: ........................</p>
                <p>{{__('lang.date')}}: {{$date}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-left">
                    <h4><b>@if(!empty($this->total_salarylog))
                            {{__('lang.grandtotal')}}:{{number_format($this->total_salarylog)}} {{__('lang.lak')}}
                            @endif</b></h4>
                </div>
                @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                (!empty($this->month)
                && !empty($this->emp_id)))
                <div class="table-responsive">
                    <table class="table table-bordered" style="text-align: center">
                        <thead>
                            <tr>
                                <th>{{__('lang.no')}}</th>
                                <th>{{__('lang.first_and_lastname')}}</th>
                                <th>{{__('lang.sex')}}</th>
                                <th>{{__('lang.month')}}/{{__('lang.year')}}</th>
                                <th>{{__('lang.salary')}}</th>
                                <th>{{__('lang.amount_finded')}}</th>
                                <th>{{__('lang.amount_out')}}</th>
                                <th>{{__('lang.fund_money')}}</th>
                                <th>{{__('lang.payroll_before')}}</th>
                                <th>{{__('lang.total')}}</th>
                                <th>{{__('lang.status')}}</th>
                                <th>{{__('lang.type')}}</th>
                                <th>{{__('lang.creator')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            @endphp

                            @foreach($salary_logs as $item)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    @if(!empty($item->employee->firstname))
                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->employee->sex))
                                    @if($item->employee->sex == 1)
                                    {{__('lang.man')}}
                                    @else
                                    {{__('lang.woman')}}
                                    @endif
                                    @endif
                                </td>
                                <td>{{$item->month}}/{{$item->year}}</td>
                                <td>
                                    @if(!empty($item->salary))
                                    {{number_format($item->salary)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->commission_total))
                                    + {{number_format($item->commission_total)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->miss_total))
                                    - {{number_format($item->miss_total)}}
                                    @endif
                                </td>
                                <td>@if($item->amount_fund > 0)- {{number_format($item->amount_fund)}} @endif</td>
                                <td>
                                    @if(!empty($item->payroll_before))
                                   - {{number_format($item->payroll_before)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->total_salary))
                                    {{number_format($item->total_salary)}}
                                    @endif
                                </td>
                                <td>
                                    @if($item->status == '1')
                                    {{__('lang.yes_pay')}}
                                    @else
                                    {{__('lang.no_pay')}}
                                    @endif
                                </td>
                                <td>
                                    @if($item->note =='ເງິນສົດ')
                                    {{$item->note}}
                                    @elseif($item->note =='ເງິນໂອນ')
                                    {{$item->note}}
                                    @else
                                    {{$item->note}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->user->name))
                                    {{$item->user->name}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                <td>
                                    <b><u>{{number_format($this->commission_total )}}</u></b>
                                </td>
                                <td><b><u>{{number_format($this->sum_miss)}}</u></b></td>
                                <td><b><u>{{number_format($this->sum_fund)}}</u></b></td>
                                <td><b><u>{{number_format($this->total_salarylog)}}</u></b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @else
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>{{__('lang.no')}}</th>
                                <th>{{__('lang.first_and_lastname')}}</th>
                                <th>{{__('lang.sex')}}</th>
                                <th>{{__('lang.month')}}/{{__('lang.year')}}</th>
                                <th>{{__('lang.salary')}}</th>
                                <th>{{__('lang.amount_finded')}}</th>
                                <th>{{__('lang.amount_out')}}</th>
                                <th>{{__('lang.fund_money')}}</th>
                                <th>{{__('lang.payroll_before')}}</th>
                                <th>{{__('lang.total')}}</th>
                                <th>{{__('lang.status')}}</th>
                                <th>{{__('lang.type')}}</th>
                                <th>{{__('lang.creator')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            @endphp

                            @foreach($salary_logs as $item)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    @if(!empty($item->employee->firstname))
                                    {{$item->employee->firstname}}-{{$item->employee->lastname}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->employee->sex))
                                    @if($item->employee->sex == 1)
                                    {{__('lang.man')}}
                                    @else
                                    {{__('lang.woman')}}
                                    @endif
                                    @endif
                                </td>
                                <td>{{$item->month}}/{{$item->year}}</td>
                                <td>
                                    @if(!empty($item->salary))
                                    {{number_format($item->salary)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->commission_total))
                                    + {{number_format($item->commission_total)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->miss_total))
                                    - {{number_format($item->miss_total)}}
                                    @endif
                                </td>
                                <td>@if($item->amount_fund > 0)- {{number_format($item->amount_fund)}} @endif</td>
                                <td>
                                    @if(!empty($item->payroll_before))
                                   - {{number_format($item->payroll_before)}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->total_salary))
                                    {{number_format($item->total_salary)}}
                                    @endif
                                </td>
                                <td>
                                    @if($item->status == '1')
                                    {{__('lang.yes_pay')}}
                                    @else
                                    {{__('lang.no_pay')}}
                                    @endif
                                </td>
                                <td>
                                    @if($item->note =='ເງິນສົດ')
                                    {{$item->note}}
                                    @elseif($item->note =='ເງິນໂອນ')
                                    {{$item->note}}
                                    @else
                                    {{$item->note}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->user->name))
                                    {{$item->user->name}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                <td>
                                    <b><u>{{number_format($this->commission_total )}}</u></b>
                                </td>
                                <td><b><u>{{number_format($this->sum_miss)}}</u></b></td>
                                <td><b><u>{{number_format($this->sum_fund)}}</u></b></td>
                                <td><b><u>{{number_format($this->total_salarylog)}}</u></b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
        @if(!empty($branchs->bill_footer))
        <div class="row">
            <div class="col-md-12">
                <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-4 text-center">
                <p><b>{{$branchs->director_sign}}</b></p>
            </div>
            <div class="col-md-4 text-center">
                <p><b>{{$branchs->chechker_sign}}</b></p>
            </div>
            <div class="col-md-4 text-center">
                <p><b>{{$branchs->staff_sign}}</b></p>
            </div>
        </div>
    </div>
    <!-- /.modal-edit-salary -->
    <div wire:ignore.self class="modal fade" id="modal-edit-salary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title">{{__('lang.edit')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="card-body">
                            <input type="hidden" wire:model="hiddenId" value="{{$hiddenId}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('lang.amount_finded')}}</label>
                                        <input wire:model="ed_commission_total" type="text" onkeypress='validate(event)' class="form-control money @error('ed_commission_total') is-invalid @enderror" placeholder="{{__('lang.ed_commission_total')}}">
                                        @error('ed_commission_total') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('lang.amount_out')}}</label>
                                        <input wire:model="miss_total" type="text" onkeypress='validate(event)' class="form-control money @error('miss_total') is-invalid @enderror" placeholder="{{__('lang.money_out')}}">
                                        @error('miss_total') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('lang.fund_money')}}</label>
                                        <input wire:model="amount_fund" type="text" onkeypress='validate(event)' class="form-control money @error('amount_fund') is-invalid @enderror" placeholder="{{__('lang.fund_money')}}">
                                        @error('amount_fund') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('lang.total_salary')}}</label>
                                        <input wire:model="total_salary" type="text" onkeypress='validate(event)' class="form-control money @error('total_salary') is-invalid @enderror" placeholder="{{__('lang.total_salary')}}">
                                        @error('total_salary') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('lang.close')}}</button>
                    <button type="button" class="btn btn-primary" wire:click="updateSalary">{{__('lang.save')}}</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        $('.money').simpleMoneyFormat();
        $(document).ready(function() {
            window.addEventListener('show-modal-edit-salary', event => {
                $('#modal-edit-salary').modal('show');
            })
            window.addEventListener('hide-modal-edit-salary', event => {
                $('#modal-edit-salary').modal('hide');
            })
        });

        function validate(evt) {
            var theEvent = evt || window.event;
            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    @endpush
</div>