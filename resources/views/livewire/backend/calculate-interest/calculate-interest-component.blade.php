<div>
    <!-- <div class="container-fluid"> -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('lang.calculate_interest')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('lang.calculate_interest')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('no_message'))
    <div class="col-md-12 text-center mb-2">
        <a href="#" class="btn btn-block btn-danger w-100 font-weight-bold">{{Session::get('no_message')}}</a>
    </div>
    @endif
    @if ($selectData == true)
    <!-- show all data -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="col-md-12 text-center mb-2">
                    <a href="#" class="btn btn-block btn-success w-100 font-weight-bold">{{Session::get('message')}}</a>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-header">
                                <button wire:click='showFrom' class="btn btn-primary mb-2 mt-2"><i
                                        class="fa fa-calculator" aria-hidden="true"></i>
                                    {{__('lang.calculate_interest')}}</button>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.fromdate')}}</label>
                                            <input wire:model="start_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.todate')}}</label>
                                            <input wire:model="end_date" class='form-control' type="date" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">{{__('lang.select')}}</label>
                                            <select wire:model="type_status" id="" class="form-control">
                                                <option value="">{{__('lang.select')}}</option>
                                                <option value="1">{{__('lang.deposit')}}</option>
                                                <option value="2">{{__('lang.withraw')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <a href="#" onclick="printDiv('printOrder')"
                                                class="btn btn-success btn-sm w-100 mt-2 p-2"><i
                                                    class="fas fa-print"></i>&nbsp;{{__('lang.print')}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th>{{__('lang.no')}}</th>
                                            <th>{{__('lang.content')}}</th>
                                            <th>{{__('lang.capital_balance')}}</th>
                                            <th>{{__('lang.interest')}}</th>
                                            <th>{{__('lang.deposit')}}</th>
                                            <th>{{__('lang.withraw')}}</th>
                                            <th>{{__('lang.total_amount')}}</th>
                                            <th>{{__('lang.remaining_fund')}}</th>
                                            <th>{{__('lang.created_at')}}</th>
                                            <th>{{__('lang.creator')}}</th>
                                            @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                            Auth::user()->role_id ==3)
                                            <th>{{__('lang.action')}}</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $i = 1;
                                        $total_withraw = 0;
                                        $total_deposit = 0;
                                        $total_interest = 0;
                                        @endphp
                                        @foreach($data as $item)
                                        @php
                                        $total_withraw += $item->withraw;
                                        $total_deposit += $item->deposit;
                                        $total_interest += $item->total_rate;
                                        @endphp
                                        <tr>
                                            <td style="text-align: center">{{$i++}}</td>
                                            <td>
                                                {{$item->content}}
                                            </td>
                                            <td style="text-align: center">
                                                {{number_format($item->total)}}
                                            </td>
                                            <td style="text-align: center;">
                                                {{number_format($item->total_rate)}}
                                            </td>
                                            <td style="text-align:center;">
                                                {{number_format($item->deposit)}}
                                            </td>
                                            <td style="text-align:center;">
                                                {{number_format($item->withraw)}}
                                            </td>
                                            <td style="text-align:center;">
                                                {{number_format($item->sum_total)}}
                                            </td>
                                            <td class="text-center">
                                                <button
                                                    class="btn btn-success btn-sm w-100">{{number_format($item->remain_total)}}
                                                </button>
                                            </td>
                                            <td style="text-align: center">
                                                {{date('d/m/Y H:i:s', strtotime($item->created_at)) }}
                                            </td>
                                            <td style="text-align: center">
                                                @if(!empty($item->user->name))
                                                {{$item->user->name}}
                                                @endif
                                            </td>
                                            @if(!empty(Auth::user()->role_id) && Auth::user()->role_id ==1 ||
                                            Auth::user()->role_id ==3)
                                            <td style="text-align: center">
                                                <button wire:click='delete({{$item->id}})'
                                                    onclick="return confirm('ທ່ານຕ້ອງການລຶບລາຍການນີ້ບໍ ？') || event.stopImmediatePropagation()"
                                                    class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        <td colspan="3" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                        <td class="text-center">
                                            @if($total_interest > 0)
                                            <b><u>{{number_format($total_interest)}}{{__('lang.lak')}}</u></b>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($total_deposit > 0)
                                            <b><u>{{number_format($total_deposit)}}{{__('lang.lak')}}</u></b>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($total_withraw > 0)
                                            <b><u>{{number_format($total_withraw)}}{{__('lang.lak')}}</u></b>
                                            @endif
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- </div> -->
            <style type="text/css" media="print">
            @page {
                size: auto;
                /* auto is the initial value */
                margin: 0;
                /* this affects the margin in the printer settings */
            }
            </style>
            <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;

            }
            </script>
            <div class="container" id='printOrder' style="display:none;">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>{!! $branchs->bill_header !!}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:50px;height:50px;"
                            src="{{asset($branchs->logo)}}">
                        <p>
                            <b>
                                @if (Config::get('app.locale') == 'lo')
                                {{$branchs->name_la}}
                                @elseif(Config::get('app.locale') == 'en')
                                {{$branchs->name_en}}
                                @endif
                            </b>
                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <br>
                        <h5><b>{{__('lang.calculate_salary')}}</b></h5>
                        <h6><b>
                                @if(!empty($this->month))
                                {{__('lang.monthly')}} {{$this->month}}/ {{$this->year}}
                                @endif
                            </b>
                        </h6>
                        <h6>@if(!empty($this->fromdate) && !empty($this->todate))
                            {{__('lang.fromdate')}} {{date('d/m/Y', strtotime($this->fromdate)) }} -
                            {{date('d/m/Y', strtotime($this->todate)) }}
                            @endif
                        </h6>
                        @if((!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) ||
                        (!empty($this->month) && !empty($this->emp_id)))
                        <h6>
                            <div class="text-center">
                                <h6><b>{{__('lang.first_and_lastname')}}: {{$this->fullname}}</b></h6>
                            </div>
                        </h6>
                        @endif
                    </div>
                    <div class="col-md-4 text-right">
                        <br><br>
                        @php
                        $date=date('d/m/Y H:i:s');
                        @endphp
                        <p>{{__('lang.tran_no')}}: ........................</p>
                        <p>{{__('lang.date')}}: {{$date}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th>{{__('lang.no')}}</th>
                                        <th>{{__('lang.content')}}</th>
                                        <th>{{__('lang.capital_balance')}}</th>
                                        <th>{{__('lang.interest')}}</th>
                                        <th>{{__('lang.deposit')}}</th>
                                        <th>{{__('lang.withraw')}}</th>
                                        <th>{{__('lang.total_amount')}}</th>
                                        <th>{{__('lang.remaining_fund')}}</th>
                                        <th>{{__('lang.creator')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    $total_withraw = 0;
                                    $total_deposit = 0;
                                    $total_interest = 0;
                                    @endphp
                                    @foreach($data as $item)
                                    @php
                                    $total_withraw += $item->withraw;
                                    $total_deposit += $item->deposit;
                                    $total_interest += $item->total_rate;
                                    @endphp
                                    <tr>
                                        <td style="text-align: center">{{$i++}}</td>
                                        <td>
                                            {{$item->content}}
                                        </td>
                                        <td style="text-align: center">
                                            {{number_format($item->total)}}
                                        </td>
                                        <td style="text-align: center;">
                                            {{number_format($item->total_rate)}}
                                        </td>
                                        <td style="text-align:center;">
                                            {{number_format($item->deposit)}}
                                        </td>
                                        <td style="text-align:center;">
                                            {{number_format($item->withraw)}}
                                        </td>
                                        <td style="text-align:center;">
                                            {{number_format($item->sum_total)}}
                                        </td>
                                        <td class="text-center">
                                            {{number_format($item->remain_total)}}
                                        </td>
                                        <td style="text-align: center">
                                            @if(!empty($item->user->name))
                                            {{$item->user->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <td colspan="3" class="text-center"><b>{{__('lang.totals')}}</b></td>
                                    <td class="text-center">
                                        @if($total_interest > 0)
                                        <b><u>{{number_format($total_interest)}}{{__('lang.lak')}}</u></b>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($total_deposit > 0)
                                        <b><u>{{number_format($total_deposit)}}{{__('lang.lak')}}</u></b>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($total_withraw > 0)
                                        <b><u>{{number_format($total_withraw)}}{{__('lang.lak')}}</u></b>
                                        @endif
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(!empty($branchs->bill_footer))
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-left p-4">{!! $branchs->bill_footer !!}</p>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->director_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->chechker_sign}}</b></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>{{$branchs->staff_sign}}</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($createData == true)
    <!-- form create by ajck -->
    <div>
        <div class="content">
            <div class="container-fluid">
                @if(Session::has('message'))
                <div class="alert alert-success text-center">
                    {{__('lang.add')}}{{__('lang.success')}}
                </div>
                @endif
                <div class="col-lg-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">
                                <h4 class="card-title">{{__('lang.calculate_interest')}}</h4>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <form>
                            <div class="card-body">
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <h1 class="text-success"><b>{{__('lang.capital_balance')}}
                                                {{number_format($this->total)}} {{__('lang.lak')}}</b></h1>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <h5>{{__('lang.rate')}}: {{$this->show_rate}}%</h5>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    @if($this->check_calculate == false)
                                    <div class="col-md-2 text-center">
                                        <input type="radio" wire:model="type" class="form-control" value="1">
                                        <h4>
                                            <b>{{__('lang.calculate_interest')}}</b>
                                        </h4>
                                    </div>
                                    @endif
                                    <!-- <div class="col-md-1 text-center">
                                        <input type="radio" wire:model="type" class="form-control" value="2">
                                        <h4 class="text-success">
                                            <b>{{__('lang.deposit')}}</b>
                                        </h4>
                                    </div> -->
                                    <!-- <div class="col-md-1 text-center">
                                        <input type="radio" wire:model="type" class="form-control" value="3">
                                        <h4 class="text-danger">
                                            <b>{{__('lang.withraw')}}</b>
                                        </h4>
                                    </div> -->
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="content">{{__('lang.content')}}</label>
                                            <textarea class="form-control" placeholder="{{__('lang.content')}}"
                                                wire:model='content' rows="2" cols="50">
                                                </textarea>
                                            <span class="text-danger"> @error('content'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    @if($this->type ==2)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="content">{{__('lang.deposit')}} <span
                                                    class="text-success">{{$this->slug}}</span></label>
                                            <input type="number" class="form-control" min="10000"
                                                placeholder="{{__('lang.deposit')}}" wire:keyup="AutoformatDeposit"
                                                wire:model="deposit">
                                            <span class="text-danger"> @error('deposit'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    @elseif($this->type ==3)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="content">{{__('lang.withraw')}} <span
                                                    class="text-success">{{$this->slug}}</span></label>
                                            <input type="number" class="form-control" min="10000"
                                                placeholder="{{__('lang.withraw')}}" wire:keyup="AutoformatWithraw"
                                                wire:model="withraw">
                                            <span class="text-danger"> @error('withraw'){{$message}} @enderror </span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <a class="btn btn-warning" wire:click="back">{{__('lang.back')}}</a>
                                @if($this->type == 1 && $this->check_calculate == false)
                                <button type="button" wire:click="CalculateFund"
                                    class="btn btn-primary">{{__('lang.calculate_interest')}}</button>
                                @elseif($this->type ==2 && $this->check_calculate == true)
                                <button type="button" wire:click="CalculateFund"
                                    class="btn btn-success">{{__('lang.deposit')}}</button>
                                @elseif($this->type ==3 && $this->check_calculate == true)
                                <button type="button" wire:click="CalculateFund"
                                    class="btn btn-danger">{{__('lang.withraw')}}</button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#employee').select2();
        $('#employee').on('change', function(e) {
            var data = $('#employee').select2("val");
            @this.set('emp_id', data);
        });
    });
    </script>
    @endif
    <!-- </div> -->
</div>