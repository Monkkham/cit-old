
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>ຈັດສົ່ງສິນຄ້າ</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
            <li class="breadcrumb-item active">ຈັດສົ່ງສິນຄ້າ</li>
          </ol>
        </div>
      </div>
      <section class="content">
    <div class="container-fluid">
    @if(Session::has('message_update'))
    <div class="col-md-12 text-center mt-6">
                <a href="#"  class="btn btn-block btn-success font-weight-bold py-3">{{Session::get('message_update')}}</a>
    </div>
    @endif
        <div class="col-lg-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
            <form class="form-horizontal" wire:submit.prevent="Updatedivid">
                @csrf
                <div class="card-body">
                      
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>{{__('lang.name')}}</label>
                        <input type="text" class="form-control" wire:model="name" placeholder="{{__('lang.name')}}">
                        @error('name')
                            <span class="text-danger">{{$message}}</span>
                        @enderror
                      </div>
                    </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>{{__('lang.percent')}}</label>
                          <input type="number" class="form-control" wire:model="percent" placeholder="{{__('lang.percent')}}">
                          @error('percent')
                              <span class="text-danger">{{$message}}</span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="bod">{{__('lang.note')}}</label>
                          <input type="text" class="form-control" wire:model="note" placeholder="{{__('lang.note')}}">
                        </div>
                      </div>
                  </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{__('lang.save')}}</button>
                    <a class="btn btn-warning" href="{{route('admin.divid')}}" >{{__('lang.back')}}</a>
                </div>
            </form>
          </div>
      </div>
    </div>
  </section>
    </div>
</section>
  
