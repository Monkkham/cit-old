
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.divid_component')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.divid_component')}}</li>
              </ol>
            </div>
          </div>
      <section class="content">
        <div class="container-fluid">
             @if(Session::has('message_updatedividsuccess'))
                        <div class="alert alert-success text-center">
                               {{__('lang.update_successfully')}}
                        </div>
            @endif
            @if(Session::has('message_adddividsuccessfully'))
                        <div class="alert alert-success text-center">
                        {{__('lang.add_successfully')}}
                        </div>
            @endif
            @if(Session::has('delete_dividsuccessfully'))
                        <div class="alert alert-success text-center">
                        {{__('lang.delete_successfully')}}
                        </div>
            @endif
            
          <div class="row">
           @if(Session::has('delete_divid'))
            <div class="col-md-12 text-center mt-6">
                        <a href="#"  class="btn btn-block btn-success font-weight-bold py-3"> {{__('lang.successfully')}}</a>
            </div>
            @endif
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <a href="{{ route('admin.adddivid') }}" class="btn btn-primary btn-sm"><i class="fas fa-add"></i>{{__('lang.add')}}</a>
                </div>
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped" style="text-align: center">
                    <thead>
                    <tr style="text-align: center">
                      <th>{{__('lang.no')}}</th>
                      <th>{{__('lang.name')}}</th>
                      <th>{{__('lang.percent')}}</</th>
                      <th>{{__('lang.note')}}</th>
                      <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $stt = 1;
                    @endphp

                    @foreach ($divids as $divid)
                    <tr>
                      <td width="5%" style="text-align: center">{{$stt++}}</td>
                      <td>{{$divid->name}}</td>
                      <td>{{$divid->percent}}%</td>
                      <td>{{$divid->note}}</td>
                      <td>
                          <a href="{{route('admin.editdivid',['divid_slug'=>$divid->id])}}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></a>
                          <button wire:click.prevent="deletedivid({{$divid->id}})" class="btn btn-danger btn-sm" onclick="return confirm('ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ ຫຼື ບໍ?')"><i class="fas fa-trash"></i></button>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </div>
      </section>
      
