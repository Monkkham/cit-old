@extends('layouts.app.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>{{__('lang.user')}}</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">{{__('lang.home')}}</a></li>
                <li class="breadcrumb-item active">{{__('lang.user')}}</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
  
      <section class="content">
        <div class="container-fluid">
          <div class="row">
          @if(count($errors)>0)
              <div class="card-body">
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <i class="icon fas fa-ban"> {{__('lang.error')}}</i></br>
                  @foreach($errors->all() as $error)
                  {{$error}} </br>
                  @endforeach
                </div>
              </div>
            @endif 
            <div class="col-12">
              <div class="card">
                <div class="card-header just">
                        <a href="{{route('user.create')}}" class="btn btn-primary btn-sm"><i class="fas fa-add"></i>{{__('lang.add')}}</a>
                        <a href="/user-excel" class="btn btn-success btn-sm"><i class="fas fa-add"></i>{{__('lang.add')}} Excel</a>
                </div>
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>{{__('lang.no')}}</th>
                      <th>{{__('lang.name')}}</th>
                      <th>{{__('lang.phone')}}</th>
                      <th>{{__('lang.email')}}</th>
                      <th>{{__('lang.name')}}</th>
                      <th>{{__('lang.percent')}}</th>
                      <th>{{__('lang.rolename')}}</th>
                      <th>{{__('lang.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                      $stt = 1;    
                    @endphp

                    @foreach ($user as $u)
                    <tr>
                      <td style="text-align: center">{{$stt++}}</td>
                      <td>
                        {{$u->name}}
                        @if($u->divid_id =='' && $u->role_id ==10)
                        <p span class="badge badge-danger p-1">{{__('lang.no_divid')}}</p>
                        @endif
                      </td>
                      <td>{{$u->phone}}</td>
                      <td>{{$u->email}}</td>
                      <td>
                        @if($u->emp_id > 0)
                        {{$u->empname->firstname}} {{$u->empname->lastname}}
                        @endif
                      </td>
                      <td style="text-align: center">
                        @if($u->divid_id > 0)
                        {{$u->divid->name}} {{$u->divid->percent}} %
                        @endif
                      </td>
                      <td style="text-align: center">{{$u->rolename->name}}</td>
                      <td>
                        <form action=" {{ route('user.destroy', $u->id) }} " method="POST">
                          @csrf
                          @method('DELETE')

                          <a href="{{ route('user.edit', $u->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i></a>
                          <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ ຫຼື ບໍ?')"><i class="fas fa-trash"></i></button>

                        </form>
                      </td>
                    </tr>
                    @endforeach
                    
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection