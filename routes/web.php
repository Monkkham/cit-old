<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\DepartController;
use App\Http\Controllers\Backend\MarriesController;
use App\Http\Controllers\Backend\VillageController;
use App\Http\Livewire\Backend\Divid\DividComponent;
use App\Http\Livewire\Backend\Order\OrderComponent;
use App\Http\Livewire\Backend\Order\PrintBillOrder;
use App\Http\Controllers\Backend\DistrictController;
use App\Http\Controllers\Backend\EmployeeController;
use App\Http\Controllers\Backend\PositionController;
//use App\Http\Controllers\Backend\Account\BranchController;
use App\Http\Controllers\Backend\ProvinceController;
use App\Http\Livewire\Backend\Order\DoSendComponent;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Livewire\Backend\Divid\AddDividComponent;
use App\Http\Livewire\Backend\Order\DoneSendComponent;
use App\Http\Livewire\Backend\Coupons\CouponsComponent;
use App\Http\Livewire\Backend\Divid\EditDividComponent;
use App\Http\Livewire\Backend\Employee\EmployeeContent;
use App\Http\Livewire\Backend\Order\EditOrderComponent;
use App\Http\Livewire\Backend\Order\OrderItemComponent;
use App\Http\Livewire\Backend\Customer\CustomerComponent;
use App\Http\Livewire\Backend\Order\CancelOrderComponent;
use App\Http\Controllers\Backend\Payroll\SalaryController;
use App\Http\Livewire\Backend\Coupons\AddCouponsComponent;
use App\Http\Livewire\Backend\Order\ConfirmOrderComponent;
//livewire
//by cheeyeeyang mouasue
use App\Http\Livewire\Backend\Order\SendingOrderComponent;
use App\Http\Controllers\Backend\Doc\StorageFileController;
use App\Http\Controllers\Backend\Ecommerce\SlideController;
use App\Http\Controllers\Backend\Payroll\PayrollController;
use App\Http\Livewire\Backend\Coupons\EditCouponsComponent;
use App\Http\Livewire\Backend\Order\DetailPaymentComponent;
use App\Http\Livewire\Backend\Order\EditOrderItemComponent;
use App\Http\Livewire\Backend\Payroll\PayrollBeforeContent;
use App\Http\Controllers\Backend\Account\CurrencyController;
use App\Http\Controllers\Backend\Doc\DocumentTypeController;
use App\Http\Controllers\Backend\Account\StatusAccController;
use App\Http\Controllers\Backend\Doc\ExternalPartsController;
use App\Http\Controllers\Backend\Doc\LocalDocumentController;
use App\Http\Controllers\Backend\Ecommerce\CatalogController;
use App\Http\Controllers\Backend\Ecommerce\ProductController;
use App\Http\Controllers\Backend\Ecommerce\ServiceController;
use App\Http\Livewire\Backend\Order\ShowCancelOrderComponent;
use App\Http\Controllers\Backend\Account\SettingAccController;
use App\Http\Controllers\Backend\Doc\ExportDocumentController;
use App\Http\Controllers\Backend\Doc\ImportDocumentController;
use App\Http\Controllers\Backend\Account\TransectionController;
use App\Http\Livewire\Backend\Order\UpdateOrderCancelComponent;

//end
/*
Route::get('/', function () {
return view('backend.login');
});*/

//App::setLocale($locale);
//Route::get('localization/{local}',[LocalController::class, 'index']);
Route::get('localization/{local}', function ($local) {
    Session::put('local', $local);
    return back();
});

//Frontend Website
Route::get('/', App\Http\Livewire\Frontend\HomeComponent::class)->name('home');
Route::get('/shop', App\Http\Livewire\Frontend\ShopComponent::class)->name('shop');
Route::get('/shop/product_detail/{id}', App\Http\Livewire\Frontend\ProductDetailComponent::class)->name('product_detail');
Route::get('/services', App\Http\Livewire\Frontend\ServiceComponent::class)->name('services');
Route::get('/services/service_detail/{id}', App\Http\Livewire\Frontend\ServiceDetailComponent::class)->name('service_detail');
Route::get('/customers', App\Http\Livewire\Frontend\CustomersComponent::class)->name('customers');
Route::get('/frontend/solutions', App\Http\Livewire\Frontend\SolutionsComponent::class)->name('solutions');
Route::get('/frontend/solutions/solution_detail/{id}', App\Http\Livewire\Frontend\SolutionsDetailComponent::class)->name('solution_detail');
Route::get('/about', App\Http\Livewire\Frontend\AboutComponent::class)->name('about');
Route::get('/contact', App\Http\Livewire\Frontend\ContactComponent::class)->name('contact');
Route::get('/cart', App\Http\Livewire\Frontend\CartComponent::class)->name('cart');
// Route::get('/checkout', App\Http\Livewire\Frontend\CheckoutComponent::class)->name('checkout');
Route::get('/wishlist', App\Http\Livewire\Frontend\WishlistComponent::class)->name('wishlist');
Route::get('/terms', App\Http\Livewire\Frontend\TermsComponent::class)->name('terms');
Route::get('/frontend/news', App\Http\Livewire\Frontend\NewsComponent::class)->name('news');
Route::get('/frontend/news/news_detail/{id}', App\Http\Livewire\Frontend\NewsDetailComponent::class)->name('news_detail');
//thankyou
Route::get('/thank-you', App\Http\Livewire\Frontend\ThankyouComponent::class)->name('thankyou');
Route::get('/privacy', function () {
    return file_get_contents(public_path() . '/privacy.html');
});

//Customers CustomerLoginComponent
Route::get('/customers/sign_in', App\Http\Livewire\Frontend\Auth\CustomerLoginComponent::class)->name('customer_sign_in');
Route::get('/customers/register', App\Http\Livewire\Frontend\Auth\CustomerRegisterComponent::class)->name('customer_register');

Route::resource('/loginadmincit', LoginController::class);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
Route::post('/saveToken', [LoginController::class, 'saveToken']);

// ============================  for admin ================= //
Route::group(['middleware' => 'adminLogin'], function () {
    //Customer
    Route::get('/customers/dashboard', App\Http\Livewire\Frontend\Customer\DashboardComponent::class)->name('customer.dashboard');
    Route::get('/customers/profile/{id}', App\Http\Livewire\Frontend\Auth\CustomerProfileComponent::class)->name('customer.profile');
    Route::get('/customers/shipping/{id}', App\Http\Livewire\Frontend\Auth\CustomerShippingComponent::class)->name('customer.shipping');
    Route::get('/customers/sign_out', [App\Http\Livewire\Frontend\Auth\CustomerLoginComponent::class, 'signOut'])->name('customer_sign_out');
    Route::get('/history', App\Http\Livewire\Frontend\Customer\CustomerHistoryComponent::class)->name('customer.history');
    Route::get('/view-history/{order_slug}/orderdetail', App\Http\Livewire\Frontend\Customer\ShowCustomerHistoryComponent::class)->name('customer.showhistory');
    Route::get('/order-divid', App\Http\Livewire\Frontend\Customer\OrderDivid::class)->name('customer.orderdivid');
    Route::get('/withraw-divid', App\Http\Livewire\Frontend\Customer\WithrawDividComponent::class)->name('customer.withrawdivid');
    Route::get('/withraw-success', App\Http\Livewire\Frontend\Customer\WithrawSuccessComponent::class)->name('customer.withdrawsuccess');
    Route::get('/liked-product', App\Http\Livewire\Frontend\Customer\LikedProductComponent::class)->name('customer.likedproduct');
    Route::get('/daily-customer-order-cart', App\Http\Livewire\Frontend\Customer\DailyCustomerCartComponent::class)->name('customer.dailycart');
    Route::get('/history-withraw-balance', App\Http\Livewire\Frontend\Customer\HistoryWithrawaComponent::class)->name('customer.historywithraw');
    Route::get('/daily-customer-shop', App\Http\Livewire\Frontend\Customer\DailyCustomerShopComponent::class)->name('customer.dailycustomershop');
    Route::get('/checkout', App\Http\Livewire\Frontend\CheckoutComponent::class)->name('checkout');
    //Backend
    Route::get('/module', [App\Http\Controllers\Backend\ModuleController::class, 'index'])->name('module.index');
    Route::resource('/dashboard', DashboardController::class);
    Route::resource('/depart', DepartController::class);
    Route::resource('/province', ProvinceController::class);
    Route::resource('/district', DistrictController::class);
    Route::resource('/village', VillageController::class);
    Route::resource('/marries', MarriesController::class);
    Route::resource('/position', PositionController::class);
    Route::resource('/user', UserController::class);

    Route::post('/user-excel', [App\Http\Controllers\Backend\ExcelUserController::class, 'store']);
    Route::get('/user-excel', [App\Http\Controllers\Backend\ExcelUserController::class, 'index']);
    Route::get('/user/changepass/{id}', [UserController::class, 'changepass'])->name('user.changepass');
    Route::PATCH('/user/updatepass/{id}', [UserController::class, 'updatepass'])->name('user.updatepass');
    Route::resource('/role', RoleController::class);
    Route::resource('/branch', App\Http\Controllers\Backend\BranchController::class);
    Route::resource('/misstype', App\Http\Controllers\Backend\MissTypeController::class);

    Route::resource('/transection', TransectionController::class);
    Route::PATCH('/approved/{id}', [TransectionController::class, 'approved'])->name('transection.approved');
    Route::PATCH('/rejected/{id}', [TransectionController::class, 'rejected'])->name('transection.rejected');
    //Route::resource('/branch',BranchController::class);
    Route::resource('/statusacc', StatusAccController::class);
    Route::resource('/currency', CurrencyController::class);
    Route::resource('/settingacc', SettingAccController::class);

    Route::resource('/import_doc', ImportDocumentController::class);
    Route::get('/import_doc/download/{id}', [ImportDocumentController::class, 'download'])->name('download_import');
    Route::resource('/export_doc', ExportDocumentController::class);
    Route::get('/export_doc/download/{id}', [ExportDocumentController::class, 'download'])->name('download_export');
    Route::resource('/local_doc', LocalDocumentController::class);
    Route::get('/local_doc/download/{id}', [LocalDocumentController::class, 'download'])->name('download_local');
    Route::resource('/doc_type', DocumentTypeController::class);
    Route::resource('/storage_file', StorageFileController::class);
    Route::resource('/external_parts', ExternalPartsController::class);

    Route::resource('/employee', EmployeeController::class);
    Route::get('/employees', EmployeeContent::class)->name('admin.employees');
    Route::resource('/salary', SalaryController::class);
    Route::resource('/payroll', PayrollController::class);
    Route::PATCH('/aceptedpayroll/{id}', [PayrollController::class, 'aceptedpayroll'])->name('payroll.aceptedpayroll');
    Route::DELETE('/deletepayroll/{id}', [PayrollController::class, 'deletepayroll'])->name('payroll.deletepayroll');

    Route::resource('/news', App\Http\Controllers\Backend\Ecommerce\NewsController::class);
    Route::resource('/page', App\Http\Controllers\Backend\Ecommerce\PageController::class);
    Route::resource('/product', ProductController::class);
    Route::get('/search', [ProductController::class, 'search'])->name('search');
    Route::resource('/catalog', CatalogController::class);
    Route::resource('/slide', SlideController::class);
    Route::resource('/service', ServiceController::class);
    Route::resource('/customer_logo', App\Http\Controllers\Backend\Ecommerce\CustomerController::class);
    Route::resource('/solutions', App\Http\Controllers\Backend\Ecommerce\SolutionController::class);
    Route::resource('/solution_type', App\Http\Controllers\Backend\Ecommerce\SolutionTypeController::class);
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

    // Order
    //by cheeyeeyangmouasue
    Route::get('/detailpayment/{detailpayment_slug}/cit-group-la', DetailPaymentComponent::class)->name('admin.detailpayment');
    Route::get('/cancel-order/{cancelorder_slug}/cit-group-la', CancelOrderComponent::class)->name('admin.cancelorder');
    Route::get('/orders', OrderComponent::class)->name('admin.order');
    Route::get('/edit-order/{order_slug}/cit-group-la', EditOrderComponent::class)->name('admin.editorder');
    Route::get('/edit-order-item', EditOrderItemComponent::class)->name('admin.editorderitem');
    Route::get('/orderitems/{order_slug}/cit-group-la', OrderItemComponent::class)->name('admin.orderitem');
    Route::get('/send-to-rider/{order_slug}/cit-group-la', DoSendComponent::class)->name('admin.dosend');
    Route::get('/sending-order', SendingOrderComponent::class)->name('admin.sendingorder');
    Route::get('/donesends', DoneSendComponent::class)->name('admin.donesend');
    Route::get('/confirm-order/{order_slug}/cit-group-la', ConfirmOrderComponent::class)->name('admin.confirmorder');
    Route::get('/edit-cancel-order/{order_slug}/cit-group-la', UpdateOrderCancelComponent::class)->name('admin.editcancelorder');
    Route::get('/show-cancel-order', ShowCancelOrderComponent::class)->name('admin.showcancelorder');
    Route::get('/customer-request-withrawing', App\Http\Livewire\Backend\DividCustomer\WithrawingDividCustomerComponent::class)->name('admin.withrawingcustomer');
    Route::get('/customer-withrawned-withrawing', App\Http\Livewire\Backend\DividCustomer\WithrawedDividCustomerComponent::class)->name('admin.withrawedcustomer');
    Route::get('/customer-divid', App\Http\Livewire\Backend\DividCustomer\DividCustomerComponent::class)->name('admin.dividcustomer');
    Route::get('/transfer-divid-to-customer/{transfer_id}/cit-group-la', App\Http\Livewire\Backend\DividCustomer\TransferDividCustomerComponent::class)->name('admin.transfercustomer');
    Route::get('/commision-of-employee', App\Http\Livewire\Backend\Commission\CommissionComponent::class)->name('admin.commission');
    Route::get('/payroll-of-employee', App\Http\Livewire\Backend\Payroll\PayrollComponent::class)->name('admin.payroll');
    Route::get('/payroll-before-employee', PayrollBeforeContent::class)->name('admin.payrollBefore');
    Route::get('/fund-of-employee', App\Http\Livewire\Backend\Fund\FundComponent::class)->name('admin.fund');
    Route::get('/calculate-interest', App\Http\Livewire\Backend\CalculateInterest\CalculateInterestComponent::class)->name('admin.calculateinterest');
    Route::get('/loan-employee', App\Http\Livewire\Backend\Loan\LoanComponent::class)->name('admin.loan');
    Route::get('/print-loan-employee/{slug}/cit-group-la', App\Http\Livewire\Backend\Loan\PrintLoanComponent::class)->name('admin.printloan');
    Route::get('/admin-exchange', App\Http\Livewire\Backend\Order\ExchangeComponent::class)->name('admin.exchange');
    //Coupons
    Route::get('/coupons', CouponsComponent::class)->name('admin.coupons');
    Route::get('/coupons-add', AddCouponsComponent::class)->name('admin.add-coupons');
    Route::get('/coupons-edit/{coupon_slug}/cit-group-la', EditCouponsComponent::class)->name('admin.edit-coupons');
    //Divid
    Route::get('/divid', DividComponent::class)->name('admin.divid');
    Route::get('/add-divid', AddDividComponent::class)->name('admin.adddivid');
    Route::get('/edit-divid/{divid_slug}/cit-group-la', EditDividComponent::class)->name('admin.editdivid');
    Route::get('/print-bill-order/{order_slug}/cit-group-la', PrintBillOrder::class)->name('admin.printbillorder');
    Route::get('/pay-debt-order/{slug}/cit-group-la', App\Http\Livewire\Backend\Order\PayDebtComponent::class)->name('admin.paydebt');
    Route::get('/track-employee-absence/cit-group-la', App\Http\Livewire\Backend\TrackMissEmployee\TrackMissEmployeeComponent::class)->name('admin.trackemployee');
    Route::get('/print-commission/{slug}/cit-group-la', App\Http\Livewire\Backend\Commission\PrintCommissionComponent::class)->name('admin.printcommission');
    //  by jack sainther
    Route::get('/admincustomer', CustomerComponent::class)->name('admin.customer');
    //by gdev
    Route::get('/p1-unit', App\Http\Livewire\Backend\P1Ecommerce\P1UnitComponent::class)->name('admin.p1-unit');
    Route::get('/p1-product-type', App\Http\Livewire\Backend\P1Ecommerce\P1ProductTypeComponent::class)->name('admin.p1-product-type');
    Route::get('/p1-product', App\Http\Livewire\Backend\P1Ecommerce\P1ProductComponent::class)->name('admin.p1-product');
    Route::get('/p1-exchange', App\Http\Livewire\Backend\P1Ecommerce\ExchangeComponent::class)->name('admin.p1-exchange');
    Route::get('/p1-order', App\Http\Livewire\Backend\P1Ecommerce\Order\OrderComponent::class)->name('admin.p1-order');
    Route::get('/cancel-p1-order', App\Http\Livewire\Backend\P1Ecommerce\Order\CancelOrderComponent::class)->name('admin.cancenl-p1-order');
    Route::get('/create-p1-product', App\Http\Livewire\Backend\P1Ecommerce\CreateP1ProductComponent::class)->name('admin.create.p1-product');
    Route::get('/edit-p1-product/{id}/cit-group.la', App\Http\Livewire\Backend\P1Ecommerce\EditP1ProductComponent::class)->name('admin.edit.p1-product');
    Route::get('/p1-contact', App\Http\Livewire\Backend\P1Ecommerce\Contact\P1ContactComponent::class)->name('admin.p1-contact');
    Route::get('/p1-purchase', App\Http\Livewire\Backend\P1Ecommerce\Purchase\PurchaseComponent::class)->name('admin.p1-purchase');
    Route::get('/p1-purchase-order', App\Http\Livewire\Backend\P1Ecommerce\Purchase\PurchaseOrderComponent::class)->name('admin.p1-purchase_order');
    //reports
    Route::get('/report-order-component', App\Http\Livewire\Backend\Report\ReportOrderComponent::class)->name('admin.report_order');
    Route::get('/report-product-component', App\Http\Livewire\Backend\Report\ReportProductComponent::class)->name('admin.report_product');
    Route::get('/report-product-isoutstock', App\Http\Livewire\Backend\Report\ReportProductIsoutstockComponent::class)->name('admin.report_product_isoutstock');
});
