<?php

use App\Http\Controllers\Api\Auth\AuthApiController;
use App\Http\Controllers\Api\LandController;
use App\Http\Controllers\Api\P1OrderController;
use App\Http\Controllers\Api\P1ProductsController;
use App\Http\Controllers\Backend\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductApiController;
use App\Http\Controllers\Api\CatalogApiController;
use App\Http\Controllers\Api\SlideApiController;
use App\Http\Controllers\Api\OrderApiController;
use App\Http\Controllers\Api\ServiceApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/products', ProductApiController::class);
Route::get('/news_products', [App\Http\Controllers\Api\ProductApiController::class, 'getNewProduct']);
Route::get('/random_products', [App\Http\Controllers\Api\ProductApiController::class, 'getRandomProduct']);
Route::resource('/catalogs', CatalogApiController::class);
Route::resource('/slide', SlideApiController::class);
Route::resource('/order', OrderApiController::class);
Route::resource('/service', ServiceApiController::class);
Route::resource('/customer', App\Http\Controllers\Api\CustomerApiController::class);
Route::get('/other_customer', [App\Http\Controllers\Api\CustomerApiController::class, 'index_other']);
Route::resource('/about', App\Http\Controllers\Api\PageApiController::class);
Route::get('/term', [App\Http\Controllers\Api\PageApiController::class, 'term']);
Route::resource('/solutions', App\Http\Controllers\Api\SolutionApiController::class);
Route::resource('/news', App\Http\Controllers\Api\NewsApiController::class);

Route::post('/register', [AuthApiController::class, 'register']);
Route::post('/new_register', [AuthApiController::class, 'new_register']);
Route::post('/login', [AuthApiController::class, 'login']);

//new first api by cheeyeeyang mouasue 2021
Route::post('addtocart/{product_id}/new-item', [App\Http\Controllers\Api\OrderApiController::class, 'addtocart']);
Route::get('carts', [App\Http\Controllers\Api\OrderApiController::class, 'getallcart']);
Route::get('coupons', [App\Http\Controllers\Api\OrderApiController::class, 'getallcoupon']);
Route::get('/applycoupon', [App\Http\Controllers\Api\OrderApiController::class, 'applycoupon']);
Route::get('/checkout', [App\Http\Controllers\Api\OrderApiController::class, 'checkout']);
Route::get('orders', [App\Http\Controllers\Api\OrderApiController::class, 'getallorder']);
Route::get('/ordered-orders', [App\Http\Controllers\Api\OrderApiController::class, 'orderedorder']);
Route::get('/delivered-orders', [App\Http\Controllers\Api\OrderApiController::class, 'deliveredorder']);
Route::get('/arrived-orders', [App\Http\Controllers\Api\OrderApiController::class, 'arrivedorder']);
Route::post('/dosend-orders', [App\Http\Controllers\Api\OrderApiController::class, 'dosendorder']);
Route::post('/confirm-orders', [App\Http\Controllers\Api\OrderApiController::class, 'confirmorder']);
Route::post('/cancel-orders', [App\Http\Controllers\Api\OrderApiController::class, 'cancelorder']);
Route::post('/edit-orders', [App\Http\Controllers\Api\OrderApiController::class, 'editorder']);
//p1 productscontroller
Route::get('get_catalogs', [P1ProductsController::class, 'get_catalogs']);
Route::post('get_prouduct_by_catalog', [P1ProductsController::class, 'get_prouduct_by_catalog']);
Route::get('get_product_by_id/{id}', [P1ProductsController::class, 'get_product_by_id']);
Route::post('get_product', [P1ProductsController::class, 'get_product']);
Route::post('search_product', [P1ProductsController::class, 'search_product']);
Route::get("/get_provinces", [LandController::class, "get_provinces"]);
Route::get("/get_districts_by_pro_id/{pro_id}", [LandController::class, "get_districts_by_pro_id"]);
Route::get("/get_villages_by_dis_id/{dis_id}", [LandController::class, "get_villages_by_dis_id"]);
Route::get("/fix_unit", [P1OrderController::class, "fix_unit"]);
Route::post('/rst_pwd_secret', [AuthApiController::class, 'rst_pwd_secret']);
Route::post('/check_phone_register', [AuthApiController::class, 'check_phone_register']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/user', [AuthApiController::class, 'user']);
    Route::post('/user/update', [AuthApiController::class, 'update']);
    Route::post('/logout', [AuthApiController::class, 'logout']);
    Route::put('/delete_account', [AuthApiController::class, 'delete_account']);
    Route::post('/reset_password', [AuthApiController::class, 'reset_password']);
    //new auth
    Route::post('save_device_token', [AuthApiController::class, 'save_device_token']);
    Route::put('confirm_otp', [AuthApiController::class, 'confirm_otp']);
    //P1OrderController
    Route::post('add_order', [P1OrderController::class, 'add_order']);
    Route::post('confirm_order_from_customer', [P1OrderController::class, 'confirm_order_from_customer']);
    Route::get('get_order_detail_by_order_id/{id}', [P1OrderController::class, 'get_order_detail_by_order_id']);
    Route::get('get_qr_code/{order_id}', [P1OrderController::class, 'get_qr_code']);
    Route::post('check_product_price', [P1OrderController::class, 'check_product_price']);
    Route::put('cancel_order/{id}', [P1OrderController::class, 'cancel_order']);
    //P1ProductController
    Route::post('add_review', [P1ProductsController::class, 'add_review']);
    Route::post('edit_review', [P1ProductsController::class, 'edit_review']);
    //history
    Route::get('/get_order_history_by_id/{id}', [P1OrderController::class, "get_order_history_by_id"]);
    Route::get('/get_all_history_by_user', [P1OrderController::class, "get_all_history_by_user"]);
    Route::get('/check_transaction/{uuid}', [P1OrderController::class, "check_transaction"]);
});
