<?php

namespace App\Http\Livewire\Backend\Employee;

use Livewire\Component;
use App\Models\Employee;
class EmployeeContent extends Component
{
    public $select_status;
    public function render()
    {
        if(!empty($this->select_status))
        {
            $employee = Employee::orderBy('id','desc')->where('status',$this->select_status)->where('del',0)->get();
            // dd();
        }else{
            $employee = Employee::orderBy('id','desc')->where('del',0)->get();
        }
        return view('livewire.backend.employee.employee-content',compact('employee'))->layout('layouts.backend.app');
    }
}
