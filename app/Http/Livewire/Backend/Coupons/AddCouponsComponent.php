<?php
namespace App\Http\Livewire\Backend\Coupons;
use App\Models\Coupon;
use Livewire\Component;

class AddCouponsComponent extends Component
{
  public $code;
  public $type;
  public $value;
  public $cart_value;
  public $expiry_date;
  public function updated($fields)
  {
    $this->validateOnly($fields,[
      'code' =>'required|unique:coupons',
      'type' =>'required',
      'value' =>'required|numeric',
      'cart_value' =>'required|numeric',
      'expiry_date' =>'required',
    ]);
  }


  public function Addcoupon(){
      $this->validate([
           'code' =>'required|unique:coupons',
           'type' =>'required',
           'value' =>'required|numeric|min:0|max:100',
           'cart_value' =>'required|numeric',
           'expiry_date' =>'required',
      ]);
      $coupon = new Coupon();
      $coupon->code = $this->code;
      $coupon->type = $this->type;
      $coupon->value = $this->value;
      $coupon->cart_value = $this->cart_value;
      $coupon->expiry_date = $this->expiry_date;
      $coupon->save();
      session()->flash('add_couponsuccessfullly');
      $this->code='';
      $this->type='';
      $this->value='';
      $this->cart_value='';
      return redirect()->route('admin.coupons');

}

    public function render()
    {
        return view('livewire.backend.coupons.add-coupons-component')->layout('layouts.backend.app');
    }
}
