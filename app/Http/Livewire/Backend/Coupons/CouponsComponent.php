<?php

namespace App\Http\Livewire\Backend\Coupons;

use App\Models\Coupon;
use Livewire\Component;
use UI\Draw\Text\Layout;
use Livewire\WithPagination;

class CouponsComponent extends Component
{
    use WithPagination;
    public function deletecoupon($id){
        $coupon=Coupon::find($id);
        $coupon->delete();
        session()->flash('delete_couponsuccessfully');
    }
    public function render()
    {
        $coupons=Coupon::orderBy('id','DESC')->paginate(10);
        return view('livewire.backend.coupons.coupons-component',['coupons'=>$coupons])->layout('layouts.backend.app');
    }

}
