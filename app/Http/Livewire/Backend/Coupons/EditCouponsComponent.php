<?php

namespace App\Http\Livewire\Backend\Coupons;

use App\Models\Coupon;
use Livewire\Component;

class EditCouponsComponent extends Component
{

    public $code;
    public $type;
    public $value;
    public $cart_value;
    public $coupon_id;
    public $coupon_slug;
    public $expiry_date;
    public function mount($coupon_slug)
    {
        $this->coupon_slug=$coupon_slug;
        $coupon=Coupon::where('id',$coupon_slug)->first();
        $this->coupon_id=$coupon->id;
        $this->code=$coupon->code;
        $this->type=$coupon->type;
        $this->value=$coupon->value;
        $this->cart_value=$coupon->cart_value;
        $this->expiry_date=$coupon->expiry_date;
    }
    public function updated($fields)
    {
      $this->validateOnly($fields,[
        'type' =>'required',
        'value' =>'required|numeric',
        'cart_value' =>'required|numeric',
      ]);
    }
    public function updatecoupon(){
        $this->validate([
            'type' =>'required',
            'value' =>'required|numeric',
            'cart_value' =>'required|numeric',
            'expiry_date' =>'required',
       ]);
        $coupon=Coupon::find($this->coupon_id);
        $coupon->code=$this->code;
        $coupon->type=$this->type;
        $coupon->value=$this->value;
        $coupon->cart_value=$this->cart_value;
        $coupon->expiry_date=$this->expiry_date;
        $coupon->save();
        session()->flash('update_couponsuccessfully');
        return redirect()->route('admin.coupons');

    }

    public function render()
    {
        return view('livewire.backend.coupons.edit-coupons-component')->layout('layouts.backend.app');
    }
}
