<?php

namespace App\Http\Livewire\Backend\P1Ecommerce;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\P1\P1Units;
use App\Models\P1\P1Catalogs;
use App\Models\P1\P1Products;
use Livewire\WithFileUploads;
use App\Models\P1\P1CurrencyRate;
use Carbon\Carbon;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;

class P1ProductComponent extends Component
{
    use WithPagination;
    use WithFileUploads;
    protected $paginationTheme = 'bootstrap';
    public $code,$name,$branch_id,$profit_rate,$file_excel,$price_to_kip,$price_to_dollar,$online_price,$search,$hiddenId,$wholesale_price,$import_price,$image,$vat,$note,$des,$long_des,$thumb,$link,$min_reserve,$catalog_id,$unit_name,$new_image;
    public function render()
    {
        $search = $this->search;
        $exchange  = P1CurrencyRate::where('branch_id', auth()->user()->branch_id)->first();
        $this->profit_rate = $exchange->profit_rate;
        $this->currency = $exchange->get_currencies->symbol !=null ? $exchange->get_currencies->symbol : '';
        $this->price_to_kip = 0;
        $cal_wholesale_price = intval(str_replace(',','',$this->wholesale_price));
        if($cal_wholesale_price > 0 && $exchange){
            if($exchange->rate_to_kip_operator =='*'){
                error_log($cal_wholesale_price);
               $this->price_to_kip = number_format((($cal_wholesale_price * ($this->profit_rate)) + $cal_wholesale_price) * $exchange->rate_to_kip   , 2);
            } if($exchange->rate_to_kip_operator =='/'){
                $this->price_to_kip = number_format((($cal_wholesale_price * ($this->profit_rate)) + $cal_wholesale_price) / $exchange->rate_to_kip   , 2);
            }
            if($exchange->rate_to_dollar_operator == '/'){
                $this->price_to_dollar = number_format((($cal_wholesale_price * ($this->profit_rate)) + $cal_wholesale_price)  / $exchange->rate_to_dollar, 2);
            }else if($exchange->rate_to_dollar_operator == '*'){
                $this->price_to_dollar = number_format((($cal_wholesale_price * ($this->profit_rate)) + $cal_wholesale_price) * $exchange->rate_to_dollar, 2);
            }
        }else{
           $this->online_price = 0;
           $this->price_to_kip = 0;
           $this->price_to_dollar = 0;
        }
        if($this->catalog_id){
            $data = P1Products::where('del', 0)->where('catalog_id', $this->catalog_id)->where(function ($q) use ($search){
                $q->where('name', 'like', '%' . $search . '%')
                ->orwhere('code', 'like', '%' . $search . '%')
                ->orwhere('wholesale_price', 'like', '%' . $search . '%')
                ->orwhere('import_price', 'like', '%' . $search . '%');
              })->orderBy('id', 'DESC')->paginate(10);
        }else{
            $data = P1Products::where('del', 0)->where(function ($q) use ($search){
            $q->where('name', 'like', '%' . $search . '%')
            ->orwhere('code', 'like', '%' . $search . '%')
            ->orwhere('wholesale_price', 'like', '%' . $search . '%')
            ->orwhere('import_price', 'like', '%' . $search . '%');
            })->orderBy('id', 'DESC')->paginate(10);
      }
        $this->page = (($this->page * 10) -10) + 1;
        $categories  = P1Catalogs::where('del',0)->get();
        $units = P1Units::where('del', 0)->get();
        return view('livewire.backend.p1-ecommerce.p1-product-component', ['exchange' => $exchange,'categories' => $categories,'units' => $units,'data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function resetstoresForm()
    {
        $this->code = '';
        $this->name = '';
        $this->branch_id = '';
        $this->wholesale_price = '';
        $this->import_price = '';
        $this->image = '';
        $this->vat = '';
        $this->note = '';
        $this->des = '';
        $this->long_des = '';
        $this->thumb = '';
        $this->link = '';
        $this->min_reserve = '';
        $this->catalog_id = '';
        $this->unit_name = '';
        $this->new_image = '';
    }
    protected $rules = [
            'code' => 'required|unique:p1_products',
            'name' => 'required',
            'catalog_id' => 'required',
            'import_price' => 'required',
            'wholesale_price' => 'required',
            'image' => 'required',
            'unit_name' => 'required',
    ];
    protected $messages = [
            'code.required' => 'ໃສ່ລະຫັດສຶນຄ້າກ່ອນ!',
            'name.required' => 'ໃສ່ຊື່ສິນຄ້າກ່ອນ!',
            'catalog_id.required' => 'ເລືອກຮ້ານກ່ອນ!',
            'wholesale_price.required' => 'ໃສ່ລາຄາຂາຍກ່ອນ!',
            'import_price.required' => 'ໃສ່ລາຄາຊື້ກ່ອນ!',
            'image.required' => 'ເລຶອກໃສ່ຮູບພາບກ່ອນ!',
            'code.unique' => 'ລະຫັດນີ້ມີໃນລະບົບແລ້ວ!',
            'unit_name.required' => 'ເລືອກຫົວໜ່ວຍກ່ອນ',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function storeStore()    
    {
        $this->validate();
        if(intval(str_replace(',','',$this->import_price)) <=0 ){
            $this->emit('alert', ['type' => 'success', 'message' => 'ລາຄາຊື້ຕ້ອງເປັນຕົວເລກ!']);
            return;
        }
        if(intval(str_replace(',','',$this->wholesale_price)) <=0 ){
            $this->emit('alert', ['type' => 'success', 'message' => 'ລາຄາຂາຍຕ້ອງເປັນຕົວເລກ!']);
            return;
        }
        $data = new P1Products();
        $data->code = $this->code;
        $data->name = $this->name;
        $data->branch_id = auth()->user()->branch_id;
        $data->wholesale_price = str_replace(',','',$this->wholesale_price);
        $data->import_price = str_replace(',','',$this->import_price);
        if($this->vat){
            $data->vat = $this->vat;
          }
        $data->note = $this->note;
        $data->des = $this->des;
        $data->long_des = $this->long_des;
        $data->thumb = $this->thumb;
        $data->link = $this->link;
        if($this->min_reserve){
           $data->min_reserve = $this->min_reserve;
        }
        $data->catalog_id = $this->catalog_id;
        $data->unit_name = $this->unit_name;
        if (!empty($this->image)) {
            $this->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            $this->image->storeAs('upload/p1-ecommerce/product', $imageName);
            $data->image = 'upload/p1-ecommerce/product/'.$imageName;
        }
        $data->save();
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('hide-modal-add');
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນຂໍ້ມູນສຳເລັດ!']);
    }
    public function create()
    {
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('show-modal-add');
    }
    public function showEditStore($ids)
    {
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('show-modal-add');
        $singleData = P1Products::find($ids);
        $this->hiddenId = $singleData->id;
        $this->code = $singleData->code;
        $this->name = $singleData->name;
        $this->branch_id = $singleData->branch_id;
        $this->wholesale_price = $singleData->wholesale_price;
        $this->import_price = $singleData->import_price;
        $this->vat = $singleData->vat;
        $this->note = $singleData->note;
        $this->des = $singleData->des;
        $this->long_des = $singleData->long_des;
        $this->thumb = $singleData->thumb;
        $this->link = $singleData->link;
        $this->min_reserve = $singleData->min_reserve;
        $this->catalog_id = $singleData->catalog_id;
        $this->unit_name = $singleData->unit_name;
        $this->new_image = $singleData->image;
    }
    public function updateStore()
    {
        $this->validate([
            'name' => 'required',
            'catalog_id' => 'required',
            'import_price' => 'required',
            'wholesale_price' => 'required',
            'unit_name' => 'required',
        ],
        [
                'name.required' => 'ໃສ່ຊື່ສິນຄ້າກ່ອນ!',
                'catalog_id.required' => 'ເລືອກຮ້ານກ່ອນ!',
                'wholesale_price.required' => 'ໃສ່ລາຄາຂາຍກ່ອນ!',
                'import_price.required' => 'ໃສ່ລາຄາຊື້ກ່ອນ!',
                'unit_name.required' => 'ເລືອກຫົວໜ່ວຍກ່ອນ',
        ]);
        $ids = $this->hiddenId;
        $singleData = P1Products::find($ids);
        $singleData->name = $this->name;
        $singleData->branch_id = auth()->user()->branch_id;
        $singleData->wholesale_price = str_replace(',','',$this->wholesale_price);
        $singleData->import_price = str_replace(',','',$this->import_price);
        if($this->vat){
          $singleData->vat = $this->vat;
        }
        $singleData->note = $this->note;
        $singleData->des = $this->des;
        $singleData->long_des = $this->long_des;
        $singleData->thumb = $this->thumb;
        $singleData->link = $this->link;
        if($this->min_reserve){
           $singleData->min_reserve = $this->min_reserve;
        }
        $singleData->catalog_id = $this->catalog_id;
        $singleData->unit_name = $this->unit_name;
        if (!empty($this->image)) {
            $this->validate([
                'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            if ($this->image != $singleData->image) {
                if (file_exists($singleData->image)) {
                    unlink($singleData->image);
                }

                if ($singleData->images) {
                    $images = explode(",", $singleData->images);
                    foreach ($images  as $image) {
                        unlink($singleData->image);
                    }
                    $singleData->delete();
                }
            }
            $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            $this->image->storeAs('upload/p1-ecommerce/product', $imageName);
            $singleData->image = 'upload/p1-ecommerce/product/'.$imageName;
        }
        $singleData->save();
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('hide-modal-add');
        $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
    }

    //Show and Delete Store
    public function showDestroyStore($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete-product');
        $singleData = P1Products::find($ids);
        $this->hiddenId = $singleData->id;
        $this->code = $singleData->code;
    }
    public function destroyStore()
    {
        $ids = $this->hiddenId;
        $singleData = P1Products::find($ids);
        $singleData->del= 1;
        $singleData->update();
        $delete = DB::table('products')->where('code', $this->code)->update(array('del' => '0'));
        $this->dispatchBrowserEvent('hide-modal-delete-product');
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    }
    public function showViewStore($ids)
    {
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('show-modal-view-product');
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $singleData = P1Products::find($ids);
        $this->hiddenId = $singleData->id;
    }

    public function destroy()
    {
        $singleData = P1Products::find($this->hiddenId);
        $singleData->del = 1;
        $singleData->update();
        $this->resetstoresForm();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    }
    public function show_import_excel(){
        $this->dispatchBrowserEvent('show-modal-excel');
    }
    public function ImportExel() 
    {
        $this->validate([
            'file_excel' => 'required|mimes:xlsx, csv, xls',
        ]);
        try{
            Excel::import(new ProductImport, $this->file_excel);
            $this->dispatchBrowserEvent('hide-modal-excel');
            $this->emit('alert',['type'=>'success','message'=>'ດຶງຂໍ້ມູນExcelສຳລັດ!']);
        }catch(\Exception $ex){
            dd($ex);
            $this->emit('alert',['type'=>'warning','message'=>'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
}