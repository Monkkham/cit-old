<?php

namespace App\Http\Livewire\Backend\P1Ecommerce;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\P1\P1CurrencyRate;
use App\Models\Branch;
use App\Models\Currency;
class ExchangeComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId, $rate_to_dollar,$currency_name, $search,$profit_rate, $rate_to_kip,$branch_id, $currencies_id, $rate_to_dollar_operator, $rate_to_kip_operator;
    public function mount(){
        if(auth()->user()->branch_id){
            $currencyData = P1CurrencyRate::where('branch_id', auth()->user()->branch_id)->first();
            $this->hiddenId = $currencyData->id;
            $this->branch_id = $currencyData->branch_id;
            $this->currencies_id = $currencyData->currencies_id;
            $this->rate_to_dollar_operator = $currencyData->rate_to_dollar_operator;
            $this->rate_to_kip_operator = $currencyData->rate_to_kip_operator;
            $this->rate_to_kip = $currencyData->rate_to_kip;
            $this->rate_to_dollar = $currencyData->rate_to_dollar;
            $this->profit_rate = $currencyData->profit_rate;
            $this->currency_name = $currencyData->get_currencies->symbol;
        }
    }
    public function render()
    {
        if($this->currencies_id){
            $this->currency_name = Currency::find($this->currencies_id)->symbol;
        }
        $search = $this->search;
        $branches = Branch::where('status', 1)->get();
        $data = P1CurrencyRate::orderBy('id', 'desc')->orderBy('id', 'DESC')->paginate(10);
        $currencies = Currency::where('del', 0)->orderBy('id', 'desc')->get();
        $this->page = (($this->page * 10) -10) + 1;
        return view('livewire.backend.p1-ecommerce.exchange-component', ['currencies' => $currencies,'branches' => $branches,'data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function resetField()
    {
        $this->rate_to_dollar = '';
        $this->rate_to_kip = '';
        $this->hiddenId = '';
        $this->mount();
    }
    protected $rules = [
        'rate_to_dollar' => 'required',
        'branch_id' => 'required',
        'currencies_id' => 'required'
    ];
    protected $messages = [
        'rate_to_dollar.required' => 'ອັດຕາແລກປ່ຽນເປັນໂດລາ!',
        'branch_id.required' => 'ເລືອກສາຂາກ່ອນ!',
        'currencies_id.required' => 'ເລືອກສະກຸນເງິນກ່ອນ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
            $data = P1CurrencyRate::find($this->hiddenId);
            $data->update([
                'currencies_id' => $this->currencies_id,
                'rate_to_dollar' => $this->rate_to_dollar,
                'profit_rate' => $this->profit_rate,
                'rate_to_dollar_operator' => $this->rate_to_dollar_operator,
                'rate_to_kip_operator' => $this->rate_to_kip_operator,
                'rate_to_kip' => $this->rate_to_kip,
            ]);
            $this->resetField();
             $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
    }
}
