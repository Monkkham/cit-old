<?php

namespace App\Http\Livewire\Backend\P1Ecommerce\Purchase;

use App\Models\Branch;
use App\Models\P1\P1Contact;
use App\Models\P1\P1Products;
use App\Models\P1\P1PurchaseLogs;
use App\Models\P1\P1PurchaseLogsDetail;
use App\Models\P1\P1PurchaseTransaction;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class PurchaseOrderComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $contact_id, $status, $confirm_status, $paid, $subtotal, $orderdetailData  = [], $transactionData, $payment_status, $start_date, $end_date, $branch_id, $search;
    public function render()
    {
        $contacts = P1Contact::where('del', 0)->orderBy('id', 'desc')->get();
        $branches = Branch::where('status', 1)->get();
        if (auth()->user()->branch_id) {
            $this->branch_id = auth()->user()->branch_id;
        }
        if ($this->contact_id && !$this->status && !$this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && $this->status && !$this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.status', $this->status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif ($this->contact_id && $this->status && !$this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->where('p1_purchase_logs.status', $this->status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && !$this->status && $this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('t.status', $this->payment_status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif ($this->contact_id && !$this->status && $this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->where('t.status', $this->payment_status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif ($this->contact_id && $this->status && $this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->where('t.status', $this->payment_status)->where('p1_purchase_logs.status', $this->status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && !$this->status && !$this->payment_status && $this->start_date && $this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->whereBetween('p1_purchase_logs.created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . '23:59:59'))])->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif ($this->contact_id && !$this->status && !$this->payment_status && $this->start_date && $this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->whereBetween('p1_purchase_logs.created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . '23:59:59'))])->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && $this->status && !$this->payment_status && $this->start_date && $this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.status', $this->status)->whereBetween('p1_purchase_logs.created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . '23:59:59'))])->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && !$this->status && $this->payment_status && $this->start_date && $this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('t.status', $this->payment_status)->whereBetween('p1_purchase_logs.created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . '23:59:59'))])->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif (!$this->contact_id && $this->status && $this->payment_status && !$this->start_date && !$this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.status', $this->status)->where('t.status', $this->payment_status)->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } elseif ($this->contact_id && $this->status && $this->payment_status && $this->start_date && $this->end_date) {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->where('p1_purchase_logs.contact_id', $this->contact_id)->where('t.status', $this->payment_status)->where('p1_purchase_logs.status', $this->status)->whereBetween('p1_purchase_logs.created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . '23:59:59'))])->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        } else {
            $data = P1PurchaseLogs::select('p1_purchase_logs.*', 't.amount', 't.paid_amount', 't.status as t_status')->join('p1_purchase_transaction as t', 't.purchase_logs_id', '=', 'p1_purchase_logs.id')->where('p1_purchase_logs.code', 'like', '%' . $this->search . '%')->orderBy('p1_purchase_logs.id', 'desc')->paginate(10);
        }
        $this->page = (($this->page * 10) - 10) + 1;
        return view('livewire.backend.p1-ecommerce.purchase.purchase-order-component', ['branches' => $branches, 'data' => $data, 'contacts' => $contacts, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function showDetail($id)
    {
        $this->transactionData = P1PurchaseTransaction::where('purchase_logs_id', $id)->first();
        $this->orderdetailData = [];
        $this->orderdetailData = P1PurchaseLogsDetail::where('purchase_log_id', $id)->get();
        $this->dispatchBrowserEvent('show-modal-detail');
    }
    public function showConfirm($id)
    {
        $this->transactionData = '';
        $this->orderdetailData = [];
        $this->confirm_status  = '';
        $this->transactionData = P1PurchaseTransaction::where('purchase_logs_id', $id)->first();
        $this->subtotal = $this->transactionData->amount;
        $this->paid = $this->transactionData->paid_amount;
        if($this->transactionData->order->status =='received'){
            $this->confirm_status  = $this->transactionData->order->status;
        }
        $this->orderdetailData = P1PurchaseLogsDetail::where('purchase_log_id', $id)->get();
        $this->dispatchBrowserEvent('show-modal-confirm');
    }
    public function confirmOrder()
    {
        $this->validate([
            'confirm_status' => 'required'
        ], [
            'confirm_status.required' => 'ກະລຸນາເລືອກສະຖານະກ່ອນ !'
        ]);
        if (intval(str_replace(',', '', $this->paid)) > $this->subtotal) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ຈໍານວນເງິນຊໍາລະຕ້ອງໜ້ອຍກວ່າ ຫຼື ເທົ່າກັບໜີ້ຄ້າງຈ່າຍ!']);
            return;
        }
        try {
            DB::beginTransaction();
            $data = P1PurchaseLogs::find($this->transactionData->order->id);
            if ($this->confirm_status == 'received') {
                $orderItem = P1PurchaseLogsDetail::where('purchase_log_id', $data->id)->get();
                foreach ($orderItem as $item) {
                    $update_stock = P1Products::find($item->product_id);
                    if ($update_stock) {
                        $update_stock->qty = $update_stock->qty + $item->qty;
                        $update_stock->update();
                    }
                }
            }
            $data->status = $this->confirm_status;
            $data->update();
            $transaction = P1PurchaseTransaction::find($this->transactionData->id);
            if ($this->subtotal == intval(str_replace(',', '', $this->paid)) && intval(str_replace(',', '', $this->paid)) > 0) {
                $transaction->paid_amount = intval(str_replace(',', '', $this->paid));
                $transaction->status = 'paid';
            } elseif (intval(str_replace(',', '', $this->paid)) < $this->subtotal && intval(str_replace(',', '', $this->paid)) > 0) {
                $transaction->paid_amount = intval(str_replace(',', '', $this->paid));
                $transaction->status = 'partial';
            }else{
                $transaction->paid_amount = intval(str_replace(',', '', $this->paid));
                $transaction->status = 'due';
            }
            $transaction->update();
            DB::commit();
            $this->dispatchBrowserEvent('hide-modal-confirm');
            $this->emit('alert', ['type' => 'success', 'message' => 'ຢືນຢັນການສັ່ງຊື້ສໍາເລັດແລ້ວ!']);
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
}
