<?php

namespace App\Http\Livewire\Backend\P1Ecommerce\Purchase;

use App\Models\Branch;
use App\Models\P1\P1Contact;
use App\Models\P1\P1Products;
use App\Models\P1\P1PurchaseLogs;
use App\Models\P1\P1PurchaseLogsDetail;
use App\Models\P1\P1PurchaseTransaction;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;


class PurchaseComponent extends Component
{
    use WithFileUploads;
    public $purchase_date, $subtotal, $status,$hiddenId, $paid, $cart = [], $code, $pro_id, $contact_id, $branch_id, $qty, $attacth_file, $transaction_status;
    public function mount()
    {
        $this->purchase_date = date('Y-m-d');
        $this->transaction_status = 'due';
        $this->status = 'order';
    }
    public  $search, $name,
        $phone,
        $email,
        $more_detail;
    public function render()
    {
        $contacts = P1Contact::where('del', 0)->orderBy('id', 'desc')->get();
        if ($this->pro_id) {
            $this->addToCart($this->pro_id);
        }
        $products = P1Products::where('del', 0)->orderBy('id', 'desc')->get();
        $branches = Branch::where('status', 1)->get();
        if (auth()->user()->branch_id) {
            $this->branch_id = auth()->user()->branch_id;
        }
        // Cart::instance('import_order_cart')->destroy();
        $this->calculateCart();
        return view('livewire.backend.p1-ecommerce.purchase.purchase-component', ['branches' => $branches, 'products' => $products, 'contacts' => $contacts])->layout('layouts.backend.app');
    }
    public function calculateCart()
    {
        $this->subtotal = 0;
        foreach (Cart::instance('import_order_cart')->content() as $item) {
            $this->subtotal += $item->qty * $item->model->import_price;
        }
    }
    public function addToCart($id)
    {
        $product = Product::find($id);
        if ($product) {
            Cart::instance('import_order_cart')->add($product->id, $product->name, 1, $product->import_price)->associate('App\Models\P1\P1Products');
        }
        $this->pro_id = '';
    }
    public function increaseQty($rowId)
    {
        try {
            $product = Cart::instance('import_order_cart')->get($rowId);
            $qty = $product->qty + 1;
            Cart::instance('import_order_cart')->update($rowId, $qty);
            $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂສໍາເລັດແລ້ວ!']);
        } catch (\Exception $ex) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
    //Qty -1
    public function decreaseQty($rowId)
    {
        try {
            $product = Cart::instance('import_order_cart')->get($rowId);
            $qty = $product->qty - 1;
            Cart::instance('import_order_cart')->update($rowId, $qty);
            $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂສໍາເລັດແລ້ວ!']);
        } catch (\Exception $ex) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
    public function showdestroy($rowId)
    {
        $this->hiddenId  = '';
        $this->hiddenId = $rowId;
        $this->dispatchBrowserEvent('show-modal-delete');
    }
    public function destroy($rowId)
    {
        $cart = Cart::instance('import_order_cart')->content()->where('rowId', $rowId);
        if ($cart->isNotEmpty()) {
            Cart::instance('import_order_cart')->remove($rowId);
            $this->dispatchBrowserEvent('hide-modal-delete');
            $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສໍາເລັດ!']);
        }
    }
    public function showeditCart($rowId)
    {
        $this->hiddenId  = '';
        $this->hiddenId = $rowId;
        $product = Cart::instance('import_order_cart')->get($rowId);
        if ($product) {
            $this->qty = $product->qty;
        }
        $this->dispatchBrowserEvent('show-modal-edit-cart');
    }
    public function updateCart($id)
    {
        Cart::instance('import_order_cart')->update($id, $this->qty);
        $this->dispatchBrowserEvent('hide-modal-edit-cart');
        $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂສໍາເລັດແລ້ວ!']);
    }
    public function resetField()
    {
        $this->name = '';
        $this->phone = '';
        $this->email = '';
        $this->more_detail = '';
        $this->hiddenId = '';
        $this->paid = '';
        $this->code = '';
        $this->attacth_file = null;
    }
    public function create()
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-add');
    }
    protected $rules = [
        'contact_id' => 'required',
        'purchase_date' => 'required',
    ];
    protected $messages = [
        'purchase_date.required' => 'ໃສ່ວັນທີກ່ອນ!',
        'contact_id.required' => 'ເລືອກຂໍ້ມຸນຕິດກ່ອນ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function storeContact()
    {

        $this->validate([
            'phone' => 'required|unique:p1_contact',
            'name' => 'required',
            // 'email' => 'required|email',
            // 'more_detail' => 'required',
        ], [
            'name.required' => 'ໃສ່ຊື່ກ່ອນ!',
            'phone.required' => 'ໃສ່ເບີໂທກ່ອນ!',
            'phone.unique' => 'ເບີໂທນີ້ມີໃນລະບົບແລ້ວ!',
            // 'email.required' => 'ໃສ່ອີເມວກ່ອນ',
            // 'email.email' => 'ອີເມວຕ້ອງເປັນ example@gmail.com',
            // 'more_detail.required' => 'ໃສ່ລາຍລະອຽດກ່ອນ',
        ]);
        $data = new P1Contact();
        $data->name = $this->name;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->more_detail = $this->more_detail;
        $data->save();
        $this->resetField();
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        return redirect()->route('admin.p1-purchase');
    }
    public function generateRandomNumber($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function placeOrder()
    {
        $this->validate();
        if ($this->attacth_file) {
            $this->validate(
                [
                    'attacth_file' => 'required|max:5MB|mimes:pdf,csv,zip,doc,docx,jpeg,jpg,png'
                ],
                [
                    'attacth_file.max' => 'ຂະໜາດໄຟລ: 5MB',
                    'attacth_file.mimes' => 'ເອກະສານທີ່ອະນຸຍາດ: .pdf, .csv, .zip, .doc, .docx, .jpeg, .jpg, .png'
                ]
            );
        }
        if (Cart::instance('import_order_cart')->count() <= 0) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ຍັງບໍ່ມີລາຍການສິນຄ້າ!']);
            return;
        }
        $this->code = $this->generateRandomNumber(6);
        if (P1PurchaseLogs::where('code', $this->code)->first()) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ລະຫັດສັ່ງຊື້ຊໍ້າກັນກະລຸນາລອງອີກຄັ້ງ!']);
            return;
        }
        if (intval(str_replace(',', '', $this->paid)) > $this->subtotal) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ຈໍານວນເງິນຊໍາລະຕ້ອງໜ້ອຍກວ່າຈໍານວນລວມທັງໝົດ!']);
            return;
        }
        try {
            DB::beginTransaction();
            $p1_purchase_logs = new P1PurchaseLogs();
            $p1_purchase_logs->contact_id = $this->contact_id;
            $p1_purchase_logs->code = $this->code;
            $p1_purchase_logs->branch_id = $this->branch_id;
            $p1_purchase_logs->purchase_date = $this->purchase_date;
            if ($this->attacth_file) {
                $p1_purchase_logs->attacth_file = $this->attacth_file->store("upload/purchase/file/");
            }
            if($this->status){
                $p1_purchase_logs->status = $this->status;
            }
            $p1_purchase_logs->save();
            foreach (Cart::instance('import_order_cart')->content() as $item) {
                $p1_purchase_logs_detail = new P1PurchaseLogsDetail();
                $p1_purchase_logs_detail->purchase_log_id = $p1_purchase_logs->id;
                $p1_purchase_logs_detail->product_id = $item->id;
                $p1_purchase_logs_detail->qty = $item->qty;
                $p1_purchase_logs_detail->product_price = $item->model->import_price;
                $p1_purchase_logs_detail->total_price = $item->model->import_price * $item->qty;
                $p1_purchase_logs_detail->save();
                $update_stock = P1Products::find($item->model->id);
                if($update_stock && $this->status =='received'){
                    $update_stock->qty = $update_stock->qty + $item->qty;
                    $update_stock->update();
                }
            }
            $p1_purchase_logs_transaction = new P1PurchaseTransaction();
            $p1_purchase_logs_transaction->purchase_logs_id = $p1_purchase_logs->id;
            $p1_purchase_logs_transaction->amount = $this->subtotal;
            if ($this->subtotal == intval(str_replace(',', '', $this->paid)) && intval(str_replace(',', '', $this->paid)) > 0) {
                $p1_purchase_logs_transaction->paid_amount = intval(str_replace(',', '', $this->paid));
                $p1_purchase_logs_transaction->status = 'paid';
            } elseif (intval(str_replace(',', '', $this->paid)) < $this->subtotal && intval(str_replace(',', '', $this->paid)) > 0) {
                $p1_purchase_logs_transaction->paid_amount = intval(str_replace(',', '', $this->paid));
                $p1_purchase_logs_transaction->status = 'partial';
            }
            $p1_purchase_logs_transaction->save();
            DB::commit();
            Cart::instance('import_order_cart')->destroy();
            $this->resetField();
            $this->emit('alert', ['type' => 'success', 'message' => 'ສັ່ງຊື້ສໍາເລັດແລ້ວ!']);
        } catch (\Exception  $ex) {
            DB::rollBack();
            // dd($ex);
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
}
