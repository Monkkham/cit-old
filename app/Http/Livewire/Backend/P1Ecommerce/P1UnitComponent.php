<?php

namespace App\Http\Livewire\Backend\P1Ecommerce;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\P1\P1Units;
class P1UnitComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId, $name_la, $search, $name_en;
    public function render()
    {
        $search = $this->search;
        $data = P1Units::where('del', 0)->where(function ($q) use ($search){
          $q->where('name_la', 'like', '%' . $search . '%')
          ->orwhere('name_en', 'like', '%' . $search . '%');
        })->orderBy('id', 'DESC')->paginate(10);
        $this->page = (($this->page * 10) -10) + 1;
        return view('livewire.backend.p1-ecommerce.p1-unit-component', ['data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function resetField()
    {
        $this->name_la = '';
        $this->name_en = '';
        $this->hiddenId = '';
    }
    protected $rules = [
        'name_la' => 'required|unique:p1_units',
    ];
    protected $messages = [
        'name_la.required' => 'ໃສ່ຊື່ຫົວໜ່ວຍສິນຄ້າກ່ອນ!',
        'name_la.unique' => 'ຊື່ຫົວໜ່ວຍສິນຄ້ານີ້ໄດ້ມີໃນລະບົບແລ້ວ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $updateId = $this->hiddenId;

        if ($updateId > 0)
        {
            $data = P1Units::find($updateId);
            $data->update([
                'name_la' => $this->name_la,
                'name_en' => $this->name_en,
            ]);
            $this->resetField();
             $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        } else //ເພີ່ມໃໝ່
        {
            $this->validate();
            $data = new P1Units();
            $data->name_la = $this->name_la;
            $data->name_en = $this->name_en;
            $data->save();
            $this->resetField();
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }

    }

    public function edit($ids)
    {
        $singleData = P1Units::find($ids);
        $this->hiddenId = $singleData->id;
        $this->name_la = $singleData->name_la;
        $this->name_en = $singleData->name_en;
    }

    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $singleData = P1Units::find($ids);
        $this->hiddenId = $singleData->id;
    }

    public function destroy()
    {
        $singleData = P1Units::find($this->hiddenId);
        $singleData->del = 1;
        $singleData->update();
        $this->resetField();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    }
    
}
