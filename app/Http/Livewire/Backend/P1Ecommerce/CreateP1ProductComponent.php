<?php

namespace App\Http\Livewire\Backend\P1Ecommerce;

use Livewire\Component;
use App\Models\P1\P1Units;
use App\Models\P1\P1Catalogs;
use App\Models\P1\P1Products;
use Livewire\WithFileUploads;
use App\Models\P1\P1CurrencyRate;
use Carbon\Carbon;
use App\Http\Controllers\Backend\HelperController;
use App\Models\P1\P1ProductImages;
use Illuminate\Support\Facades\DB;

class CreateP1ProductComponent extends Component
{
    use WithFileUploads;
    public $code, $name, $branch_id, $profit_rate, $currency, $price_to_kip, $price_to_dollar, $online_price, $search, $hiddenId, $wholesale_price, $import_price, $image, $vat, $note, $des, $long_des, $thumb, $link, $min_reserve, $catalog_id, $unit_name,
        $qty, $new_image, $mutiple_image, $product_id;
    public function mount()
    {
        $this->product_id = '';
    }
    public function render()
    {
        $exchange  = P1CurrencyRate::where('branch_id', auth()->user()->branch_id)->first();
        $this->profit_rate = $exchange->profit_rate;
        $this->currency = $exchange->get_currencies->symbol != null ? $exchange->get_currencies->symbol : '';
        $this->price_to_kip = 0;
        $cal_wholesale_price = intval(str_replace(',', '', $this->wholesale_price));
        if ($cal_wholesale_price > 0 && $exchange) {
            if ($exchange->rate_to_kip_operator == '*') {
                $this->price_to_kip = number_format((($cal_wholesale_price * ($this->profit_rate))) * $exchange->rate_to_kip, 2);
            } else if ($exchange->rate_to_kip_operator == '/') {
                $this->price_to_kip = number_format((($cal_wholesale_price * ($this->profit_rate))) / $exchange->rate_to_kip, 2);
            }
            if ($exchange->rate_to_dollar_operator == '/') {
                $this->price_to_dollar = number_format((($cal_wholesale_price * ($this->profit_rate)))  / $exchange->rate_to_dollar, 2);
            } else if ($exchange->rate_to_dollar_operator == '*') {
                $this->price_to_dollar = number_format((($cal_wholesale_price * ($this->profit_rate))) * $exchange->rate_to_dollar, 2);
            }
        } else {
            $this->online_price = 0;
            $this->price_to_kip = 0;
            $this->price_to_dollar = 0;
        }
        $categories  = P1Catalogs::where('del', 0)->get();
        $units = P1Units::where('del', 0)->get();
        $productmages = P1ProductImages::where('product_id', $this->product_id)->get();
        return view('livewire.backend.p1-ecommerce.create-p1-product-component', compact('productmages', 'units', 'exchange', 'categories'))->layout('layouts.backend.app');
    }
    public function resetstoresForm()
    {
        $this->code = '';
        $this->name = '';
        $this->branch_id = '';
        $this->wholesale_price = '';
        $this->import_price = '';
        $this->image = '';
        $this->vat = '';
        $this->note = '';
        $this->des = '';
        $this->long_des = '';
        $this->thumb = '';
        $this->link = '';
        $this->min_reserve = '';
        $this->catalog_id = '';
        $this->unit_name = '';
        $this->new_image = '';
        $this->qty = '';
    }
    protected $rules = [
        'code' => 'required|unique:p1_products',
        'name' => 'required',
        'catalog_id' => 'required',
        'import_price' => 'required',
        'wholesale_price' => 'required',
        'unit_name' => 'required',
        // 'qty' => 'required',
    ];
    protected $messages = [
        'code.required' => 'ໃສ່ລະຫັດສຶນຄ້າກ່ອນ!',
        'name.required' => 'ໃສ່ຊື່ສິນຄ້າກ່ອນ!',
        'catalog_id.required' => 'ເລືອກຮ້ານກ່ອນ!',
        'wholesale_price.required' => 'ໃສ່ລາຄາຂາຍກ່ອນ!',
        'import_price.required' => 'ໃສ່ລາຄາຊື້ກ່ອນ!',
        'code.unique' => 'ລະຫັດນີ້ມີໃນລະບົບແລ້ວ!',
        'unit_name.required' => 'ເລືອກຫົວໜ່ວຍກ່ອນ',
        // 'qty.required' => 'ໃສ່ຈໍານວນກ່ອນ',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function storeStore()
    {
        $this->validate();
        $this->product_id = '';
        if (intval(str_replace(',', '', $this->import_price)) <= 0) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ລາຄາຊື້ຕ້ອງເປັນຕົວເລກ!']);
            return;
        }
        if (intval(str_replace(',', '', $this->wholesale_price)) <= 0) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ລາຄາຂາຍຕ້ອງເປັນຕົວເລກ!']);
            return;
        }
        if (str_replace(',', '', $this->wholesale_price) <= str_replace(',', '', $this->import_price)) {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ລາຄາຊື້ຕ້ອງໜ້ອຍກ່ວາລາຄາຂາຍ!']);
            return;
        }
        // try {
            DB::beginTransaction();
            $data = new P1Products();
            $data->code = $this->code;
            $data->name = $this->name;
            $data->branch_id = auth()->user()->branch_id;
            $data->wholesale_price = str_replace(',', '', $this->wholesale_price);
            $data->import_price = str_replace(',', '', $this->import_price);
            if ($this->vat) {
                $data->vat = $this->vat;
            }
            $data->note = $this->note;
            $data->des = $this->des;
            $data->long_des = $this->long_des;
            $data->thumb = $this->thumb;
            $data->link = $this->link;
            if ($this->min_reserve) {
                $data->min_reserve = $this->min_reserve;
            }
            if ($this->qty) {
                $data->qty = $this->qty;
            }
            $data->catalog_id = $this->catalog_id;
            $data->unit_name = $this->unit_name;
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/p1-ecommerce/product', $imageName);
                $data->image = 'upload/p1-ecommerce/product/' . $imageName;
            }
            $data->save();
            $this->product_id = $data->id;
            DB::commit();
            $this->resetstoresForm();
            $this->dispatchBrowserEvent('hide-modal-add');
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນຂໍ້ມູນສຳເລັດ!']);
            // return redirect()->route('admin.p1-product');
        // } catch (\Exception $ex) {
        //     DB::rollBack();
        //     $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        // }
    }
    public function close()
    {
        $this->product_id = '';
        return redirect()->route('admin.p1-product');
    }
    public function add_product_images()
    {
        $this->validate([
            'mutiple_image' => 'required|mimes:jpg,png,jpeg',
        ]);
        $check_product = P1ProductImages::where('product_id', $this->product_id)->count();
        if ($check_product <= 1) {
            $mutiple_imageName = Carbon::now()->timestamp . '.' . $this->mutiple_image->extension();
            $this->mutiple_image->storeAs('upload/p1-ecommerce/product', $mutiple_imageName);
            P1ProductImages::create([
                'product_id' => $this->product_id,
                'image' => 'upload/p1-ecommerce/product/' . $mutiple_imageName,
            ]);
            $this->mutiple_image = null;
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນຂໍ້ມູນສຳເລັດ!']);
        } else {
            $this->emit('alert', ['type' => 'warning', 'message' => 'ເພີ່ມໄດ້ສູງສຸດສອງຮູບ!']);
        }
    }
}
