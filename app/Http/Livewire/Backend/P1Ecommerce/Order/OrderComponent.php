<?php

namespace App\Http\Livewire\Backend\P1Ecommerce\Order;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\P1\P1OrderTransaction;
use App\Models\P1\P1Order;
use App\Models\P1\P1OrderDetail;

use Carbon\Carbon;
use App\Http\Controllers\Backend\HelperController;
use App\Http\Controllers\CustomFunction;
use Illuminate\Support\Facades\DB;

class OrderComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search, $hiddenId, $orderdetailData = [], $orderData, $transactionData = [], $status, $check_orderdetail_status;
    public function render()
    {
        $search = $this->search;
        if ($this->status) {
            if ($this->status == 'all_delivery') {
                $data = P1Order::select(
                    'p1_order.*',
                    DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"),
                    DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"),
                    DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"),
                    DB::raw("(SELECT p1_order_detail.status from p1_order_detail where order_id = p1_order.id limit 0,1) as order_detail_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id) as all_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2) as partial_delivery"),
                )
                    ->where('p1_order.status', '=', 2)->where(DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id)"), '=', DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2)"))->where(DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2)"), '>', 0)->where(
                        function ($q) use ($search) {
                            $q->where('code', 'like', '%' . $search . '%')
                                ->orwhere('bcel_code', 'like', '%' . $search . '%');
                        }
                    )->paginate(10);
            } else if ($this->status == 'partial_delivery') {
                $data = P1Order::select(
                    'p1_order.*',
                    DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"),
                    DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"),
                    DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"),
                    DB::raw("(SELECT p1_order_detail.status from p1_order_detail where order_id = p1_order.id limit 0,1) as order_detail_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id) as all_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2) as partial_delivery"),
                )
                    ->where('p1_order.status', '=', 2)->where(DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id)"), '!=', DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2)"))->where(DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2)"), '>', 0)->where(
                        function ($q) use ($search) {
                            $q->where('code', 'like', '%' . $search . '%')
                                ->orwhere('bcel_code', 'like', '%' . $search . '%');
                        }
                    )->paginate(10);
            } else {
                $data = P1Order::select(
                    'p1_order.*',
                    DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"),
                    DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"),
                    DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"),
                    DB::raw("(SELECT p1_order_detail.status from p1_order_detail where order_id = p1_order.id limit 0,1) as order_detail_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id) as all_status"),
                    DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2) as partial_delivery"),
                )
                    ->where('p1_order.status', '!=', 0)->where('p1_order.status', $this->status)->where(
                        function ($q) use ($search) {
                            $q->where('code', 'like', '%' . $search . '%')
                                ->orwhere('bcel_code', 'like', '%' . $search . '%');
                        }
                    )->paginate(10);
            }
        } else {
            $data = P1Order::select(
                'p1_order.*',
                DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"),
                DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"),
                DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"),
                DB::raw("(SELECT p1_order_detail.status from p1_order_detail where order_id = p1_order.id limit 0,1) as order_detail_status"),
                DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id) as all_status"),
                DB::raw("(SELECT COUNT(*) from p1_order_detail where order_id = p1_order.id and status=2) as partial_delivery")
            )
                ->where('p1_order.status', '!=', 0)->where(
                    function ($q) use ($search) {
                        $q->where('code', 'like', '%' . $search . '%')
                            ->orwhere('bcel_code', 'like', '%' . $search . '%');
                    }
                )->paginate(10);
        }
        $this->page = (($this->page * 10) - 10) + 1;
        return view('livewire.backend.p1-ecommerce.order.order-component', ['data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function showDetail($id)
    {
        $this->orderData = P1Order::find($id);
        $this->orderdetailData = [];
        $this->transactionData = [];
        $this->transactionData = P1OrderTransaction::where('order_id', $id)->get();
        if (!empty(auth()->user()->branch_id)) {
            $this->orderdetailData = P1OrderDetail::where('order_id', $id)->where('branch_id', auth()->user()->branch_id)->get();
            $this->check_orderdetail_status = P1OrderDetail::where('order_id', $id)->where('branch_id', auth()->user()->branch_id)->first()->status;
        } else {
            $this->orderdetailData = [];
        }
        $this->dispatchBrowserEvent('show-modal-detail');
    }
    public function showapprove($id)
    {
        $this->orderData = [];
        $this->orderdetailData = [];
        $this->transactionData = [];
        $this->orderData = P1Order::find($id);
        $this->transactionData = P1OrderTransaction::where('order_id', $id)->get();
        $this->orderdetailData = P1OrderDetail::where('order_id', $id)->where('branch_id', auth()->user()->branch_id)->get();
        $this->check_orderdetail_status = P1OrderDetail::where('order_id', $id)->where('branch_id', auth()->user()->branch_id)->first()->status;
        $this->dispatchBrowserEvent('show-modal-confirm');
    }
    public function confirm_order()
    {
        try {
            DB::beginTransaction();
            if (!empty(auth()->user()->branch_id)) {
                $orderdetail = P1OrderDetail::where('order_id', $this->orderData->id)->where('branch_id', auth()->user()->branch_id)->get();
            } else {
                $orderdetail = [];
            }
            foreach ($orderdetail as $item) {
                if ($item->status == 1) {
                    $item->status = 2;
                    $item->delivery_confirm_time = Carbon::now();
                    $item->update();
                }
            }
            DB::commit();
            $custom = new CustomFunction();
            $custom->send_notification_to_user('ອໍເດີ້ຂອງທ່ານກໍາລັງຖືກຈັດສົ່ງ', 'ລະຫັດ' . $this->orderData->code, $this->orderData->user_id, 'confirm_delivery');
            $this->dispatchBrowserEvent('hide-modal-confirm');
            $this->emit('alert', ['type' => 'success', 'message' => 'ຢືນຢັນທຸລະກໍາສໍາເລັດແລ້ວ!']);
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
    public function show_cancel($id)
    {
        $this->hiddenId = '';
        $this->hiddenId = $id;
        $this->dispatchBrowserEvent('show-modal-cancel');
    }
    public function confirm_cancel()
    {
        try {
            DB::beginTransaction();
            $order = P1Order::find($this->hiddenId);
            $orderdetail = P1OrderDetail::where('order_id', $this->hiddenId)->where('branch_id', auth()->user()->branch_id)->get();
            foreach ($orderdetail as $item) {
                $item->status = 0;
                $item->update();
            }
            $order->status = 0;
            $order->update();
            DB::commit();
            $this->dispatchBrowserEvent('hide-modal-cancel');
            $this->emit('alert', ['type' => 'success', 'message' => 'ຍົກທຸລະກໍາສໍາເລັດແລ້ວ!']);
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->emit('alert', ['type' => 'warning', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
    }
}
