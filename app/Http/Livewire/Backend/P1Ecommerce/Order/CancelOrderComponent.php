<?php

namespace App\Http\Livewire\Backend\P1Ecommerce\Order;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\P1\P1OrderTransaction;
use App\Models\P1\P1Order;
use App\Models\P1\P1OrderDetail;

use Carbon\Carbon;
use App\Http\Controllers\Backend\HelperController;
use App\Http\Controllers\CustomFunction;
use Illuminate\Support\Facades\DB;

class CancelOrderComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search, $hiddenId, $orderdetailData = [],$orderData, $transactionData = [];
    public function render()
    {
       $search = $this->search;
        $data = P1Order::select('p1_order.*', DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"), DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"), DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"))
        ->where('p1_order.status',0)->where(function ($q) use ($search){
            $q->where('code', 'like', '%' . $search . '%')
            ->orwhere('bcel_code', 'like', '%' . $search . '%');}
            )->paginate(10);
        $this->page = (($this->page * 10) -10) + 1;
        return view('livewire.backend.p1-ecommerce.order.cancel-order-component', ['data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function showDetail($id){
     $this->orderData = P1Order::find($id);
     $this->orderdetailData = [];
     $this->transactionData = [];
     $this->transactionData = P1OrderTransaction::where('order_id', $id)->get();
     if(!empty(auth()->user()->branch_id)){
        $this->orderdetailData = P1OrderDetail::where('order_id',$id)->where('branch_id', auth()->user()->branch_id)->get();
    }else{
        $this->orderdetailData = [];
    }
     $this->dispatchBrowserEvent('show-modal-detail');
    }
}