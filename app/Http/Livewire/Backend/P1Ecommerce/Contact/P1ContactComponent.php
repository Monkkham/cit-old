<?php

namespace App\Http\Livewire\Backend\P1Ecommerce\Contact;

use App\Models\P1\P1Contact;
use Livewire\Component;
use Livewire\WithPagination;
class P1ContactComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $hiddenId, $search,$name,
    $phone,
    $email,
    $more_detail;
    public function render()
    {
        $search = $this->search;
        $data = P1Contact::where('del', 0)->where(function ($q) use ($search){
          $q->where('name', 'like', '%' . $search . '%')
          ->orwhere('phone', 'like', '%' . $search . '%')
          ->orwhere('email', 'like', '%' . $search . '%');
        })->orderBy('id', 'DESC')->paginate(10);
        $this->page = (($this->page * 10) -10) + 1;
        return view('livewire.backend.p1-ecommerce.contact.p1-contact-component', ['data' => $data, 'page' => $this->page])->layout('layouts.backend.app');
    }
    public function resetField()
    {
        $this->name = '';
        $this->phone = '';
        $this->email ='';
        $this->more_detail = '';
        $this->hiddenId = '';
    }
    protected $rules = [
        'phone' => 'required|unique:p1_contact',
        'name' => 'required',
        // 'email' => 'required|email',
        // 'more_detail' => 'required'
    ];
    protected $messages = [
        'name.required' => 'ໃສ່ຊື່ກ່ອນ!',
        'phone.required' => 'ໃສ່ເບີໂທກ່ອນ!',
        'phone.unique' => 'ເບີໂທນີ້ມີໃນລະບົບແລ້ວ!',
        // 'email.required' => 'ໃສ່ອີເມວກ່ອນ',
        // 'email.email' => 'ອີເມວຕ້ອງເປັນ example@gmail.com',
        // 'more_detail.required' => 'ໃສ່ລາຍລະອຽດກ່ອນ'
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $updateId = $this->hiddenId;

        if ($updateId > 0)
        {
            $data = P1Contact::find($updateId);
            $data->update([
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email,
                'more_detail' => $this->more_detail,
            ]);
            $this->resetField();
             $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        } else //ເພີ່ມໃໝ່
        {
            $this->validate();
            $data = new P1Contact();
            $data->name = $this->name;
            $data->phone = $this->phone;
            $data->email = $this->email;
            $data->more_detail = $this->more_detail;
            $data->save();
            $this->resetField();
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
        }

    }

    public function edit($ids)
    {
        $singleData = P1Contact::find($ids);
        $this->hiddenId = $singleData->id;
        $this->name = $singleData->name;
        $this->phone = $singleData->phone;
        $this->email = $singleData->email;
        $this->more_detail = $singleData->more_detail;
    }

    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $singleData = P1Contact::find($ids);
        $this->hiddenId = $singleData->id;
    }

    public function destroy()
    {
        $singleData = P1Contact::find($this->hiddenId);
        $singleData->del = 1;
        $singleData->update();
        $this->resetField();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    }
}
