<?php

namespace App\Http\Livewire\Backend\Payroll;

use DB;
use Livewire\Component;
use App\Models\Employee;
use App\Models\SalaryLog;
use Livewire\WithPagination;
use App\Models\Payroll_before;
use App\Models\CalculateFundLog;
use Carbon\Carbon;
class PayrollBeforeContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sum_payroll;
    public $selectData = true;
    public $createData = false;
    public $updateData = false, $miss_total, $amount_fund, $total_salary,
        $month, $year, $fromdate, $todate, $rate, $fund_money, $_fund = false, $fund_money_format, $sum_miss, $sum_fund, $pay_total, $add_fund_money_format, $add_fund_money, $hiddenId, $note, $emp_id, $total_salarylog, $total_all_salary, $commission_total, $fullname,
        $ed_commission_total;
        public function mount()
        {
            $this->month = date('m');
            $this->year = date('Y');
        }
    public function render()
    {
        $check_calulate_fund = CalculateFundLog::whereMonth('created_at', Carbon::now()->month)->first();
        if ($check_calulate_fund) {
            $this->_fund = true;
        }
        $employees = Employee::orderBy('employees.id', 'desc')->where('status', 1)->get();
        if (!empty($this->fromdate) && !empty($this->todate) && empty($this->emp_id)) {
            $salary_logs = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->sum('commission_total');
            $this->sum_miss =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('amount_fund');
        } elseif (!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (!empty($this->month) && !empty($this->year) && empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('amount_fund');
        } elseif (!empty($this->month) && !empty($this->year) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (empty($this->fromdate) && empty($this->todate) && empty($this->month) && empty($this->year) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } else {
            $salary_logs = SalaryLog::orderBy('id', 'desc')->paginate(10);
            $this->total_salarylog = SalaryLog::sum('total_salary');
            $this->commission_total = SalaryLog::sum('commission_total');
            $this->sum_miss =  SalaryLog::sum('miss_total');
            $this->sum_fund =  SalaryLog::sum('amount_fund');
            $this->sum_payroll = Payroll_before::sum('amount');
        }
        $this->page = (($this->page * 10) - 10) + 1;
        return view('livewire.backend.payroll.payroll-before-content', compact('salary_logs','check_calulate_fund','employees'))->layout('layouts.backend.app');
    }
    public function resetform()
    {
        $this->employee_id = '';
        $this->amount = '';
        $this->des = '';
    }
    public function ShowPayrollBefore($ids)
    {
        $this->amount = "";
        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
        $Data = SalaryLog::find($ids);
        $this->ID = $Data->id;
        $this->employee_id = $Data->emp_id;
        $this->payroll_before = $Data->payroll_before;
        $this->total_salary = $Data->total_salary;
    }
    public function PayrollBefore()
    {
        if($this->total_salary < $this->amount)
        {
            $this->emit('alert', ['type' => 'error', 'message' => 'ຂໍອາໄພຈຳນວນເງິນບໍ່ພຽງພໍທີ່ຈະເບີກ!']);
        }else{
            $this->validate([
                'amount' => 'required',
            ], [
                'amount.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $salary_log = SalaryLog::find($this->ID);
            $data = new Payroll_before();
            $data->employee_id = $salary_log->emp_id;
            $data->user_id = auth()->user()->id;
            $data->amount = $this->amount;
            $data->des = $this->des;
            $data->save();
            $salary_log->payroll_before = $salary_log->payroll_before + $this->amount;
            $salary_log->total_salary = $salary_log->total_salary - $this->amount;
            $salary_log->save();
            $this->dispatchBrowserEvent('hide-modal-add');
            $this->emit('alert', ['type' => 'success', 'message' => 'ເບີກເງິນສຳເລັດເເລ້ວ!']);
        }
    }
}
