<?php

namespace App\Http\Livewire\Backend\Payroll;
use Livewire\Component;
use App\Models\Employee;
use App\Models\Commission;
use App\Models\SalaryLog;
use App\Models\Fund;
use App\Models\Payroll_before;
use App\Models\FundDetail;
use App\Models\TrackMissEmployee;
use App\Models\CalculateFundLog;
use Carbon\Carbon;
use App\Models\Branch;
use Auth;
use Livewire\WithPagination;

class PayrollComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sum_payroll;
    public $selectData = true;
    public $createData = false;
    public $select_page;
    public $updateData = false, $miss_total, $amount_fund, $total_salary,
        $month, $year, $fromdate, $todate, $rate, $fund_money, $_fund = false, $fund_money_format, $sum_miss, $sum_fund, $pay_total, $add_fund_money_format, $add_fund_money, $hiddenId, $note, $emp_id, $total_salarylog, $total_all_salary, $commission_total, $fullname,
        $ed_commission_total;
    public function mount()
    {
        $this->month = date('m');
        $this->year = date('Y');
    }
    public function render()
    {
        $branch = Branch::first();
        if ($branch) {
            $this->rate = $branch->rate / 100;
        } else {
            $this->rate = 0;
        }
        $check_calulate_fund = CalculateFundLog::whereMonth('created_at', Carbon::now()->month)->first();
        if ($check_calulate_fund) {
            $this->_fund = true;
        }
        $employees = Employee::orderBy('employees.id', 'desc')->where('status', 1)->get();
        if (!empty($this->fromdate) && !empty($this->todate) && empty($this->emp_id)) {
            $salary_logs = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->sum('commission_total');
            $this->sum_miss =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->orderBy('id', 'desc')->sum('amount_fund');
        } elseif (!empty($this->fromdate) && !empty($this->todate) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::whereBetween('created_at', [$this->fromdate, date('Y-m-d H:i:s', strtotime($this->todate . ' +1 day'))])->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (!empty($this->month) && !empty($this->year) && empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('month', $this->month)->where('year', $this->year)->orderBy('id', 'desc')->sum('amount_fund');
        } elseif (!empty($this->month) && !empty($this->year) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('month', $this->month)->where('year', $this->year)->where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (empty($this->fromdate) && empty($this->todate) && empty($this->month) && empty($this->year) && !empty($this->emp_id)) {
            $salary_logs = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('total_salary');
            $this->commission_total = SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('commission_total');
            $this->sum_miss =  SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('miss_total');
            $this->sum_fund =  SalaryLog::where('emp_id', $this->emp_id)->orderBy('id', 'desc')->sum('amount_fund');
            $employee = Employee::where('id', $this->emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } else {
            $salary_logs = SalaryLog::orderBy('id', 'desc')->paginate($this->select_page);
            $this->total_salarylog = SalaryLog::sum('total_salary');
            $this->commission_total = SalaryLog::sum('commission_total');
            $this->sum_miss =  SalaryLog::sum('miss_total');
            $this->sum_fund =  SalaryLog::sum('amount_fund');
            $this->sum_payroll = Payroll_before::sum('amount');
        }
        $this->page = (($this->page * $this->select_page) - $this->select_page) + 1;
        return view('livewire.backend.payroll.payroll-component', ['page' => $this->page, 'salary_logs' => $salary_logs, 'employees' => $employees])
            ->layout('layouts.backend.app');
    }

    public function back()
    {
        return  redirect()->to('/payroll-of-employee');
    }

    public function showFrom()
    {
        if ($this->_fund == true) {
            $this->createData = true;
            $this->selectData = false;
            $this->updateData = false;
        } else {
            session()->flash('no_message', 'ຂໍອະໄພ ທ່ານຕ້ອງຄິດໄລ່ດອກເບ້ຍກ່ອນຈຶ່ງສາມາດບອກເງິນເດືອນໄດ້');
            return;
        }
    }
    public function resetField()
    {
        $this->month = "";
        $this->year = "";
        $this->note = "";
        $this->emp_id = "";
        $this->hiddenId = "";
        $this->total_all_salary = "";
        $this->miss_total = '';
        $this->amount_fund = '';
        $this->commission_total = '';
        $this->total_salary = '';
        $this->ed_commission_total = '';
    }
    public function updated($fields)
    {
        $this->validateOnly($fields, [
            'month' => 'required',
            'year' => 'required'
        ]);
    }
    public function payroll()
    {
        $this->validate([
            'month' => 'required',
            'year' => 'required',
        ]);
        $employees = Employee::where('status', 1)->where('salary_id', '!=', '')->get();
        $check_rate = CalculateFundLog::orderBy('id', 'desc')->whereMonth('created_at', Carbon::today()->month)->first();
        if ($check_rate) {
            foreach ($employees as $item) {
                $check_salarylog = SalaryLog::where('emp_id', $item->id)->where('month', $this->month)->where('year', $this->year)->first();
                $sum_commission = Commission::where('emp_id', $item->id)->whereMonth('date', $this->month)->whereYear('date', $this->year)->sum('total');
                $track_miss_employee = TrackMissEmployee::where('emp_id', $item->id)->whereMonth('date', $this->month)->whereYear('date', $this->year)->sum('total_salary');
                // if($sum_commission > 0){
                if (empty($check_salarylog->id)) {
                    $salary_logs = new SalaryLog();
                    $salary_logs->month = $this->month;
                    $salary_logs->year = $this->year;
                    $salary_logs->emp_id = $item->id;
                    if (!empty($item->salaryamount->salary)) {
                        $salary_logs->salary = $item->salaryamount->salary;
                    }
                    $salary_logs->commission_total = $sum_commission;
                    $salary_logs->miss_total = $track_miss_employee;
                    $salary_logs->amount_fund = 0;
                    if (!empty($item->salaryamount->salary)) {
                        $salary_logs->total_salary = ($sum_commission + $item->salaryamount->salary) - $track_miss_employee;
                    }
                    $salary_logs->user_id = Auth::user()->id;
                    $salary_logs->save();
                } else {
                    session()->flash('no_message', 'ບໍ່ສາມາດເບີກໄດ້ ເພາະວ່າການເບີກໃນ ເດືອນ' . $this->month . ' ປີ' . $this->year . ' ມີໃນລະບົບແລ້ວ');
                    return  redirect()->to('/payroll-of-employee');
                }
            }
            $this->createData = false;
            $this->selectData = true;
            $this->updateData = false;
            $this->resetField();
            session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
            return  redirect()->to('/payroll-of-employee');
        } else {
            session()->flash('no_message', 'ກະລຸນາໄປຄິດໄລ່ດອກເບ້ຍກ່ອນ');
            return;
        }
    }
    public function delete($id)
    {
        $commisions = SalaryLog::find($id);
        $commisions->delete();
        session()->flash('message', 'ລຶບຂໍ້ມູນສຳແລັດແລ້ວ');
        return  redirect()->to('/payroll-of-employee');
    }
    public function createFund($id)
    {
        $this->resetField();
        $this->createData = true;
        $this->selectData = false;
        $this->updateData = false;
        $this->note = "ເງິນສົດ";
        $singleData = SalaryLog::find($id);
        $this->hiddenId = $singleData->id;
        $this->emp_id = $singleData->emp_id;
        if ($singleData->employee->sex == 1) {
            $this->fullname = 'ທ້າວ ' . $singleData->employee->firstname . ' ' . $singleData->employee->lastname;
        } else {
            $this->fullname = 'ນາງ ' . $singleData->employee->firstname . ' ' . $singleData->employee->lastname;
        }
        $this->total_all_salary = $singleData->total_salary;
    }
    public function AutoformatFund()
    {
        if (!empty($this->fund_money)) {
            $this->fund_money_format = number_format($this->fund_money);
        }
    }
    public function AutoformatAddfund()
    {
        if (!empty($this->add_fund_money)) {
            $this->add_fund_money_format = number_format($this->add_fund_money);
        }
    }
    public function SaveFund()
    {
        $check_employee = Employee::where('status', 1)->where('salary_id', '!=', '')->where('id', $this->emp_id)->first();
        if ($check_employee) {
            $fullname = $check_employee->firstname . ' ' . $check_employee->lastname;
        }
        $check_calulate_fund = CalculateFundLog::orderBy('id', 'desc')->whereMonth('created_at', Carbon::today()->month)->first();
        $start_date = CalculateFundLog::orderBy('id', 'asc')->first();
        $end_date = CalculateFundLog::orderBy('id', 'desc')->first();
        if ($this->fund_money <= $this->total_all_salary) {
            $singleData = $singleData = SalaryLog::find($this->hiddenId);
            if (!empty($this->fund_money)) {
                $singleData->total_salary = $this->total_all_salary - $this->fund_money;
                $singleData->amount_fund = $this->fund_money + $singleData->amount_fund;
                $singleData->status = '1';
                $singleData->note = $this->note;
                $singleData->update();
                //ເພີ່ມເງິນກອງທຶນຈາກການເບີກເງິນເດືອນ
                $check_fund = Fund::where('emp_id', $singleData->emp_id)->first();
                if ($check_fund) {
                    $check_fund->fund_total = $check_fund->fund_total + $this->fund_money;
                    $check_fund->remain_total = $check_fund->remain_total + $this->fund_money;
                    $check_fund->user_id = Auth::user()->id;
                    $check_fund->update();
                    $funddetail = new FundDetail();
                    $funddetail->fund_id = $check_fund->id;
                    $funddetail->amount = $this->fund_money;
                    $funddetail->user_id = Auth::user()->id;
                    $funddetail->date = date('Y-m-d');
                    $funddetail->save();
                } else {
                    $fund = new Fund();
                    $fund->emp_id = $singleData->emp_id;
                    $fund->fund_total = $this->fund_money;
                    $fund->remain_total = $this->fund_money;
                    $fund->user_id = Auth::user()->id;
                    $fund->save();
                    $funddetail = new FundDetail();
                    $funddetail->fund_id = $fund->id;
                    $funddetail->amount = $this->fund_money;
                    $funddetail->user_id = Auth::user()->id;
                    $funddetail->date = date('Y-m-d');
                    $funddetail->save();
                }
                if ($check_calulate_fund) {
                    if ($start_date && $end_date) {
                        $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24 * 60 * 60);
                        if (intval($date) >= 1) {
                            $profit = ($check_calulate_fund->remain_total * $this->rate * intval($date)) / 365;
                        } else {
                            $profit = 0;
                        }
                    } else {
                        $profit = ($check_calculate->remain_total * $this->rate * 30) / 365;
                    }
                    $calculate_fund = new  CalculateFundLog();
                    $calculate_fund->content = $fullname;
                    $calculate_fund->total = $check_calulate_fund->remain_total;
                    $calculate_fund->total_rate = $profit;
                    $calculate_fund->sum_total = $check_calulate_fund->sum_total + $this->fund_money + $profit;
                    $calculate_fund->deposit = $this->fund_money;
                    $calculate_fund->remain_total = $check_calulate_fund->remain_total + $this->fund_money + $profit;
                    $calculate_fund->user_id = auth()->user()->id;
                    $calculate_fund->save();
                }
            } else {
                $singleData->amount_fund =  0;
                $singleData->status = '1';
                $singleData->note = $this->note;
                $singleData->update();
            }
            $this->createData = false;
            $this->selectData = true;
            $this->updateData = false;
            $this->resetField();
            session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
            return  redirect()->to('/payroll-of-employee');
        } else {
            session()->flash('no_message', 'ບໍ່ສາມາດເພີ່ມໄດ້ ເນື່ອງຈາກຈໍານວນເງິນຫັກກອງທຶນຫຼາຍກວ່າຈໍານວນເບີກເງິນເດືອນ');
            return;
        }
    }
    public function show_edit($id)
    {
        $this->resetField();
        $data =  SalaryLog::find($id);
        if ($data) {
            $this->hiddenId = $id;
            $this->miss_total = $data->miss_total;
            $this->amount_fund = $data->amount_fund;
            $this->ed_commission_total = $data->commission_total;
            $this->total_salary = $data->total_salary;
        }
        $this->dispatchBrowserEvent('show-modal-edit-salary');
    }
    public function updateSalary()
    {
        // try {
            $data = SalaryLog::find($this->hiddenId);
            if ($data) {
                $data->miss_total  = str_replace(',', '', $this->miss_total);
                $data->amount_fund  = str_replace(',', '', $this->amount_fund);
                $data->commission_total  = str_replace(',', '', $this->ed_commission_total);
                $data->total_salary  = str_replace(',', '', $this->total_salary);
                $data->update();
                $this->resetField();
                $this->dispatchBrowserEvent('hide-modal-edit-salary');
                $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສໍາເລັດແລ້ວ']);
            }
        // } catch (\Exception $ex) {
        //     $this->emit('alert', ['type' => 'warning', 'message' => $ex]);
        // }
    }
}
