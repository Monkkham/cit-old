<?php

namespace App\Http\Livewire\Backend\Customer;

use App\Models\CustomerTransition;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Branch;
use App\Models\P1\P1Customer;
use App\Models\P1\P1Products;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use DB;

class CustomerComponent extends Component
{
    public $selectData = true;
    public $createData = false;
    public $updateData = false;
    public $showData = false;

    public $customer_id, $search, $product_id, $note, $start_date, $end_date, $status = 1, $picture,$search_customer;
    public $ids, $ed_customer_id, $ed_product_id, $ed_note, $ed_start_date, $ed_end_date, $ed_status, $ed_picture;
    public $vid, $vcustomer_id, $vproduct_id, $vnote, $vstart_date, $vend_date, $vstatus, $vpicture;
    public $name,
        $email,
        $phone,
        $address,
        $birthday, $code;
    use WithFileUploads;
    use WithPagination;
    public function mount()
    {
        $branchs = Branch::where('id', 1)->first();
        if ($branchs) {
            $this->search = $branchs->qtydate_range;
        }
    }
    public function render()
    {
        // $customers = DB::connection('mysql2')->table('customers')->get();
        // $products = DB::connection('mysql2')->table('products')->get();
        $products = P1Products::orderBy('id', 'DESC')->get();
        $customers = P1Customer::orderBy('id', 'DESC')->get();
        $customer_transition = CustomerTransition::orderBy('id', 'DESC')
        ->where('customer_id', 'like', '%' . $this->search_customer . '%')
        ->get();
        // $customer_transition =CustomerTransition::orderBy('id', 'DESC')->paginate(5);
        return view('livewire.backend.customer.customer-component', ['customer_transition' => $customer_transition, 'products' => $products, 'customers' => $customers])->layout('layouts.backend.app');
    }
    public function showFrom()
    {
        $this->createData = true;
        $this->selectData = false;
        $this->showData = false;
    }
    public function back()
    {
        return  redirect()->to('/admincustomer');
    }

    public function resetFied()
    {
        $this->customer_id = "";
        $this->product_id = "";
        $this->note = "";
        $this->start_date = "";
        $this->end_date = "";
        $this->status = "";
        $this->picture = "";
        // edit
        $this->ids = "";
        $this->ed_customer_id = "";
        $this->ed_product_id = "";
        $this->ed_note = "";
        $this->ed_start_date = "";
        $this->ed_end_date = "";
        $this->ed_status = "";
        $this->ed_picture = "";
        $this->name = '';
        $this->email = '';
        $this->phone = '';
        $this->address = '';
        $this->birthday = '';
        $this->code = '';
    }
    protected $rules = [
        'customer_id' => 'required',
        'product_id' => 'required',
        'note' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        // 'status' => 'required',
    ];
    protected $messages = [
        'customer_id.required' => 'ເລຶອກລູກຄ້າກ່ອນ',
        'product_id.required' => 'ເລືອກສິນຄ້າ',
        'note.required' => 'ໃສ່່ໝາຍເຫດກ່ອນ',
        'start_date.required' => 'ເລືອກວັນທີເລີ່ມກ່ອນ',
        'end_date.required' => 'ເລືອກວັນທີສຶ້ນສຸດກ່ອນ',
        // 'status.required' => 'required',
    ];
    public function create()
    {
        $this->validate();
        $customer_transition = new CustomerTransition();
        if (!empty($this->picture)) {
            $this->picture->store('pictures/ct');
            // dd($this->customer_id);
            $customer_transition->customer_id = $this->customer_id;
            $customer_transition->product_id = $this->product_id;
            $customer_transition->note = $this->note;
            $customer_transition->start_date = $this->start_date;
            $customer_transition->end_date = $this->end_date;
            $customer_transition->status = $this->status;
            $customer_transition->picture = $this->picture->hashName();
        } else {
            $customer_transition->customer_id = $this->customer_id;
            $customer_transition->product_id = $this->product_id;
            $customer_transition->note = $this->note;
            $customer_transition->start_date = $this->start_date;
            $customer_transition->end_date = $this->end_date;
            $customer_transition->status = $this->status;
        }
        $customer_transition->save();
        session()->flash('message', 'ເພີ່ມລູກຄ້າໃໝ່ສຳແລັດແລ້ວ');
        return  redirect()->to('/admincustomer');
        $this->resetFied();
        $this->selectData = true;
        $this->createData = false;
        $this->showData = false;
    }

    // edi funtion
    public function edit($id)
    {
        $this->selectData = false;
        $this->showData = false;
        $this->updateData = true;

        $customer_transition = CustomerTransition::findOrFail($id);
        $this->ids = $customer_transition->id;
        $this->ed_customer_id = $customer_transition->customer_id;
        $this->ed_product_id = $customer_transition->product_id;
        $this->ed_note = $customer_transition->note;
        $this->ed_start_date = $customer_transition->start_date;
        $this->ed_end_date = $customer_transition->end_date;
        $this->ed_status = $customer_transition->status;
        // $this->ed_picture = $customer_transition->picture;

    }

    // update funtion
    public function update($id)
    {
        $customer_transition = CustomerTransition::findOrFail($id);
        $this->validate([
            'ed_customer_id' => 'required',
            'ed_product_id' => 'required',
            'ed_note' => 'required',
            'ed_start_date' => 'required',
            'ed_end_date' => 'required',
            'ed_status' => 'required',
        ]);
        if (!empty($this->ed_picture)) {
            $this->ed_picture->store('pictures/ct');

            $customer_transition->customer_id = $this->ed_customer_id;
            $customer_transition->product_id = $this->ed_product_id;
            $customer_transition->note = $this->ed_note;
            $customer_transition->start_date = $this->ed_start_date;
            $customer_transition->end_date = $this->ed_end_date;
            $customer_transition->status = $this->ed_status;
            $customer_transition->picture = $this->ed_picture->hashName();
        } else {
            $customer_transition->customer_id = $this->ed_customer_id;
            $customer_transition->product_id = $this->ed_product_id;
            $customer_transition->note = $this->ed_note;
            $customer_transition->start_date = $this->ed_start_date;
            $customer_transition->end_date = $this->ed_end_date;
            $customer_transition->status = $this->ed_status;
        }


        $result = $customer_transition->save();
        session()->flash('message', 'ອັບແດດລູຄ້າສຳແລັດແລ້ວ');
        return redirect()->to('/admincustomer');
        $this->resetFied();
        $this->selectData = true;
        $this->createData = false;
        $this->updateData = false;
        $this->showData  = false;
    }

    // delete data
    public function delete($id)
    {
        $customer_transition = CustomerTransition::findOrFail($id);
        $result = $customer_transition->delete();
        session()->flash('message', 'ລົບລູກຄ້າແລັດແລ້ວ');
        return redirect()->to('/admincustomer');
    }

    // show default view
    public function show($id)
    {
        $this->selectData = false;
        $this->createData = false;
        $this->updateData = false;
        $this->showData  = true;

        $vct = CustomerTransition::findOrFail($id);
        // dd($vct->product_id);
        $this->vid = $vct->id;
        $this->vcustomer_id = $vct->customer_id;
        $this->vproduct_id = $vct->product_id;
        $this->vnote = $vct->note;
        $this->vstart_date = $vct->start_date;
        $this->vend_date = $vct->end_date;
        $this->vstatus = $vct->status;
        $this->vpicture = $vct->picture;
        // dd($this->vpicture);
        return view('livewire.backend.customer.customer-component', compact('vct'));
    }
    public function create_customer()
    {
        $this->dispatchBrowserEvent('show-modal-create-customer');
    }
    public function storeCustomer()
    {
        $this->validate([
            'code' => 'required|unique:p1_customers',
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            // 'birthday' => 'required',
        ], [
            'code.required' => 'ລະຫັດລູກຄ້າ',
            'code.unique' => 'ລະຫັດລູກຄ້າມີໃນລະບົບແລ້ວ',
            'name.required' => 'ໃສ່ຊື່ລູກຄ້າກ່ອນ',
            'phone.required' => 'ໃສ່ເບີໂທລະສັບກ່ອນ',
            'address.required' => 'ໃສ່ທີ່ຢູ່ກ່ອນ',
            // 'birthday.required' => 'ໃສ່ວັນທີກ່ອນ',
        ]);
        P1Customer::create(
            [
                'code' => $this->code,
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'branch_id' => auth()->user()->branch_id  > 0 ? auth()->user()->branch_id  : 0,
                'address' => $this->address,
                'birthday' => $this->birthday,
            ]
        );
        $this->resetFied();
        $this->render();
        $this->selectData = true;
        $this->createData = false;
        $this->updateData = false;
        $this->showData  = false;
        $this->dispatchBrowserEvent('hide-modal-create-customer');
        $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນເລັດແລ້ວ']);
        return redirect()->to('/admincustomer');
    }
}
