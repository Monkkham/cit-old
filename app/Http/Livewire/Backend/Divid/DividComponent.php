<?php

namespace App\Http\Livewire\Backend\Divid;

use Livewire\Component;
use App\Models\Divid;
class DividComponent extends Component
{
    public function deletedivid($id){
        $divid=Divid::find($id);
        $divid->delete();
        session()->flash('delete_dividsuccessfully');
    }
    public function render()
    {
        $divids=Divid::orderBy('id','DESC')->paginate(10);
        return view('livewire.backend.divid.divid-component',['divids' => $divids])->layout('layouts.backend.app');
    }
}
