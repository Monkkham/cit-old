<?php

namespace App\Http\Livewire\Backend\Divid;
use App\Models\Divid;
use Livewire\Component;

class AddDividComponent extends Component
{
    public $name, $percent, $note;
    
    public function updated($fields)
    {
      $this->validateOnly($fields,[
        'percent' =>'required|numeric',
      ]);
    }
    public function Adddivid(){
        $this->validate([
            'percent' =>'required|numeric',
        ]);
        $divids = new Divid();
        $divids->name = $this->name;
        $divids->percent = $this->percent;
        $divids->note = $this->note;
        $divids->save();
        $this->percent='';
        $this->note='';
        session()->flash('message_adddividsuccessfully');
        return redirect()->route('admin.divid');
       
}
    public function render()
    {
        return view('livewire.backend.divid.add-divid-component')->layout('layouts.backend.app');
    }
}
