<?php

namespace App\Http\Livewire\Backend\Divid;

use Livewire\Component;
use App\Models\Divid;
class EditDividComponent extends Component
{
    public $name;
    public $percent;
    public $note;
    public $divid_id;
    public $divid_slug;
    public function mount($divid_slug)
    {
      $divids=Divid::where('id',$this->divid_slug)->first();
       $this->name = $divids->name;
       $this->percent = $divids->percent;
       $this->note = $divids->note;
       $this->divid_id=$divids->id;
    }
    public function Updatedivid(){
        $divid=Divid::find($this->divid_id);
        $divid->name =$this->name;
        $divid->percent = $this->percent;
        $divid->note = $this->note;
        $divid->save();
        session()->flash('message_updatedividsuccess');
        return redirect()->route('admin.divid');
    }
    public function render()
    {
        return view('livewire.backend.divid.edit-divid-component')->layout('layouts.backend.app');
    }
}
