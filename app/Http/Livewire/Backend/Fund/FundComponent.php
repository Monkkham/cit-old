<?php

namespace App\Http\Livewire\Backend\Fund;

use Livewire\Component;
use App\Models\Fund;
use App\Models\FundDetail;
use App\Models\Employee;
use Carbon\Carbon;
use App\Models\Branch;
use Livewire\WithFileUploads;
use App\Models\CalculateFundLog;
use Auth;
class FundComponent extends Component
{
    use WithFileUploads;
    public $DataDetail = [],$slug,$bcel_image,$pay_status,$check_interest =false, $selectOne = [], $another_amount,$created_at,$total,$amount, $note, $withraw = false, $hiddenId,$fund_amount, $fullname,$show_detail_status = false,$createData, $updateData = false,
    $emp_id,$fund_money,$date;
    public function mount(){
        $this->pay_status = 1;
    }
    public function render()
    {
        $data = Fund::get();
        $check_interest = CalculateFundLog::first();
        $check_fund = Fund::first();
        if($check_interest || $check_fund){
            $this->check_interest  = true;
        }
        $employees = Employee::where('status',1)->orderBy('id','desc')->where('salary_id', '!=', '')->get();
        return view('livewire.backend.fund.fund-component', compact('data', 'employees'))->layout('layouts.backend.app');
    }
    public function showdetail($id){
        $singleData = Fund::where('id', $id)->first();
        if($singleData){
             if(!empty($singleData->emp_id)){
                $this->fullname = $singleData->employee->firstname.' '.$singleData->employee->lastname;
             }
        }
        $this->DataDetail = FundDetail::where('fund_id', $id)->get();
        $this->show_detail_status = true;
    }
    public function back(){
        $this->show_detail_status = false;
        $this->createData = false;
        $this->updateData = false;
        $this->withraw = false;
    }
    public function Amount(){
        $this->ResetData();
        if(!empty($this->fund_amount)){
            $this->slug= number_format($this->fund_amount);
        }
    }
    public function showFrom()
    {
        $this->createData = true;
        $this->updateData = false;
        $this->show_detail_status = false;
        $this->withraw = false;
    }
    public function ResetData(){
        $this->emp_id = '';
        $this->fund_amount = '';
        $this->date = '';
        $this->slug = '';
        $this->note = '';
        $this->total = '';
        $this->hiddenId = '';
        $this->amount = '';
        $this->another_amount = '';
        $this->bcel_image = null;
    }
    protected $rules = [
        'fund_amount' => 'required|numeric|min:1000',
        'date' => 'required',
    ];
    protected $messages = [
        'fund_amount.required' => 'ໃສ່ເງິນກອງທຶນກ່ອນ',
        'fund_amount.min' => 'ຕ້ອງໃຫຍ່ກວ່າ 1,000',
        'date.required' => 'ໃສ່ວັນທີກ່ອນ'
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function create(){
            $this->validate();
            $check_fund = Fund::where('emp_id', $this->emp_id)->first();
            if($check_fund){
                $check_fund->fund_total = $check_fund->fund_total + $this->fund_amount;
                $check_fund->remain_total = $check_fund->remain_total + $this->fund_amount;
                $check_fund->user_id = auth()->user()->id;
                $check_fund->update();
                $funddetail = new FundDetail();
                $funddetail->fund_id = $check_fund->id;
                $funddetail->amount = $this->fund_amount;
                $funddetail->date = $this->date;
                $funddetail->user_id = Auth::user()->id;
                $funddetail->note = $this->note;
                if(empty($this->emp_id)){
                    $funddetail->status = 2;
                }
                $funddetail->save();
                session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
                $this->ResetData();
            }else{
                $fund = new Fund();
                $fund->emp_id = $this->emp_id;
                $fund->fund_total = $this->fund_amount;
                $fund->remain_total = $this->fund_amount;
                $fund->user_id = auth()->user()->id;
                $fund->save();
                $funddetail = new FundDetail();
                $funddetail->fund_id = $fund->id;
                $funddetail->amount = $this->fund_amount;
                $funddetail->date = $this->date;
                $funddetail->user_id = Auth::user()->id;
                if(empty($this->emp_id)){
                    $funddetail->status = 2;
                }
                $funddetail->note = $this->note;
                $funddetail->save();
                session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
                $this->ResetData();
            }
    }
    public function showEdit($id){
        $this->ResetData();
        $singleData = FundDetail::find($id);
        $this->fund_amount = $singleData->amount;
        $this->hiddenId = $singleData->id;
    }
    public function edit(){
        $this->validate([
            'fund_amount' => 'required'
        ],[
            'fund_amount.required' => 'ໃສ່ເງິນກອງທຶນກ່ອນ'
        ]);
        $singleData = FundDetail::find($this->hiddenId);
        $singleData->amount = $this->fund_amount;
        $singleData->update();
        session()->flash('message', 'ແກ້ໄຂຂໍ້ມູນສຳແລັດແລ້ວ');
        $this->ResetData();
        $this->selectOne = [];
        $this->createData = false;
        $this->updateData = false;
        $this->show_detail_status = false;
    }
    public function showWithraw($id)
    {
        $this->ResetData();
        $this->createData = false;
        $this->updateData = false;
        $this->show_detail_status = false;
        $this->withraw = true;
        $singleData = Fund::find($id);
        if($singleData){
            $this->hiddenId = $singleData->id;
            $this->emp_id = $singleData->emp_id;
            $this->total = $singleData->remain_total;
            $this->another_amount = $singleData->remain_total;
        }
    }
    public function AutoformatAmount(){
        $this->amount='';
        if(!empty($this->another_amount)){
            $this->slug= number_format($this->another_amount);
        }
    }
    public function Autoformat(){
        $this->amount='';
        if(!empty($this->fund_amount)){
            $this->slug= number_format($this->fund_amount);
        }
    }
    public function withrawFund(){
        if($this->another_amount > $this->total){
            session()->flash('no_message', 'ບໍ່ສາມາດຖອນໄດ້ເພາະເງິນກອງທຶນທ່ານບໍ່ພຽງພໍ');
            return;
        }else{
            $fund = Fund::find($this->hiddenId);
            if($fund){
                if(!empty($this->another_amount)){
                    $this->validate([
                        'another_amount' => 'required',
                    ],[
                        'another_amount.required' => 'ໃສ່ຈໍານວນເງິນຖອນກ່ອນ'
                    ]);
                    $fund->remain_total = $fund->remain_total - $this->another_amount;
                    $fund->withrawed_total = $fund->withrawed_total +  $this->another_amount;
                }
                if(!empty($this->pay_status ==2)){
                    $this->validate([
                        'bcel_image' => 'required|mimes:jpg,png,jpeg',
                    ],[
                        'bcel_image.required' => 'ເລືອກຮູບຊໍາລະຜ່ານ BCEL ONE ກ່ອນ'
                    ]);
                    $imageName=Carbon::now()->timestamp. '.' .$this->bcel_image->extension();
                    $this->bcel_image->storeAs('admin/dist/img/fund',$imageName);
                    $fund->bcel_image = 'admin/dist/img/fund/'.$imageName;
                }
                $fund->user_id = auth()->user()->id;
                $fund->update();
            }
            session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
            $this->ResetData();
            return redirect()->to('/fund-of-employee');
        }
    }
}