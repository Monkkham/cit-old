<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Transaction;
use DB;
class PayDebtComponent extends Component
{
    public $order_id;
    public $slug;
    public $total;
    public $qty;
    public $price;
    public $orders;
    public $check_edit;
    public $orderitem_id;
    public $order_subtotal;
    public $order_total;
    public $subtotal;
    public function mount($slug){
        $orders=Order::where('id', $this->slug)->first();
        $this->order_id=$orders->id;
        $this->total=$orders->total;
        $this->subtotal=$orders->subtotal;

    }
    public function render()
    {
        $orderitems=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->get();
        return view('livewire.backend.order.pay-debt-component',['orderitems' => $orderitems])->layout('layouts.backend.app');
    }
    public function paydebt(){
        DB::update('update transactions set mode=?  where order_id = ?',['cod',$this->order_id]);
        session()->flash('pay_debt', 'ຊໍາລະໜີ້ສໍາເລັດແລ້ວ');
        return redirect()->route('admin.order');
    }
}
