<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Exchange;
class ExchangeComponent extends Component
{
    public $hiddenId, $name, $rate, $active, $symbol, $search;
    public function mount(){
        $this->active = '1';
    }
    public function render()
    {
        $search = $this->search;
        $data = Exchange::where(function ($q) use ($search){
            $q->where('name', 'like', '%'.$search.'%')
            ->orwhere('symbol', 'like', '%'.$search.'%')
            ->orwhere('rate', 'like', '%'.$search.'%');
        })->get();
        return view('livewire.backend.order.exchange-component', compact('data'))->layout('layouts.backend.app');
    }
    public function resetField()
    {
        $this->name = '';
        $this->symbol = '';
        $this->rate = '';
        $this->active = '';
    }
    //Validate real time
    protected $rules = [
        'name' => 'required',
        'symbol' => 'required',
        'rate' => 'required|numeric|min:1'
    ];
    protected $messages = [
        'name.required' => 'ໃສ່ຊື່ອັດຕາແລກປ່ຽນກ່ອນ!',
        'symbol.required' => 'ໃສ່ສັນຍາລັກກ່ອນ!',
        'rate.required' => 'ໃສ່ອັດຕາແລກປ່ຽນກ່ອນ!',
        'rate.numeric' => 'ຕ້ອງເປັນຕົວເລກ!',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();
        $updateId = $this->hiddenId;

        if ($updateId > 0) //Update ໂຕທີ່ເລືອກ
        {
            $singleData =  Exchange::find($this->hiddenId);
            $singleData->name = $this->name;
            $singleData->symbol = $this->symbol;
            $singleData->rate = $this->rate;
            if($this->active =='0'){
                $singleData->active = 0;
            }else{
                $singleData->active = 1;
            }
            $singleData->update();
            $this->resetField();
            session()->flash('success', 'ແກ້ໄຂຂໍ້ມູນສໍາເລັດແລ້ວ');
        } else //ເພີ່ມໃໝ່
        {
           $singleData = new Exchange();
           $singleData->name = $this->name;
           $singleData->symbol = $this->symbol;
           $singleData->rate = $this->rate;
           if($this->active =='0'){
            $singleData->active = 0;
           }
           $singleData->save();
           $this->resetField();
           session()->flash('success', 'ເພີ່ມຂໍ້ມູນສໍາເລັດແລ້ວ');
        }

    }

    public function edit($ids)
    {
        $this->resetField();
        $singleData = Exchange::find($ids);
        $this->name = $singleData->name;
        $this->symbol = $singleData->symbol;
        $this->rate = $singleData->rate;
        $this->hiddenId = $singleData->id;
        $this->active = $singleData->active;
    }
    public function delete($ids)
    {
        $singleData = Exchange::find($ids);
        $singleData->delete();
        $this->resetField();
        session()->flash('success', 'ລຶບຂໍ້ມູນສໍາເລັດແລ້ວ');

    }
}