<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\User;
use App\Models\Order;
use App\Models\Divid;
use App\Models\Transaction;
use App\Models\LogDivid;
use DB;
class DoSendComponent extends Component
{
    public $rider_id;
    public $search;
    public $note;
    public $amount;
    public $order_id;
    public $order_slug;
    public $order_total;
    public $divid_percent;
    public $divid_id;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
        $this->order_total=$orders->total;
    }
    public function updated($fields)
    {
    $this->validateOnly($fields,[
        'rider_id' =>'required',
      ]);
    }
    public function checkpaymentOnebcelOne(){
        $transaction=Transaction::where('order_id', $this->order_id)->first();
        if($transaction->mode =='onepay' && $transaction->check_payment =='0'){
          session()->flash('message_confirmpaymentbefore', 'ກະລຸນາກວດເບິ່ງລາຍລະອຽດຂອງການຊໍາລະເງິນຜ່ານ Bcel One ກ່ອນ !');
          return redirect()->route('admin.order');
        }
        if($transaction->status =='approved' && $transaction->order->status =='delivered'){
            session()->flash('message_sended', 'ຂໍອະໄພ ກໍາລັງສົ່ງລາຍການສັ່ງຊື້ນີ້ !');
            return redirect()->route('admin.order');
          }
    }
    public function SendToRider(){
        $this->validate([
            'rider_id' =>'required',
       ]);
        $users=User::where('id', $this->rider_id)->first();
        if($users->divid->id > 0){
            $this->divid_id=$users->divid->id;
            $this->divid_percent=$users->divid->percent;
            $divids=Divid::where('id', $this->divid_id)->first();
            $this->amount=($this->order_total * $this->divid_percent)/100;
        }else{
            $this->amount=0;
        }
         $logdivid= new LogDivid();
         $logdivid->order_id = $this->order_id;
         $logdivid->user_id = $this->rider_id;
         $logdivid->amount=$this->amount;
         $logdivid->status='p';
         $logdivid->save();
        DB::update('update transactions set rider_id = ?, amount =?, status = ?, note = ? where order_id = ?',[$this->rider_id, $this->amount,'approved',$this->note,$this->order_id]);
        // DB::update('update divids set name = ? where id = ?',[$this->amount,$this->divid_id]);
        DB::update('update orders set status = ? where id = ?',['delivered',$this->order_id]);
        session()->flash('message_sendtorider','Order has been send to Rider successfully! ');
        return redirect()->route('admin.order');
    }
    public function render()
    {
        $this->checkpaymentOnebcelOne();
        $riders=User::where('role_id','LIKE', '8')
        ->orderBy('id', 'desc')->get();
        return view('livewire.backend.order.do-send-component',['riders' => $riders])->layout('layouts.backend.app');
    }
}
