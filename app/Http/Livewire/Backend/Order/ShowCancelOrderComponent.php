<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Transaction;
use Livewire\WithPagination;
class ShowCancelOrderComponent extends Component
{
    use WithPagination;
    public function render()
    {
        $ordercanceled = Transaction::where('status', 'declined')->orderBy('id','desc')->paginate(10);
        return view('livewire.backend.order.show-cancel-order-component',['ordercanceled' => $ordercanceled])->layout('layouts.backend.app');
    }
}
