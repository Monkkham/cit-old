<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
use DB;
class EditOrderItemComponent extends Component
{
    public $orderitem_id;
    public $order_slug;
    public $total=0;
    public $qty;
    public $orderitem_qty;
    public $order_subtotal;
    public $order_id;
    public $price;
    public $orders;
    public $order_total;
    public $orderitem_slug;
    public $quantity;
    public $type;
    public function mount(){
        $selectOrderitems=OrderItem::where('id', session()->get('orderitem')['orderitem_id'])->first();
        $this->orderitem_id=$selectOrderitems->id;
        $this->quantity=$selectOrderitems->quantity;
        $this->price=$selectOrderitems->price;
    }
    public function calculate(){
        if($this->qty > 0){
            $this->total= $this->qty * $this->price;
        }else{
        $this->total=0;
        }
    }
    public function updated($fields)
    {
      $this->validateOnly($fields,[
        'qty' => 'required|numeric|min:0|not_in:0',
        'type' => 'required'
      ]);
    }
    public function UpdateOrderItem(){
       $this->validate([
        'qty' => 'required|numeric|min:0|not_in:0',
        'type' => 'required' 
       ]);
      if($this->type =='1'){
          $this->order_subtotal= session()->get('order')['subtotal'] + $this->total;
          $this->order_total= session()->get('order')['total'] + $this->total;
          $this->orderitem_qty=$this->qty + $this->quantity;
          DB::update('update order_items set quantity = ? where id = ?',[$this->orderitem_qty, $this->orderitem_id]);
          DB::update('update orders set subtotal = ?, total = ? where id = ?',[$this->order_subtotal, $this->order_total, session()->get('order')['order_id']]);
          session()->forget('order');
          session()->forget('orderitem');
          return redirect()->route('admin.order');
          session()->flash('editorderitem_success', 'ແກ້ໄຂສໍາເລັດແລ້ວ !');
      }
      if($this->type =='0' && $this->qty < $this->quantity){
        $this->order_subtotal= session()->get('order')['subtotal'] - $this->total;
        $this->order_total= session()->get('order')['total'] - $this->total;
        $this->orderitem_qty=$this->quantity - $this->qty;
        DB::update('update order_items set quantity = ? where id = ?',[$this->orderitem_qty, $this->orderitem_id]);
        DB::update('update orders set subtotal = ?, total = ? where id = ?',[$this->order_subtotal, $this->order_total, session()->get('order')['order_id']]);
        session()->forget('order');
        session()->forget('orderitem');
        return redirect()->route('admin.order');
        session()->flash('editorderitem_success', 'ແກ້ໄຂສໍາເລັດແລ້ວ !');
      }else{
        session()->flash('errore_qty');
      }
    }
    public function deleteirderitem($id){
        $orderitems=OrderItem::find($id);
        $orderitems->delete();
        session()->flash('message_removeorderitem','Delete successfully !');
    }
    public function render()
    {
        $this->calculate();
        $orderitems=OrderItem::where('id', $this->orderitem_id)->paginate(10);
        return view('livewire.backend.order.edit-order-item-component',['orderitems' => $orderitems])->layout('layouts.backend.app');
    }
}
