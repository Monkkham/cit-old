<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use DB;
use App\Models\OrderItem;
use App\Models\Order;
class UpdateOrderCancelComponent extends Component
{
    public $order_id;
    public $order_slug;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
    }
    public function Editcancelorder(){
        DB::update('update orders set status = ? where id = ?',['ordered',$this->order_id]);
        DB::update('update transactions set status = ? where order_id = ?',['pending',$this->order_id]);
        return redirect()->route('admin.order');
    }
    public function render()
    {
        $editcancelorder=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->get();
        return view('livewire.backend.order.update-order-cancel-component',['editcancelorder' => $editcancelorder])->layout('layouts.backend.app');
}
}