<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Balance;
use App\Models\BalanceItem;
use App\Models\Transaction;
use DB;
class ConfirmOrderComponent extends Component
{
    public $order_id;
    public $order_slug;
    public $check_balance;
    public $user_id;
    public $amount_divid;
    public $update_balance;
    public $balance_id;
    public $update_allbalance;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
        $this->user_id=$orders->user_id;
        $this->amount_divid=$orders->amount_divid;

    }
    public function checkpaymentOnebcelOne(){
        $transaction=Transaction::where('order_id', $this->order_id)->first();
        if($transaction->mode =='onepay' && $transaction->check_payment =='0'){
          session()->flash('message_confirmpaymentbefore', 'ກະລຸນາກວດເບິ່ງລາຍລະອຽດຂອງການຊໍາລະເງິນຜ່ານ Bcel One ກ່ອນ !');
          return redirect()->route('admin.order');
        }
    }
    public function Confirmorder(){

        //customer get divid
        $check_balance=Balance::where('user_id',$this->user_id)->first();
        if(!$check_balance){
            $balances = new Balance();
            $balances->all_balance=$this->amount_divid;
            $balances->balance=$this->amount_divid;
            $balances->user_id=$this->user_id;
            $balances->save();
        }else{
            $this->update_allbalance=$check_balance->all_balance + $this->amount_divid;
            $this->update_balance=$check_balance->balance + $this->amount_divid;
            DB::update('update balances set all_balance = ? , balance = ? where user_id = ?',[$this->update_allbalance, $this->update_balance,$this->user_id]);
        }
        if($this->amount_divid !=''){
            $check_balance=Balance::where('user_id',$this->user_id)->first();
            if($check_balance){
                $this->balance_id=$check_balance->id;
                $balance_item = new BalanceItem();
                $balance_item->amount=$this->amount_divid;
                $balance_item->balance_id=$this->balance_id;
                $balance_item->save();
            }
        }
        DB::update('update orders set status = ? where id = ?',['arrived',$this->order_id]);
        DB::update('update transactions set check_payment=? , status = ? where order_id = ?',['1','refunded',$this->order_id]);
        DB::update('update log_divids set status = ? where order_id = ?',['s',$this->order_id]);
        return redirect()->route('admin.order');
        session()->flash('confirmorder_successfully');
    }
    public function render()
    {
        $this->checkpaymentOnebcelOne();
        $confirmorderitem=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->get();
        return view('livewire.backend.order.confirm-order-component',['confirmorderitem' => $confirmorderitem])->layout('layouts.backend.app');
    }
}
