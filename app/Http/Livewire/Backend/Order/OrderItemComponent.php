<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
class OrderItemComponent extends Component
{
    public $order_id;
    public $order_slug;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
    }
    public function render()
    {
        $orderitems=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->paginate(10);
        return view('livewire.backend.order.order-item-component', ['orderitems' => $orderitems])->layout('layouts.backend.app');
    }
}
