<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Transaction;
use App\Models\Order;
use DB;
class DetailPaymentComponent extends Component
{
    public $order_id;
    public $detailpayment_slug;
    public function mount($detailpayment_slug){
        $orders=Order::where('id', $this->detailpayment_slug)->first();
        $this->order_id=$orders->id;
    }
    public function confirmpayonebcelone(){
        DB::update('update transactions set check_payment = ? where order_id = ?',['1',$this->order_id]);
        return redirect()->route('admin.order');
    }
    public function render()
    {
        $detailpayments = Transaction::where('order_id', $this->order_id)->where('mode','LIKE','onepay')->first();
        return view('livewire.backend.order.detail-payment-component',['detailpayments' => $detailpayments])->layout('layouts.backend.app');
    }
}
