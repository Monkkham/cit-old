<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Transaction;
use DB;
class EditOrderComponent extends Component
{
    public $order_id;
    public $order_slug;
    public $total;
    public $qty;
    public $price;
    public $orders;
    public $check_edit;
    public $orderitem_id;
    public $order_subtotal;
    public $order_total;
    public $subtotal;
    public $selectOrderItem = [];
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
        $this->total=$orders->total;
        $this->subtotal=$orders->subtotal;

    }
    public function deleteirderitem($id){
        $orderitems=OrderItem::find($id);
        $this->price=$orderitems->price;
        $this->qty=$orderitems->quantity;
        $this->order_subtotal=$this->subtotal - ($this->price * $this->qty);
        $this->order_total=$this->total - ($this->price * $this->qty);
        if($this->selectOrderItem == $orderitems->id){
            $check_orderitem=OrderItem::where('order_id', $this->order_id)->get();
            if($check_orderitem->count() <=1){
                $cleanorder=Order::where('id', $this->order_id)->first();
                $transaction_order=Transaction::where('order_id', $this->order_id)->first();
                $cleanorder->delete();
                $transaction_order->delete();
                return redirect()->route('admin.order');
            }else{
                DB::update('update orders set subtotal = ? , discount = ?,  total = ? where id = ?',[$this->order_subtotal,0,$this->order_total,$this->order_id]);
                $orderitems->delete();
                return redirect()->route('admin.order');
            }
        }else{
            session()->flash('no_deleteorderitem');
        }
    }
    public function getOrderItemId(){
        if($this->selectOrderItem) {
            session()->put('orderitem', [
                'orderitem_id' => $this->selectOrderItem , 
            ]);
            session()->put('order', [
                'order_id' => $this->order_id,
                'total' => $this->total,
                'subtotal' => $this->subtotal,
            ]);
        }else{
            session()->forget('order');
            session()->forget('orderitem');
        }
    }
    public function updated($fields)
    {
      $this->validateOnly($fields,[
        'qty' =>'required|numeric',
      ]);
    }
    public function updateorderitem($id){
       $this->validate([
        'qty' =>'required|numeric', 
       ]);
    }
    public function render()
    {
        $this->getOrderItemId();
        $orderitems=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->get();
        return view('livewire.backend.order.edit-order-component',['orderitems' => $orderitems])->layout('layouts.backend.app');
    }
}
