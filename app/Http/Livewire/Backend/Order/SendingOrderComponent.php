<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Order;
use App\Models\Transaction;
class SendingOrderComponent extends Component
{
    use WithPagination;
    public function render()
    {
        $sendingorders = Transaction::where('status','LIKE','approved')->orderBy('id', 'desc')->paginate(10);
        return view('livewire.backend.order.sending-order-component', ['sendingorders' => $sendingorders])->layout('layouts.backend.app');
    }
}
