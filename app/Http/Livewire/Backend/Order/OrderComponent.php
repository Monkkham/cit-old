<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Order;
use App\Models\Transaction;
use Livewire\WithPagination;
use App\Models\OrderItem;
use DB;
class OrderComponent extends Component
{
    use WithPagination;
    public $selectOrder = [];
    public $order_id, $search_by_date=0;
    public function CheckEditOrder(){
        if($this->selectOrder) {
            session()->put('Orderedit', [
                'order_id' => $this->selectOrder , 
            ]);
        }else{
            session()->forget('Orderedit');
        }
    }
    public function render()
    {
        if($this->search_by_date == 0){
            $this->CheckEditOrder();
            $transactions = Transaction::orderBy('id','desc')->paginate(10);
            
        }else{
            $this->CheckEditOrder();
            $transactions = Transaction::orderBy('id','desc')->where('created_at', 'like', '%' .$this->search_by_date. '%')->paginate(10);
        }
        return view('livewire.backend.order.order-component',['transactions' => $transactions])->layout('layouts.backend.app');
    }
}
