<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Transaction;
use Livewire\WithPagination;
use DB;
class DoneSendComponent extends Component
{
    use WithPagination;
    public function render()
    {
        $orderdonesends = Transaction::where('status','LIKE','refunded')->orderBy('id', 'desc')->paginate(10);
        return view('livewire.backend.order.done-send-component',['orderdonesends' => $orderdonesends])->layout('layouts.backend.app');
    }
}
