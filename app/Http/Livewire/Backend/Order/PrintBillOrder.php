<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
class PrintBillOrder extends Component
{
    public $order_id;
    public $order_slug;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
    }
    public function render()
    {
        $orderitems=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')->paginate(10);
        $orders=Order::where('id', $this->order_id)->first();
        return view('livewire.backend.order.print-bill-order',['orderitems' => $orderitems,'orders' =>$orders])->layout('layouts.backend.app');
    }
}
