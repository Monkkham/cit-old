<?php

namespace App\Http\Livewire\Backend\Order;

use Livewire\Component;
use App\Models\Order;
use DB;
class CancelOrderComponent extends Component
{
    
    public $order_id;
    public $cancelorder_slug;
    public function mount($cancelorder_slug){
        $orders=Order::where('id', $this->cancelorder_slug)->first();
        $this->order_id=$orders->id;
        DB::update('update orders set status = ? where id = ?',['canceled',$this->order_id]);
        DB::update('update transactions set status = ? where order_id = ?',['declined',$this->order_id]);
    }
    public function render()
    {
        return view('livewire.backend.order.cancel-order-component')->layout('layouts.backend.app');
    }
}
