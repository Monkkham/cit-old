<?php

namespace App\Http\Livewire\Backend\Commission;

use Livewire\Component;
use App\Models\Commission;
class PrintCommissionComponent extends Component
{
    public $user_id, $slug;
    public function mount($slug){
        $chek_commission  = Commission::where('user_id',$this->slug)->first();
        if(!empty($chek_commission->user_id)){
            $this->user_id  = $chek_commission->user_id;
        }
    }
    public function render()
    {
        $printcommissions = Commission::where('user_id', $this->user_id)->orderBy('id', 'desc')->get();
        return view('livewire.backend.commission.print-commission-component')
        ->layout('layouts.backend.app');
    }
}
