<?php

namespace App\Http\Livewire\Backend\Commission;

use App\Models\Commission;
use App\Models\Customer;
use App\Models\Divid;
use App\Models\Employee;
use Auth;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class CommissionComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $selectData = true;
    public $createData = false;
    public $updateData = false, $select_page;
    public $ids, $ed_slug, $fromdate, $select_divid, $ed_cash_format, $search, $year, $cash_format, $customer, $date, $ed_select_divid, $total_all_commission, $amount_all_commission, $month, $total_commission, $select_emp_id, $fullname, $todate, $totals, $emp_id, $slug, $amount, $percent, $fixed, $ed_fixed, $note, $des, $ed_emp_id, $ed_amount, $ed_percent, $ed_note, $ed_des, $total;
    public function mount()
    {
        $this->select_divid;
        $this->ed_select_divid;
        $this->month = Carbon::now()->month;
        $this->year = date('Y');
        $this->select_page = 10;
    }
    public function render()
    {
        $employees = Employee::orderBy('employees.id', 'desc')->where('status', 1)->where('salary_id', '!=', '')->get();
        $customers = Customer::orderBy('customers.id', 'desc')->get();
        $divids = Divid::orderBy('id', 'desc')->get();
        if (!empty($this->fromdate) && !empty($this->todate) && empty($this->select_emp_id) && empty($this->month)) {
            $commissions = Commission::whereBetween('date', [$this->fromdate, $this->todate])->orderBy('id', 'desc')
            ->where(function($query){
                $query->where('customer', 'like', '%' .$this->search. '%')
                ->orWhere('des', 'like', '%' .$this->search. '%');
            })   
            ->paginate($this->select_page);
            $this->total_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->orderBy('id', 'desc')->sum('total');
            $this->total_all_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->sum('total');
            $this->amount_all_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->sum('amount');
            $employee = Employee::where('id', $this->select_emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (!empty($this->fromdate) && !empty($this->todate) && !empty($this->select_emp_id)) {
            $commissions = Commission::whereBetween('date', [$this->fromdate, $this->todate])->where('emp_id', $this->select_emp_id)->orderBy('id', 'desc')
            ->where(function($query){
                $query->where('customer', 'like', '%' .$this->search. '%')
                ->orWhere('des', 'like', '%' .$this->search. '%');
            })   
            ->paginate($this->select_page);
            $this->total_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->where('emp_id', $this->select_emp_id)->orderBy('id', 'desc')->sum('total');
            $this->total_all_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->where('emp_id', $this->select_emp_id)->sum('total');
            $this->amount_all_commission = Commission::whereBetween('date', [$this->fromdate, $this->todate])->where('emp_id', $this->select_emp_id)->sum('amount');
            $employee = Employee::where('id', $this->select_emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (!empty($this->month) && !empty($this->year) && empty($this->select_emp_id)) {
            $commissions = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->orderBy('id', 'desc')
            ->where(function($query){
                $query->where('customer', 'like', '%' .$this->search. '%')
                ->orWhere('des', 'like', '%' .$this->search. '%');
            })
            ->paginate($this->select_page);
            $this->total_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->orderBy('id', 'desc')->sum('total');
            $this->total_all_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->sum('total');
            $this->amount_all_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->sum('amount');
            $employee = Employee::where('id', $this->select_emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } elseif (!empty($this->month) && !empty($this->year) && !empty($this->select_emp_id)) {
            $commissions = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->where('emp_id', $this->select_emp_id)->orderBy('id', 'desc')
            ->where(function($query){
                $query->where('customer', 'like', '%' .$this->search. '%')
                ->orWhere('des', 'like', '%' .$this->search. '%');
            })
            ->paginate($this->select_page);
            $this->total_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->where('emp_id', $this->select_emp_id)->orderBy('id', 'desc')->sum('total');
            $this->total_all_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->where('emp_id', $this->select_emp_id)->sum('total');
            $this->amount_all_commission = Commission::whereMonth('date', $this->month)->whereYear('date', $this->year)->where('emp_id', $this->select_emp_id)->sum('amount');
            $employee = Employee::where('id', $this->select_emp_id)->where('status', '1')->first();
            if (!empty($employee->id)) {
                $this->fullname = $employee->firstname . ' ' . $employee->lastname;
            }
        } else {
            $commissions = Commission::orderBy('id', 'desc')
            ->where(function($query){
                $query->where('customer', 'like', '%' .$this->search. '%')
                ->orWhere('des', 'like', '%' .$this->search. '%');
            })
                ->paginate($this->select_page);
            $this->total_all_commission = Commission::sum('total');
            $this->amount_all_commission = Commission::sum('amount');
        }
        $this->page = (($this->page * $this->select_page) - $this->select_page) + 1;
        return view('livewire.backend.commission.commission-component', ['page' => $this->page, 'employees' => $employees, 'divids' => $divids, 'commissions' => $commissions, 'customers' => $customers])
            ->layout('layouts.backend.app');
    }
    public function back()
    {
        return redirect()->to('/commision-of-employee');
    }
    public function Autoformat()
    {
        if (!empty($this->amount)) {
            $this->slug = number_format($this->amount);
        }
    }
    public function AutoformatCash()
    {
        if (!empty($this->fixed)) {
            $this->cash_format = number_format($this->fixed);
        }
    }
    public function AutoformatCashEdit()
    {
        if (!empty($this->ed_fixed)) {
            $this->ed_cash_format = number_format($this->ed_fixed);
        }
    }
    public function Edit_Autoformat()
    {
        if (!empty($this->ed_amount)) {
            $this->ed_slug = number_format($this->ed_amount);
        }
    }
    public function ResetData()
    {
        $this->emp_id = "";
        $this->amount = "";
        $this->note = "";
        $this->percent = "";
        $this->fixed = "";
        $this->customer = "";
        $this->des = "";
        // edit
        $this->ids = "";
        $this->ed_emp_id = "";
        $this->ed_amount = "";
        $this->ed_note = "";
        $this->ed_percent = "";
        $this->ed_fixed = "";
        $this->date = "";
        $this->total = "";
        $this->select_divid = "";
        $this->slug = "";
        $this->cash_format = "";
        $this->ed_cash_format = "";
        $this->ed_slug = "";
    }
    public function showFrom()
    {
        $this->createData = true;
        $this->selectData = false;
        $this->updateData = false;
    }
    protected $rules = [
        'emp_id' => 'required',
        'date' => 'required',
        'select_divid' => 'required',
    ];
    protected $messages = [
        'emp_id.required' => 'ເລືອກພະນັກງານກ່ອນ',
        'date.required' => 'ໃສ່ວັນທີກ່ອນ',
        'select_divid.required' => 'ເລືອກປະເພດກ່ອນ',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function create()
    {
        $this->validate();
        if (!empty($this->amount)) {
            $this->total = $this->amount * $this->percent / 100;
        } else {
            $this->total = 0;
        }
        $commisions = new Commission();
        $commisions->emp_id = $this->emp_id;
        $commisions->customer = $this->customer;
        $commisions->des = $this->des;
        if (!empty($this->amount)) {
            $commisions->amount = $this->amount;
        } else {
            $commisions->amount = 0;
        }
        if (!empty($this->percent)) {
            $commisions->percent = $this->percent;
        } else {
            $commisions->percent = 0;
        }
        if (!empty($this->fixed)) {
            $commisions->total = $this->fixed;
        } else {
            $commisions->total = $this->total;
        }
        $commisions->note = $this->note;
        $commisions->user_id = Auth::user()->id;
        $commisions->date = $this->date;
        $commisions->save();
        $this->ResetData();
        session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
        // return  redirect()->to('/commision-of-employee');
        // $this->selectData = true;
        // $this->createData = false;
    }

    // edi funtion
    public function edit($id)
    {
        $this->ResetData();
        $this->selectData = false;
        $this->updateData = true;
        $commisions = Commission::find($id);
        $this->ids = $commisions->id;
        $this->ed_emp_id = $commisions->emp_id;
        $this->customer = $commisions->customer;
        $this->ed_des = $commisions->des;
        $this->ed_amount = $commisions->amount;
        $this->ed_percent = $commisions->percent;
        $this->ed_fixed = $commisions->total;
        $this->ed_total = $commisions->total;
        $this->ed_note = $commisions->note;
    }
    public function delete($id)
    {
        $commisions = Commission::find($id);
        $commisions->delete();
        session()->flash('message', 'ລຶບຂໍ້ມູນສໍາເລັດແລ້ວ');
        return redirect()->to('/commision-of-employee');
    }

    // update funtion
    public function update()
    {
        $this->validate([
            'ed_emp_id' => 'required',
        ]);
        if (!empty($this->ed_amount)) {
            $this->totals = ($this->ed_amount * $this->ed_percent) / 100;
        } else {
            $this->totals = 0;
        }
        $commisions = Commission::find($this->ids);
        $commisions->emp_id = $this->ed_emp_id;
        $commisions->des = $this->ed_des;
        $commisions->customer = $this->customer;
        $commisions->amount = $this->ed_amount;
        if ($this->ed_select_divid == 'percent') {
            $commisions->total = $this->totals;
            $commisions->percent = $this->ed_percent;
        } else {
            $commisions->total = $this->ed_fixed;
            $commisions->percent = 0;
        }
        $commisions->note = $this->ed_note;
        $commisions->user_id = Auth::user()->id;
        $commisions->update();
        $this->ResetData();
        session()->flash('message', 'ອັບແດດສຳແລັດແລ້ວ');
        return redirect()->to('/commision-of-employee');
        $this->selectData = true;
        $this->createData = false;
        $this->updateData = false;
    }
}
