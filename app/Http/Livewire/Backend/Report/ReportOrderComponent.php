<?php

namespace App\Http\Livewire\Backend\Report;

use App\Models\Currency;
use App\Models\P1\P1Contact;
use App\Models\P1\P1CurrencyRate;
use App\Models\P1\P1Order;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class ReportOrderComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $start_date, $end_date, $sum_total_cost, $sum_total_sale, $sum_profit, $sum_lost;
    public function render()
    {
        if (auth()->user()->branch_id) {
            $data = P1Order::select('p1_order.*', 't.branch_id', DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where order_id = p1_order.id) as total_qty"), 
            DB::raw("(SELECT SUM((p1_order_detail.total_online_price_dollar - p1_products.import_price) * p1_order_detail.qty) from p1_products inner join p1_order_detail where p1_products.id = p1_order_detail.product_id AND p1_order_detail.order_id = p1_order.id) as sum_profit"),
            DB::raw("(SELECT SUM(p1_products.import_price * p1_order_detail.qty) from p1_products inner join p1_order_detail where p1_products.id = p1_order_detail.product_id AND p1_order_detail.order_id = p1_order.id) as sum_total_cost"), 
            DB::raw("(SELECT SUM(p1_order_transaction.shipping_cost) from p1_order_transaction where order_id = p1_order.id) as shipping_cost"), 
            DB::raw("(SELECT SUM(p1_order_transaction.vat) from p1_order_transaction where order_id = p1_order.id) as vat"))
                ->join('p1_order_transaction as t', 't.order_id', '=', 'p1_order.id')->where('branch_id', auth()->user()->branch_id)->where('p1_order.status', 3)->get();
            if ($this->start_date && $this->end_date) {
                $data  =  $data->whereBetween('created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' 23:59:59'))]);
            }
            $check_currentcy = P1CurrencyRate::where('branch_id', auth()->user()->branch_id)->first();
            $this->sum_total_cost = $data->sum('sum_total_cost');
            if($check_currentcy->rate_to_kip_operator =='*'){
                $this->sum_total_sale = $data->sum('total_price') / $check_currentcy->rate_to_kip;
            }else if($check_currentcy->rate_to_kip_operator =='/'){
                $this->sum_total_sale = $data->sum('total_price') * $check_currentcy->rate_to_kip;
            }
            $this->sum_profit = $this->sum_total_sale - $this->sum_total_cost;
        } else {
            $data = [];
            $this->sum_total_cost = 0;
            $this->sum_total_sale = 0;
            $this->sum_profit = 0;
            $this->sum_lost = 0;
        }
        return view('livewire.backend.report.report-order-component', compact('data'))->layout('layouts.backend.app');
    }
}
