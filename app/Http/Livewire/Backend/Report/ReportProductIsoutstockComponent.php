<?php

namespace App\Http\Livewire\Backend\Report;

use App\Models\P1\P1Products;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ReportProductIsoutstockComponent extends Component
{
    public $start_date, $end_date, $sum_total, $sum_profit, $sum_lost,$select_page;
    public function mount(){
        $this->select_page = 10;
    }
    public function render()
    {
        if($this->select_page){
          $data  =  P1Products::select('p1_products.*',DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where product_id = p1_products.id) as sum_total"))->whereNotIn('id', DB::table('p1_order_detail')->pluck('product_id'))->orderBy('sum_total', 'desc')->paginate($this->select_page);
        }else{
            $data  =  P1Products::select('p1_products.*',DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where product_id = p1_products.id) as sum_total"))->whereNotIn('id', DB::table('p1_order_detail')->pluck('product_id'))->orderBy('sum_total', 'desc')->get();
        }
        return view('livewire.backend.report.report-product-isoutstock-component', compact('data'))->layout('layouts.backend.app');
    }
}
