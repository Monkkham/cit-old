<?php

namespace App\Http\Livewire\Backend\Report;
use App\Models\P1\P1Order;
use App\Models\P1\P1Products;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class ReportProductComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $start_date, $end_date, $sum_total, $sum_profit, $sum_lost;
    public function render()
    {
        $data =  P1Products::select('p1_products.*',DB::raw("(SELECT SUM(p1_order_detail.qty) from p1_order_detail where product_id = p1_products.id) as sum_total"))->whereIn('id', DB::table('p1_order_detail')->pluck('product_id'))->orderBy('sum_total', 'desc')->get();
        return view('livewire.backend.report.report-product-component', compact('data'))->layout('layouts.backend.app');
    }
}
