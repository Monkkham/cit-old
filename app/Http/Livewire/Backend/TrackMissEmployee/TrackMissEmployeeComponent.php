<?php

namespace App\Http\Livewire\Backend\TrackMissEmployee;

use Livewire\Component;
use App\Models\Employee;
use App\Models\TrackMissEmployee;
use App\Models\MissType;
use Carbon\Carbon;
use DB;
use Auth;
use Livewire\WithPagination;

class TrackMissEmployeeComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $selectData=true;
    public $createData=false;
    public $updateData=false,
    $emp_id, $qty_date, $note, $date,$date_today, $total,$hiddenId,$start_date,$end_date,$miss_total, $select_page;
    public function mount(){
        $this->select_page = 10;
    }
    public function render()
    {
        $this->date_today = Carbon::today();
        $misstypes = MissType::get();
        $employees = Employee::where('status',1)->orderBy('id','desc')->where('salary_id', '!=', '')->get();
        $employees_add = DB::table('employees')->where('status',1)->orderBy('id','desc')->where('salary_id', '!=', '')
            ->whereNotIn('id',function($query) {
                $query->select('emp_id')->from('track_miss_employees')->whereDate('date', '=', Carbon::today());
                })->get();
        if(!empty($this->start_date) && !empty($this->end_date) && empty($this->emp_id)){
            $trackmissemployees = TrackMissEmployee::orderBy('id','desc')->whereBetween('date', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->paginate($this->select_page);
            $this->miss_total = TrackMissEmployee::orderBy('id','desc')->whereBetween('date', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->sum('total_salary');
        }elseif(empty($this->start_date) && empty($this->end_date) && !empty($this->emp_id)){
            $trackmissemployees = TrackMissEmployee::orderBy('id','desc')->where('emp_id', $this->emp_id)->paginate($this->select_page);
            $this->miss_total = TrackMissEmployee::orderBy('id','desc')->where('emp_id', $this->emp_id)->sum('total_salary');
        }elseif(!empty($this->start_date) && !empty($this->end_date) && !empty($this->emp_id)){
            $trackmissemployees = TrackMissEmployee::orderBy('id','desc')->where('emp_id', $this->emp_id)->whereBetween('date', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->paginate($this->select_page);
            $this->miss_total = TrackMissEmployee::orderBy('id','desc')->where('emp_id', $this->emp_id)->whereBetween('date', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->sum('total_salary');
        }
        else{
            $trackmissemployees = TrackMissEmployee::orderBy('id','desc')->paginate($this->select_page);
            $this->miss_total = TrackMissEmployee::orderBy('id','desc')->sum('total_salary');
        }
        $this->page = (($this->page * 10) - 10) +1;
        return view('livewire.backend.track-miss-employee.track-miss-employee-component', ['page' => $this->page,'employees_add' => $employees_add, 'misstypes' => $misstypes,'employees' => $employees, 'trackmissemployees' => $trackmissemployees])
        ->layout('layouts.backend.app');
    }
    public function back(){
        return  redirect()->to('/track-employee-absence/cit-group-la');
    }
    
    public function showFrom()
    {
        $this->createData = true;
        $this->selectData = false;
        $this->updateData = false;
    }
    protected $rules = [
        'emp_id' => 'required',
        'qty_date' => 'required',
        'date' => 'required',
    ];
    protected $messages = [
        'emp_id.required' => 'ເລືອກພະນັກງານກ່ອນ',
        'qty_date.required' => 'ເລືອກປະເພດຂາດກ່ອນ',
        'date.required' => 'ໃສ່ວັນທີຂາດກ່ອນ'
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function create(){
        $this->validate();
        $check_misstype = MissType::find($this->qty_date);
        if($check_misstype){
            $trackmissemployee = new TrackMissEmployee();
            $trackmissemployee->emp_id = $this->emp_id;
            $trackmissemployee->misstype_id = $this->qty_date;
            $trackmissemployee->total_salary = $check_misstype->amount;
            $trackmissemployee->note = $this->note;
            $trackmissemployee->user_id = Auth::user()->id;
            $trackmissemployee->date = $this->date;
            $trackmissemployee->save();
            session()->flash('message', 'ເພີ່ມຂໍ້ມູນສຳແລັດແລ້ວ');
            // return  redirect()->to('/track-employee-absence/cit-group-la');
            $this->ResetData();
            // $this->selectData = true;
            // $this->createData = false;
        }else{
            return;
        }
    }
    public function ResetData(){
        $this->qty_date = '';
        $this->emp_id = '';
        $this->note = '';
        $this->date = '';
    }
    public function edit($id){
        $this->ResetData();
        $this->createData = true;
        $this->selectData = false;
        $this->updateData = false;
        $singleData = TrackMissEmployee::find($id);
        $this->hiddenId = $singleData->id;
        $this->qty_date = $singleData->misstype_id;
        $this->emp_id = $singleData->emp_id;
        $this->note = $singleData->note;
        $this->date = $singleData->date;
    }
    public function update(){
        $check_misstype = MissType::find($this->qty_date);
            if($check_misstype){
            $singleData = TrackMissEmployee::find($this->hiddenId);
            $singleData->misstype_id = $this->qty_date;
            $singleData->emp_id = $this->emp_id;
            $singleData->total_salary = $check_misstype->amount;
            $singleData->note = $this->note;
            $singleData->date = $this->date;
            $singleData->update();
            $this->ResetData();
            $this->selectData = true;
            $this->createData = false;
            }else{
                return;
            }
    }
    public function delete($id){
        $singleData = TrackMissEmployee::find($id);
        $singleData->delete();
        session()->flash('message', 'ລຶບຂໍ້ມູນສຳແລັດແລ້ວ');
    }
}
