<?php

namespace App\Http\Livewire\Backend\Loan;

use Livewire\Component;
use App\Models\Loan;
use App\Models\Fund;
use Carbon\Carbon;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\LoanDetail;
use App\Models\CalculateFundLog;
class LoanComponent extends Component
{
    public $search;
    public $selectData=true;
    public $createData=false,$showDetail = false,$period,$rate,$selectOne = [],$fullname,$showdata = [],$start_date,$end_date,$show_percent_loan, $percent_loan,$total,$type,$slug,$emp_id,$amount,$percent,$qty_pay,$date;
    public function mount(){
         $this->type = 'ຕາຍຕົວ';
         $this->period = 'month';
         if(!empty($this->type)){
            $this->type = 'ຕາຍຕົວ';
         }
         if(!empty($this->period)){
            $this->period = 'month';
         }
    }
    public function render()
    {
       if(!empty($this->start_date) && !empty($this->end_date) && empty($this->type_status)){
        $data = Loan::whereBetween('created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->orderBy('id', 'desc')->get();
       }
        else{
            $data = Loan::orderBy('id', 'desc')->get();
        }
        $branch = Branch::first();
        if($branch){
            $this->rate = $branch->rate / 100;
            $this->percent_loan = $branch->percent_loan / 100;
            $this->show_percent_loan = $branch->percent_loan;
        }else{
            $this->rate = 0;
            $this->percent_loan = 0;
            $this->show_percent_loan = 0;
        }
        $check_total = Loan::orderBy('id', 'desc')->first();
        if($check_total){
            $this->total = $check_total->remain_total;
        }else{
            $this->total = Fund::sum('remain_total');
        }
        $check_calculate = Loan::whereMonth('created_at', Carbon::today()->month)->first();
        if($check_calculate){
            $this->check_calculate = true;
        }
        $check_total = CalculateFundLog::orderBy('id', 'desc')->first();
        if($check_total){
            $this->total = $check_total->remain_total;
        }else{
            $this->total = Fund::sum('remain_total');
        }
        $employees = Employee::where('status',1)->orderBy('id','desc')->where('salary_id', '!=', '')->get();
        return view('livewire.backend.loan.loan-component', compact('data','employees'))->layout('layouts.backend.app');
    }
    public function showFrom()
    {
        $this->createData = true;
        $this->selectData = false;
    }
     public function resetData(){
        $this->amount = '';
        $this->date = '';
        $this->qty_pay = '';
        $this->emp_id = '';
        $this->type = '';
     }
     public function AutoformatAmount(){
        $this->slug = '';
        if(!empty($this->amount)){
            $this->slug= number_format($this->amount);
        }
    }
    public function back()
    {
        return  redirect()->to('/loan-employee');
    }
    public function create(){
        $this->validate([
            'amount' => 'required|numeric|min:1000',
            'qty_pay' => 'required'
        ],[
            'amount.required' => 'ຕ້ອງໃສ່ຈໍານວນເງິນກູ້ຢືມກ່ອນ',
            'amount.min' => 'ຕ້ອງໃຫຍ່ກວ່າ 1,000',
            'qty_pay.required' => 'ຕອ້ງໃສ່ຈໍານວນແບ່ງງວດກ່ອນ'
        ]);
        $start_date = CalculateFundLog::orderBy('id', 'asc')->first();
        $end_date = CalculateFundLog::orderBy('id', 'desc')->first();
        if($this->amount <= $this->total){
           $loan = new Loan();
           $loan->emp_id = $this->emp_id;
           $loan->amount = $this->amount;
           $loan->qty_pay = $this->qty_pay;
           $loan->percent = $this->show_percent_loan;
           $loan->type = $this->type;
           $loan->user_id =  auth()->user()->id;
           $loan->save();
           if($this->period =='month'){
                  if($this->type =='ຕາຍຕົວ'){
                        $a = 30; 
                        $day = 0;
                        for($i = 1; $i <= $this->qty_pay;$i++){
                            $day += $a;
                            $loandetail = new LoanDetail();
                            $loandetail->loan_id = $loan->id;
                            $loandetail->amount = $this->amount / $this->qty_pay;
                            $loandetail->profit = $this->percent_loan *  $this->amount;
                            $loandetail->total = $loandetail->amount + $loandetail->profit;
                            $loandetail->date = Carbon::now()->addDays($day);
                            $loandetail->save();
                        }
                    }else{
                        $a = 30; 
                        $day = 0;
                        for($i = 1; $i <= $this->qty_pay;$i++){
                            $day += $a;
                            $sum_amount = LoanDetail::where('loan_id', $loan->id)->sum('amount');
                            $loandetail = new LoanDetail();
                            $loandetail->loan_id = $loan->id;
                            $loandetail->amount = $this->amount / $this->qty_pay;
                            if($sum_amount > 0){
                                $loandetail->profit = $this->percent_loan *  ($this->amount - $sum_amount);
                            }else{
                            $loandetail->profit = $this->percent_loan *  ($this->amount - ($this->amount / $this->qty_pay));
                            }
                            $loandetail->total = $loandetail->amount + $loandetail->profit;
                            $loandetail->date = Carbon::now()->addDays($day);
                            $loandetail->save();
                        }
                    }
                }else{
                    if($this->type =='ຕາຍຕົວ'){
                        $a = 365; 
                        $year = 0;
                        for($i = 1; $i <= $this->qty_pay;$i++){
                            $year += $a;
                            $loandetail = new LoanDetail();
                            $loandetail->loan_id = $loan->id;
                            $loandetail->amount = $this->amount / $this->qty_pay;
                            $loandetail->profit = $this->percent_loan *  $this->amount;
                            $loandetail->total = $loandetail->amount + $loandetail->profit;
                            $loandetail->date = Carbon::now()->addDays($year);
                            $loandetail->save();
                        }
                    }else{
                        $a = 365;
                        $year = 0;
                        for($i = 1; $i <= $this->qty_pay;$i++){
                            $year += $a;
                            $sum_amount = LoanDetail::where('loan_id', $loan->id)->sum('amount');
                            $loandetail = new LoanDetail();
                            $loandetail->loan_id = $loan->id;
                            $loandetail->amount = $this->amount / $this->qty_pay;
                            if($sum_amount > 0){
                                $loandetail->profit = $this->percent_loan *  ($this->amount - $sum_amount);
                            }else{
                            $loandetail->profit = $this->percent_loan *  ($this->amount - ($this->amount / $this->qty_pay));
                            }
                            $loandetail->total = $loandetail->amount + $loandetail->profit;
                            $loandetail->date = Carbon::now()->addDays($year);
                            $loandetail->save();
                        }
                    }
                }
            $check_calculate = CalculateFundLog::orderBy('id', 'desc')->first();
            $singleData = Loan::where('id', $loan->id)->first();
            if($singleData){
                if(!empty($singleData->emp_id)){
                    $fullname = $singleData->employee->firstname.' '.$singleData->employee->lastname;
                }
            }
            if($check_calculate){
                    if($start_date && $end_date){
                        $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24*60*60);
                        if(intval($date) >= 1){
                            $profit = ($check_calculate->remain_total * $this->rate * intval($date) ) / 365;
                            $sum_total = $check_calculate->remain_total + $profit;
                            }else{
                            $profit = 0;
                            }
                    }else{
                        $profit = 0;
                    }
                $singleData = new CalculateFundLog();
                $singleData->content = $fullname;
                $singleData->total = $check_calculate->total - $this->amount;
                $singleData->total_rate = $profit;
                $singleData->sum_total = $check_calculate->sum_total - $this->amount + $profit;
                $singleData->withraw = $this->amount;
                $singleData->remain_total = $check_calculate->remain_total - $this->amount + $profit;
                $singleData->user_id = auth()->user()->id;
                $singleData->save();
            }
            $this->resetData();
            session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
            return  redirect()->to('/loan-employee');
        }else{
            session()->flash('no_message', 'ຂໍອະໄພ ເງິນກອງທຶນບໍ່ພຽງພໍ!');
            return;   
        }
    }
    public function delete($id){
        $singleData = Loan::where('id', $id)->first();
        $singleData->delete();
        session()->flash('message', 'ລຶບຂໍ້ມູນສໍາເລັດແລ້ວ');
        return  redirect()->to('/loan-employee');
    }
    public function showDetail($id){
        $this->createData = false;
        $this->selectData = false;
        $this->showDetail = true;
        $singleData = Loan::where('id', $id)->first();
        if($singleData){
             if(!empty($singleData->emp_id)){
                $this->fullname = $singleData->employee->firstname.' '.$singleData->employee->lastname;
             }
        }
        $this->showdata  = LoanDetail::where('loan_id', $id)->get();
    }
     public function Payloan(){
        $start_date = CalculateFundLog::orderBy('id', 'asc')->first();
        $end_date = CalculateFundLog::orderBy('id', 'desc')->first();
        $singleData = LoanDetail::where('id', $this->selectOne)->first();
        $singleData_check = Loan::where('id', $singleData->loan_id)->first();
        $check_calculate = CalculateFundLog::orderBy('id', 'desc')->first();
        if($singleData_check){
             if(!empty($singleData_check->emp_id)){
                $this->fullname = $singleData_check->employee->firstname.' '.$singleData_check->employee->lastname;
             }
        }
        if($start_date && $end_date){
            $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24*60*60);
            if(intval($date) >= 1){
                $profit = ($check_calculate->remain_total * $this->rate * intval($date) ) / 365;
             }else{
                $profit = 0;
             }
        }else{
            $profit = ($check_calculate->remain_total * $this->rate * 30 ) / 365;
        }
        if($singleData){
            $singleData->status = 1;
            $singleData->date = date('Y-m-d');
            $singleData->user_id = auth()->user()->id;
            $singleData->update();
            $singleData_cal = new CalculateFundLog();
            $singleData_cal->content = $this->fullname;
            $singleData_cal->total = $check_calculate->remain_total;
            $singleData_cal->total_rate = $profit;
            $singleData_cal->deposit = $singleData->total;
            $singleData_cal->sum_total = $check_calculate->sum_total + $singleData->total + $profit;
            $singleData_cal->remain_total = $check_calculate->remain_total + $singleData->total + $profit;
            $singleData_cal->user_id = auth()->user()->id;
            $singleData_cal->save();
            $this->resetData();
            return redirect()->route('admin.printloan', ['slug' => $singleData->loan_id]);
        }
     }
}