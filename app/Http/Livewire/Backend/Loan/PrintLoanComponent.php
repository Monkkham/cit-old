<?php

namespace App\Http\Livewire\Backend\Loan;

use Livewire\Component;
use App\Models\Loan;
use App\Models\LoanDetail;
class PrintLoanComponent extends Component
{
    public $slug,$loan_id,$fullname;
    public function mount($slug){
        $this->loan_id = $slug;
        $singleData = Loan::where('id', $slug)->first();
        if($singleData){
            if(!empty($singleData->emp_id)){
                $this->fullname = $singleData->employee->firstname.' '.$singleData->employee->lastname;
            }
        }
    }
    public function render()
    {
        $data = LoanDetail::where('loan_id', $this->loan_id)->get();
        return view('livewire.backend.loan.print-loan-component', compact('data'))->layout('layouts.backend.app');
    }
}
