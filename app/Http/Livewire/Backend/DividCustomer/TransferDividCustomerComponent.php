<?php

namespace App\Http\Livewire\Backend\DividCustomer;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Livewire\Component;
use App\Models\RequestWithraw;
use App\Models\HistoryBalance;
use App\Models\Balance;
use Carbon\Carbon;
use DB;
class TransferDividCustomerComponent extends Component
{
    use WithFileUploads;
    public $transfer_id;
    public $request_money;
    public $image;
    public $user_id;
    public $balance_total;
    public $balance_update;
    public $balance_withraw;
    public $balance_withrawupdate;
    public $request_id, $balance_id, $bcelone_image;
    public function mount($transfer_id){
        $requestmoney=RequestWithraw::where('id', $this->transfer_id)->first();
        $this->request_id=$requestmoney->id;
        $this->request_money=$requestmoney->request_money;
        $this->user_id=$requestmoney->user_id;
        if($requestmoney->user_id){
            $this->balance_id = $requestmoney->user->balance_id;
            $this->bcelone_image =$requestmoney->user->bcelone_image;
        }
        $balance_totals=Balance::where('user_id', $this->user_id)->first();
        $this->balance_total=$balance_totals->balance;
        $this->balance_withraw=$balance_totals->withraw_balance;
    }
    public function updated($fields){
        $this->validateOnly($fields,[
            'image' => 'required|mimes:jpg,png,jpeg',
        ]);
    }
    public function Transfermoney(){
        $this->validate([
            'image' => 'required|mimes:jpg,png,jpeg',
        ]);
        if($this->balance_total == 0 || $this->request_money > $this->balance_total){
            session()->flash('balance_no_enough');
        }else{
            $transfers= new HistoryBalance();
            $transfers->amount=$this->request_money;
            $transfers->user_id=$this->user_id;
            $imageName=Carbon::now()->timestamp. '.' .$this->image->extension();
            $this->image->storeAs('upload/transfermoneytocustomer',$imageName);
            $transfers->image = $imageName;
            $transfers->save();
            $this->balance_update = $this->balance_total - $this->request_money;
            $this->balance_withrawupdate=$this->balance_withraw + $this->request_money;
            DB::update('update balances set withraw_balance = ?, balance = ? where user_id = ?',[$this->balance_withrawupdate, $this->balance_update, $this->user_id]);
            $RequestWithraw= RequestWithraw::find($this->request_id);
            $RequestWithraw->delete();
            return redirect()->route('admin.withrawingcustomer');
            session()->flash('transfer_success');
        }
    }
    public function render()
    {
        return view('livewire.backend.divid-customer.transfer-divid-customer-component')->layout('layouts.backend.app');
    }
}
