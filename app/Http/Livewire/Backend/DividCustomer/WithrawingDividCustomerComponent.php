<?php

namespace App\Http\Livewire\Backend\DividCustomer;

use Livewire\Component;
use App\Models\RequestWithraw;
class WithrawingDividCustomerComponent extends Component
{
    public function render()
    {
        $RequestWithraws=RequestWithraw::orderBy('id', 'desc')->get();
        return view('livewire.backend.divid-customer.withrawing-divid-customer-component' ,['RequestWithraws' => $RequestWithraws])->layout('layouts.backend.app');
    }
}
