<?php

namespace App\Http\Livewire\Backend\DividCustomer;

use Livewire\Component;
use App\Models\Balance;
class DividCustomerComponent extends Component
{
    public function render()
    {
        $Banlances=Balance::orderBy('id', 'desc')->get();
        return view('livewire.backend.divid-customer.divid-customer-component' ,['Banlances' => $Banlances])->layout('layouts.backend.app');
    }
}
