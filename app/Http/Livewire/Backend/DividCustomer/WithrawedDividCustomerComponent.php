<?php

namespace App\Http\Livewire\Backend\DividCustomer;

use Livewire\Component;
use App\Models\HistoryBalance;
class WithrawedDividCustomerComponent extends Component
{
    public function render()
    {
        $historywithrawal=HistoryBalance::orderBy('id', 'desc')->get();
        return view('livewire.backend.divid-customer.withrawed-divid-customer-component',['historywithrawal' => $historywithrawal])->layout('layouts.backend.app');
    }
}
