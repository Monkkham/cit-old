<?php

namespace App\Http\Livewire\Backend\CalculateInterest;

use Livewire\Component;
use App\Models\CalculateFundLog;
use App\Models\Fund;
use Carbon\Carbon;
use App\Models\Branch;
class CalculateInterestComponent extends Component
{
    public $search;
    public $selectData=true;
    public $createData=false,
    $content,$start_date,$end_date,$type,$slug, $check_calculate = false,$type_status,$show_rate,$deposit,$withraw, $rate,$total;
    public function mount(){
         $this->type = 1;
    }
    public function render()
    {
       if(!empty($this->start_date) && !empty($this->end_date) && empty($this->type_status)){
        $data = CalculateFundLog::whereBetween('created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->get();
        }
        elseif(!empty($this->start_date) && !empty($this->end_date) && $this->type_status ==1){
            $data = CalculateFundLog::whereBetween('created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->where('deposit', '>', 0)->get();
        }elseif(!empty($this->start_date) && !empty($this->end_date) && $this->type_status ==2){
            $data = CalculateFundLog::whereBetween('created_at', [$this->start_date, date('Y-m-d H:i:s', strtotime($this->end_date . ' +1 day'))])->where('withraw', '>', 0)->get();
        }elseif($this->type_status ==1){
            $data = CalculateFundLog::where('deposit', '>', 0)->get();
        }elseif($this->type_status ==2){
            $data = CalculateFundLog::where('withraw', '>', 0)->get();
        }
        else{
            $data = CalculateFundLog::all();
        }
        $branch = Branch::first();
        if($branch){
            $this->rate = $branch->rate / 100;
            $this->show_rate = $branch->rate;
        }else{
            $this->rate = 0;
            $this->show_rate = 0;
        }
        $check_total = CalculateFundLog::orderBy('id', 'desc')->first();
        if($check_total){
            $this->total = $check_total->remain_total;
        }else{
            $this->total = Fund::sum('remain_total');
        }
        $check_calculate = CalculateFundLog::whereMonth('created_at', Carbon::today()->month)->first();
        if($check_calculate){
            $this->check_calculate = true;
        }
        return view('livewire.backend.calculate-interest.calculate-interest-component', compact('data'))->layout('layouts.backend.app');
    }
    public function showFrom()
    {
        $this->createData = true;
        $this->selectData = false;
    }
     public function resetData(){
        $this->content = '';
        $this->total = '';
        $this->deposit = '';
        $this->withraw = '';
        $this->slug = '';
        $this->type = '';
     }
     public function AutoformatDeposit(){
        $this->slug = '';
        if(!empty($this->deposit)){
            $this->slug= number_format($this->deposit);
        }
    }
    public function AutoformatWithraw(){
        $this->slug = '';
        if(!empty($this->withraw)){
            $this->slug= number_format($this->withraw);
        }
    }
    public function back()
    {
        return  redirect()->to('/calculate-interest');
    }
    public function CalculateFund(){
        $check_calculate_fund = Fund::orderBy('id', 'desc')->first();
        $start_date = CalculateFundLog::orderBy('id', 'asc')->first();
        $end_date = CalculateFundLog::orderBy('id', 'desc')->first();
        $check_count = CalculateFundLog::count();
        $check_calculate = CalculateFundLog::orderBy('id', 'desc')->first();
        if($check_calculate_fund){
            if($this->type ==1){
                if($check_calculate){
                        if($start_date && $end_date){
                            $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24*60*60);
                            if(intval($date) >= 1){
                                $profit = ($check_calculate->remain_total * $this->rate * intval($date) ) / 365;
                                $sum_total = $this->total + $profit;
                             }elseif($check_count == 0){
                                $profit = ($check_calculate->remain_total * $this->rate * 30 ) / 365;
                                $sum_total = $this->total + $profit;
                             }
                             else{
                                $profit = 0; 
                                $sum_total = $this->total + $profit;
                             }
                        }else{
                            $profit = ($check_calculate->remain_total * $this->rate * 30 ) / 365;
                            $sum_total = $this->total + $profit;
                        }
                        $singleData = new CalculateFundLog();
                        $singleData->content = $this->content;
                        $singleData->total = $this->total;
                        $singleData->total_rate = $profit;
                        $singleData->sum_total = $sum_total;
                        $singleData->remain_total = $sum_total;
                        $singleData->user_id = auth()->user()->id;
                        $singleData->save();
                        $this->resetData();
                        session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
                        return  redirect()->to('/calculate-interest');
                }else{
                     if($start_date && $end_date){
                        $date = round((strtotime($end_date->created_at) - strtotime($end_date->created_at))) / (24*60*60);
                            if(intval($date) >= 1){
                                $profit = ($this->total * $this->rate * intval($date) ) / 365;
                                $sum_total = $this->total + $profit;
                            }else{
                                $profit = ($this->total * $this->rate * 30 ) / 365;
                                $sum_total = $this->total + $profit;                           
                            }
                    }else{
                        $profit = ($this->total * $this->rate * 30 ) / 365;
                        $sum_total = $this->total + $profit;  
                    }
                    $singleData = new CalculateFundLog();
                    $singleData->content = $this->content;
                    $singleData->total = $this->total;
                    $singleData->total_rate = $profit;
                    $singleData->sum_total = $sum_total;
                    $singleData->remain_total = $sum_total;
                    $singleData->user_id = auth()->user()->id;
                    $singleData->save();
                    $this->resetData();
                    session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
                    return  redirect()->to('/calculate-interest');
                 }
            }
            elseif($this->type ==2){
                $this->validate([
                    'deposit' => 'required|numeric|min:1000',
                    'content' => 'required'
                ],[
                    'deposit.required' => 'ຕ້ອງໃສ່ຈໍານວນເງິນຝາກກ່ອນ',
                    'deposit.min' => 'ຕ້ອງໃຫຍ່ກວ່າ 1,000',
                    'content.required' => 'ຕອ້ງໃສ່ເນື້ອໃນກ່ອນ'
                ]);
                if($check_calculate){
                    if($start_date && $end_date){
                        $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24*60*60);
                        if(intval($date) >= 1){
                            $profit = ($check_calculate->remain_total * $this->rate * intval($date) ) / 365;
                            $sum_total = $check_calculate->remain_total + $profit;
                         }else{
                            $profit = 0;
                            $sum_total = 0;
                         }
                    }else{
                        $profit = 0;
                        $sum_total = $check_calculate->sum_total + $profit;
                    }
                    $singleData = new CalculateFundLog();
                    $singleData->content = $this->content;
                    $singleData->total = $this->total;
                    $singleData->total_rate = $profit;
                    $singleData->deposit = $this->deposit;
                    $singleData->sum_total = $check_calculate->sum_total + $this->deposit + $profit;
                    $singleData->remain_total = $check_calculate->remain_total + $this->deposit + $profit;
                    $singleData->user_id = auth()->user()->id;
                    $singleData->save();
                    $this->resetData();
                    session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
                    return  redirect()->to('/calculate-interest');
                }
             }elseif($this->type ==3){
                $this->validate([
                'withraw' => 'required|numeric|min:1000',
                'content' => 'required'
                ],[
                'withraw.required' => 'ຕ້ອງໃສ່ຈໍານວນເງິນຢືມກ່ອນ',
                'withraw.min' => 'ຕ້ອງໃຫຍ່ກວ່າ 1,000',
                'content.required' => 'ຕອ້ງໃສ່ເນື້ອໃນກ່ອນ'
                ]);
                if($this->withraw <= $this->total){
                    $check_date = CalculateFundLog::orderBy('id', 'desc')->first();
                        if($check_date){
                            if($start_date && $end_date){
                                $date = round((strtotime($end_date->created_at) - strtotime($start_date->created_at))) / (24*60*60);
                                if(intval($date) >= 1){
                                    $profit = ($check_calculate->remain_total * $this->rate * intval($date) ) / 365;
                                    $sum_total = $check_calculate->remain_total + $profit;
                                 }else{
                                    $profit = 0;
                                 }
                            }else{
                                $profit = 0;
                            }
                        $singleData = new CalculateFundLog();
                        $singleData->content = $this->content;
                        $singleData->total = $check_date->total - $this->withraw;
                        $singleData->total_rate = $profit;
                        $singleData->sum_total = $check_date->sum_total - $this->withraw + $profit;
                        $singleData->withraw = $this->withraw;
                        $singleData->remain_total = $check_date->remain_total - $this->withraw + $profit;
                        $singleData->user_id = auth()->user()->id;
                        $singleData->save();
                        $this->resetData();
                        session()->flash('message', 'ທຸລະກໍາສສໍາເລັດ');
                        return  redirect()->to('/calculate-interest');
                    }else{
                        session()->flash('no_message', 'ຂໍອະໄພ ເງິນກອງທຶນບໍ່ພຽງພໍ!');
                        return;             
                    }
                }
            }
        }else{
            session()->flash('no_message', 'ຂໍອະໄພ ຍັງບໍ່ສາມາດຄິດໄລ່ດອກເບ້ຍ!');
            return;   
        }
    }
    public function delete($id){
        $singleData = CalculateFundLog::where('id', $id)->first();
        $singleData->delete();
        session()->flash('message', 'ລຶບຂໍ້ມູນສໍາເລັດແລ້ວ');
        return  redirect()->to('/calculate-interest');
    }
}