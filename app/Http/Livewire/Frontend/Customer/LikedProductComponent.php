<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\OrderItem;
use Livewire\WithPagination;
use Auth;
use DB;
use Cart;
class LikedProductComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public $search_products, $searchByCatalog;

    public function mount()
    {
        $this->searchByCatalog;
    }
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        session()->flash('success_message','ເພີ່ມເຂົ້າກະຕ່າສຳເລັດ!');
        return redirect()->route('customer.likedproduct');
    
    }
    public function render()
    {
        // $likeproducts=OrderItem::orderBy('id','desc')
        // ->where('id','like', '%'. $this->search . '%')
        // ->paginate(12);
        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $likeproducts = DB::connection('mysql2')->table('products')
        ->join('u600483925_dev.order_items', 'order_items.product_id', '=', 'products.id')
        ->join('u600483925_dev.orders', 'orders.id', '=', 'order_items.order_id')
        ->join('u600483925_dev.users', 'users.id', '=', 'orders.user_id')
        ->select('products.id','products.name','products.image','products.price','products.catalog_id')
        ->where('products.name','like', '%'. $this->search_products . '%')
        ->where('products.catalog_id','like', '%'. $this->searchByCatalog . '%')
        ->where('users.id', '=', Auth::user()->id)
        ->orderBy('products.price','desc')
        ->where('trash',0)
        //->where('image', '<>', '')
        ->paginate(12); 

        $count_likeproduct = DB::table('order_items')
        ->join('orders', 'orders.id', '=', 'order_items.order_id')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->where('users.id', '=', Auth::user()->id)
        ->get();
        return view('livewire.frontend.customer.liked-product-component',['all_catalogs'=>$all_catalogs,'count_likeproduct' =>$count_likeproduct,'likeproducts' =>$likeproducts])
        ->layout('layouts.frontend.base-frontend');
    }
}
