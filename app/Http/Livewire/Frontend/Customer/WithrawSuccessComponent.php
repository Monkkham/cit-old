<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use Auth;
class WithrawSuccessComponent extends Component
{
    public function check_success(){
        if(!Auth::user() ==10){
            return redirect()->route('home');
        }
    }
    public function render()
    {  
        $this->check_success();
        return view('livewire.frontend.customer.withraw-success-component')
        ->layout('layouts.frontend.base-frontend');
    }
    public function close_withraw(){
        session()->forget('request_money');
        return redirect()->route('customer.dashboard');
    }
}
