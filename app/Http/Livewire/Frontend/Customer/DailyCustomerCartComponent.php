<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use Cart;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Exchange;
use Illuminate\Support\Facades\Auth;
class DailyCustomerCartComponent extends Component
{
    public $amount_divid;
    public $subtotalAfterTax;
    public $tax;
    public $totalAfterTax;
    public $divid_percent;
    //Qty +1
    public function increaseQty($rowId)
    {
        $product =Cart::instance('cart')->get($rowId);
        $qty = $product->qty + 1;
        Cart::instance('cart')->update($rowId,$qty);
       $this->emitTo('frontend.cart-count-component','refreshComponent');
        // return redirect()->route('customer.dailycart');
       
    }
    //Qty -1
    public function decreaseQty($rowId)
    {
        $product =Cart::instance('cart')->get($rowId);
        $qty = $product->qty - 1;
        Cart::instance('cart')->update($rowId,$qty);
        $this->emitTo('frontend.cart-count-component','refreshComponent');
        //  return redirect()->route('customer.dailycart');
    }
    //Remove product from list cart
    public function destroy($rowId)
    {
       Cart::instance('cart')->remove($rowId);
        session()->flash('success_message','ລຶບລາຍການສຳເລັດ!');
       $this->emitTo('frontend.cart-count-component','refreshComponent');
        // return redirect()->route('customer.dailycart');
    }
    public function deleteallcart(){
        Cart::instance('cart')->destroy();
       $this->emitTo('frontend.cart-count-component','refreshComponent');
        // return redirect()->route('customer.dailycart');
    }
    public function checkout(){
        if(Auth::check()){
            return redirect()->route('checkout');
        }else{
            return redirect()->route('customer_sign_in');
        }
    }
    public function mount(){
        $check_user=User::where('id', Auth::user()->id)->first();
        if(!$check_user->divid_id){
            $this->divid_percent=0;
        }else{
            $this->divid_percent=$check_user->divid->percent;
        }

    }
    public function setAmountForCheckout(){
            $this->amount_divid = (intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) * $this->divid_percent)/100;
            $this->tax = (intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) * config('cart.tax'))/100;
            $this->subtotalAfterTax = intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) + $this->tax;
            $this->totalAfterTax = $this->subtotalAfterTax + $this->totalAfterTax;
        if(!Cart::instance('cart')->count() > 0){
            session()->forget('checkout');
            return;
        }
            session()->put('checkout', [
                'amount_divid' => $this->amount_divid,
                'subtotal' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())),
                'tax' => $this->tax,
                'total' => $this->totalAfterTax
         ]);
    }
    public function check_dailycustomer(){
        if(Auth::check() && Auth::user()->divid_id =''){
            return;
        }
    }
    public function render()
    {
        $exchange = Exchange::where('active',1)->get();
        $this->setAmountForCheckout();
        return view('livewire.frontend.customer.daily-customer-cart-component', compact('exchange'))
        ->layout('layouts.frontend.base-frontend');
    }
}
