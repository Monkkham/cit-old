<?php

namespace App\Http\Livewire\Frontend\Customer;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\HistoryBalance;
use Auth;
class HistoryWithrawaComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $historywithrawal=HistoryBalance::orderBy('id', 'desc')->paginate(10);
        $sum_historywithraw=HistoryBalance::where('user_id', Auth::user()->id)->sum('amount');
        return view('livewire.frontend.customer.history-withrawa-component', ['sum_historywithraw'=>$sum_historywithraw, 'historywithrawal' => $historywithrawal])
        ->layout('layouts.frontend.base-frontend');
    }
}
