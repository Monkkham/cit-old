<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\Order;
use App\Models\BalanceItem;
use App\Models\Balance;
use Auth;
use DB;
class OrderDivid extends Component
{
    protected $paginationTheme = 'bootstrap';
    public $balance_id;
    public $all_balance;
    public function mount(){
        $balance_total=Balance::where('user_id', Auth::user()->id)->first();
        if(!$balance_total){
             $this->all_balance=0;
        }else{
            $this->balance_id=$balance_total->id;
            $this->all_balance=$balance_total->all_balance;
        }
    }
    public function render()
    {
        $order_count=Order::where('user_id', Auth::user()->id)
        ->where('status', 'arrived')
        ->count();
        $divids = BalanceItem::where('balance_id', $this->balance_id)->orderBy('id', 'desc')
        ->groupBy('id')
        ->paginate(10);
        // $divids = DB::table('divids')
        // ->join('users', 'users.id', '=', 'divids.user_id')
        // ->join('orders', 'orders.user_id', '=', 'users.id')
        // ->where('divids.user_id', '=', Auth::user()->id)
        // ->orderBy('divids.id', 'desc')
        // ->select('orders.id','orders.total','divids.name')
        // ->paginate(10);
        return view('livewire.frontend.customer.order-divid', ['divids' =>$divids, 'order_count' =>$order_count])
        ->layout('layouts.frontend.base-frontend');
    }
}
