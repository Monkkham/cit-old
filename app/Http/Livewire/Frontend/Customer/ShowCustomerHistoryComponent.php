<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Shipping;
use App\Models\Exchange;
use Cart;


class ShowCustomerHistoryComponent extends Component
{
    public $order_id;
    public $order_slug;
    public $search;
    public $discount;
    public $total, $firstname, $lastname, $phone, $total_THB,$total_USA, $address, $home_no, $vill_id, $dis_id, $pro_id, $created_at;
    public function mount($order_slug){
        $orders=Order::where('id', $this->order_slug)->first();
        $this->order_id=$orders->id;
        $this->discount=$orders->discount;
        $this->total=$orders->subtotal;
        $this->firstname = $orders->firstname;
        $this->lastname = $orders->lastname;
        $this->phone = $orders->phone;
        $this->address = $orders->address;
        $this->home_no = $orders->home_no;
        $this->vill_id = $orders->vill_id;
        $this->dis_id = $orders->dis_id;
        $this->pro_id = $orders->pro_id;

        $delivery = Transaction::where('order_id', $this->order_slug)->first();
        if($delivery){
            if(!empty($delivery->user->name)){
           $this->d_name = $delivery->user->name;
            }
            if(!empty($delivery->user->phone)){
           $this->d_phone = $delivery->user->phone;
            }
        }
        $this->created_at = $orders->created_at;
    }

    public function render()
    {
        $showcustomerhistory=OrderItem::where('order_id', $this->order_id)->orderBy('id','desc')
        ->where('id','like', '%'. $this->search . '%')
        ->paginate(10);

        $orders=Order::where('id', $this->order_id)->first();
        $shipping_address = Shipping::where('order_id', $this->order_id)->first();
        $exchange = Exchange::where('active',1)->get();
        return view('livewire.frontend.customer.show-customer-history-component', compact('showcustomerhistory','shipping_address','orders','exchange'))
        ->layout('layouts.frontend.base-frontend');
    }
}
