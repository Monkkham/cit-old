<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\Balance;
use Auth;
use Carbon\Carbon;
use App\Models\RequestWithraw;
class WithrawDividComponent extends Component
{
    public $request_money;
    public $total_balance;
    public $date_withraw;
    public $slug;
    public $check_status;
    public function mount(){
        $this->total_balance=Balance::where('user_id', Auth::user()->id)->sum('balance');
    }
    public function Autoformat(){
        if($this->request_money < 1){
             $this->slug =0;
        }else{
            $this->slug= number_format($this->request_money);
        }
    }
    protected $rules = [
        'request_money' =>'required|numeric',
    ];
    public function request_withraw(){
        $this->validate();
        if($this->total_balance == 0 || $this->request_money >  $this->total_balance){
            // session()->flash('balance_no_enough');
            $this->check_status =1;
        }else{
            $this->date_withraw =Carbon::now();
            $request_withraw= new RequestWithraw();
            $request_withraw->request_money=$this->request_money;
            $request_withraw->user_id=Auth::user()->id;
            $request_withraw->save();
            session()->put('request_money', [
                'money' => $this->request_money,
                'date' => $this->date_withraw->toDateTimeString()
            ]);
            $this->request_money='';
            return redirect()->route('customer.withdrawsuccess');
        } 
    }
    public function render()
    {
        $divid_total=Balance::where('user_id', Auth::user()->id)->sum('balance');
        return view('livewire.frontend.customer.withraw-divid-component', ['divid_total' => $divid_total])
        ->layout('layouts.frontend.base-frontend');
    }
}
