<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;
use DB;
use Cart;
use Auth;
class DailyCustomerShopComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $percent;
    public $search_products, $searchByCatalog;

    public function mount()
    {
        $this->searchByCatalog;
        $check_percent=User::where('id',Auth::user()->id)->first();
        if($check_percent->divid_id){
            $this->percent=$check_percent->divid->percent;
        }
    }
    public function Log_detail(){
        session()->put('check_detail',[
            'log_detail' =>1
        ]);
    }
    public function render()
    {
        $this->Log_detail();
        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price','catalog_id','min_reserve')
                    ->where('name','like', '%'. $this->search_products . '%')
                    ->where('catalog_id','like', '%'. $this->searchByCatalog . '%')
                    ->orderBy('price','desc')
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    ->paginate(20);
        return view('livewire.frontend.customer.daily-customer-shop-component',compact('all_catalogs','products'))
        ->layout('layouts.frontend.base-frontend');

    }
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        if(Cart::instance('cart')->count() <= 1){
            return redirect()->route('customer.dailycustomershop');
        }else{
            $this->emitTo('frontend.cart-count-component','refreshComponent');
            $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
         
        }
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມເຂົ້າກະຕ່າສໍາເລັດແລ້ວ!']);
        // if(Auth::user()->role_id ==10){
        //     session()->put('check_customer',[
        //         'log_id' => 1
        //     ]);
        // }
        // session()->flash('success_message','ເພີ່ມເຂົ້າກະຕ່າສຳເລັດ!');
        // return redirect()->route('customer.dailycart');

    }
    public function addToWishlist($product_id,$product_name,$product_price){
        Cart::instance('wishlist')->add($product_id,$product_name,1,$product_price)->associate('App\Models\Product');
        return redirect()->route('customer.dailycustomershop');
   }
   public function removeWishList($product_id){
       foreach(Cart::instance('wishlist')->content() as $witem){
        if($witem->id == $product_id){
            Cart::instance('wishlist')->remove($witem->rowId);
            return redirect()->route('customer.dailycustomershop');
        }
      }
   }
}
