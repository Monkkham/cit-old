<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\Order;
use App\Models\Transaction;
use Auth;
use DB;
use Livewire\WithPagination;
class CustomerHistoryComponent extends Component
{
    public $search;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $search= $this->search;
        // $customerhistory=Order::where('user_id', Auth::user()->id)
        // ->where('status', 'arrived')
        // ->where('id','like', '%'. $this->search . '%')
        // ->orderBy('id','desc')
        // ->paginate(10);
        $customerhistory=DB::table('orders as o')->join('transactions as t', 't.order_id','=','o.id')
        ->where('o.user_id', Auth::user()->id)
        ->where('o.status', 'arrived')
        ->where('o.status', 'arrived')->where(function ($q) use ($search){
            $q->where('o.id','like', '%'. $search . '%');
        })
        ->orderBy('o.id','desc')
        ->select('o.*','t.mode as mode', 't.check_payment as check_payment')
        ->paginate(10);
        $order_total=Order::where('user_id', Auth::user()->id)
        ->where('status', 'arrived')->sum('total');
        return view('livewire.frontend.customer.customer-history-component',['customerhistory' => $customerhistory,'order_total' => $order_total])->layout('layouts.frontend.base-frontend');
    }
}
