<?php

namespace App\Http\Livewire\Frontend\Customer;

use Livewire\Component;
use App\Models\BalanceItem;
use App\Models\Order;
use App\Models\Balance;
use App\Models\OrderItem;
use App\Models\RequestWithraw;
use Auth;
use DB;
class DashboardComponent extends Component
{
    public $sum_balance;
    public $balance;
    public $withrawal_balance;
    public $sum_requestwithraw;
    public function render()
    {
        $balance_total=Balance::where('user_id', Auth::user()->id)->first();
        if(!$balance_total){
          $this->sum_balance=0;
          $this->balance=0;
          $this->withrawal_balance=0;
        }else{
            $balances=Balance::where('user_id', Auth::user()->id)
            ->first();
            $this->sum_balance=$balances->all_balance;
            $this->balance=$balances->balance;
            $this->withrawal_balance=$balances->withraw_balance;
            $sum_amountrequest=RequestWithraw::where('user_id', Auth::user()->id)->sum('request_money');
            $this->sum_requestwithraw=$sum_amountrequest;

        }
        $sum_ordertotal=Order::where('user_id', Auth::user()->id)
        ->where('status', 'arrived')
        ->sum('total');
        $count_totalorder=Order::where('user_id', Auth::user()->id)
        ->where('status', 'arrived')
        ->count();
        // $check_order=Order::where('user_id', Auth::user()->id)->first();
        // $count_likeproduct=OrderItem::where('order_id', $check_order->id)->get();
        $count_likeproduct = DB::table('order_items')
        ->join('orders', 'orders.id', '=', 'order_items.order_id')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->where('users.id', '=', Auth::user()->id)
        ->get();
        return view('livewire.frontend.customer.dashboard-component',['count_likeproduct'=>$count_likeproduct, 'sum_ordertotal' => $sum_ordertotal, 'count_totalorder' => $count_totalorder])
        ->layout('layouts.frontend.base-frontend');
    }
}
