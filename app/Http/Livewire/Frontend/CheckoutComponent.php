<?php
namespace App\Http\Livewire\Frontend;
use Livewire\Component;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use App\Models\Transaction;
use App\Models\Village;
use App\Models\District;
use App\Models\Province;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Branch;
use App\Models\Divid;
use App\Models\User;
use App\Models\Exchange;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Cart;
use DB;
use Image;
use Kreait\Firebase\Factory;
class CheckoutComponent extends Component
{
    use WithFileUploads;
    public $ship_to_different;
    public $firstname;
    public $lastname;
    public $phone;
    public $email;
    public $address;
    public $home_no;
    public $vill_id;
    public $dis_id;
    public $pro_id;
    public $status;
    public $subtotal;
    public $discount;
    public $tax;
    public $total;
    public $paymentmode;
    public $image; 
    public $s_firstname;
    public $s_lastname;
    public $s_phone;
    public $s_email;
    public $s_address;
    public $s_home_no;
    public $s_vill_id;
    public $s_dis_id;
    public $s_pro_id;
    public $token="";
    public $thankyou,$shipping_id = [];
    public function updated($fields){
        $this->validateOnly($fields,[
            'firstname' => 'required',
            'phone' => 'required|numeric|min:8',
            'email' => 'required|email',
            'paymentmode' => 'required',
             'vill_id' => 'required',
            'dis_id' => 'required',
            'pro_id' => 'required',
            // 'image' => 'required|mimes:jpg,png,jpeg',
        ]);
        if($this->ship_to_different){
            $this->validateOnly($fields,[
              'image' => 'required|mimes:jpg,png,jpeg',
            ]);
        }
        if($this->ship_to_different){
            $this->validateOnly($fields,[
                's_firstname' => 'required',
                's_lastname' => 'required',
                's_phone' => 'required|numeric|min:8',
                's_email' => 'required|email',
                's_vill_id' => 'required',
                's_dis_id' => 'required',
                's_pro_id' => 'required',
            ]);
        }
    }
    public function PlaceOrder(){
        $this->validate([
            'firstname' => 'required',
            'phone' => 'required|numeric|min:8',
            'email' => 'required|email',
            'paymentmode' => 'required',
             'vill_id' => 'required',
            'dis_id' => 'required',
            'pro_id' => 'required',
            // 'image' => 'required|mimes:jpg,png,jpeg',
        ],[
            'firstname.required' => 'ໃສ່ນາມສະກຸນກ່ອນ',
            'phone.required' => 'ໃສ່ເບີໂທກ່ອນ',
            'email.required' => 'ໃສ່ອີເມວກ່ອນ',
            'paymentmode.required' => 'ເລືອກຮູບແບບການຊໍາລະເງິນກ່ອນ',
            'vill_id.required' => 'ເລືອກບ້ານກ່ອນ',
            'dis_id.required' => 'ເລືອກເມືອງກ່ອນ',
            'pro_id.required' => 'ເລືອກແຂວງກ່ອນ',
        ]);
        if($this->ship_to_different){
            $this->validate([
                's_firstname' => 'required',
                's_lastname' => 'required',
                's_phone' => 'required|numeric|min:8',
                's_email' => 'required|email',
                's_vill_id' => 'required',
                's_dis_id' => 'required',
                's_pro_id' => 'required',
            ],[
                's_firstname.required' => 'ໃສ່ຊື່ຜູ້ຮັບກ່ອນ',
                's_lastname.required' => 'ໃສ່ນາມສະກຸນຜູ້ຮັບກ່ອນ',
                's_phone.required' => 'ໃສ່ເບີໂທຜູ້ຮັບກ່ອນ',
                's_email.required' => 'ໃສ່ອີເມວຜູ້ຮັບກ່ອນ',
                's_vill_id.required' => 'ເລືອກບ້ານຜູ້ຮັບກ່ອນ',
                's_dis_id.required' => 'ເລືອກເມືອງຜູ້ຮັບກ່ອນ',
                's_pro_id.required' => 'ເລືອກແຂວງຜູ້ຮັບກ່ອນ',
            ]);
            $check_shipping = Shipping::where('user_id', auth()->user()->id)->where('phone', $this->s_phone)->first();
            if(!$check_shipping){
                $shipping = new Shipping();
                $shipping->order_id = 0;
                $shipping->firstname = $this->s_firstname;
                $shipping->lastname = $this->s_lastname;
                $shipping->phone = $this->s_phone;
                $shipping->email = $this->s_email;
                $shipping->address = $this->s_address;
                $shipping->home_no = $this->s_home_no;
                $shipping->vill_id = $this->s_vill_id;
                $shipping->dis_id = $this->s_dis_id;
                $shipping->pro_id = $this->s_pro_id;
                $shipping->user_id = auth()->user()->id;
                $shipping->save();
            }
        }
            if(empty(auth()->user()->vill_id) && empty(auth()->user()->dis_id) && empty(auth()->user()->pro_id)){
                $user = User::where('id', auth()->user()->id)->first();
                if($user){
                    $user->vill_id = $this->vill_id;
                    $user->dis_id = $this->dis_id;
                    $user->pro_id =  $this->pro_id;
                    $user->update();
                }
            }
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->subtotal = session()->get('checkout')['subtotal'];
            $order->discount = session()->get('checkout')['discount'];
            $order->tax = session()->get('checkout')['tax'];
            $order->total = session()->get('checkout')['total'];
            $order->firstname = $this->firstname;
            $order->lastname = $this->lastname;
            $order->phone = $this->phone;
            $order->email = $this->email;
            $order->address = $this->address;
            $order->home_no = $this->home_no;
            $order->vill_id = $this->vill_id;
            $order->dis_id = $this->dis_id;
            $order->pro_id = $this->pro_id;
            $order->status = 'ordered';
            if($this->paymentmode == 'banktransfer'){
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
            ]);
            
            // $imageName=Carbon::now()->timestamp. '.' .$this->image->extension();
            // $this->image->storeAs('admin/dist/img',$imageName);
            //$order->image = $imageName;
            $image = $this->image;
            $filename = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(333, 720);
            $image_resize->save(public_path('admin/dist/img/' . $filename));
            $order->image = $filename;
            }else{
                $order->image = '0';
            }
            $order->is_shipping_different = $this->ship_to_different ? 1:0;
            if(!empty( $this->shipping_id)){
            $order->shipping_id = $this->shipping_id;
            }elseif(!empty($shipping->id)){
                $order->shipping_id = $shipping->id;
            }
            $order->save();
        foreach(Cart::instance('cart')->content() as $item){
            $orderItem = new OrderItem();
            $orderItem->product_id = $item->id;
            $orderItem->order_id = $order->id;
            $orderItem->price = $item->price;
            $orderItem->quantity = $item->qty;
            $orderItem->save();
        }
        // if($this->ship_to_different){
            // $this->validate([
            //     's_firstname' => 'required',
            //     's_lastname' => 'required',
            //     's_phone' => 'required|numeric|min:8',
            //     's_email' => 'required|email',
            //     's_vill_id' => 'required',
            //     's_dis_id' => 'required',
            //     's_pro_id' => 'required',
            // ],[
            //     's_firstname.required' => 'ໃສ່ຊື່ຜູ້ຮັບກ່ອນ',
            //     's_lastname.required' => 'ໃສ່ນາມສະກຸນຜູ້ຮັບກ່ອນ',
            //     's_phone.required' => 'ໃສ່ເບີໂທຜູ້ຮັບກ່ອນ',
            //     's_email.required' => 'ໃສ່ອີເມວຜູ້ຮັບກ່ອນ',
            //     's_vill_id.required' => 'ເລືອກບ້ານຜູ້ຮັບກ່ອນ',
            //     's_dis_id.required' => 'ເລືອກເມືອງຜູ້ຮັບກ່ອນ',
            //     's_pro_id.required' => 'ເລືອກແຂວງຜູ້ຮັບກ່ອນ',
            // ]);
            // $shipping = new Shipping();
            // $shipping->order_id = $order->id;
            // $shipping->firstname = $this->s_firstname;
            // $shipping->lastname = $this->s_lastname;
            // $shipping->phone = $this->s_phone;
            // $shipping->email = $this->s_email;
            // $shipping->address = $this->s_address;
            // $shipping->home_no = $this->s_home_no;
            // $shipping->vill_id = $this->s_vill_id;
            // $shipping->dis_id = $this->s_dis_id;
            // $shipping->pro_id = $this->s_pro_id;
            // $shipping->save();
        // }  
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->rider_id= 0;
            $transaction->order_id = $order->id;
            $transaction->amount = 0;
            if($this->paymentmode == 'cod'){
                $transaction->mode = 'cod';
            }else{
                $transaction->mode = 'onepay';
            }
            $transaction->check_payment='0';
            $transaction->status = 'pending'; 
            $transaction->note = '';
            $transaction->save();
       Cart::instance('cart')->destroy();
       session()->forget('checkout');
       session()->forget('check_detail');
       $this->thankyou=1;
    }
    public function verifyForCheckout(){
        if(!Auth::check()){
            return redirect()->route('customer_sign_in');
        }
        else if($this->thankyou){
            return redirect()->route('thankyou');
        }
        else if(!session()->get('checkout')){
            return redirect()->route('cart');
        }
    }
     public function mount(){
        error_log("----------------------hello--------------------");
        $this->paymentmode = 'banktransfer';
        if(Auth::check()){
            $this->firstname=Auth::user()->name;
            $this->phone=Auth::user()->phone;
            $this->email=Auth::user()->email;
            $this->vill_id = Auth::user()->vill_id;
            $this->dis_id = Auth::user()->dis_id;
            $this->pro_id = Auth::user()->pro_id;
            $this->home_no = Auth::user()->home_no;
            $this->address=Auth::user()->address;
            // $this->address=Auth::user()->address;

            $this->saveToken($this->token);
        }
     }
   
 
     private function saveToken(string $token)
     {
        error_log("----------------------yeah it come here--------------------");
         $user = auth()->user();
         if ($user) {
             $user->fcm_token = $token;
             $user->update();
         }
     }
     public function Daily_customer_placeorder(){
        $this->validate([
            'paymentmode' => 'required',
        ]);
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->subtotal = session()->get('checkout')['subtotal'];
            if(!empty(session()->get('checkout')['amount_divid'])){
                $order->amount_divid = session()->get('checkout')['amount_divid'];
            }
            $order->tax = session()->get('checkout')['tax'];
            $order->total = session()->get('checkout')['total'];
            $order->firstname = $this->firstname;
            $order->lastname = $this->lastname;
            $order->phone = $this->phone;
            $order->email = $this->email;
            $order->address = $this->address;
            $order->home_no = $this->home_no;
            $order->vill_id = $this->vill_id;
            $order->dis_id = $this->dis_id;
            $order->pro_id = $this->pro_id;
            $order->status = 'ordered';
            if($this->paymentmode == 'banktransfer'){
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
            $imageName=Carbon::now()->timestamp. '.' .$this->image->extension();
            $this->image->storeAs('admin/dist/img',$imageName);
            $order->image = $imageName;
             // $newimage = time().$this->image->getClientOriginalName();
            // $this->image->move('admin/dist/img/', $newimage);
            }else{
                $order->image = '0';
            }
            $order->is_shipping_different = $this->ship_to_different ? 1:0;
            $order->save();
        foreach(Cart::instance('cart')->content() as $item){
            $orderItem = new OrderItem();
            $orderItem->product_id = $item->id;
            $orderItem->order_id = $order->id;
            $orderItem->price = $item->price;
            $orderItem->quantity = $item->qty;
            $orderItem->save();
        }
        $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->rider_id= 0;
            $transaction->order_id = $order->id;
            $transaction->amount = 0;
            if($this->paymentmode == 'cod'){
                $transaction->mode = 'cod';
            }elseif($this->paymentmode == 'banktransfer'){
                $transaction->mode = 'onepay';
            }else{
                $transaction->mode ='debt';
            }
            $transaction->check_payment='0';
            $transaction->status = 'pending'; 
            $transaction->note = '';
            $transaction->save();
            Cart::instance('cart')->destroy();
            session()->forget('checkout');
            session()->forget('check_customer');
            session()->forget('check_detail');
            $this->thankyou=1;   
     }
    public function render() 
    {
        if($this->ship_to_different){
            $this->shipping_id = [];
        }elseif(!empty($this->shipping_id)){
            $this->ship_to_different = '';
        }
        $provinces=Province::orderBy('id','desc')->where('del',1)->get();
        $districts=District::where('pro_id', $this->pro_id)->orderBy('id','desc')->where('del',1)->get();
        $villages=Village::where('dis_id', $this->dis_id)->orderBy('id','desc')->where('del',1)->get();
        $s_districts=District::where('pro_id', $this->s_pro_id)->orderBy('id','desc')->where('del',1)->get();
        $s_villages=Village::where('dis_id', $this->s_dis_id)->orderBy('id','desc')->where('del',1)->get();
        $branch=Branch::all();
        $this->verifyForCheckout();

        $exchange = Exchange::where('active',1)->get();
        $shipping = Shipping::where('user_id', auth()->user()->id)->get();
        return view('livewire.frontend.checkout-component',['provinces'=>$provinces,'districts'=>$districts,'villages'=>$villages,'branches'=>$branch, 'exchange'=>$exchange, 's_districts' => $s_districts, 's_villages' => $s_villages, 'shipping' => $shipping])
        ->layout('layouts.frontend.base-frontend');
    }
}