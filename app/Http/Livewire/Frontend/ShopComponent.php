<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Livewire\WithPagination;
use DB;
use Cart;
use Auth;
class ShopComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public $search_products, $searchByCatalog;

    public function mount()
    {
        $this->searchByCatalog;
    }
    public function render()
    {
        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','catalog_id','min_reserve')
                    ->where('name','like', '%'. $this->search_products . '%')
                    ->where('catalog_id','like', '%'. $this->searchByCatalog . '%')
                    //->orderBy('id','desc')
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    ->inRandomOrder()
                    ->paginate(32);

        return view('livewire.frontend.shop-component', compact('all_catalogs','products'))
        ->layout('layouts.frontend.base-frontend');
    }

    //Search by Customer type
    public function searchByCatalog($id)
    {
        //dd($id);
        $singleData = DB::connection('mysql2')->table('catalogs')->select('id','name')->where('id', $id)->first();
        $this->searchByCatalog = $singleData->id;
        //dd($this->search_by_cat);
    }

    //Add to cart
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        if(Cart::instance('cart')->count() <= 1){
            return redirect()->route('shop');
        }else{
            $this->emitTo('frontend.cart-count-component','refreshComponent');
            $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
         
        }
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມເຂົ້າກະຕ່າສໍາເລັດແລ້ວ!']);
        // session()->flash('success_message','ເພີ່ມເຂົ້າກະຕ່າສຳເລັດ!');
        // return redirect()->route('cart');
        // if(Auth::check()){
        //     if(Auth::user()->role_id ==10){
        //         return redirect()->route('customer.dailycart');
        //     }else{
        //         return redirect()->route('cart');
        //     }
        // }
        // else{
        //     return redirect()->route('cart');
        // }
    }
    //add to wishlist
    public function addToWishlist($product_id,$product_name,$product_price){
        Cart::instance('wishlist')->add($product_id,$product_name,1,$product_price)->associate('App\Models\Product');
        return redirect()->route('shop');
   }
   public function removeWishList($product_id){
    foreach(Cart::instance('wishlist')->content() as $witem){
        if($witem->id == $product_id){
            Cart::instance('wishlist')->remove($witem->rowId);
            return redirect()->route('shop');
        }
    }
}
}
