<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class CartCountMobileComponent extends Component
{
    protected $listeners=['refreshComponent'=>'$refresh'];
    public function render()
    {
        return view('livewire.frontend.cart-count-mobile-component');
    }
}
