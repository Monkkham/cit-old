<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class ThankyouComponent extends Component
{
    public function render()
    {
        return view('livewire.frontend.thankyou-component')->layout('layouts.frontend.base-frontend');
    }
}
