<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use DB;
use Cart;
use Auth;

class ProductDetailComponent extends Component
{
    public $hiddenId;

    public function mount($id)
    {
        $product = DB::connection('mysql2')->table('products')->select('id')->where('id', $id)->first();
        $this->hiddenId = $product->id;
    }

    public function render()
    {
        $product = DB::connection('mysql2')->table('products')->select('id','image','name','price_online','price','des','long_des','min_reserve')->where('id', $this->hiddenId)->get();
        $products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price','price_online','min_reserve')
                    ->where('trash',0)->where('image', '<>', '')
                    ->inRandomOrder()->take(12)->get();

        return view('livewire.frontend.product-detail-component', compact('product','products'))
        ->layout('layouts.frontend.base-frontend');
    }

    //Add to cart
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        session()->flash('success_message','ເພີ່ມເຂົ້າກະຕ່າສຳເລັດ');
        if(Auth::check()){
            if(Auth::user()->role_id ==10 && session()->get('check_detail')['log_detail'] ==1){
                session()->put('check_customer',[
                    'log_id' => 1
                ]);
                return redirect()->route('customer.dailycart');
            }else{
                return redirect()->route('cart');
            }
        }else{
            return redirect()->route('cart');
        }
    }
}
