<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Cart;
use Auth;
class WishlistComponent extends Component
{
    public function removeWishList($product_id){
        foreach(Cart::instance('wishlist')->content() as $witem){
            if($witem->id == $product_id){
                Cart::instance('wishlist')->remove($witem->rowId);
               $this->emitTo('frontend.wishlist-count-component','refreshComponent');
            }
        }
    }
    public function deleteallwishlist(){
        Cart::instance('wishlist')->destroy();
    }
    public function moveProductFormwishListToCart($rowId){
        $item=Cart::instance('wishlist')->get($rowId);
        Cart::instance('wishlist')->remove($rowId);
        Cart::instance('cart')->add($item->id,$item->name,1,$item->price)->associate('App\Models\Product'); 
       $this->emitTo('frontend.wishlist-count-component','refreshComponent');
       }
    public function checkwishlist(){
        if(!Cart::instance('wishlist')->count()){
         if(Auth::check()){
            if(Auth::user()->role_id ==10){
                return redirect()->route('customer.dailycustomershop');
            }else{
                return redirect()->route('shop');
            }
        }else{
            return redirect()->route('shop');
        }
       }
    }
    public function render()
    {
        $this->checkwishlist();
        return view('livewire.frontend.wishlist-component')
        ->layout('layouts.frontend.base-frontend');
    }
}
