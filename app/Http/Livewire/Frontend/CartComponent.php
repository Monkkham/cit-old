<?php
namespace App\Http\Livewire\Frontend;
use Livewire\Component;
use App\Models\Coupon;
use App\Models\Exchange;
use Cart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class CartComponent extends Component
{
    public $couponCode;
    public $discount;
    public $subtotalAfterDiscount;
    public $taxAfterDiscount;
    public $totalAfterDiscount;
    public $datenow;
    public $cart_value;
    //Qty +1
    public function increaseQty($rowId)
    {
        $product =Cart::instance('cart')->get($rowId);
        $qty = $product->qty + 1;
        Cart::instance('cart')->update($rowId,$qty);
        $this->emitTo('frontend.cart-count-component','refreshComponent');
         $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
        // return redirect()->route('cart');
       
    }
    //Qty -1
    public function decreaseQty($rowId)
    {
        $product =Cart::instance('cart')->get($rowId);
        $qty = $product->qty - 1;
        Cart::instance('cart')->update($rowId,$qty);
         $this->emitTo('frontend.cart-count-component','refreshComponent');
          $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
        //  return redirect()->route('cart');
    }
    //Remove product from list cart
    public function destroy($rowId)
    {
        $cart = Cart::instance('cart')->content()->where('rowId',$rowId);
        if($cart->isNotEmpty()){
            Cart::instance('cart')->remove($rowId);
            $this->emitTo('frontend.cart-count-component','refreshComponent');
             $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
        }
    //    Cart::instance('cart')->remove($rowId);
    //     session()->flash('success_message','ລຶບລາຍການສຳເລັດ!');
        // return redirect()->route('cart');
    }
    public function deleteallcart(){
        Cart::instance('cart')->destroy();
        $this->emitTo('frontend.cart-count-component','refreshComponent');
         $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
        // return redirect()->route('cart');
    }
    public function updated($fields)
    {
      $this->validateOnly($fields,[
        'couponCode' =>'required',
      ]);
    }
    public function applycouponCode(){
        $this->validate([ 
            'couponCode' =>'required',
       ]);
        $advicecoupon=Coupon::where('code',$this->couponCode)->first();
        if(!$advicecoupon){
            session()->flash('no_coupon');
        }
        elseif(!Coupon::where('code',$this->couponCode)->where('expiry_date','>=',Carbon::today())->first()){
            session()->flash('coupon_expirydate');
        }
        else{
          $coupon = Coupon::where('code',$this->couponCode)->where('expiry_date','>=',Carbon::today())->where('cart_value','<=',intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())))->first();
          $this->cart_value=number_format($advicecoupon->cart_value);
          if(!$coupon){
                    session()->flash('coupon_buymore');
                    return;
                } 
                session()->put('coupon',[
                    'code' => $coupon->code,
                    'type' => $coupon->type,
                    'value' => $coupon->value,
                    'cart_value' => $coupon->cart_value,
                ]);
                $this->couponCode='';
       }
    }
    public function calculateDiscount(){
        if(session()->has('coupon')){
            $this->discount = (intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) * session()->get('coupon')['value'])/100;
            $this->subtotalAfterDiscount = intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) - $this->discount;
            $this->taxAfterDiscount = ($this->subtotalAfterDiscount * config('cart.tax'))/100;
            $this->totalAfterDiscount = $this->subtotalAfterDiscount + $this->totalAfterDiscount;
            
        }
    }
    public function removeCoupon(){
        session()->forget('coupon');
        $this->subtotalAfterDiscount = 0;
        $this->taxAfterDiscount = 0;
        $this->totalAfterDiscount = 0;
    }
    public function checkout(){
        if(Auth::check()){
            return redirect()->route('checkout');
        }else{
            return redirect()->route('customer_sign_in');
        }
    }
    public function setAmountForCheckout(){
        if(!Cart::instance('cart')->count() > 0){
            session()->forget('checkout');
            return;
        }
        if(session()->has('coupon'))
        {
            session()->put('checkout', [
                'discount' => $this->discount,
                'subtotal' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())),
                'tax' => $this->taxAfterDiscount,
                'total' => $this->totalAfterDiscount,
                'amount_divid' => 0,
            ]);
        }
        else
        {
            session()->put('checkout', [
                'discount' => 0,
                'subtotal' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())),
                'tax' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->tax())),
                'total' => intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->total())),
                'amount_divid' => 0,

            ]);
        }
    }
    public function render()
    {
        $exchange = Exchange::where('active',1)->get();
        if(session()->has('coupon')){
            if(intval(preg_replace('/[^\d.]/', '',Cart::instance('cart')->subtotal())) < session()->get('coupon')['cart_value']){
                session()->forget('coupon');
            }else{
                $this->calculateDiscount();
            }
        }
        $this->setAmountForCheckout();
        return view('livewire.frontend.cart-component', compact('exchange'))
        ->layout('layouts.frontend.base-frontend');
    }
}
