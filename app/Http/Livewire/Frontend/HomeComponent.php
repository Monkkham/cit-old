<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Slide;
use App\Models\Service;
use App\Models\Customer;
use DB;
use Cart;
use Auth;

class HomeComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search_products, $searchByCatalog;

    public function mount()
    {
        $this->searchByCatalog;
    }
    public function render()
    {
        $sliders = Slide::orderBy('id','desc')->where('del',1)->get();
        $services = Service::where('status',1)->get();

        $all_products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price','price_online','catalog_id','min_reserve')
                    ->orderBy('id','desc')
                    //->where('catalog_id',1)
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    //->inRandomOrder()
                    //->take(4)
                    ->get();

        $new_cctv_hdcvi_products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','catalog_id','min_reserve')
                    ->orderBy('id','desc')
                    ->where('catalog_id',1)
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    //->inRandomOrder()
                    //->take(4)
                    ->get();
        $new_cctv_ipc_products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','catalog_id','min_reserve')
                    ->orderBy('id','desc')
                    ->where('catalog_id',2)
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    //->inRandomOrder()
                    //->take(4)
                    ->get();
        $new_cctv_kits_products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','catalog_id','min_reserve')
                    ->orderBy('id','desc')
                    ->where('catalog_id',8)
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    //->inRandomOrder()
                    //->take(4)
                    ->get();

        $new_pos_products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','catalog_id','min_reserve')
                    ->orderBy('id','desc')
                    ->where('catalog_id',3)
                    ->where('trash',0)
                    //->where('image', '<>', '')
                    //->inRandomOrder()
                    //->take(4)
                    ->get();

        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price','price_online','catalog_id','min_reserve')
                    ->where('name','like', '%'. $this->search_products . '%')
                    ->where('catalog_id','like', '%'. $this->searchByCatalog . '%')
                    //->orderBy('price_online','desc')
                    ->where('trash',0)->inRandomOrder()
                    //->where('image', '<>', '')
                    ->paginate(32);

        /*
        $products = DB::connection('mysql2')->table('products')
                    ->select('id','name','image','price_online','min_reserve')
                    ->where('trash',0)->where('image', '<>', '')
                    ->inRandomOrder()->take(32)->get();
                    */
        $gov_customers = Customer::select('image')->orderBy('id','desc')->where('customer_type_id',1)->get();
        $original_customers = Customer::select('image')->orderBy('id','desc')->where('customer_type_id',2)->get();

        return view('livewire.frontend.home-component', compact('sliders','services','all_products','new_cctv_hdcvi_products','new_cctv_ipc_products','new_cctv_kits_products','new_pos_products','all_catalogs','products','gov_customers','original_customers'))
        ->layout('layouts.frontend.base-frontend');
    }

    //Search by Customer type
    public function searchByCatalog($id)
    {
        //dd($id);
        $singleData = DB::connection('mysql2')->table('catalogs')->select('id','name')->where('id', $id)->first();
        $this->searchByCatalog = $singleData->id;
        //dd($this->search_by_cat);
    }

    //Add to cart
    public function addtoCart($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        if(Cart::instance('cart')->count() <= 1){
            return redirect()->route('home');
        }else{
            $this->emitTo('frontend.cart-count-component','refreshComponent');
            $this->emitTo('frontend.cart-count-mobile-component','refreshComponent');
         
        }
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມເຂົ້າກະຕ່າສໍາເລັດແລ້ວ!']);
        // session()->flash('success_message','ເພີ່ມເຂົ້າກະຕ່າສຳເລັດ!');
        // if(Auth::check()){
        //     if(Auth::user()->role_id ==10){
        //         return redirect()->route('customer.dailycart');
        //     }else{
        //         return redirect()->route('cart');
        //     }
        // }
        // else{
        //     return redirect()->route('cart');
        // }
    }
    public function addToWishlist($product_id,$product_name,$product_price){
        Cart::instance('wishlist')->add($product_id,$product_name,1,$product_price)->associate('App\Models\Product');
        return redirect()->route('home');
   }
   public function removeWishList($product_id){
    foreach(Cart::instance('wishlist')->content() as $witem){
        if($witem->id == $product_id){
            Cart::instance('wishlist')->remove($witem->rowId);
            return redirect()->route('shop');
        }
    }
}
}
