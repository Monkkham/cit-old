<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Service;

class ServiceComponent extends Component
{
    public function render()
    {
        $services = Service::where('status',1)->get();
        return view('livewire.frontend.service-component', compact('services'))
        ->layout('layouts.frontend.base-frontend');
    }
}
