<?php

namespace App\Http\Livewire\Frontend\Auth;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use DB;
use Cart;
class CustomerLoginComponent extends Component
{
    public $phone, $password;

    public function render()
    {
        return view('livewire.frontend.auth.customer-login-component')
        ->layout('layouts.frontend.base-frontend');
    }

    public function signIn()
    {
        $this->validate([
            'phone'=>'required',
            'password'=>'required'
        ],[
            'phone.required'=>'ກະລຸນາໃສ່ຊື່ຜູ້ໃຊ້ກ່ອນ!',
            'password.required'=>'ກະລຸນາໃສ່ລະຫັດຜ່ານກ່ອນ!'
        ]);
        if(Auth::attempt([
            'phone'=>$this->phone,
            'password'=>$this->password
        ]) && (!Cart::instance('cart')->count()))
        {
            if(Auth::check() && Auth::user()->role_id == 10){
                session()->put('check_detail',[
                    'log_detail' =>1
                ]);
            }
            if(Auth::user()->role_id==10 && Auth::user()->divid_id > 0){
                return redirect(route('customer.dashboard'));
            }
            elseif(Auth::user()->role_id !=10){
                return redirect(route('customer.dashboard'));
            }
            else{
                session()->flash('no_divid');
            }
        }
        elseif( Auth::attempt([
            'phone'=>$this->phone,
            'password'=>$this->password
        ])  && (Cart::instance('cart')->count() > 0)){
            if(Auth::check() && Auth::user()->role_id == 10){
                session()->put('check_detail',[
                    'log_detail' =>1
                ]);
            }
            if(Auth::user()->role_id==10 && Auth::user()->divid_id > 0){
                return redirect()->route('checkout');
            }
            elseif(Auth::user()->role_id !=10){
                return redirect()->route('checkout');
            }
            else{
                session()->flash('no_divid');
            }
        }
        else
        {
            session()->flash('message', 'ຂໍ້ມູນເຂົ້າລະບົບ ບໍ່ຖືກຕ້ອງ!ກະລຸນາລອງໃໝ່');
        }

        // if(Auth::attempt([
        //     'phone'=>$this->phone,
        //     'password'=>$this->password
        // ]) && (!Cart::instance('cart')->count()))
        // {
        //     if(Auth::user()->role_id !=1){
        //         return redirect(route('customer.dashboard'));
        //     }else{
        //         return redirect()->route('home');
        //     }
        // }
        // elseif( Auth::attempt([
        //     'phone'=>$this->phone,
        //     'password'=>$this->password
        // ])  && (Cart::instance('cart')->count() > 0 && Auth::user()->role_id==10)){
        //     return redirect()->route('customer.dailycart');
        // }elseif( Auth::attempt([
        //     'phone'=>$this->phone,
        //     'password'=>$this->password
        // ])  && (Cart::instance('cart')->count() > 0 && Auth::user()->role_id !=10)){
        //     return redirect()->route('checkout');
        // }
        // {
        //     session()->flash('message', 'ຂໍ້ມູນເຂົ້າລະບົບ ບໍ່ຖືກຕ້ອງ!ກະລຸນາລອງໃໝ່');
        // }
    }

    public function signOut()
    {
        Auth::logout();
        Cart::instance('cart')->destroy();
        session()->forget('checkout');
        session()->forget('check_customer');
        session()->forget('check_detail');
        return redirect()->route('home');
    }
}
