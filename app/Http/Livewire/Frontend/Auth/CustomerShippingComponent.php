<?php

namespace App\Http\Livewire\Frontend\Auth;

use Livewire\Component;
use App\Models\Province;
use App\Models\District;
use App\Models\Village;
use App\Models\Shipping;
class CustomerShippingComponent extends Component
{
    public $name, $phone, $email, $lastname, $hiddenId, $address,$home_no, $pro_id,$vill_id, $dis_id;
    public function render()
    {
        $data = Shipping::where('user_id', auth()->user()->id)->get();
        $provinces=Province::orderBy('id','desc')->where('del',1)->get();
        $districts=District::where('pro_id', $this->pro_id)->orderBy('id','desc')->where('del',1)->get();
        $villages=Village::where('dis_id', $this->dis_id)->orderBy('id','desc')->where('del',1)->get();
        return view('livewire.frontend.auth.customer-shipping-component', compact('data','provinces', 'districts', 'villages'))->layout('layouts.frontend.base-frontend');
    }
    public function resetField()
    {
        $this->hiddenId = '';
        $this->name = '';
        $this->lastname = '';
        $this->home_no = '';
        $this->email = '';
        $this->address = '';
        $this->pro_id = '';
        $this->dis_id = '';
        $this->vill_id = '';
        $this->phone = '';
    }
    //Validate real time
    protected $rules = [
        'name' => 'required',
        'lastname' => 'required',
        'email' => 'required|email',
        'pro_id' => 'required',
        'dis_id' => 'required',
        'vill_id' => 'required',
        'phone' => 'required|unique:shippings'
    ];
    protected $messages = [
        'name.required' => 'ໃສ່ຊື່ກ່ອນ',
        'lastname.required' => 'ໃສ່ນາມສະກຸນ',
        'email.required' => 'ໃສ່ອີເມວກ່ອນ',
        'email.email' => 'ໃສ່ອີເມວ example@gmail.com',
        'pro_id.required' => 'ເລືອກແຂວງກ່ອນ',
        'dis_id.required' => 'ເລືອກເມືອງກ່ອນ',
        'vill_id.required' => 'ເລືອກບ້ານກ່ອນ',
        'phone.required' => 'ໃສ່ເບີໂທກ່ອນ',
        'phone.unique' => 'ເບີໂທນີ້ມີໃນລະບົບແລ້ວ'
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function  Saveshipping(){
        if(!empty($this->hiddenId)){
            $this->validate(
                [
                    'name' => 'required',
                    'lastname' => 'required',
                    'email' => 'required|email',
                    'pro_id' => 'required',
                    'dis_id' => 'required',
                    'vill_id' => 'required',
                    'phone' => 'required'
                ],[
                    'name.required' => 'ໃສ່ຊື່ກ່ອນ',
                    'lastname.required' => 'ໃສ່ນາມສະກຸນ',
                    'email.required' => 'ໃສ່ອີເມວກ່ອນ',
                    'email.email' => 'ໃສ່ອີເມວ example@gmail.com',
                    'pro_id.required' => 'ເລືອກແຂວງກ່ອນ',
                    'dis_id.required' => 'ເລືອກເມືອງກ່ອນ',
                    'vill_id.required' => 'ເລືອກບ້ານກ່ອນ',
                    'phone.required' => 'ໃສ່ເບີໂທກ່ອນ',
                ]
            );
            $singleData = Shipping::find($this->hiddenId);
            $singleData->firstname = $this->name;
            $singleData->lastname = $this->lastname;
            $singleData->phone = $this->phone;
            $singleData->email = $this->email;
            $singleData->home_no = $this->home_no;
            $singleData->address = $this->address;
            $singleData->pro_id = $this->pro_id;
            $singleData->dis_id = $this->dis_id;
            $singleData->vill_id = $this->vill_id;
            $singleData->update();
            $this->resetField();
            $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສໍາເລັດແລ້ວ!']);
        }else{
            $this->validate();
            $singleData = new Shipping();
            $singleData->order_id = 0;
            $singleData->firstname = $this->name;
            $singleData->lastname = $this->lastname;
            $singleData->phone = $this->phone;
            $singleData->email = $this->email;
            $singleData->home_no = $this->home_no;
            $singleData->address = $this->address;
            $singleData->pro_id = $this->pro_id;
            $singleData->dis_id = $this->dis_id;
            $singleData->vill_id = $this->vill_id;
            $singleData->user_id = auth()->user()->id;
            $singleData->save();
            $this->resetField();
            $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສໍາເລັດແລ້ວ!']);
        }

    }
    public function showEdit($id){
        $this->resetField();
        $singleData = Shipping::find($id);
        if($singleData){
            $this->hiddenId = $singleData->id;
            $this->name = $singleData->firstname;
            $this->lastname = $singleData->lastname;
            $this->phone = $singleData->phone;
            $this->email = $singleData->email;
            $this->home_no = $singleData->home_no;
            $this->address = $singleData->address;
            $this->pro_id = $singleData->pro_id;
            $this->dis_id = $singleData->dis_id;
            $this->vill_id = $singleData->vill_id;
        }
    }
}