<?php

namespace App\Http\Livewire\Frontend\Auth;
use Auth;
use Livewire\Component;
use App\Models\User;
class CustomerSidebarComponent extends Component
{
    public $percent;
    public function mount(){
        $check_percent=User::where('id',Auth::user()->id)->first();
        if($check_percent->divid_id){
            $this->percent=$check_percent->divid->percent;
        }
    }
    public function render()
    {
        return view('livewire.frontend.auth.customer-sidebar-component');
    }
}
