<?php

namespace App\Http\Livewire\Frontend\Auth;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Province;
use App\Models\District;
use App\Models\Village;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Livewire\WithFileUploads;
use DB;

class CustomerProfileComponent extends Component
{
    use WithFileUploads;
    public $name, $phone, $email, $pro_id, $dis_id, $vill_id, $home_no,$address, $password, $confirmpassword, $image,$balance_id,$imageName,$newimage;

    public function mount()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $this->name = $user->name;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->address = $user->address;
        $this->balance_id = $user->balance_id;
        $this->newimage=$user->bcelone_image;
        $this->home_no = $user->home_no;
        $this->vill_id = $user->vill_id;
        $this->dis_id = $user->dis_id;
        $this->pro_id = $user->pro_id;
    }

    public function render()
    {
        $provinces=Province::orderBy('id','desc')->where('del',1)->get();
        $districts=District::where('pro_id', $this->pro_id)->orderBy('id','desc')->where('del',1)->get();
        $villages=Village::where('dis_id', $this->dis_id)->orderBy('id','desc')->where('del',1)->get();
        return view('livewire.frontend.auth.customer-profile-component', compact('provinces', 'districts', 'villages'))
        ->layout('layouts.frontend.base-frontend');
    }

    public function updateProfile()
    {
        $updateId = auth()->user()->id;

        if($updateId > 0)
        {
            $this->validate([
                'name'=>'required',
                'phone'=>'required|min:8|numeric|',
                'email'=>'email',
                // 'image' => 'required|mimes:jpg,png,jpeg',
            ],[
                'name.required'=>'ໃສ່ຊື່ລູກຄ້າກ່ອນ',
                'phone.required'=>'ໃສ່ເບີ້ໂທລະສັບກ່ອນ',
                'phone.min'=>'ເບີ້ໂທລະສັບຕ້ອງ 8 ຕົວ',
                'phone.numeric'=>'ເບີ້ໂທຕ້ອງແມ່ນຕົວເລກ',
                'email.email'=>'Email ບໍ່ຖືກຮູບແບບ',
            ]);
        }
        $user = User::where('id', auth()->user()->id)->first();
        if(!empty($this->image)){
            if($user->bcelone_image && $this->image != $user->bcelone_image){
                if($user->bcelone_image){
                    unlink('images/qrcode'.'/'.$user->bcelone_image);
                }
                if($user->images){
                    $images = explode(",",$user->images);
                    foreach($images  as $image){
                        unlink('images/qrcode'.'/'.$user->bcelone_image);
                    }
                    $user->delete();
                }
           } 
            $this->imageName=Carbon::now()->timestamp. '.' .$this->image->extension();
            $this->image->storeAs('images/qrcode',$this->imageName);
         }
        if(!empty($this->password))
        {
            if(!empty($this->image)){
                $user_data = [
                    'name'=> $this->name,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                    'address'=> $this->address,
                    'password'=> bcrypt($this->password),
                    'balance_id' => $this->balance_id,
                    'bcelone_image' => $this->imageName,
                    'home_no' => $this->home_no,
                    'vill_id' => $this->vill_id,
                    'dis_id' => $this->dis_id,
                    'pro_id' => $this->pro_id,
                ];
            }else{
                $user_data = [
                    'name'=> $this->name,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                    'address'=> $this->address,
                    'password'=> bcrypt($this->password),
                    'balance_id' => $this->balance_id,
                    'home_no' => $this->home_no,
                    'vill_id' => $this->vill_id,
                    'dis_id' => $this->dis_id,
                    'pro_id' => $this->pro_id,
                ];
            }
          
        }
        else
        {
            if(!empty($this->image)){
                $user_data = [
                    'name'=> $this->name,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                    'address'=> $this->address,
                    'balance_id' => $this->balance_id,
                    'bcelone_image' => $this->imageName,
                    'home_no' => $this->home_no,
                    'vill_id' => $this->vill_id,
                    'dis_id' => $this->dis_id,
                    'pro_id' => $this->pro_id,
                ];
            }else{
                $user_data = [
                    'name'=> $this->name,
                    'phone'=> $this->phone,
                    'email'=> $this->email,
                    'address'=> $this->address,
                    'balance_id' => $this->balance_id,
                    'home_no' => $this->home_no,
                    'vill_id' => $this->vill_id,
                    'dis_id' => $this->dis_id,
                    'pro_id' => $this->pro_id,
                ];
            }
        }
        DB::table('users')->where('id', $updateId)->update($user_data);
        $this->image='';
        $this->newimage='';
        session()->flash('message', 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!');
    }
}
