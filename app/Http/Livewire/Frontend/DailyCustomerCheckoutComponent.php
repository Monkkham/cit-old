<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class DailyCustomerCheckoutComponent extends Component
{
    public function render()
    {
        return view('livewire.frontend.daily-customer-checkout-component')
        ->layout('layouts.frontend.base-frontend');
    }
}
