<?php

namespace App\Http\Resources;

use App\Models\P1\P1CurrencyRate;
use App\Models\P1\P1ProductImages;
use Illuminate\Http\Resources\Json\JsonResource;

class P1ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $currency = P1CurrencyRate::where('branch_id', $this->branch_id)->first();
        return [
            "id" => $this->id,
            "code" => $this->code,
            "name" => $this->name,
            "branch" => $this->branch,
            "wholesale_price" => $this->wholesale_price,
            "import_price" => $this->import_price,
            "online_price_lak" => $currency->rate_to_kip_operator == "*" ? ceil((($this->wholesale_price * $currency->profit_rate) * $currency->rate_to_kip) / 1000) * 1000 : ceil((($this->wholesale_price * $currency->profit_rate) / $currency->rate_to_kip) / 1000) * 1000,
            "online_price_dollar" => $currency->rate_to_dollar_operator == "*" ? ceil(($this->wholesale_price * $currency->profit_rate) * $currency->rate_to_dollar) : ceil(($this->wholesale_price * $currency->profit_rate) / $currency->rate_to_dollar),
            "image" => $this->image,
            "vat" => $this->vat,
            "note" => $this->note,
            "des" => $this->des,
            "long_des" => $this->long_des,
            "thumb" => $this->thumb,
            "link" => $this->link,
            "min_reserve" => $this->min_reserve,
            "qty" => $this->qty,
            "catalog_id" => $this->catalog_id,
            "catalog" => $this->catalog,
            "unit_name" => $this->unit->name_la ?? null,
            "del" => $this->del,
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
            "updated_at" => date('Y-m-d H:i:s', strtotime($this->updated_at)),
            "image_list" => P1ProductImages::select('image')->where('product_id', $this->id)->get(),
        ];
    }
}
