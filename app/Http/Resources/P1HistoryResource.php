<?php

namespace App\Http\Resources;

use App\Models\District;
use App\Models\P1\P1OrderDetail;
use App\Models\P1\P1OrderTransaction;
use App\Models\Province;
use App\Models\Village;
use Illuminate\Http\Resources\Json\JsonResource;

class P1HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "bcel_code" => $this->bcel_code,
            "code" => $this->code,
            "pro_id" => Province::find($this->pro_id) ?? null,
            "dis_id" => District::find($this->dis_id) ?? null,
            "vil_id" => Village::find($this->vil_id) ?? null,
            "address_detail" => $this->address_detail,
            "lat" =>  $this->lat,
            "lng" =>  $this->lng,
            "total_price" => $this->total_price,
            "paid_type" => $this->paid_type,
            "status" =>  $this->status,
            'count' => P1OrderDetail::where('order_id', $this->id)->count('id'),
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
            "updated_at" => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}
