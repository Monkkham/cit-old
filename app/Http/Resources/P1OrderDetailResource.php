<?php

namespace App\Http\Resources;

use App\Models\Branch;
use App\Models\P1\P1ProductReviewByUser;
use App\Models\P1\P1Products;
use Illuminate\Http\Resources\Json\JsonResource;

class P1OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $check_review = P1ProductReviewByUser::where('user_id',$this->order->user_id)->where('product_id', $this->product_id)->first();
        return [
            'id' => $this->id,
            "order_id" => $this->order_id,
            "branch_id" => Branch::select('id', 'name_la', 'phone')->where('id', $this->branch_id)->first() ?? null,
            "product_id" => new P1ProductForHistoryResource(P1Products::find($this->product_id, )) ?? null,
            "qty" => $this->qty,
            "total_online_price_kip" => $this->total_online_price_kip,
            "total_online_price_dollar" => $this->total_online_price_dollar,
            "delivery_confirm_time" => $this->delivery_confirm_time,
            "customer_confirm" => $this->customer_confirm,
            "review" => $check_review,
            "status" => $this->status,
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
            "updated_at" => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}