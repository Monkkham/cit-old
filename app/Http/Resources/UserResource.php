<?php

namespace App\Http\Resources;

use App\Models\District;
use App\Models\Province;
use App\Models\Village;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'emp_id' => $this->emp_id,
            'role_id' => $this->role_id,
            'device_token' => $this->device_token,
            'branch_id' => $this->branch_id,
            'pro_id' => $this->pro_id,
            'dis_id' => $this->dis_id,
            'vill_id' => $this->vill_id,
            'pro_name' => Province::find($this->pro_id)->name ?? null,
            'dis_name' => District::find($this->dis_id)->name ?? null,
            'vill_name' => Village::find($this->vill_id)->name ?? null,
            'address' => $this->address,
            'image' => $this->image,
            'del' => $this->del,
            "created_at" => date('Y-m-d H:i:s', strtotime($this->created_at)),
            "updated_at" => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}
