<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\Balance;
use App\Models\BalanceItem;
use App\Models\Cart;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Coupon;
class OrderApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $order_id;
    public $order_slug;
    public $check_balance;
    public $user_id;
    public $amount_divid;
    public $update_balance;
    public $balance_id;
    public $update_allbalance, $qty, $product_id;
    public function index()
    {
        $order = Order::where('del',0)->get();
        return $order;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //new api by cheeyeeyang mouasue
    public function addtocart(Request $request, $product_id)
    {
        $products =  DB::connection('mysql2')->table('products')->where('id', $this->product_id)->first();
        if(!empty($products->price_online)){
            $carts = new Cart();
            $carts->product_id = $products->id;
            $carts->qty = $this->qty;
            $carts->price = $products->price_online;
            $carts->user_id = Auth::user()->id;
            $carts->save();
            return response()->json(['Add_to_cart_succesfully' => $carts],200);
        }else{
            return response()->json(['no_add_to_cart' => 'ບໍ່ສາມາດສັ່ງຊື້ໄດ້ ເພາະສ່າບໍ່ມີລາຄາສິນຄ້າ'],200);
        }
    }
    public function getallcart(Request $request){
       $carts = Cart::orderBy('id', 'desc')->get();
        return response()->json(['all_carts' => $carts], 200);
    }
    public function getallcoupon(Request $request){
        $coupons = Coupon::orderBy('id', 'desc')->get();
        return response()->json(['get_all_coupon' => $coupons], 200);
    }
    public function getallorder(Request $request){
        $orders = Order::orderBy('id', 'desc')->get();
        return response()->json(['get_all_order' => $orders], 200);
    }
    public function orderedorder(Request $request){
        $orders = Order::where('status', 'ordered')->orderBy('id', 'desc')->get();
        return response()->json(['Ordered_order:' => $orders], 200);
    }
    public function deliveredorder(Request $request){
        $orders = Order::where('status', 'delivered')->orderBy('id', 'desc')->get();
        return response()->json(['Delivered_dorder' => $orders], 200);
    }
    public function arrivedorder(Request $request){
        $orders = Order::where('status', 'arrived')->orderBy('id', 'desc')->get();
        return response()->json(['Arrived_order' => $orders], 200);
    }
    public function dosendorder(){
        
    }
    public function confirmorder(){
        $check_balance=Balance::where('user_id',$this->user_id)->first();
        if(!$check_balance){
            $balances = new Balance();
            $balances->all_balance=$this->amount_divid;
            $balances->balance=$this->amount_divid;
            $balances->user_id=$this->user_id;
            $balances->save();
        }else{
            $this->update_allbalance=$check_balance->all_balance + $this->amount_divid;
            $this->update_balance=$check_balance->balance + $this->amount_divid;
            DB::update('update balances set all_balance = ? , balance = ? where user_id = ?',[$this->update_allbalance, $this->update_balance,$this->user_id]);
        }
        if($this->amount_divid !=''){
            $check_balance=Balance::where('user_id',$this->user_id)->first();
            if($check_balance){
                $this->balance_id=$check_balance->id;
                $balance_item = new BalanceItem();
                $balance_item->amount=$this->amount_divid;
                $balance_item->balance_id=$this->balance_id;
                $balance_item->save();
            }
        }
        DB::update('update orders set status = ? where id = ?',['arrived',$this->order_id]);
        DB::update('update transactions set check_payment=? , status = ? where order_id = ?',['1','refunded',$this->order_id]);
        DB::update('update log_divids set status = ? where order_id = ?',['s',$this->order_id]);
    }
    public function cancelorder(){
          
    }
}
