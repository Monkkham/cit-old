<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Laravel\Firebase\Facades\Firebase;

class AuthApiController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ], [
            'name.required' => 'ກະລຸນາໃສ່ຊື່ກ່ອນ!',
            'phone.required' => 'ໃສ່ເບີ້ໂທລະສັບກ່ອນ!',
            'phone.numeric' => 'ເບີ້ໂທຕ້ອງເປັນຕົວເລກ',
            'phone.unique' => 'ເບີ້ໂທນີ້ມີໃນລະບົບແລ້ວ',
            'password.required' => 'ໃສ່ລະຫັດຜ່ານກ່ອນ!',
            'password.min' => 'ລະຫັດຕ້ອງ 6 ຕົວຂື້ນໄປ',
            'password_confirmation.required' => 'ໃສ່ຢັ້ງຢືນລະຫັດຜ່ານກ່ອນ!',
            'password_confirmation.same' => 'ຢັ້ງຢືນລະຫັດຜ່ານບໍ່ຕົງກັນ!',
        ]);

        $image = $request->image;
        $imageName = time() . $image->getClientOriginalName();

        $users = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'password' => bcrypt($request->password),
            'image' => 'images/profile/' . $imageName
        ]);
        $image->move('images/profile/', $imageName);

        return response([
            'user' => $users,
            'token' => $users->createToken('secret')->plainTextToken
        ]);
    }
    public function new_register(Request $request)
    {
        $users = User::create([
            'name' => $request->firstname,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'otp_status' => 1,
        ]);
        return response([
            'messsage' => 'register_successfully',
            'token' => $users->createToken('secret')->plainTextToken
        ], 200);
    }
    public function login(Request $request)
    {
        if (Auth::attempt($request->all())) {
            $user = auth()->user();
            if (auth()->user()->del != 0) {
                return response([
                    'message' => 'ຊື່ຜູ້ໃຊ້ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!'
                ], 403);
            }
            if ($user instanceof User) {
                if (!empty($user->tokens())) {
                    $user->tokens()->delete();
                }
                return response([
                    'user' => auth()->user(),
                    'token' => auth()->user()->createToken('secret')->plainTextToken
                ], 200);
            }
        } else {
            return response([
                'message' => 'ຊື່ຜູ້ໃຊ້ ຫຼື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ!'
            ], 403);
        }
    }
    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response([
            'message' => 'ອອກລະບົບສຳເລັດ!'
        ], 200);
    }
    public function user()
    {
        return response([
            'user' => new UserResource(auth()->user())
        ], 200);
    }

    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'address' => 'required',
                'image' => 'nullable|image',
            ], [
                'name.required' => 'ໃສ່ຊື່ ແລະ ນາມສະກຸນກ່ອນ!',
                'address.required' => 'ໃສ່ທີ່ຢູ່!',
                'image.image' => 'ທ່ານໃສ່ບໍ່ຖືກຮູບແບບຮູບ!'
            ]);

            if ($validator->fails()) {
                $error = $validator->errors()->all()[0];
                return response()->json(['status' => 'false', 'message' => $error, 'data' => []], 422);
            } else {
                $user = User::find($request->user()->id);
                $user->name = $request->name;
                $user->address = $request->address;

                if ($request->image && $request->image->isValid()) {
                    $file_name = time() . '.' . $request->image->extension();
                    $request->image->move(public_path('images/profile'), $file_name);
                    $path = "images/profile/$file_name";
                    $user->image = $path;
                }

                if ($request->pro_id) {
                    $user->pro_id = $request->pro_id;
                }
                if ($request->dis_id) {
                    $user->dis_id = $request->dis_id;
                }
                if ($request->vil_id) {
                    $user->vill_id = $request->vil_id;
                }
                if ($request->email) {
                    $user->email = $request->email;
                }

                $user->update();

                return response()->json(['status' => 'true', 'message' => "ແກ້ໄຂຂໍ້ມູນສຳເລັດ!", 'data' => $user]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []], 500);
        }
    }
    public function save_device_token(Request $request)
    {
        //field: device_token
        if (!$request->device_token) {
            return response(["please_enter_token"], 405);
        }
        $user = User::find(auth()->user()->id);
        $user->device_token = $request->device_token;
        $user->update();
        return response(["message" => "successfully_save_device_token"], 200);
    }
    public function confirm_otp()
    {
        $user = User::find(auth()->user()->id);
        $user->otp_status = 1;
        $user->update();
        return response(["message" => "otp_confirmed"], 200);
    }
    public function rst_pwd_secret(Request $request)
    {
        if ($request->unlock == '123456789') {
            $userData = User::where('phone', $request->phone)->first();
            $userData->password = bcrypt($request->password);
            $userData->save();
            return response([
                'message' => 'ປ່ຽນລະຫັດຜ່ານໃຫ່ມສຳເລັດແລ້ວ'
            ], 200);
        }
        return response([
            'message' => 'you cannot access'
        ], 405);
    }
    public function check_phone_register(Request $request)
    {
        $userData = User::where('phone', $request->phone)->first();
        if ($userData) {
            return   response([
                'message' => 'ເບີໂທນີ້ລົງທະບຽນແລ້ວ!'
            ], 200);
        } else {
            return   response([
                'message' => 'ເບີໂທນີ້ຍັງບໍ່ໄດ້ລົງທະບຽນ!'
            ], 404);
        }
    }
    public function delete_account()
    {
        $data = User::find(auth()->user()->id);
        $data->del = 1;
        $data->update();
        return response()->json(['message' => "ລົບບັນຊີສຳເລັດ!"]);
    }
    public function reset_password(Request $request)
    {
        $userData = User::where('id', auth()->user()->id)->first();
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        if (request()->old_password && request()->new_password) {
            if (Hash::check($old_password, $userData->password)) {
                if ($old_password == $new_password) {
                    return response([
                        'message' => 'ລະຫັດໃຫມ່ຕ້ອງຕ່າງຈາກລະຫັດເກົ່າ'
                    ], 403);
                } else {
                    $setUserData = User::find($userData->id);
                    $setUserData->password = bcrypt($new_password);
                    $setUserData->save();
                    return response([
                        'message' => 'ປ່ຽນລະຫັດຜ່ານໃຫ່ມສຳເລັດແລ້ວ'
                    ], 200);
                }
            } else {
                return response([
                    'message' => 'ລະຫັດເກົ່າບໍ່ຖືກຕ້ອງ'
                ], 401);
            }
        } else {
            return response([
                'message' => 'ປ້ອນຂໍ້ມູນກອນ'
            ], 400);
        }
    }
}
