<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\P1ProductResource;
use App\Http\Resources\ReviewResource;
use App\Models\P1\P1Catalogs;
use App\Models\P1\P1CurrencyRate;
use App\Models\P1\P1ProductReviewByUser;
use Exception;
use Illuminate\Http\Request;
use App\Models\P1\P1Products;

class P1ProductsController extends Controller
{
    public function get_product(Request $request)
    {
        //field : not_id, number
        return response([
            "data" => P1ProductResource::collection(P1Products::where('del', 0)->whereNotIn('id', json_decode($request->not_in))->inRandomOrder()->limit($request->number)->get())
        ], 200);
    }
    public function get_product_by_id($id)
    {
        return response([
            "data" => new P1ProductResource(P1Products::where('id', $id)->first()) ?? null,
            "review" => ReviewResource::collection(P1ProductReviewByUser::where('product_id', $id)->get()),
        ], 200);
    }
    public function get_catalogs()
    {
        return response([
            "data" => P1Catalogs::where('del', 0)->get(),
        ], 200);
    }
    public function get_prouduct_by_catalog(Request $request)
    {
        return response([
            "data" => P1ProductResource::collection(P1Products::where('catalog_id', $request->catalog_id)->whereNotIn('id', json_decode($request->not_in))->inRandomOrder()->limit($request->number)->get())
        ], 200);
    }
    public function search_product(Request $request)
    {
        $string = $request->text;
        return response([
            "data" => P1ProductResource::collection(P1Products::where(function ($query) use ($string) {
                $query->where('name', 'LIKE', "%" . $string . "%")
                    ->orWhere('des', 'LIKE', "%" . $string . "%")
                    ->orWhere('long_des', 'LIKE', "%" . $string . "%");
            })->limit(10)->get())
        ], 200);
    }
    public function add_review(Request $request)
    {
        //field: comment, product_id, star_point;
        $newReview = new P1ProductReviewByUser();
        $newReview->user_id = auth()->user()->id;
        $newReview->product_id = $request->product_id;
        $newReview->star_points = $request->star_points;
        $newReview->comment = $request->comment;
        $newReview->save();
        return response([
            "message" => "success"
        ], 200);
    }
    public function edit_review(Request $request)
    {
        //field: id, comment, product_id, star_point;
        $editReview = P1ProductReviewByUser::find($request->id);
        $editReview->product_id = $request->product_id;
        $editReview->star_points = $request->star_points;
        $editReview->comment = $request->comment;
        $editReview->update();
        return response([
            "message" => "success"
        ], 200);
    }
}
