<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomFunction;
use App\Http\Resources\P1HistoryResource;
use App\Http\Resources\P1OrderDetailResource;
use App\Http\Resources\P1OrderResource;
use App\Models\P1\P1CurrencyRate;
use App\Models\P1\P1Order;
use App\Models\P1\P1OrderDetail;
use App\Models\P1\P1OrderTransaction;
use App\Models\P1\P1Products;
use App\Models\P1\P1Units;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class P1OrderController extends Controller
{
    public function add_order(Request $request)
    {
        /*field = pro_id, dis_id, vil_id, address_detail, lat, lng, total_price 
        order_detail = [
        {
        product_id,
        qty,
        branch_id,
        total_online_price, 
        }
        ]
        */
        try {
            DB::beginTransaction();
            //save order
            $custom = new CustomFunction();
            $newOrder = new P1Order();
            while (1) {
                $randomNumber = $custom->generateRandomNumber(20);
                $check = P1Order::where('code', $randomNumber)->first();
                if (!$check) {
                    break;
                }
            }
            $newOrder->code = $randomNumber;
            $newOrder->user_id = auth()->user()->id;
            $newOrder->pro_id = $request->pro_id;
            $newOrder->dis_id = $request->dis_id;
            $newOrder->vil_id = $request->vil_id;
            $newOrder->address_detail = $request->address_detail;
            $newOrder->lat = $request->lat;
            $newOrder->lng = $request->lng;
            $newOrder->total_price = $request->total_price;
            $newOrder->paid_type = $request->paid_type;

            $newOrder->save();
            if (!$request->order_detail) {
                DB::rollBack();
                return response(["message" => "ຕ້ອງເພິ່ມຂໍ້ມູນສິນຄ້າ",], 405);
            }
            //save transaction
            $branchListGroup = [];
            foreach ($request->order_detail as $product) {
                if (!in_array($product["branch_id"], $branchListGroup)) {
                    $branchListGroup[] = $product["branch_id"];
                }
            }
            foreach ($branchListGroup as $branch_id) {
                $transaction_total_price = 0.0;
                $newTransaction = new P1OrderTransaction();
                $newTransaction->order_id = $newOrder->id;
                $newTransaction->branch_id = $branch_id;
                $newTransaction->save();
                foreach ($request->order_detail as $order_detail) {
                    if ($order_detail["branch_id"] == $branch_id) {
                        //save order detail
                        $newOrderDetail = new P1OrderDetail();
                        $newOrderDetail->order_id = $newOrder->id;
                        $newOrderDetail->branch_id = $branch_id;
                        $newOrderDetail->product_id = $order_detail["product_id"];
                        $newOrderDetail->qty = $order_detail["qty"];
                        $newOrderDetail->total_online_price_kip = $order_detail["total_online_price_kip"];
                        $newOrderDetail->total_online_price_dollar = $order_detail["total_online_price_dollar"];
                        $newOrderDetail->save();
                        if ($newOrder->paid_type == "LAK") {
                            $transaction_total_price += $order_detail["total_online_price_kip"];
                        } else {
                            $transaction_total_price += $order_detail["total_online_price_dollar"];
                        }
                    }
                }
                $updateTransaction = P1OrderTransaction::find($newTransaction->id);
                $updateTransaction->total_price = $transaction_total_price;
                $updateTransaction->update();
            }

            DB::commit();
            // $custom->send_notification_by_role("ມີສັ່ງຊື້ໃໝ່", "ລະຫັດສັ່ງຊື້ " . $newOrder->code, 1);
            $qr_code = $this->generate_bcel_qr($transaction_total_price, $newOrder->id, $newOrder->code, $newOrder->user_id, $newOrder->code);
            return response(["message" => "ordered_success", "order_id" => $newOrder->id, "order_code" => $newOrder->code, "qr_code" => $qr_code], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["message" => "something_went_wrong", "error" => $e->getMessage()], 405);
        }
    }
    public function get_order_detail_by_order_id($id)
    {
        $data = P1OrderDetailResource::collection(P1OrderDetail::where('order_id', $id)->get());
        return response(["data" => $data,], 200);
    }

    public function get_order_history_by_id($id)
    {
        $order = P1Order::find($id);
        $orderTransaction = P1OrderTransaction::where('order_id', $id)->get();
        if (!$order) {
            return response(["message" => "not_have_order"], 404);
        }
        if (empty($orderTransaction)) {
            return response(["message" => "not_have_order"], 404);
        }
        $orderDetailList = [];
        foreach ($orderTransaction as $transaction) {
            $orderDetailList[] = [
                'transaction' => $transaction,
                'detail' => P1OrderDetailResource::collection(P1OrderDetail::where('order_id', $transaction->order_id)->where('branch_id', $transaction->branch_id)->get())
            ];
        }
        return response([
            "data" => [
                'order' => new P1OrderResource($order),
                'order_detail' => $orderDetailList,
            ]
        ], 200);
    }
    public function get_all_history_by_user()
    {
        $order = P1Order::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();

        return response(["data" => P1HistoryResource::collection($order)], 200);
    }
    public function confirm_order_from_customer(Request $request)
    {
        //field: order_id, branch_id  
        try {
            DB::beginTransaction();
            $data = P1OrderDetail::where('order_id', $request->order_id)->where('branch_id', $request->branch_id)->get();
            if (count($data) < 1) {
                return response(["message" => "not_have_order"], 404);
            }
            foreach ($data as $item) {
                if ($item->status == 2) {
                    $item->status = 3;
                    $item->update();
                } else {
                    DB::rollBack();
                    return response(["message" => "invalid_data"], 404);
                }
            }
            $data = P1OrderDetail::where('order_id', $request->order_id)->whereIn('status', [1, 2])->count("id");
            if ($data == 0) {
                //update order
                $order = P1Order::find($request->order_id);
                $order->status = "3";
                $order->update();
            }
            DB::commit();
            return response(["message" => "confirm_success"], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["message" => "something_went_wrong", "error" => $e->getMessage(),], 405);
        }
    }
    public function check_product_price(Request $request)
    {
        try {
            // order_detail = [
            //     {
            //     product_id,
            //     }
            //     ]
            $listOfData = [];
            foreach ($request->order_detail as $product) {
                $product_detail = P1Products::find($product);
                $currency = P1CurrencyRate::where('branch_id', $product_detail->branch_id)->first();
                $online_price_lak = $currency->rate_to_kip_operator == "*" ? ceil((($product_detail->wholesale_price * $currency->profit_rate) * $currency->rate_to_kip) / 1000) * 1000 : ceil((($product_detail->wholesale_price * $currency->profit_rate) / $currency->rate_to_kip) / 1000) * 1000;
                $online_price_dollar = $currency->rate_to_dollar_operator == "*" ? ceil(($product_detail->wholesale_price * $currency->profit_rate) * $currency->rate_to_dollar) : ceil(($product_detail->wholesale_price * $currency->profit_rate) / $currency->rate_to_dollar);
                $listOfData[] = [
                    "id" => $product_detail->id,
                    "online_price_lak" => $online_price_lak,
                    "online_price_dollar" => $online_price_dollar,
                ];
            }
            return response(["data" => $listOfData], 200);
        } catch (Exception $e) {
            return response(["message" => "something_went_wrong", "error" => $e->getMessage(),], 405);
        }
    }
    public function get_qr_code($order_id)
    {
        $order = P1Order::find($order_id);
        $orderTransaction = P1OrderTransaction::where('order_id', $order_id)->first();

        $qr_code = $this->generate_bcel_qr($orderTransaction->total_price, $order->id, $order->code, $order->user_id, $order->code);
        return response(["qr_code" => $qr_code], 200);
    }
    public function cancel_order($id)
    {
        DB::beginTransaction();
        try {
            $order = P1Order::find($id);
            $order->status = 0;
            $order->update();
            P1OrderDetail::where('order_id', $id)->update(array("status" => 0));
            DB::commit();
            return response(["message" => "success"], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["message" => "something_went_wrong", "error" => $e->getMessage(),], 405);
        }
    }


    //temp
    public function generate_bcel_qr($amount, $invoiceid, $uuid, $terminalid, $description)
    {
        try {
            $mcid = "mch5e0d466c6970a";
            $mcc = "5045";
            $ccy = 418;
            $country = "LA";
            $province = "VTE";
            $rawqr = $this->buildqr([
                "00" => "01",
                "01" => "11",
                "33" => $this->buildqr([
                    "00" => "BCEL",
                    "01" => "ONEPAY",
                    "02" => $mcid
                ]),
                "52" => $mcc,
                "53" => $ccy,
                "54" => $amount,
                "58" => $country,
                "60" => $province,
                "62" => $this->buildqr([
                    "01" => $invoiceid,
                    "05" => $uuid,
                    "07" => $terminalid,
                    "08" => $description
                ])
            ]);
            $fullqr = $rawqr . $this->buildqr([63 => $this->crc16($rawqr . "6304")]); // Adding itself into
            return $fullqr;
        } catch (Exception $e) {
            return null;
        }
    }



    public function buildqr($arr)
    {
        $res = "";
        foreach ($arr as $key => $val) {
            if (!$val)
                continue;
            $res .= str_pad($key, 2, "0", STR_PAD_LEFT) .
                str_pad(strlen($val), 2, "0", STR_PAD_LEFT) .
                $val;
        }
        return $res;
    }
    function crc16($sStr, $aParams = array())
    {
        $aDefaults = array(
            "polynome" => 0x1021,
            "init" => 0xFFFF,
            "xor_out" => 0,
        );
        foreach ($aDefaults as $key => $val) {
            if (!isset($aParams[$key])) {
                $aParams[$key] = $val;
            }
        }
        $sStr .= "";
        $crc = $aParams['init'];
        $len = strlen($sStr);
        $i = 0;
        while ($len--) {
            $crc ^= ord($sStr[$i++]) << 8;
            $crc &= 0xffff;
            for ($j = 0; $j < 8; $j++) {
                $crc = ($crc & 0x8000) ? ($crc << 1) ^ $aParams['polynome'] :
                    $crc << 1;
                $crc &= 0xffff;
            }
        }
        $crc ^= $aParams['xor_out'];
        return str_pad(strtoupper(dechex($crc)), 4, "0", STR_PAD_LEFT);
    }
    public function check_transaction($uuid)
    {
        $url = "https://bcel.la:8083/onepay/gettransaction.php?mcid=mch5e0d466c6970a&uuid=" . $uuid;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $resp = curl_exec($curl);
        curl_close($curl);
        if ($resp != "") {
            if (json_decode($resp)->result == 0) {
                $order = P1Order::where('code', $uuid)->first();
                $order->bcel_code = json_decode($resp)->ticket;
                $order->status = 2;
                $order->update();
                $custom = new CustomFunction();
                $custom->send_notification_by_role("ມີອໍເດີ້ມາໃໝ່", "ລະຫັດ " . $order->code, "1");
                $custom->send_notification_by_role("ມີອໍເດີ້ມາໃໝ່", "ລະຫັດ " . $order->code, "2");
                $custom->send_notification_by_role("ມີອໍເດີ້ມາໃໝ່", "ລະຫັດ " . $order->code, "3");
                return response(['message' => "success"], 200);
            }
        }
        return response(['message' => "not_found_payment"], 404);
    }
}