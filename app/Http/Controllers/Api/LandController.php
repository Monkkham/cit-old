<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Province;
use App\Models\Village;
use Illuminate\Http\Request;

class LandController extends Controller
{
    public function get_provinces()
    {
        $data = Province::all();
        return response(["data" => $data], 200);
    }
    public function get_districts_by_pro_id($pro_id)
    {
        $data = District::where('pro_id', $pro_id)->get();
        return response(["data" => $data], 200);
    }
    public function get_villages_by_dis_id($dis_id)
    {
        $data = Village::where('dis_id', $dis_id)->get();
        return response(["data" => $data], 200);
    }
}
