<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\P1\P1Notifications;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class CustomFunction extends Controller
{
  public function generateRandomNumber($length)
  {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
  public function send_notification_to_user($title, $message, $user_id, $key)
  {
    try {
      $user = User::find($user_id);
      $sender = auth()->user();
      $SERVER_API_KEY = 'AAAAlNfdTQI:APA91bFYNlal0Yx4bftYRAEEyiRmgGmadWFQ4yOVMDEw1rHbK1b9wMFTPizJUQatVQq0bcGJLAeAyZ8VDK1F8nl4HUvZtWYl-fPLrsv86TiCeHyM7pVBq1p1yc1HSTppFTZhmtI4fWi5';
      $data = [
        "registration_ids" => [$user->device_token],
        "notification" => [
          "title" => $title,
          "body" => $message,
          "icon" => "info"
        ],
        "data" => [
          "key" => $key
        ]
      ];
      $dataString = json_encode($data);
      $headers = [

        'Authorization: key=' . $SERVER_API_KEY,

        'Content-Type: application/json',

      ];

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

      curl_setopt($ch, CURLOPT_POST, true);

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

      curl_exec($ch);
      if ($user->device_token) {
        $new_noti = new P1Notifications();
        $new_noti->user_id = $user->id;
        $new_noti->title = $title;
        $new_noti->body = $message;
        $new_noti->sender_id = $sender->id;
        $new_noti->save();
      }
      return;
    } catch (Exception $e) {
      return;
    }
  }
  public function send_notification_by_role($title, $message, $role_id)
  {
    $tokens = User::where('role_id', $role_id)->get();
    $sender = auth()->user();
    $all_device_tokens = [];
    $SERVER_API_KEY = 'AAAAlNfdTQI:APA91bFYNlal0Yx4bftYRAEEyiRmgGmadWFQ4yOVMDEw1rHbK1b9wMFTPizJUQatVQq0bcGJLAeAyZ8VDK1F8nl4HUvZtWYl-fPLrsv86TiCeHyM7pVBq1p1yc1HSTppFTZhmtI4fWi5';
    foreach ($tokens as $token) {
      if ($token->device_token) {
        $all_device_tokens[] = $token->device_token;
        $new_noti = new P1Notifications();
        $new_noti->user_id = $token->id;
        $new_noti->title = $title;
        $new_noti->body = $message;
        $new_noti->sender_id = $sender->id;
        $new_noti->save();
      }

    }

    $data = [

      "registration_ids" => $all_device_tokens,

      "notification" => [

        "title" => $title,

        "body" => $message,

        "sound" => "default" // required for sound on ios

      ],

    ];

    $dataString = json_encode($data);

    $headers = [

      'Authorization: key=' . $SERVER_API_KEY,

      'Content-Type: application/json',

    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

    curl_setopt($ch, CURLOPT_POST, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
    curl_exec($ch);
  }
}