<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MissType;

class MissTypeController extends Controller
{
    public function index()
    {
        $misstypes = MissType::orderBy('id','desc')->get();
        return view('backend.settings.misstype.index', compact('misstypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.settings.misstype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'amount'=>'required',
        ],[
            'type.required'=>'ໃສ່ປະເພດຂາດກ່ອນ!',
            'amount.required'=>'ໃສ່ຈໍານວນເງິນກ່ອນ!',
        ]);
        MissType::create([
            'type'=> $request->type,
            'amount'=> $request->amount,
        ]);
        return redirect()->route('misstype.index')->with('success','ເພີ່ມຂໍ້ມູນສຳເລັດ!');
    }
    public function edit($id)
    {
        $misstype = MissType::find($id);
        return view('backend.settings.misstype.edit', compact('misstype'));
    }
    public function update(Request $request, $id)
    {
        $misstype = MissType::find($id);
        $request->validate([
            'type' => 'required',
            'amount'=>'required',
        ],[
            'type.required'=>'ໃສ່ປະເພດຂາດກ່ອນ!',
            'amount.required'=>'ໃສ່ຈໍານວນເງິນກ່ອນ!',
        ]);
            $misstype_data = [
                'type'=> $request->type,
                'amount'=> $request->amount,
            ];
           $misstype->update($misstype_data);
        return redirect()->route('misstype.index')->with('success','ແກ້ໄຂຂໍ້ມູນສຳເລັດ!');
     }
    public function destroy($id)
    {
        $user = MissType::find($id);
        $user->delete();
        return redirect()->back()->with('success','ລຶບຂໍ້ມູນສຳເລັດ!');
    }
}
