<?php

namespace App\Http\Controllers\Backend\Ecommerce;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $product = DB::connection('mysql2')->table('products')
                    ->join('catalogs','products.catalog_id','=','catalogs.id')
                    ->select('products.*','catalogs.name as catalogname')
                    ->orderBy('products.id','desc')
                    ->where('trash',0)
                    ->paginate(15);
        return view('backend.ecommerce.product.index', compact('product','all_catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(){
        $all_catalogs = DB::connection('mysql2')->table('catalogs')->select('id','name')->get();
        $searchByCatalog = $_GET['searchByCatalog'];
        $search_products = $_GET['search_products'];
        $product=DB::connection('mysql2')->table('products')
        ->join('catalogs','products.catalog_id','=','catalogs.id')
        ->select('products.*','catalogs.name as catalogname')
        ->orderBy('products.id','desc')
        ->where('products.name','like', '%'. $search_products . '%')
        ->where('products.catalog_id','like', '%'. $searchByCatalog . '%')
        ->where('trash',0)
        ->paginate(15);
        return view('backend.ecommerce.product.search', compact('product','all_catalogs'));
           $search_products ='';
           $searchByCatalog='';
    }
    public function show($id)
    {
        $product = DB::connection('mysql2')->table('products')->find($id);
        $product = Product::find($id);

        $cata = Catalog::where('note',0)->get();
        return view('backend.ecommerce.product.show', compact('product','cata'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = DB::connection('mysql2')->table('products')->find($id);

        $product = Product::find($id);

        $cata = Catalog::where('note',0)->get();
        return view('backend.ecommerce.product.edit', compact('product','cata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(Request $request, $id)
    {
        $product = DB::connection('mysql2')->table('products')->where('products.id',$id)->limit(1);
        $this->validate($request,[
            'name' => 'required',
            
        ],[
            'name.required' => 'ກະລຸນາໃສ່ຊື່ສິນຄ້າກ່ອນ!',
        ]);

        if($request->has('file'))
        {
        $img = $request->file;
        $imagename = time().$img->getClientOriginalName();
        $img->move('upload/product/', $imagename);

        $pro_data = [
            'name' => $request->name,
            'price'=>str_replace(',','',$request->price),
            'price_online'=>str_replace(',','',$request->price_online),
            'image' => 'upload/product/'.$imagename,
            'note' => $request->note,
            'des' => $request->des,
            'long_des' => $request->long_des,
        ];
        }elseif($request->has('file'))
        {
        $img = $request->file;
        $imagename = time().$img->getClientOriginalName();
        $img->move('upload/product/', $imagename);

        $pro_data = [
            'name' => $request->name,
            'price'=>str_replace(',','',$request->price),
            'price_online'=>str_replace(',','',$request->price_online),
            'image' => 'upload/product/'.$imagename,
            'note' => $request->note,
            'des' => $request->des,
            'long_des' => $request->long_des,
        ];
        } else{
        $pro_data = [
            'name' => $request->name,
            'price'=>str_replace(',','',$request->price),
            'price_online'=>str_replace(',','',$request->price_online),
            'note' => $request->note,
            'des' => $request->des,
            'long_des' => $request->long_des,
        ];

        }
        $product->update($pro_data);
        return redirect()->route('product.index')->with('success','ແກ້ໄຂຂໍ້ມູນສຳເລັດ!');
    }








    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}