<?php

namespace App\Http\Controllers\Backend;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExcelUserController extends Controller
{
    public function index()
    {
        return view('backend.settings.user.excel');
    }

    public function store(Request $request) 
    {
        $request->validate([
            'file_excel' => 'required|mimes:xlsx, csv, xls'
        ]);
        $file =$request->file('file_excel');
        Excel::import(new UsersImport, $file);
        return redirect('/user')->with('success','ເພີ່ມຂໍ້ມູນສຳເລັດ!');
    }
}
