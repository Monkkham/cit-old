<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ExportDoc;
use App\Models\ImportDoc;
use App\Models\LocalDoc;
use App\Models\Employee;
use App\Models\Payroll;
use App\Models\User;
use App\Models\Order;
use App\Models\Transaction;
use Livewire\WithPagination;

class DashboardController extends Controller
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $im_count = ImportDoc::where('del',0)->count();
        $ex_count = ExportDoc::where('del',0)->count();
        $local_count = LocalDoc::where('del',0)->count();
        $all_emp_count = Employee::where('del',0)->count();
        $woman_emp_count = Employee::where('del',0)->where('sex',2)->count();
        $product_count = DB::connection('mysql2')->table('products')->where('trash',0)->count();
        $user_count = User::where('del',0)->count();
        $sum_order=Order::where('status', 'ordered')->sum('total');
        $sum_delivery = Order::where('status', 'delivered')->sum('total');
        $sum_arrived= order::where('status','arrived')->sum('total');
        $sum_canceled=order::where('status','canceled')->sum('total');
        $order_count = Order::where('status','ordered')->count();
        $delivered_count = Order::where('status','delivered')->count();
        $arrived_count=Order::where('status','arrived')->count();
        $canceled_count = Order::where('status','canceled')->count();
        $new_orders= Order::where('status','ordered')->orderBy('id','desc')->paginate(10);
        return view('backend.dashboard', compact('im_count','ex_count','local_count','all_emp_count','woman_emp_count','product_count','user_count','order_count','sum_order','delivered_count','canceled_count','new_orders','arrived_count','sum_delivery',
        'arrived_count','sum_arrived','sum_canceled'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
