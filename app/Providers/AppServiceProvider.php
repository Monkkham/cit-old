<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use View;
use App\Models\Branch;
use App\Models\Order;
use App\Models\CustomerTransition;
use App\Models\RequestWithraw;
use App\Models\P1\P1Order;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        $branchs = Branch::where('id',1)->first();
        $customer_transition =CustomerTransition::orderBy('id', 'DESC')->where('status',1)->get();
        $notif = 0;
        $no = 0;
        $orders=Order::where('status','LIKE', 'ordered');
        $ordersending=Order::where('status','LIKE', 'delivered');
        $orderdonesend=Order::where('status','LIKE', 'arrived');
        $count_requestwithraw=RequestWithraw::count();
        $count_p1_order = P1Order::where('status',2)->count();
        View::share(compact('branchs','customer_transition','notif','no','orders','ordersending','orderdonesend','count_requestwithraw','count_p1_order'));
    }
}
