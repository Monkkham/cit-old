<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackMissEmployee extends Model
{
    use HasFactory;
    protected $table = "track_miss_employees";
    public function employee(){
        return $this->belongsTo('App\Models\Employee','emp_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User','user_id', 'id');
    }
    public function misstype()
    {
        return $this->belongsTo('App\Models\MissType','misstype_id','id');
    }
}
