<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalaryLog extends Model
{
    use HasFactory;
    protected $fillable = ['id','month','year','emp_id','salary','commission_total','miss_total', 'amount_fund','total_salary','note','status', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function employee(){
        return $this->belongsTo('App\Models\Employee','emp_id', 'id');
    }
}
