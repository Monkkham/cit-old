<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payroll_before extends Model
{
    use HasFactory;
    protected $table = "payroll_before";
    protected $fillable = ['id','employee_id','amount','des','created_at','updated_at'];

public function employee()
{
    return $this->belongsTo('App\Models\Employee','employee_id','id');
}
}
