<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalculateFundLog extends Model
{
    use HasFactory;
    protected $fillable = [
        'content',
        'total',
        'total_rate',
        'sum_total',
       'deposit',
       'withraw',
       'remain_total',
       'user_id'
    ];
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}