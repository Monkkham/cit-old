<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "orders";

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function shipping()
    {
        return $this->hasOne(Shipping::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }
    public function village()
    {
        return $this->belongsTo('App\Models\Village','vill_id','id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District','dis_id','id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province','pro_id','id');
    }
    public function shippingname()
    {
        return $this->belongsTo('App\Models\Shipping','shipping_id','id');
    }
}
