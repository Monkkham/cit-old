<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasFactory;

    protected $table = "shippings";
    protected $fillable = ['id','order_id','firstname','lastname','phone','email','address','home_no','vill_id','dis_id','pro_id','created_at'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function villname()
    {
        return $this->belongsTo('App\Models\Village','vill_id','id');
    }

    public function disname()
    {
        return $this->belongsTo('App\Models\District','dis_id','id');
    }

    public function proname()
    {
        return $this->belongsTo('App\Models\Province','pro_id','id');
    }
}
