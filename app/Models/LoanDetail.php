<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'loan_id',
        'amount',
        'profit',
        'total',
        'status',
        'user_id',
        'date',
    ];
    public function loan(){
        return $this->belongsTo('App\Models\Loan', 'loan_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
