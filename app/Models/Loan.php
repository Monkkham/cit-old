<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;
    protected $fillable = [
        'emp_id',
        'amount',
        'qty_pay',
        'percent',
        'type',
        'user_id',
        'status',
    ];
    public function employee(){
        return $this->belongsTo('App\Models\Employee', 'emp_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
