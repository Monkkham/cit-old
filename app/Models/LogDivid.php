<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogDivid extends Model
{
    use HasFactory;
    protected $table="log_divids";
    public function order()
    {
        return $this->belongsTo('App\Models\Order','order_id','id');
    }
}
