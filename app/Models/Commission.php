<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    use HasFactory;
    protected $table = "commissions";
    protected $fillable = ['id','customer_id','created-at','updated_at'];
    public function Customers()
    {
        return $this->belongsTo('App\Models\Customer','customer_id', 'id');
    }
    public function employee(){
        return $this->belongsTo('App\Models\Employee','emp_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User','user_id', 'id');
    }
}
