<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FundDetail extends Model
{
    use HasFactory;
    protected $fillable = [
       'fund_id',
        'amount',
        'date',
        'user_id',
        'status',
        'note'
    ];
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}