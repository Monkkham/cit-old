<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Products extends Model
{
    use HasFactory;
    protected $table = "p1_products";
    protected $fillable = [
        "id",
        "code",
        "name",
        "branch_id",
        "wholesale_price",
        "import_price",
        "image",
        "vat",
        "note",
        "des",
        "long_des",
        "thumb",
        "link",
        "min_reserve",
        "qty",
        "catalog_id",
        "unit_name",
        "del",
        "created",
        "updated",
    ];
    public function catalog()
    {
        return $this->belongsTo('App\Models\P1\P1Catalogs','catalog_id','id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Models\P1\P1Units','unit_name','id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }

}