<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Contact extends Model
{
    use HasFactory;
    protected $table = "p1_contact";
    protected $fillable = [
        "id",
        "name",
        "phone",
        "email",
        "more_detail",
        "del",
        "created_at",
        "updated_at",
    ];
}
