<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Order extends Model
{
    use HasFactory;
    protected $table = "p1_order";
    protected $fillable = [
        "id",
        "user_id",
        "bcel_code",
        "code",
        "lat",
        "lng",
        "pro_id",
        "dis_id",
        "vil_id",
        "address_detail",
        "total_price",
        "paid_type",
        "status",
        "created_at",
        "updated_at",
    ];
    public function customer(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}