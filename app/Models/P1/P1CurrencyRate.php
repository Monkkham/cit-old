<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1CurrencyRate extends Model
{
    use HasFactory;
    protected $table = "p1_currency_rate";
    protected $fillable = [
        "id",
        "currencies_id",
        "branch_id",
        "rate_to_dollar",
        "rate_to_dollar_operator",
        "rate_to_kip",
        "rate_to_kip_operator",
        "profit_rate",
        "created_at",
        "updated_at",
    ];
    public function get_currencies()
    {
        return $this->belongsTo('App\Models\Currency','currencies_id','id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }
}