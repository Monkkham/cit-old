<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1ProductImages extends Model
{
    use HasFactory;
    protected $table = "p1_product_images";
    protected $fillable = [
        "id",
        "product_id",
        "image",
        "created_at",
        "updated_at",
    ];
}
