<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1OrderTransaction extends Model
{
    use HasFactory;
    protected $table = "p1_order_transaction";
    protected $fillable = [
        "id",
        "order_id",
        "branch_id",
        "total_price",
        "sub_total_price",
        "shipping_cost",
        "vat",
        "total_qty",
        "status",
        "created_at",
        "updated_at",
    ];
    public function order(){
        return $this->belongsTo('App\Models\P1\P1Order', 'order_id', 'id');
    }
}