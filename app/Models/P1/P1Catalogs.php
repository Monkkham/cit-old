<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Catalogs extends Model
{
    use HasFactory;
    protected $table = "p1_catalogs";
    protected $fillable = [
        "id",
        "name_la",
        "name_en",
        "del",
        "created_at",
        "updated_at",
    ];
}