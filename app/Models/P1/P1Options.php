<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Options extends Model
{
    use HasFactory;
    protected $table = "p1_options";
    protected $fillable = [
        "id",
        "parent_id",
        "name_la",
        "name_en",
        "created_at",
        "updated_at",
    ];
    public function parent_option()
    {
        return $this->belongsTo('App\Models\P1\P1Options','parent_id','id');
    }
}