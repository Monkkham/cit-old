<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1OrderDetail extends Model
{
    use HasFactory;
    protected $table = "p1_order_detail";
    protected $fillable = [
        "id",
        "order_id",
        "branch_id",
        "product_id",
        "qty",
        "total_online_price_kip",
        "total_online_price_dollar",
        "delivery_confirm_time",
        "customer_confirm",
        "status",
        "created_at",
        "updated_at",
    ];
    public function product()
    {
        return $this->belongsTo('App\Models\P1\P1Products', 'product_id', 'id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }
    public function order()
    {
        return $this->belongsTo('App\Models\P1\P1Order', 'order_id', 'id');
    }
}