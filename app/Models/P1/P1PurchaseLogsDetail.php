<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1PurchaseLogsDetail extends Model
{
    use HasFactory;
    protected $table = "p1_purchase_logs_detail";
    protected $fillable = [
        "id",
        "purchase_log_id",
        "product_id",
        "qty",
        "product_price",
        "total_price",
        "created_at",
        "updated_at",
    ];
    public function order()
    {
        return $this->belongsTo('App\Models\P1\P1PurchaseLogs','purchase_log_id','id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\P1\P1Products','product_id','id');
    }
}
