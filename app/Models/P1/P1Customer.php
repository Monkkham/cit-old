<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Customer extends Model
{
    use HasFactory;
    protected $table = 'p1_customers';
    protected $fillable = [
        'id',
        'code',
        'branch_id',
        'user_id',
        'name',
        'email',
        'phone',
        'address',
        'tax_code',
        'birthday',
        'note',
        'left',
        'trash',
        'receiver_address',
        'receiver',
        'ccatalog_id',
        'customer_branch_id',
        'customer_sale_person_id',
    ];
}
