<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1OptionProduct extends Model
{
    use HasFactory;
    protected $table = "p1_option_product";
    protected $fillable = [
        "id",
        "option_id",
        "product_id",
        "created_at",
        "updated_at",
    ];
    public function products()
    {
        return $this->belongsTo('App\Models\P1\P1Products','product_id','id');
    }
    public function get_option()
    {
        return $this->belongsTo('App\Models\P1\P1Options','option_id','id');
    }
}