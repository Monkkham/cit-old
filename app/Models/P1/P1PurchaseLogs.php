<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1PurchaseLogs extends Model
{
    use HasFactory;
    protected $table = "p1_purchase_logs";
    protected $fillable = [
        "id",
        "contact_id",
        "code",
        "branch_id",
        "status",
        "purchase_date",
        "attacth_file",
        "created_at",
        "updated_at",
    ];
    public function contact()
    {
        return $this->belongsTo('App\Models\P1\P1Contact','contact_id','id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id','id');
    }
}
