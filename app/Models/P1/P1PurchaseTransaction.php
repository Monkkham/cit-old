<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1PurchaseTransaction extends Model
{
    use HasFactory;
    protected $table = "p1_purchase_transaction";
    protected $fillable = [
        "id",
        "purchase_logs_id",
        "amount",
        "paid_amount",
        "status",
        "created_at",
        "updated_at",
    ];
    public function order()
    {
        return $this->belongsTo('App\Models\P1\P1PurchaseLogs','purchase_logs_id','id');
    }
}
