<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Notifications extends Model
{
    use HasFactory;
    protected $table = "p1_notifications";
    protected $fillable = [
        "id",
        "user_id",
        "title",
        "body",
        "sender_id",
        "status",
        "created_at",
        "updated_at",
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id', 'id');
    }
}