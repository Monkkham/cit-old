<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1Units extends Model
{
    use HasFactory;
    protected $table = "p1_units";
    protected $fillable = [
        "id",
        "name_la",
        "name_en",
        "created_at",
        "updated_at",
    ];
}
