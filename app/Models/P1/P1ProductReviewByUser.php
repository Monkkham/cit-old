<?php

namespace App\Models\P1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class P1ProductReviewByUser extends Model
{
    use HasFactory;
    protected $table = "p1_product_review_by_user";
    protected $fillable = [
        "id",
        "user_id",
        "product_id",
        "star_points",
        "comment",
        "del",
        "created_at",
        "updated_at",
    ];
    public function product()
    {
        return $this->belongsTo('App\Models\P1\P1Products', 'product_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}