<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryBalance extends Model
{
    use HasFactory;
    protected $table="history_balances";
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
