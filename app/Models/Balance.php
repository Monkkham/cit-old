<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;
    protected $table="balances";
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
