<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    use HasFactory;
    protected $fillable = [
        'emp_id',
        'fund_total',
        'withrawed_total',
        'remain_total',
        'status',
        'bcel_image',
        'user_id'
    ];
    public function employee(){
        return $this->belongsTo('App\Models\Employee', 'emp_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
