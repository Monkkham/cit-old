<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceItem extends Model
{
    use HasFactory;
    protected $table="balance_items";
    public function balance()
    {
        return $this->belongsTo('App\Models\Balance','balance_id','id');
    }

}
