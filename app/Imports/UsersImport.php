<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
class UsersImport implements ToModel, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user = User::create([
           'name'     => $row[0],
           'phone'   => $row[1],
           'email'    => $row[2], 
           'password' => Hash::make($row[3]),
           'address' => $row[4],
           'role_id' => '10'
        ]);
    }
    public function rules(): array
    {
        return [
            '1' => 'unique:users,phone',
        ];
    }

}