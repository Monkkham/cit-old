<?php

namespace App\Imports;

use App\Models\P1\P1Products;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
class ProductImport implements ToModel, WithValidation,WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }
    public function model(array $row)
    {
            $product = P1Products::create([
                'code'=> $row[0],
                'name'=> $row[1],
                'import_price'=> str_replace(',', '',  $row[2]),
                'wholesale_price'=> str_replace(',', '', $row[3]),
             ]);
    }
    public function rules(): array
    {
        return [
            '1' => 'unique:p1_products,name',
        ];
    }

}