importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
    apiKey: "AIzaSyDE1quzpvtKpis_hfmcXHdruqt7QQfq3Mo",
    authDomain: "citgroupapp2023.firebaseapp.com",
    projectId: "citgroupapp2023",
    storageBucket: "citgroupapp2023.appspot.com",
    messagingSenderId: "639276764418",
    appId: "1:639276764418:web:f6e84fe58c3f326f6ec61b"
  };

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
    };
    self.registration.showNotification(notificationTitle, notificationOptions);
});