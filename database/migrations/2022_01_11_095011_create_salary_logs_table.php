<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->biginteger('emp_id')->nullable();
            $table->biginteger('salary')->nullable();
            $table->biginteger('commission_total')->default(0);
            $table->integer('miss_total');
            $table->integer('amount_fund');
            $table->biginteger('total_salary')->default(0 );
            $table->string('note')->nullable();
            $table->string('status', 2)->nullable();
            $table->biginteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_logs');
    }
}
