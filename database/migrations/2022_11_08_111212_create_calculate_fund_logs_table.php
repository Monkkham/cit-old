<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculateFundLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculate_fund_logs', function (Blueprint $table) {
            $table->id();
            $table->string('content')->nullable();
            $table->biginteger('total')->default(0);
            $table->biginteger('total_rate')->default(0);
            $table->biginteger('sum_total')->default(0);
            $table->integer('deposit')->default(0);
            $table->integer('withraw')->default(0);
            $table->biginteger('remain_total')->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculate_fund_logs');
    }
}
