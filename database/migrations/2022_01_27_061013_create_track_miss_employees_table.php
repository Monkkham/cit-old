<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackMissEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_miss_employees', function (Blueprint $table) {
            $table->id();
            $table->biginteger('emp_id');
            $table->integer('misstype_id');
            $table->biginteger('total_salary');
            $table->string('note')->nullable();
            $table->biginteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_miss_employees');
    }
}
